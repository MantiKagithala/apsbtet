<!DOCTYPE html>

<%      response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache,no-store, must-revalidate");
            response.addHeader("Cache-Control", "pre-check=0, post-check=0");
            response.setHeader("Expires", "0");
            response.setDateHeader("Expires", 0);
            response.addHeader("Referrer-Policy", "no-referrer");
            response.addHeader("X-Frame-Options", "SAMEORIGIN");
            response.addHeader("Strict-Transport-Security", " max-age=63072000; includeSubDomains; preload");
            response.addHeader("Content-Security-Policy", "default-src https: 'self' 'unsafe-eval' 'unsafe-inline'  image-src 'self' data:; object-src 'none' ");
%>

<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	  <link rel="icon" href="assets/img/aptonline.png" type="image/gif" sizes="32x32">
		<title>:: SBTET ::</title>

	  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
	  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css"> -->
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css'>
<link rel="stylesheet" href="assets/css/slider.css">

</head>
<body>

<section class="top-content">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<!-- <div class="page_social_icons"> -->
								<!-- <a class="fa fa-facebook" href="#" title="Facebook"></a> -->
								<!-- <a class="fa fa-twitter" href="#" title="Twitter"></a> -->
								
								<!-- <a class="fa fa-instagram" href="#" title="Instagram"></a> -->
								<!-- <a class="fa fa-google" href="#" title="Google"></a> -->
							<!-- </div> -->
							
				<div class="guidelines f-l">
					
					
					<ul class="incrs-font">
						<li><a href="">A+</a></li>
						<li><a href="">A</a></li>
						<li><a href="">A-</a></li>
						<li><a href="">A</a></li>
						<li style="background:#2f363c"><a href="">A</a></li>
						
					</ul>&nbsp;&nbsp;&nbsp;
					<a href="">Screen Reader</a>&nbsp;&nbsp;&nbsp;
					<a href="">Skip to Main Content</a>&nbsp;&nbsp;&nbsp;
					
					
				</div>				
			</div>
			<div class="col-md-7">
				<div class="guidelines">
					
					<!--<a href=""><i class="fa fa-commenting" aria-hidden="true"></i>&nbsp; Feed Back</a>&nbsp;&nbsp;&nbsp;-->
					<!--<a href="officialLogin.do" class="login"><i class="fa fa-lock" aria-hidden="true"></i> &nbsp;Login</a>-->
					
				</div>	
			</div>
		</div>
	</div>
</section>

<!-- <section class="page_topline ls section_padding_top_15 section_padding_bottom_15 columns_padding_0"> -->
				<!-- <div class="container-fluid"> -->
					<!-- <div class="row"> -->
						<!-- <div class="col-sm-2 text-center text-sm-left greylinks"> -->
							<!-- <div class="page_social_icons"> -->
								<!-- <a class="social-icon soc-facebook" href="#" title="Facebook"></a> -->
								<!-- <a class="social-icon soc-twitter" href="#" title="Twitter"></a> -->
								
								<!-- <a class="social-icon soc-instagram" href="#" title="Instagram"></a> -->
								<!-- <a class="social-icon soc-google" href="#" title="Google"></a> -->
							<!-- </div> -->
						<!-- </div> -->
						<!-- <div class="col-sm-10 text-right"> -->

							<!-- <a href="">Screen Reader</a>&nbsp;&nbsp;&nbsp; -->
							<!-- <a href="">Skip to Main Content</a> -->
							<!-- <ul class="incrs-font"> -->
								<!-- <li><a href="">A+</a></li> -->
								<!-- <li><a href="">A</a></li> -->
								<!-- <li><a href="">A-</a></li> -->
								<!-- <li><a href="">A</a></li> -->
								<!-- <li style="background:#2f363c"><a href="">A</a></li> -->
							<!-- </ul> -->
						<!-- </div> -->
						
					<!-- </div> -->
				<!-- </div> -->
			<!-- </section> -->




<nav class="navbar navbar-inverse">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			  </button>
			  <a class="navbar-brand" href="#"><img src="assets/img/logo.png"></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
			  <ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="Welcome.do">Home</a></li>
				<!--<li class=""><a href="#">Govt Orders</a></li>-->
				<!--<li class=""><a href="#"> Publications</a></li>-->
<!--				<li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Eligibility & Forms</a>
                                  <ul class="dropdown-menu">
					<li><a href="register.do">TWSH RegistrationForm</a></li>
					<li><a href="formPrint.do">TWSH RegistrationForm Print</a></li>
					
				  </ul>
                                </li>-->
<!--				<li class="dropdown">
				  <a class="dropdown-toggle" data-toggle="dropdown" href="#">ACT'S <span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href="#">Acts & Rules</a></li>
					<li><a href="#">RTI Act</a></li>
					<li><a href="#">Citizen Chart</a></li>
					
				  </ul>
				</li>-->
				
				<!--<li class="dropdown menu-large ">	<a href="# " class="dropdown-toggle " data-toggle="dropdown ">Other Links <b class="caret "></b></a>-->
<!--                    <ul class="dropdown-menu megamenu row ">
                        <li class="col-sm-3 ">
                            <ul>
                                
                                <li><a href="# "> Polytechnic In AP</a>

                                </li>
                                <li class="disabled "><a href="# ">AICTE</a>

                                </li>
                                <li><a href="# ">DTEAP</a>

                                </li>
                                
                                <li><a href="# ">Tenders</a>

                                </li>
                                <li><a href="# "> Downloads</a>

                                </li>
                                <li><a href="# ">Anti Raging</a>

                                </li>
                                <li><a href="# ">Media Releases</a>

                                </li>
                            </ul>
                        </li>
                        <li class="col-sm-3 ">
                            <ul>
                                <li><a href="# ">DTE Telangana</a>

                                </li>
                                <li><a href="# ">Polycet Telangana State</a>

                                </li>
                                <li><a href="# ">ITI Bridge Course</a>

                                </li>
                                <li><a href="# ">Diploma Results</a>

                                </li>
                                <li><a href="# ">Status of Biometric Devices In Private Polytechnic</a>

                                </li>
                                <li><a href="# ">Industrial Training Branch Wise Data</a>
                                </li>
								 <li><a href="# ">Requirement for CISCO training Programmes</a>

                                </li>
                               
                               
                            </ul>
                        </li>
                        <li class="col-sm-3 ">
                            <ul>
								 <li><a href="# ">Teaching experience (Subject wise) � certain infor</a>

                                </li>
                                <li><a href="# ">PRACTICAL SUBJECTS HANDLING STAFF - DETAILS</a>

                                </li>
                                <li><a href="# ">lndustrial training for 2019-20 details of student</a>

                                </li>
                                <li><a href="# ">CISCO Conference Registration Link</a>

                                </li>
                                <li><a href="# ">Industrial Training Branch & Industry wise Details</a>

                                </li>
                            </ul>
                        </li>
                        <li class="col-sm-3 ">
                            <ul>
                                <li><a href="# ">Industrial Training Students Details from the Prls</a>

                                </li>
                                <li><a href="# ">LEARNING MANAGEMENT SYSTEM</a>

                                </li>
                                <li><a href="# ">POLYCET-2020</a>

                                </li>
                                
                            </ul>
                        </li>
                    </ul>-->
                </li>
				<li class=""><a href="#"> Contact</a></li>
                                        <li class=""><a href="officialLogin.do" class="login"> &nbsp;Login<i class="fa fa-lock" aria-hidden="true"></i></a></li>

				<!-- <li class=""><a href="#">Gallery</a></li> -->
				<!-- <li class=""><a href="#">CSR</a></li> -->
				
			  </ul>
			  <!-- <ul class="nav navbar-nav navbar-right"> -->
				<!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> -->
				<!-- <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li> -->
			  <!-- </ul> -->
			</div>
		  </div>
		</nav>
		<div class="nav-border"></div>
	<script src="assets/js/jquery.min.js"></script>
  <script src="assets/js/bootstrap.min.js"></script>	
	 	<script src="assets/js/fontjs.js"></script>
	
<script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js'></script>
<script  src="assets/js/slider.js"></script>
<script>
	jQuery(document).ready(function () {
    $(".dropdown").hover(

    function () {
        $('.dropdown-menu', this).stop().fadeIn("fast");
    },

    function () {
        $('.dropdown-menu', this).stop().fadeOut("fast");
    });
});
</script>

</body>
</html>	