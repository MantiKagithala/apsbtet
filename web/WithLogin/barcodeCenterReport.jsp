<%-- 
    Document   : CourseStatusReport
    Created on : Nov 23, 2020, 1:51:42 PM
    Author     : 1582792
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html>
<html>

    <head>
        <%int i = 0;%>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script> 
        <script>
            $(document).ready(function() {
                $('#example').DataTable();
            });
        </script>
        <style type="text/css">
              .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #9fa7ff !important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }

            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }
            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <script>
            function getDocumentDetailsData() {
                document.forms[0].mode.value = "unspecified";
                document.forms[0].submit();
            }
            function getPrint() {
                if($("#ecenter").val()=="0"){
                    alert("Select Examination Center")
                    $("#ecenter").focus();
                }else{
                document.forms[0].mode.value = "getDataPrint";
                document.forms[0].submit();
                }
            }
        </script>
    </head>
    <body>
        
         <div class="row mainbodyrow">
        <div class="container" style="padding-top: 15px;">
            <div class="col-xs-12">
                <div class="maindodycnt">
<!--        <div class="container">
            <div class="row" style="margin: 0px; margin-top: 10px;margin-bottom: 10px;">
                <div  class="main_div" style="background-color:#fff;min-height:435px;box-shadow: 0 0 5px #ccc;">-->
                    </br>
                <h3 class="block-head-News">Center Wise Scanning Status of Booklets</h3>
                  <div class="line-border"></div>
                    <html:form action="/barcodeCenterReport" >
                        <html:hidden property="mode"/>
                            <div class="row">
                                        <div class="col-md-8">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination Center<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:select property="ecenter" styleId="ecenter" styleClass="form-control" >
                                                        <html:option value="0">--Select Center--</html:option> 
                                                        <html:optionsCollection property="gradeslist" label="cname" value="ccode"/>
                                                    </html:select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                 <div class="col-md-12 center">
                                                <center><input type="button" onclick="return getPrint()" value="submit" class="btn btn-primary" style="max-width: 96px;"/>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                        <logic:present  name="candiList">
                            <div style="text-align: right;padding-right: 31px;">
                                <a href="./barcodeCenterReport.xls?mode=getExcelData&ecenter=<%=request.getAttribute("ecenter")%>"><img src="img/excel.png" style="width: 35px"/></a>
                            </div>
                            <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>SNO</th>
                                        <th>Grade</th>
                                        <th>Batch</th>
                                        <th>Paper</th>
                                        <th>Total Booklets</th>
                                        <th>Scanned</th>
                                        <th>Mismatch</th>
                                        <th>To be Scanned</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <logic:iterate name="candiList" id="list">
                                        <tr>
                                            <td style="text-align: center;"><%=++i%></td>
                                            <td style="text-align: left;">${list.grade}</td>
                                            <td style="text-align: center;">${list.batch}</td>     
                                            <td style="text-align: center;">${list.paper}</td>
                                            <td style="text-align:center;">${list.totalbooklets}</td>
                                            <td style="text-align:center;">${list.scanned}</td>
                                            <td style="text-align: center;">${list.mismatch}</td>
                                            <td style="text-align: center;">${list.tobescan}</td>
                                        </tr>
                                    </logic:iterate>
                                </tbody>

                                <tr>
                                    <td style="text-align: center;" colspan="4">Total</td>
                                    <td style="text-align: center;">${list.totalbookletstotal}</td>     
                                    <td style="text-align: center;">${list.scannedtotal}</td>
                                    <td style="text-align:center;">${list.mismatchtotal}</td>
                                    <td style="text-align:center;">${list.tobescantotal}</td> 
                                </tr>

                            </table>
                        </logic:present>
                        <logic:present  name="candiList1">
                            <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>SNO</th>
                                        <th>Name</th>
                                        <th>Total Assigned</th>
                                        <th>Not Verified</th>
                                        <th>Send To Reupload</th>
                                        <th>Pending for 2nd Verification</th>
                                        <th>Verified</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="7">No Data</td>
                                    </tr>
                                </tbody>
                            </table>
                        </logic:present>    

                    </html:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>                                       