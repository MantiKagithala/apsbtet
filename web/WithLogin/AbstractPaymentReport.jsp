<%-- 
    Document   : AbstractPaymentReport
    Created on : Jul 16, 2021, 3:03:29 PM
    Author     : 1820530
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  


        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">


        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": true,
                     "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                });
                $('#example1').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
            tfoot {
                background-color: #6699ff!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>

        <script>

            function goBack() {

                document.forms[0].action = "./abstractReport.do?mode=unspecified";
                document.forms[0].submit();

            }

        </script>
    </head>
    <body>
        <html:form action="/abstractReport">
            <html:hidden property="courseCode"/>


            <div class="row mainbodyrow">
                <div class="container" style="padding-top: 15px;">
                    <div class="col-xs-12">
                        <div class="maindodycnt">
                            <h3 class="block-head-News">Course Wise Report</h3>
                            <div class="line-border"></div>


                            <%int count = 1;%>
                            <logic:present name="nodata">
                                <center><font color="red">${nodata}</font></center>
                                </logic:present>

                            <logic:present name="paymentAbstractReport">
                                <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                    <a href="./abstractReport.xls?mode=downloadExcel ">
                                        <img src="img/excel.png" style="width: 35px"/></a>
                                </div>
                                <br>
                                <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Course Name</th>
                                            <th>Course Code</th>
                                            <th>Limit</th>
                                            <th>Payment Done</th>
                                            <th>Payment Not Done</th>
                                            <th>Total</th>

                                        </tr>
                                        <bean:define id="limitTotal" value="0"/>
                                        <bean:define id="paymentDoneTotal" value="0"/>
                                        <bean:define id="paymentNotDoneTotal" value="0"/>
                                        <bean:define id="allTotal" value="0"/>
                                    </thead>
                                    <tbody>
                                        <logic:iterate name="paymentAbstractReport" id="list">
                                            <tr>
                                                <td style="text-align: center;"><%=count++%></td>
                                                <td style="text-align: left;">${list.courseName}</td>
                                                <td style="text-align: left;">${list.courseCode}</td>     
                                                <td style="text-align: center;">${list.limit}</td>
                                                <c:set var="payCount" value="${list.paymentDone}"></c:set> 
                                                <c:set var="payNotCount" value="${list.paymentNotDone}"></c:set> 
                                                <c:set var="totalCount" value="${list.total}"></c:set> 
                                                    <td style="text-align: center;">
                                                    <%
                                                        String payCount = (String) pageContext.getAttribute("payCount");
                                                        if (payCount.equalsIgnoreCase("0")) {
                                                    %>
                                                    0
                                                    <% } else {%>
                                                    <a href="abstractReport.do?mode=getSubReport&statusId=<%="1"%>&courseCode=${list.courseCode}" id="statusCode"> ${list.paymentDone}</a>
                                                    <%}%>

                                                </td>
                                                <td style="text-align: center;">
                                                    <%
                                                        String payNotCount = (String) pageContext.getAttribute("payNotCount");
                                                        if (payNotCount.equalsIgnoreCase("0")) {
                                                    %>
                                                    0
                                                    <% } else {%>
                                                    <a href="abstractReport.do?mode=getSubReport&statusId=<%="2"%>&courseCode=${list.courseCode}" id="statusCode"> ${list.paymentNotDone}</a>
                                                    <%}%>

                                                </td>
                                                <td style="text-align: center;">
                                                    <%String totalCount = (String) pageContext.getAttribute("totalCount");
                                                        if (totalCount.equalsIgnoreCase("0")) {
                                                    %>
                                                    0
                                                    <% } else {%>
                                                    <a href="abstractReport.do?mode=getSubReport&statusId=<%="3"%>&courseCode=${list.courseCode}" id="statusCode"> ${list.total}</a>
                                                    <%}%>
                                                </td>                                                                                      
                                            </tr>

                                            <bean:define id="limitTotal" value="${limitTotal+list.limit}"/>
                                            <bean:define id="paymentDoneTotal" value="${paymentDoneTotal+list.paymentDone}"/> 
                                            <bean:define id="paymentNotDoneTotal" value="${paymentNotDoneTotal+list.paymentNotDone}"/> 
                                            <bean:define id="allTotal" value="${allTotal+list.total}"/> 

                                        </logic:iterate>  
                                    </tbody>
                                    <tfoot>
                                        <tr  style="background-color: white;">
                                            <td style="text-align: center " colspan="3" >
                                                Total
                                            </td>
                                            <td style="text-align: center ">
                                                <b> ${limitTotal} </b>
                                            </td>
                                            <td style="text-align: center ">
                                                <b> ${paymentDoneTotal} </b>
                                            </td> 
                                            <td style="text-align: center ">
                                                <b> ${paymentNotDoneTotal} </b>
                                            </td>
                                            <td style="text-align: center ">
                                                <b> ${allTotal} </b>
                                            </td>
                                        </tr>                                       
                                    </tfoot>

                                </table>
                            </logic:present>

                            <logic:present name="paymentSubList">
                                <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                    <a href="./abstractReport.xls?mode=downloadSubReportExcel&statusCodeExcel=<%=request.getAttribute("statusCode")%>&courseCodeExcel=<%=request.getAttribute("courseCode")%>" >
                                        <img src="img/excel.png" style="width: 35px"/></a>
                                    <input type="button" name="backbtn" class="btn btn-primary" value="Back" onclick="goBack()" />
                                </div>
                                <br>
                                <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Name</th>
                                            <th>Father Name </th>
                                            <th>Date Of Birth</th>
                                            <th>Gender</th>
                                            <th>Mobile </th>
                                            <th>Course Name</th>
                                            <th>category</th>
                                            <th>Institute Name</th>
                                            <th>Studied District</th>
                                            <th>Exam Center</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <logic:iterate name="paymentSubList" id="list">
                                            <tr>
                                                <td style="text-align: center;"><%=count++%></td>
                                                <td style="text-align: left;">${list.name}</td>
                                                <td style="text-align: left;">${list.fatherName}</td>     
                                                <td style="text-align: left;">${list.dob}</td>
                                                <td style="text-align: left;">${list.gender}</td>
                                                <td style="text-align: center;">${list.mobileNum}</td>
                                                <td style="text-align: left;">${list.courseName}</td>
                                                <td style="text-align: left;">${list.category}</td>
                                                <td style="text-align: left;">${list.institution}</td>
                                                <td style="text-align: left;">${list.studiedDist}</td>
                                                <td style="text-align: center;">${list.examCenterCode}</td>
                                            </tr>
                                        </logic:iterate>  
                                    </tbody>
                                </table>
                            </logic:present>

                        </div>
                    </div>
                </div>
            </div>

        </html:form>
    </body>
</html>
