
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>

        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <Style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .form-control-plaintext{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit
            {
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $("form").attr('autocomplete', 'off');
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
            });

            function getData() {
                if ($("#aadhar1").val() == "") {
                    $("#aadhar1").val("");
                    alert("Please Enter Registration No");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#grade1").val().length != 10) {
                    $("#grade1").val("");
                    alert("Please Enter Valid Registered Mobile Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#grade1").val() == "0" || $("#grade1").val() == "") {
                    alert("Please Enter Registered Mobile Number");
                    $("#grade1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    document.forms[0].mode.value = "getDownloadHallticket";
                    document.forms[0].submit();
                }
            }

            function validateAdharDetails1() {
                if ($("#aadhar1").val() == "") {
                    $("#aadhar1").val("");
                    alert("Please Enter Aadhaar Card Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                } else if ($("#aadhar1").val().length != 12) {
                    $("#aadhar1").val("");
                    alert("Aadhaar Card Number should be 12 digit number.");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar1").val() == "999999999999" || $("#aadhar1").val() == "333333333333") {
                    $("#aadhar1").val("");
                    alert("Please Enter Valid Aadhaar Card Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (validateVerhoeff($("#aadhar1").val()) != true) {
                    $("#aadhar1").val("");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div class="page-title title-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Download Hall Ticket </h1>
                        <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">
                            <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                            <span>Download HallTicket</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feedback-form">
            <div class="container">
                <div class="row">
                    <h3 class="block-head-News">Download HallTicket</h3>
                    <div class="line-border"></div>
                    <div class="row">
                        <div class=" col-md-16">
                            <html:form action="/downloadHallticket"  styleId="d" method="post" enctype="multipart/form-data">
                                <html:hidden property="mode"/>
                                <logic:present name="result2">
                                    <span id="msg"><center> <font color="green" style="font-weight: bold">${result2}</font></center></span>
                                    </logic:present>
                                    <logic:present name="result1">
                                    <span id="msg"><center><font color="red" style="font-weight: bold">${result1}</font></center></span>
                                </logic:present><br>

                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Registration No<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="aadhar1" styleId="aadhar1" maxlength="20"  onkeydown="return space(event, this);"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Registered Mobile No<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="grade1" styleId="grade1" maxlength="10"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);" />
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group row">
                                        <input type="button" onclick="getData();" value="DOWNLOAD" class="btn btn-primary"/>
                                    </div>
                                </div>
                            </html:form>                
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </body>
</html>