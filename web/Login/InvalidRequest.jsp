<%@page contentType="text/html"%>
<%@page pageEncoding="UTF-8"%>
<%--
    Document   : InvalidRequest
    Created on : July 18, 2019, 13:39:52 PM
    Author     : 1451421
--%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/officialLogin.do";
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><bean:message key="title"/></title>
        <script type = "text/javascript" >

            function preventBack(){
                window.history.forward();
            }

            setTimeout("preventBack()", 0);

            window.onunload=function(){null};

        </script>
    </head>
    <body style="padding: 0px;margin: 0px;">

        <table align="center" cellpadding="0" cellspacing="0" border="0" height="60%">
            <tr>
                <td><br/><br/><br/><br/><br/>
                    <b><font color="red" size="7">Invalid Request</font></b>
                </td>
            </tr>
            <tr>
                <td align="center"><br/>
                    <a href="./home.do" <font color="Blue" size="4"><b>Home</b></font></a>
                </td>
            </tr>
        </table>
    </body>
</html>
