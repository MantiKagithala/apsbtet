/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


 function space(evt, thisvalue) {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
            function CheckfilePdfOrOther(myval) {
                var fup = $("#" + myval).val();
                var ext = fup.split('.').pop().toUpperCase();
                if (ext === "JPG")
                {
                    var file = $("#" + myval)[0].files[0];
                    var size = file.size / 1024;
                    if (size < 1001 && size > 3) {
                        return true;
                    } else {
                        alert("File size should be greater than 3 KB and less than 1MB only");
                        $("#" + myval).val('').clone(true);
                        $("#" + myval).focus();
                        return false;
                    }
                } else {
                    alert("Upload JPG Files only & File Size must be less than or Equal to 1MB");
                    $("#" + myval).val('').clone(true);
                    $("#" + myval).focus();
                    return false;
                }
            }

            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }

            
       
            function centersList(centerval) {
                var data = "district=" + $("#distrcit1").val().trim();
                $.ajax({ 
                    type: "POST",
                    url: "./onlineExaminationPayment.do?mode=getCenters&district=" + $("#distrcit1").val().trim() ,
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#centerCode").empty().append(response);
                       
                        $("#centerCode").val(centerval);
                    },
                    error: function(e) {
//            alert('Error: ' + e);
                    }
                });
            }
            function getFormDetails() {
                if ($("#aadhar1").val() == "") {
                    $("#aadhar1").val("");
                    alert("Please Enter PIN Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                document.forms[0].mode.value = "getDetails";
                document.forms[0].submit();
            }
            function hideData() {
                $("#hideData").hide();
            }
//            function SubmitForm(appstatus) {
//               
//                if ($("#aadhar1").val() == "") {
//                    alert("Please Enter PIN No");
//                    $("#aadhar1").focus().css({'border': '1px solid red'});
//                    return false;
//                }
//
//                if ( appstatus== '0') {
//                    if ($("#mobile").val() == "") {
//                        $("#mobile").val("");
//                        alert("Please Enter  Mobile Number");
//                        $("#mobile").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                    var mobileNo = $("#mobile").val();
//                    var len = mobileNo.length;
//                    if (len < 10) {
//                        alert("Mobile Number Should be 10 digits");
//                        $("#mobile").val("");
//                        $("#mobile").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                    else if (($("#mobile").val().trim().charAt(0) != '9'
//                            && $("#mobile").val().trim().charAt(0) != '8' &&
//                            $("#mobile").val().trim().charAt(0) != '7' &&
//                            $("#mobile").val().trim().charAt(0) != '6')) {
//                        alert("Please Enter valid Mobile Number");
//                        $("#mobile").val("");
//                        $("#mobile").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                    if ($("#upload1").val() == "") {
//                        $("#upload1").val("");
//                        alert("Please Upload Photo");
//                        $("#upload1").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                    if ($("#email").val() == "") {
//                        $("#email").val("");
//                        alert("Please Enter Email");
//                        $("#email").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                    
//                    if ($("#ecenter").val() == "0") {
//
//                        alert("Please Select Do You need to change Center");
//                        $("#ecenter").focus().css({'border': '1px solid red'});
//                        return false;
//                    }
//                    if ($("#ecenter").val() == "1") {
//                        
//                         if ($("#distrcit1").val() == "0") {
//                            alert("Please Select District Name");
//                            $("#distrcit1").focus().css({'border': '1px solid red'});
//                            return false;
//                        }
//                        if ($("#centerCode").val() == "0") {
//                            alert("Please Select Center Name");
//                            $("#centerCode").focus().css({'border': '1px solid red'});
//                            return false;
//                        }
//                    }
//                }
//
//               alert("edewfef");
//                var checkedBoxes = [];
//                var r = false;
//                var chkbox = document.getElementsByName('checkid');
//                
//                if (chkbox.length > 0) {
//                    for (var i = 0, n = chkbox.length; i < n; i++) {
//                        if (chkbox [i].checked)
//                        {
//                            checkedBoxes.push(chkbox [i].value);
//                            r = true;
//                        }
//                    }
//                    if (r == false) {
//                        alert("please select at least once check box");
//                        return false;
//                    }
//                    if (r) {
////                        alert(checkedBoxes)
//                        var returnval = confirm("Please Confirm to Submit the Details");
//                        if (returnval == true) {
//                            $("#statusresult").val(checkedBoxes);
//
//                            document.forms[0].mode.value = "submitApplicantDetails";
//                            document.forms[0].submit();
//                        }
//                    }
//                }else{
//                   alert("Your are Not Eligible to pay");
//                        return false;  
//                }
//            }
            
            function showAmtDts() {
                var r = false;
                var chkbox = document.getElementsByName('checkid');
                var amount = 0;
                if (chkbox.length > 0) {
                    for (var i = 0, n = chkbox.length; i < n; i++) {
                        if (chkbox [i].checked)
                        {
                            var j = i + 1;
                            amount = amount + parseInt($("#amount" + j).val() != "undefined" ? $("#amount" + j).val() : 0)
                            r = true;
                        }
                    }
                }
                if (r) {
                    $(".showAmount").show();
                    $("#totalmarks").val(amount);
                    $("#semamount").val(amount);
                } else {
                    $(".showAmount").hide();
                }
            }
            
            function isEmail() {
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test($("#email").val())) {
                    $("#email").val("");
                    alert("Invalid eMail")
                    return true;  //wrong mail
                } else {
                    return false;  //correct mail

                }
            }