<%-- 
    Document   : CourseStatusReport
    Created on : Jul 23, 2021, 1:51:42 PM
    Author     : APTOL301294
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>
<html>
    <head>
        <%int i = 0;%>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  
         <link rel ="stylesheet" href="css/jquery-ui.css">
        <script type="text/javascript">

            $(document).ready(function() {
                $("#datediv").hide();
                $('#example').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]]
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }
select option { color: black !important; }
select:invalid {
  color: green;
}
        </style>
      
    </head>
    <body>
        <div class="row mainbodyrow">
            <div class="container" style="padding-top: 15px;">
                <div class="col-xs-12">
                    <div class="maindodycnt">
                        <h3 class="block-head-News">Diploma Examinations - College wise status report </h3>
                        <div class="line-border"></div>
                        <html:form action="/examinationCollegeWiseReport" >
                            <html:hidden property="mode"/>
                         
                               <logic:present name="result1">
                                <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                                </logic:present>
                                <logic:present name="result">
                                <br><br><br><br><br><br><br><br>
                                <center> <font color="red" style="font-weight: bold">${result}</font></center>
                                </logic:present>
                                <div>

                            <logic:present  name="listData">
                                <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                       <a href="./examinationCollegeWiseReport.xls?mode=downloadExcel"><img src="img/excel.png" style="width: 35px"/></a>
                               </div>
                                <div id="dataDiv">
                                    <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th>S.NO</th>
                                                <th>PIN</th>
                                                <th>Student Name</th>
                                                <th>Branch</th>
                                                <th>I</th>
<!--                                                <th>II</th>-->
                                                <th>III</th>
                                                <th>IV</th>
                                                <th>V</th>
                                                <th>VI/ I. Training</th>
                                             </tr>
                                        </thead>
                                        <tbody>
                                            <logic:iterate name="listData" id="list">
                                                <tr>
                                                    <td style="text-align: center;">${list.sno}</td>
                                                    <td style="text-align: left;">${list.pin}</td>
                                                    <td style="text-align: center;">${list.stName}</td>     
                                                    <td style="text-align: center;">${list.branch}</td>
                                                    <td style="text-align: center;">${list.I}</td>
<!--                                                    <td style="text-align: center;">${list.II}</td>-->
                                                    <td style="text-align: center;">${list.III}</td>
                                                    <td style="text-align: left;">${list.IV}</td>
                                                    <td style="text-align: left;">${list.V}</td>
                                                    <td style="text-align: center;">${list.VI}</td>     
                                                  </tr>
                                            </logic:iterate>
                                        </tbody>
                                   </table>
                                </div>
                            </logic:present>
                           </div>
                        </html:form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>                                       