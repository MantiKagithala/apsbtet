<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <%int i = 0;%>
        <script src="./js/jquery.min.js"></script>
        <!--        <script src="./js/GenerateAadharCardValidation.js"></script>-->
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <!--        <link rel="stylesheet" href="css/style1.css">-->
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  
        <!--<script src="js/onlineExaminationPayment.js" type="text/javascript"></script>--> 
        <style>
            .ui-datepicker-calendar thead{
                background-color:#2dc1c9 !important;
            }
            .feedback-form {
                padding: 22px 0px;
                min-height: 384px;
            }
            .block-head-News1 {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 15px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
        </style>
         <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>

        <script type="text/javascript">
            $(document).ready(function() {
                $('input[type=text]').attr('autocomplete', 'off');
                $(".centers").hide();
                hideCenter();
                $('input[type=text]').bind("cut copy paste", function(e) {
                    alert("Cut copy paste not allowed here");
                    e.preventDefault();
                });
            });


            function hideCenter() {
                var estatus = $("#ecenter").val();
                if (estatus == "1" || '${samecollege}' == "Yes" || '${samecollege}' == "1") {
                    $(".centers").show();
                } else {
                    $(".centers").hide();
                    $("#centerCode").val("0");
                    $("#distrcit1").val("0");

                }
            }
        </script>


        <script type="text/javascript">
            function hideData() {
                $(".hideData").hide();
            }
            function centersList1() {
                var data = "district=" + $("#distrcit1").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./onlineExaminationCenterChange.do?mode=getCenters&district=" + $("#distrcit1").val().trim(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#centerCode").empty().append(response);

                    },
                    error: function(e) {
//            alert('Error: ' + e);
                    }
                });
            }
            function space1(evt, thisvalue) {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
            function inputLimiter1(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            function getFormDetails1() {
                if ($("#aadhar1").val() == "") {
                    $("#aadhar1").val("");
                    alert("Please Enter PIN Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                document.forms[0].mode.value = "getData";
                document.forms[0].submit();
            }
            function getUpdateForm() {
                if ($("#ecenter").val() == "0") {
                    $("#ecenter").val("0");
                    alert("Please Select Do you want to change the examination/exam center due to pandemic");
                    $("#ecenter").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#distrcit1").val() == "" || $("#distrcit1").val() == "0") {
                    $("#distrcit1").val("0");
                    alert("Please Select District");
                    $("#distrcit1").focus().css({'border': '1px solid red'});
                    return false;
                }
                if ($("#centerCode").val() == "" || $("#centerCode").val() == "0") {
                    $("#centerCode").val("0");
                    alert("Please Select Center Name");
                    $("#centerCode").focus().css({'border': '1px solid red'});
                    return false;
                }
                document.forms[0].mode.value = "updateData";
                document.forms[0].submit();
            }
            $(function() {
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
            });
        </script>

    <body>
        <div class="row mainbodyrow">
            <div class="container" style="padding-top: 15px;">
                <html:form action="/onlineExaminationCenterChange"  method="post" enctype="multipart/form-data">
                    <html:hidden property="mode"/>
                    <html:hidden property="statusresult" styleId="statusresult"/>
                    <html:hidden property="totalmarks" styleId="totalmarks"/>
                    <html:hidden property="upload1New1" styleId="upload1New1"/>

                    <div class="col-xs-12">
                        <div class="maindodycnt">

                            <h3 class="block-head-News">Online Application Exam Center Change Form
                                <logic:present name="result1">
                                    <span id="msg"> <center> <font color="red" style="font-weight: bold;font-size: 15px;">${result1}</font></center></span>
                                        </logic:present>
                                        <logic:present name="result">
                                    <span id="msg"> <center> <font color="green" style="font-weight: bold;font-size: 15px;">${result}</font></center></span>
                                </logic:present><br>
                            </h3>

                            <div class="row">
                                <div class=" col-md-12">
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Enter PIN<font color="red">*</font></label>
                                            <div class="col-sm-8">
                                                <html:text  styleClass="form-control-plaintext" property="aadhar1" styleId="aadhar1" maxlength="20"  onkeydown="return space1(event, this);" onkeypress="return inputLimiter1(event,'NameCharactersAndNumbers');" onkeyup="hideData();"/>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-sm-8">
                                                <center> <input type="button" onclick="return getFormDetails1();" value="GO"  class="btn btn-warning"/></center> 
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br> 
                            <logic:present name="showDiv">
                                <div class="hideData">
                                    <h3 class="block-head-News1">Opted Examination Center Through Registration</h3><br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name" class="form-label">District Name</label>
                                                <div style="padding-top: 21px;">
                                                    <b> ${DistrictName}</b>
                                                </div>
                                            </div> 
                                        </div>     
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name" class="form-label"> Center Name</label>
                                                <div style="padding-top: 21px;">
                                                    <b> ${collegeName}</b>
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <br> 
                                    <h3 class="block-head-News1">Change Center</h3><br>

                                    <div class="row">
                                        <div class="col-md-4" >
                                            <div class="form-group">
                                                <label for="name" class="form-label">Do you want to change the examination/exam center due to pandemic<font color="red">*</font></label>
                                                <div class="">
                                                    <html:select property="ecenter"  styleId="ecenter" styleClass="form-control" onchange="hideCenter();">  
                                                        <html:option value="0">---Select---</html:option>
                                                        <html:option value="1">Yes</html:option>
                                                        <html:option value="2">No</html:option>
                                                    </html:select>
                                                </div>
                                            </div> 
                                        </div>  
                                        <div class="col-md-4 centers">
                                            <div class="form-group">
                                                <label for="name" class="form-label">
                                                    District Name<font color="red">*</font></label>
                                                <div style="padding-top: 21px;">
                                                    <html:select property="distrcit1" styleClass="form-control" styleId="distrcit1" onchange="centersList1()">  
                                                        <html:option value="0">---Select---</html:option>
                                                        <logic:present name="distLists">
                                                            <html:optionsCollection property="distLists" label="district_Name" value="district_ID"/>
                                                        </logic:present>
                                                    </html:select> 
                                                </div>
                                            </div> 
                                        </div>     

                                        <div class="col-md-4 centers">
                                            <div class="form-group">
                                                <label for="name" class="form-label"> Center Name<font color="red">*</font></label>
                                                <div class="" style="padding-top: 21px;">
                                                    <html:select property="centerCode" styleClass="form-control" styleId="centerCode">  
                                                        <html:option value="0">---Select---</html:option>
                                                    </html:select> 
                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                    <div class="centers"> 
                                        <center> <input type="button" onclick="return getUpdateForm();" value="UPDATE"  class="btn btn-warning"/></center> 
                                    </div>
                                </div>
                            </logic:present>
                        </div>
                    </div>                  

                </html:form>
            </div>
        </div>

    </body>
</html>