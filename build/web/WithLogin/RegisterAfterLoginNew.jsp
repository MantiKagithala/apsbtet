
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>

        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <Style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit
            {
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $("form").attr('autocomplete', 'off');
                $("#blind").val("NO");
                $("#highdiv").hide();
                $(".viifile").hide();
                $(".hsfile").hide();
//                $('input[type=text], textarea').bind("cut copy paste", function(e) {
//                    alert("Cut copy paste not allowed here");
//                    e.preventDefault();
//                });
                $("#DistrictDiv").show();
                $("#DistrictDiv1").hide();
                $("#MandalDiv").show();
                $("#MandalDiv1").hide();
                $("#noDiv").show();
                $("#blindDiv").hide();
            });
            function space(evt, thisvalue)
            {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    alert("Allow Numbers Only");
                    return false;
                }
                return true;
            }
            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            function telephoneValidation() {

                var mobileNo = $("#mobile").val();
                var len = mobileNo.length;
                if (len < 10) {
                    alert("Mobile Number Should be 10 digits");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#mobile").val().trim().charAt(0) != '9'
                        && $("#mobile").val().trim().charAt(0) != '8' &&
                        $("#mobile").val().trim().charAt(0) != '7' &&
                        $("#mobile").val().trim().charAt(0) != '6')) {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
            }
            function validateAdharDetails() {
                if ($("#aadhar").val() == "") {
                    $("#aadhar").val("");
                    alert("Please Enter Aadhaar Card Number");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                } else if ($("#aadhar").val().length != 12) {
                    $("#aadhar").val("");
                    alert("Aadhaar Card Number should be 12 digit number.");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar").val() == "999999999999" || $("#aadhar").val() == "333333333333") {
                    $("#aadhar").val("");
                    alert("Please Enter Valid Aadhaar Card Number");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (validateVerhoeff($("#aadhar").val()) != true) {
                    $("#aadhar").val("");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                    return false;
                }



            }
            function CheckfilePdfOrOther(myval) {
                var fup = $("#" + myval).val();
                var ext = fup.split('.').pop().toUpperCase();
                if (ext === "JPG")
                {
                    var file = $("#" + myval)[0].files[0];
                    var size = file.size / 1024;
                    if (size < 1001) {
                        return true;
                    } else {
                        alert("File size should be less than 1MB only");
                        $("#" + myval).val('').clone(true);
                        $("#" + myval).focus();
                        return false;
                    }
                } else {
                    alert("Upload JPG Files only & File Size must be less than or Equal to 1MB");
                    $("#" + myval).val('').clone(true);
                    $("#" + myval).focus();
                    return false;
                }
            }
            function CheckfilePdfOrOther40kb(myval) {
                var fup = $("#" + myval).val();
                var ext = fup.split('.').pop().toUpperCase();
                if (ext === "JPG")
                {
                    var file = $("#" + myval)[0].files[0];
                    var size = file.size / 1024;
                    if (size < 41) {
                        return true;
                    } else {
                        alert("File size should be less than 40KB only");
                        $("#" + myval).val('').clone(true);
                        $("#" + myval).focus();
                        return false;
                    }
                } else {
                    alert("Upload JPG Files only & File Size must be less than or Equal to 40KB");
                    $("#" + myval).val('').clone(true);
                    $("#" + myval).focus();
                    return false;
                }
            }
            function showStatus() {
                var statusflag = $("input[name='statusflag']:checked").val();
                if (statusflag == "1") {
                    $("#startDiv").hide();
                    $("#yesDiv").show();
                    $("#noDiv").hide();
                } else if (statusflag == "2") {
                    $("#startDiv").hide();
                    $("#yesDiv").hide();
                    $("#noDiv").show();
                }
            }

            function blindStatus() {
                if ($("#blind").val() == "YES") {
                    $("#blindDiv").show();
                } else if ($("#blind").val() == "NO") {
                    $("#blindDiv").hide();
                }
            }
            function examinationStatus() {
                $("#grade").val("0");
                if ($("#examination").val() == "TW") {
                    $("#gradeDiv").show();
                    $("#language").empty().append('<option value="0">--select language--</option>\n\ \n\
           <option value="Telugu">Telugu</option>\n\
                                        <option value="English">English</option>\n\
                                        <option value="Hindi">Hindi</option>\n\\n\
<option value="Urdu">Urdu</option>')
                } else if ($("#examination").val() == "SH") {
                    $("#gradeDiv").hide();
                    $("#language").empty().append('<option value="0">--select language--</option>\n\ \n\
           <option value="Telugu">Telugu</option>\n\
                                        <option value="English">English</option>\n\
                                        <option value="Urdu">Urdu</option>')
                }
            }
            function batchStatus() {
                if ($("#edate").val() == "2021-05-01") {
                    $("#ebatch").empty().append('<option value="0">--Select Batch--</option>\n\ \n\
                            <option value="B1">Batch-I</option>\n\
                            <option value="B2">Batch-II</option>')
                } else if ($("#edate").val() == "2021-05-02") {
                    $("#ebatch").empty().append('<option value="0">--Select Batch--</option>\n\ \n\
                            <option value="B3">Batch-III</option>\n\
                            <option value="B4">Batch-IV</option>')
                }
            }
            function languageStatus() {
                if ($("#examination").val() == "TW") {
                    if ($("#language").val() == "Telugu") {
                        $("#grade").empty().append('<option value="0">--Select Grade--</option>\n\ \n\
           <option value="TTL">TYPEWRITING TELUGU LOWER(25 WPM)</option>\n\
                                        <option value="TTH">TYPEWRITING TELUGU HIGHER(40 WPM)</option>\n\
                                        <option value="TTHS">TYPEWRITING TELUGU HIGH SPEED</option>')
                    } else if ($("#language").val() == "English") {
                        $("#grade").empty().append('<option value="0">--Select Grade--</option>\n\ \n\
           <option value="TEJ">TYPEWRITING ENGLISH JUNIOR(25 WPM)</option>\n\
           <option value="TEL">TYPEWRITING ENGLISH LOWER(30 WPM)</option>\n\
                                        <option value="TEH">TYPEWRITING ENGLISH HIGHER(40 WPM)</option>\n\
                                        <option value="TEHS">TYPEWRITING ENGLISH HIGH SPEED</option>')

                    }
                    else if ($("#language").val() == "Hindi") {
                        $("#grade").empty().append('<option value="0">--Select Grade--</option>\n\ \n\
           <option value="THL">TYPEWRITING HINDI LOWER(25 WPM)</option>\n\
                                        <option value="THH">TYPEWRITING HINDI HIGHER(40 WPM)</option>')

                    }
                    else if ($("#language").val() == "Urdu") {
                        $("#grade").empty().append('<option value="0">--Select Grade--</option>\n\ \n\
           <option value="TUL">TYPEWRITING URDU LOWER(25 WPM)</option>\n\
                                        <option value="TUH">TYPEWRITING URDU HIGHER(40 WPM)</option>')

                    }
                } else if ($("#examination").val() == "SH")
                    if ($("#language").val() == "Telugu") {
                        $("#grade").empty().append('<option value="0">--Select Grade--</option>\n\ \n\
           <option value="STL">Shorthand TELUGU LOWER</option>\n\
                                        <option value="STH">Shorthand TELUGU HIGHER</option>\n\
                                        <option value="STHS">Shorthand TELUGU HIGH SPEED</option>')
                    } else if ($("#language").val() == "English") {
                        $("#grade").empty().append('<option value="0">--Select Grade--</option>\n\ \n\
           <option value="SEL">Shorthand ENGLISH LOWER</option>\n\
           <option value="SEI">Shorthand ENGLISH INTERMEDIATE</option>\n\
                                        <option value="SEH">Shorthand ENGLISH HIGHER</option>\n\
                                        <option value="SEHS">Shorthand ENGLISH HIGH SPEED</option>')

                    } else if ($("#language").val() == "Urdu") {
                        $("#grade").empty().append('<option value="0">--Select Grade--</option>\n\ \n\
           <option value="SUL">Shorthand URDU LOWER</option>\n\
                                        <option value="SUH">Shorthand URDU HIGHER</option>')

                    }
            }
            function fileStatus() {
                if ($("#grade").val() == "TTL" || $("#grade").val() == "THL" || $("#grade").val() == "TUL" || $("#grade").val() == "TEL" || $("#grade").val() == "SEL" || $("#grade").val() == "STL" || $("#grade").val() == "SUL") {
                    $("#lowdiv").show();
                    $('.xfile').show();
                    $('.viifile').hide();
                    $('.hsfile').hide();
                    $("#highdiv").hide();
                } else if ($("#grade").val() == "TEJ") {
                    $("#lowdiv").show();
                    $('.xfile').hide();
                    $('.viifile').show();
                    $('.hsfile').hide();
                    $("#highdiv").hide();
                } else if ($("#grade").val() == "TTHS" || $("#grade").val() == "TEHS" || $("#grade").val() == "SEHS" || $("#grade").val() == "STHS") {
                    $("#lowdiv").show();
                    $('.xfile').hide();
                    $('.viifile').hide();
                    $('.hsfile').show();
                    $("#highdiv").hide();
                } else if ($("#grade").val() == "TTH" || $("#grade").val() == "THH" || $("#grade").val() == "TUH" || $("#grade").val() == "TEH") {
                    $("#highdiv").show();
                    $(".highfile").show();
                    $(".highfile1").hide();
                    $("#lowdiv").hide();

                } else if ($("#grade").val() == "STH" || $("#grade").val() == "SEI" || $("#grade").val() == "SUH") {
                    $("#highdiv").show();
                     $("#seidiv").hide();
                    $(".highfile").hide();
                    $(".highfile1").show();
                    $("#highspeeddiv").hide();
                    $("#lowdiv").hide();
                } else if ($("#grade").val() == "SEH") {
                    $("#highdiv").show();
                    $("#seidiv").show();
                    $(".highfile").hide();
                    $(".highfile1").show();
                    $("#highspeeddiv").show();
                    $("#lowdiv").hide();
                }

            }
            function examinationDistricts() {
                var data = "district=" + $("#apdistrict").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./registerAfterLogin.do?mode=getCentersDistrict&examination=" + $("#examination").val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#apdistrict").empty().append(response);

                    },
                    error: function(e) {
//            alert('Error: ' + e);
                    }
                });
            }
            function centersList() {
                var data = "district=" + $("#apdistrict").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./registerAfterLogin.do?mode=getCenters&district=" + $("#apdistrict").val().trim() + "&examination=" + $("#examination").val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#ecenter").empty().append(response);

                    },
                    error: function(e) {
//            alert('Error: ' + e);
                    }
                });
            }
            function getDistricts() {
                $("#district").val("0");
                $("#mandal").val("0");
                $("#district1").val("");
                $("#mandal1").val("");
                if ($("#state").val() != "3") {
                    $("#DistrictDiv").show();
                    $("#DistrictDiv1").hide();
                    $("#MandalDiv").show();
                    $("#MandalDiv1").hide();
                    var data = "district=" + $("#state").val().trim();
                    $.ajax({
                        type: "POST",
                        url: "./registerAfterLogin.do?mode=getDistrictListAt&state=" + $("#state").val().trim(),
                        data: data,
                        cache: true,
                        contentType: true,
                        processData: true,
                        success: function(response) {
                            $("#district").empty().append(response);
                        },
                        error: function(e) {
                        }
                    });
                } else {
                    $("#DistrictDiv").hide();
                    $("#DistrictDiv1").show();
                    $("#MandalDiv").hide();
                    $("#MandalDiv1").show();
                }
            }
            function getMandals() {
                var data = "district=" + $("#state").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./registerAfterLogin.do?mode=getMandalsAt&state=" + $("#state").val().trim() + "&district=" + $("#district").val().trim(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#mandal").empty().append(response);

                    },
                    error: function(e) {
                    }
                });
            }
            function SubmitForm() {
//            adharDuplicatevalidation();
                if (document.getElementById("declaration").checked === false) {
                    alert("Accept Declaration ");
                    $("#declaration").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#bname").val() === undefined || $("#bname").val() === "") {
                    alert("Please enter Name of the applicant");
                    $("#bname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#fname").val() === undefined || $("#fname").val() === "") {
                    alert("Please enter Father Name of the applicant");
                    $("#fname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#dob").val() === undefined || $("#dob").val() === "") {
                    alert("Select  Date of Birth");
                    $("#dob").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#gender").val() === undefined || $("#gender").val() === "" || $("#gender").val() === "0") {
                    alert("Please Select Gender.");
                    $("#gender").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#caste").val() === undefined || $("#caste").val() === "" || $("#caste").val() === "0") {
                    alert("Select Caste.");
                    $("#caste").focus().css({'border': '1px solid red'});
                    return false;
                }

                else if ($("#blind").val() === undefined || $("#blind").val() === "" || $("#blind").val() === "0") {
                    alert("Select Are you Visually Impaired Candidate?");
                    $("#blind").focus().css({'border': '1px solid red'});
                    return false;
                }

                else if ($("#blind").val() == "YES" && ($("#blindfile").val() === undefined || $("#blindfile").val() === "")) {
                    alert("Upload Blind Certificate.");
                    $("#blindfile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#examination").val() === undefined || $("#examination").val() === "" || $("#examination").val() === "0") {
                    alert("Select Examination Appearing.");
                    $("#examination").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#language").val() === undefined || $("#language").val() === "" || $("#language").val() === "0") {
                    alert("Select Language.");
                    $("#language").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#grade").val() === undefined || $("#grade").val() === "" || $("#grade").val() === "0") {
                    alert("Select Grade");
                    $("#grade").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#apdistrict").val() === undefined || $("#apdistrict").val() === "" || $("#apdistrict").val() === "0") {
                    alert("Select Examination District");
                    $("#apdistrict").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#ecenter").val() === undefined || $("#ecenter").val() === "" || $("#ecenter").val() === "0") {
                    alert("Select Examination Center");
                    $("#ecenter").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#edate").val() === undefined || $("#edate").val() === "" || $("#edate").val() === "0") {
                    alert("Select Examination Date");
                    $("#edate").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#ebatch").val() === undefined || $("#ebatch").val() === "" || $("#ebatch").val() === "0") {
                    alert("Select Examination Batch");
                    $("#ebatch").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if ($("#upload1").val() === undefined || $("#upload1").val() === "") {
//                    alert("Upload Certificate.");
//                    $("#upload1").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if ($("#houseno").val() === undefined || $("#houseno").val() === "" || ($.trim($("#houseno").val()).length === 0)) {
                    alert("Enter Present Address house Number");
                    $("#houseno").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#locality").val() === undefined || $("#locality").val() === "" || ($.trim($("#locality").val()).length === 0)) {
                    alert("Enter Present Address Locality");
                    $("#locality").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#village").val() === undefined || $("#village").val() === "" || ($.trim($("#village").val()).length === 0)) {
                    alert("Enter Present Address Village/Town");
                    $("#village").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#state").val() === undefined || $("#state").val() === "" || $("#state").val() === "0") {
                    alert("Enter  Present Address State");
                    $("#state").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#state").val() != "3" && ($("#district").val() == "0" || $("#district").val() === ""))
                {
                    alert("Enter Present Address District");
                    $("#district").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#state").val() != "3" && ($("#mandal").val() == "0" || $("#mandal").val() === ""))
                {
                    alert("Enter Present Address Mandal");
                    $("#mandal").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#state").val() == "3" && ($("#district1").val() == "" || ($.trim($("#district1").val()).length === 0)))
                {
                    alert("Enter Present Address District");
                    $("#district1").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#state").val() == "3" && ($("#mandal1").val() == "" || ($.trim($("#mandal1").val()).length === 0)))
                {
                    alert("Enter Present Address Mandal");
                    $("#mandal1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#pincode").val() === undefined || $("#pincode").val() === "" || ($.trim($("#pincode").val()).length === 0)) {
                    alert("Enter Present Address Pincode");
                    $("#pincode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#mobile").val() === undefined || ($.trim($("#mobile").val()).length === 0 ||
                        $("#mobile").val().length < 10 || parseInt($("#mobile").val().trim()) < 6000000000)) {
                    alert("Enter Mobile Number");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#email").val() === undefined || $("#email").val() === "" || ($.trim($("#email").val()).length) === 0) {
                    alert("Enter EMail");
                    $("#email").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#aadhar").val() === undefined || $("#aadhar").val() === "" && $("#aadhar").val().length < 12) {
                    alert("Enter  valid  Aadhar Number");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#photo").val() === undefined || $("#photo").val() === "") {
                    alert("Upload your Photo.");
                    $("#photo").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#signature").val() === undefined || $("#signature").val() === "") {
                    alert("Upload your Signature.");
                    $("#signature").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    var x = confirm("Do you Want to Proceed?")
                    if (x == false) {
                        return false
                    } else {
                        document.forms[0].mode.value = "submitData";
                        document.forms[0].submit();
                    }
                }
            }
            function isEmail() {
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test($("#email").val())) {
                    $("#email").val("");
                    alert("Invalid eMail")
                    return true;  //wrong mail
                } else {
                    return false;  //correct mail

                }
            }
            function isPincode() {
                var pincode =$("#pincode").val();
                if (pincode.length<6) {
                    $("#pincode").val("");
                    alert("Pincode must be 6 digits")
                    return true;  //wrong mail
                } else {
                    return false;  //correct mail

                }
            }
            function getData(){
                 document.forms[0].mode.value = "editData";
                        document.forms[0].submit();
            }
            function adharDuplicatevalidation() {
                var paraData = "aadhaar=" + $("#aadhaar").val();
                $.ajax({
                    type: "POST",
                    url: "registerAfterLogin.do?mode=validatingaadharNum&aadharNum=" + $("#aadhar").val() + "&mobile=" + $("#mobile").val() + "&email=" + $("#email").val(),
                    data: paraData,
                    success: function(response) {
                        if (response == 1) {
                            alert("Aadhar/eMail/Mobile Number already Exist")
                            $("#aadhar").val("")
                            $("#mobile").val("")
                            $("#email").val("")

                        } else {
                        }
                    }


                });


            }
            function showData(){
            $("#bname").val('${bname}')
            $("#fname").val('${fname}')
            $("#email").val('${email}')
            $("#mobile").val('${mobile}')
            $("#dob").val('${dob}')
            $("#gender").val('${gender}')
            }
        </script>
        <!--    <div class="page-title title-1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Registration </h1>
                            <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">
        
                                <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                                <span>Registration</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
    <br/>
    <br/>
    <body onload="showData()">
    <div class="feedback-form">
        <div class="container">
            <div class="row">

                <h3 class="block-head-News">REGISTRATION FORM</h3>
                <div class="line-border"></div>
                <br>
                <div class="row">
                    <div class=" col-md-12">
                        <!--<div class="login-form">-->
                        <div>
                            <html:form action="/registerAfterLogin"  method="post" enctype="multipart/form-data">
                                <html:hidden property="mode"/>
                                <logic:present name="result2">
                                    <center> <font color="green" style="font-weight: bold">${result2}</font>  </center>  
                                    </logic:present>
                                    <logic:present name="result">
                                    <center> <font color="red" style="font-weight: bold">${result}</font></center>
                                </logic:present><br>
                            </div>
                        </div>
                    </div>

                    <div id="noDiv">
                        <p class="p_textred" style="float: right; color: #f00;">All uploads must be in  JPG Format<br/>
                            Registered  Mobile Number and Email will be used for all future communications </p>
                        <br/>
                        <br/>
                        <h5 class="block-head-News">Personal Details (As per SSC Certificate)</h5>
                        <div class="line-border"></div>
                        <div class="row">
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Name of the candidate<font color="red">*</font> </label>
                                    <div class="col-sm-8">
                                        <html:text  styleClass="form-control" property="bname" styleId="bname" maxlength="250" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Father Name <font color="red">*</font></label>
                                    <div class="col-sm-8">
                                        <html:text  styleClass="form-control" property="fname" styleId="fname" maxlength="250" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Date of Birth <font color="red">*</font></label>
                                    <div class="col-sm-8">
                                        <html:text property="dob" styleId="dob" styleClass="form-control" readonly="true" />
                                        <Script>
             $(document).ready(function() {
                 var today = new Date();
                 yrRange = '1998' + ":" + '2010';
                 $("#dob").datepicker({
                     dateFormat: 'dd/mm/yy',
                     changeMonth: true,
                     maxDate: new Date(2010, 08 - 1, 31),
                     minDate: new Date(1998, 12 - 04, 1),
                     yearRange: yrRange,
                     changeYear: true
                 });
             });
                                        </Script>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Gender<font color="red">*</font> </label>
                                    <div class="col-sm-8">
                                        <html:select property="gender" styleId="gender" styleClass="form-control">
                                            <html:option value="0">--Select Gender--</html:option>
                                            <html:option value="MALE">MALE</html:option>
                                            <html:option value="FEMALE">FEMALE</html:option>
                                            <html:option value="TRANSGENDER">TRANSGENDER</html:option>
                                        </html:select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Community <font color="red">*</font></label>
                                    <div class="col-sm-8">
                                        <html:select property="caste" styleId="caste" styleClass="form-control">
                                            <html:option value="0">--Select Caste--</html:option>
                                            <html:option value="OC">OC</html:option>
                                            <html:option value="BC-A">BC-A</html:option>
                                            <html:option value="BC-B">BC-B</html:option>
                                            <html:option value="BC-C">BC-C</html:option>
                                            <html:option value="BC-D">BC-D</html:option>
                                            <html:option value="BC-E">BC-E</html:option>
                                            <html:option value="SC">SC</html:option>
                                            <html:option value="ST">ST</html:option>
                                        </html:select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Are you Visually Impaired Candidate?<font color="red">*</font></label>
                                    <div class="col-sm-8">
                                        <html:select property="blind" styleId="blind" styleClass="form-control" onchange="blindStatus()">
                                            <html:option value="0">--Select BlindStatus--</html:option>
                                            <html:option value="YES">YES</html:option>
                                            <html:option value="NO">NO</html:option>
                                        </html:select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="blindDiv" class="row">
                            <div class="col-md-8">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Upload Certificate(should not be more than 1MB)<font color="red">*</font> </label>
                                    <div class="col-sm-8">
                                        <html:file property="blindfile" styleId="blindfile" onchange="return CheckfilePdfOrOther('blindfile');"/>

                                        <!--<textarea rows="3" class="form-control"></textarea>-->

                                    </div>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <h5 class="block-head-News">Examination Details</h5>
                        <div class="line-border"></div>
                        <div class="row">
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Institution<font color="red">*</font></label>
                                    <div class="col-sm-8">
                                        <html:text  styleClass="form-control" property="institute" styleId="institute" value="${institute}" readonly="true"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Examination Appearing<font color="red">*</font> </label>
                                    <div class="col-sm-8">
                                        <html:select property="examination" styleId="examination" styleClass="form-control" onchange="examinationStatus();examinationDistricts()">
                                            <html:option value="0">--Select--</html:option>
                                            <html:option value="TW">Typewriting</html:option>
                                            <html:option value="SH">Shorthand</html:option>
                                        </html:select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Language<font color="red">*</font> </label>
                                    <div class="col-sm-8">
                                        <html:select property="language" styleId="language" styleClass="form-control" onchange="languageStatus()">
                                            <html:option value="0">--Select--</html:option>
                                        </html:select>

                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Grade<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:select property="grade" styleId="grade" styleClass="form-control" onchange="fileStatus()">
                                        <html:option value="0">--Select--</html:option>
                                    </html:select>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Examination District <font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:hidden property="apdistrict" styleId="apdistrict" value="${sessionScope.distcd}"/>
                                    <html:text property="apdistrict1" styleId="apdistrict1" styleClass="form-control" value="${sessionScope.distname}" readonly="true"/>
                                 <%--   <html:select property="apdistrict" styleId="apdistrict" styleClass="form-control" onchange="centersList();">
                                        <html:option value="0">--Select District--</html:option> 
                                        <html:optionsCollection property="distLists" label="district_Name" value="district_ID"/>
                                    </html:select>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Examination Center<font color="red">*</font> </label>
                                <div class="col-sm-8">
                                     <html:hidden property="ecenter" styleId="ecenter" value="${sessionScope.ccode}"/>
                                    <html:text property="ecenter1" styleId="ecenter1" styleClass="form-control" value="${sessionScope.cname}" readonly="true"/>
                               
                               <%--     <html:select property="ecenter" styleId="ecenter" styleClass="form-control">
                                        <html:option value="0">--Select--</html:option>
                                    </html:select>--%>

                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Examination Date<font color="red">*</font></label>
                                    <div class="col-sm-8">
                                        <html:select property="edate" styleId="edate" styleClass="form-control" onchange="batchStatus()">
                                            <html:option value="0">--Select Examination Date--</html:option>
                                            <html:option value="2021-05-01">DAY-I</html:option>
                                            <html:option value="2021-05-02">DAY-II</html:option>
                                        </html:select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Examination Batch<font color="red">*</font> </label>
                                    <div class="col-sm-8">
                                        <html:select property="ebatch" styleId="ebatch" styleClass="form-control">
                                            <html:option value="0">--Select Batch--</html:option> 
                                        </html:select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4" id="lowdiv">

                                <div class="form-group row">
                                    <div class="viifile" > <label for="name" class="col-sm-4 col-form-label">Upload VII Bonafide Certificate<font color="red">*</font></label></div>
                                    <div class="xfile" > <label for="name" class="col-sm-4 col-form-label">Upload SSC Hallticket/Memo<font color="red">*</font></label></div>
                                    <div class="hsfile" > <label for="name" class="col-sm-4 col-form-label">Upload Higher Grade Certificate<font color="red">*</font></label></div>
                                    <div class="col-sm-8">
                                        <html:file property="upload1" styleId="upload1" onchange="return CheckfilePdfOrOther40kb('upload1');"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div id="highdiv">
                                <div class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Upload Lower Grade Certificate<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:file property="upload4" styleId="upload4" onchange="return CheckfilePdfOrOther40kb('upload4');"/>
                                        </div>
                                    </div>
                                </div>

                               
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <div class="highfile"> <label for="name" class="col-sm-4 col-form-label">Upload SSC  Certificate<font color="red">*</font></label></div>
                                        <div class="highfile1"> <label for="name" class="col-sm-4 col-form-label"><font color="red">(OR)</font>Upload Graduation  Certificate<font color="red">*</font></label></div>
                                        <div class="col-sm-8">
                                            <html:file property="upload2" styleId="upload2" onchange="return CheckfilePdfOrOther40kb('upload2');"/>
                                        </div>
                                    </div>
                                </div>
                                <div id="seidiv">
                                
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <div class="highfile"> <label for="name" class="col-sm-4 col-form-label"><font color="red">(OR)</font>Upload Intermediate Certificate<font color="red">*</font></label></div>
                                        <div class="highfile1"><label for="name" class="col-sm-4 col-form-label"><font color="red">(OR)</font>Upload Vocational Certificate<font color="red">*</font></label></div>
                                        <div class="col-sm-8">
                                            <html:file property="upload3" styleId="upload3" onchange="return CheckfilePdfOrOther40kb('upload3');"/>
                                        </div>
                                    </div>
                                </div>
                                </div>
                            </div>
                            </div>
                            <br/>
                          
                            <h5 class="block-head-News">Communication Details</h5>
                            <div class="line-border"></div>
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">House No<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="houseno" styleId="houseno" maxlength="250" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Street<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="locality" styleId="locality" maxlength="250" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Village<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="village" styleId="village" maxlength="100" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');"/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">State<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:select property="state" styleId="state" styleClass="form-control" onchange="getDistricts()">
                                                <html:option value="0">--Select--</html:option>
                                                <html:option value="1">ANDHRA PRADESH</html:option>
                                                <html:option value="2">TELANGANA</html:option>
                                                <html:option value="3">OTHERS</html:option>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>
                                <div id="DistrictDiv" class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">District<font color="red">*</font> </label>
                                        <div class="col-sm-8">
                                            <html:select property="district" styleId="district" styleClass="form-control" onchange="getMandals()">
                                                <html:option value="0">--Select--</html:option>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>
                                <div id="MandalDiv" class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Mandal <font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:select property="mandal" styleId="mandal" styleClass="form-control" >
                                                <html:option value="0">--Select--</html:option>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>
                                <div id="DistrictDiv1" class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">District<font color="red">*</font> </label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="district1" styleId="district1" maxlength="100" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                        </div>
                                    </div>
                                </div>
                                <div id="MandalDiv1" class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Mandal<font color="red">*</font> </label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="mandal1" styleId="mandal1"   maxlength="100"  onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                        </div>
                                    </div>
                                </div>


                            </div>
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Pincode<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="pincode" styleId="pincode" maxlength="6" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);" onchange="isPincode();"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Mobile No<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="mobile" styleId="mobile"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"  onchange="telephoneValidation(),adharDuplicatevalidation()" maxlength="10"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">eMail<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="email" styleId="email"  onkeydown="return space(event, this);" maxlength="100" onchange='return isEmail(this),adharDuplicatevalidation()'/>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="col-md-4">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Aadhar Number<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control" property="aadhar" styleId="aadhar" maxlength="12"  onchange="validateAdharDetails(),adharDuplicatevalidation()" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-8">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Upload Photo  (should not be more than 40KB)<font color="red">*</font></label>
                                        <div class="col-sm-8">                                           
                                            <html:file property="photo" styleId="photo" onchange="return CheckfilePdfOrOther40kb('photo');"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Upload Signature (should not be more than 40KB)<font color="red">*</font></label>
                                        <div class="col-sm-8">                                           
                                            <html:file property="signature" styleId="signature" onchange="return CheckfilePdfOrOther40kb('signature');"/>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            <div>  <input type="checkbox" id="declaration" style="max-width: 15px;max-height: 15px;font-size:24px;"/>&nbsp;&nbsp;&nbsp;I promise to abide by the rules/regulations and the orders of the University, its Authorities and Officers.I do hereby declare that the information furnished in this application is true to the best of my knowledge and belief.
                                I am aware that in the event of any information being found to be false or untrue, I shall be liable to such action by SBTET.
                                <br/>
                            </div>
                            <br/>
                            <br/>
                            <div class="row">
                                <div class="col-md-12 center">

                                    <center>   <input type="button" onclick="return SubmitForm();" value="SUBMIT"  class="submit"/></center>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <br/>
                    </html:form>
                </div>
            </div> 
        </div>
    </div>
</div>	
</div>
</body>
</html>
