<%-- 
    Document   : CourseStatusReport
    Created on : Nov 23, 2020, 1:51:42 PM
    Author     : 1582792
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html>
<html>

    <head>
        <%int i = 0;%>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script> 
        <script>
            $(document).ready(function() {
                $('#example1').DataTable({
                    "scrollX": true,
                    "paging": false,
                    "bInfo": false,
                    bFilter: false,
                });
                $('#example2').DataTable({
                    "scrollX": true,
                    "paging": false,
                    "bInfo": false,
                    bFilter: false,
                });
                $('#example3').DataTable({
                    "scrollX": true,
                    "paging": false,
                    "bInfo": false,
                    bFilter: false,
                });
            });
        </script>
        <style type="text/css">
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .block-head-News1 {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 15px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #9fa7ff !important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }

            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }
            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <script>
            function getDocumentDetailsData() {
                document.forms[0].mode.value = "unspecified";
                document.forms[0].submit();
            }
            function getPrint() {
                if ($("#ecenter").val() == "") {
                    alert("Enter PIN Number")
                    $("#ecenter").focus();
                } else {
                    document.forms[0].mode.value = "getData";
                    document.forms[0].submit();
                }
            }
        </script>
    </head>
    <body>

        <div class="row mainbodyrow">
            <div class="container" style="padding-top: 15px;">
                <div class="col-xs-12">
                    <div class="maindodycnt">
                        </br>
                        <h3 class="block-head-News">DIPLOMA STUDENT STATUS</h3>
                        <div class="line-border"></div>
                        <html:form action="/diplomaBasicSupport">
                            <html:hidden property="mode"/>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Enter PIN<font color="red">*</font></label>
                                        <div class="col-sm-4">
                                            <html:text property="ecenter" styleId="ecenter" styleClass="form-control" ></html:text> 
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <div class="col-md-2 center">
                                                <center><input type="button" onclick="return getPrint()" value="submit" class="btn btn-primary" style="max-width: 96px;"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <br/>

                            <logic:present  name="candiList1">
                                <h3 class="block-head-News1">STUDENT STATUS</h3>
                                <br/>
                                <table id="example1" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>PIN</th>
                                            <th>Name</th>
                                            <th>Father Name</th>
                                            <th>Gender</th>
                                            <th>Scheme</th>
                                            <th>Branch</th>
                                            <th>Sem.Year</th>
                                            <th>Category</th>
                                            <th>Passed Year</th>
                                            <th>Subjects</th>

                                            <th>Exam Fee</th>
                                            <th>Backlog Fee</th>
                                            <th>Late Fee</th>
                                            <th>Tatkal Fee</th>
                                            <th>Certificate Fee</th>
                                            <th>Condonation Fee</th>
                                            <th>Created Date</th>
                                            <th>Attendance</th>
                                            <th>Payment Status</th>
                                            <th>Eligibility</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <logic:iterate name="candiList1" id="list">
                                            <tr>
                                                <td style="text-align: left;">${list.pin}</td>
                                                <td style="text-align: center;">${list.name}</td>     
                                                <td style="text-align: center;">${list.fname}</td>
                                                <td style="text-align:center;">${list.gender}</td>
                                                <td style="text-align:center;">${list.scheme}</td>
                                                <td style="text-align: center;">${list.brach}</td>
                                                <td style="text-align: center;">${list.semY}</td>
                                                <td style="text-align:center;">${list.cat}</td>
                                                <td style="text-align: center;">${list.pyear}</td>
                                                <td style="text-align: center;">${list.subjects}</td>

                                                <td style="text-align: left;">${list.examFee}</td>
                                                <td style="text-align: center;">${list.backLogFee}</td>     
                                                <td style="text-align: center;">${list.lateFee}</td>
                                                <td style="text-align:center;">${list.tatFee}</td>
                                                <td style="text-align:center;">${list.cerFee}</td>
                                                <td style="text-align: center;">${list.cfee}</td>
                                                <td style="text-align: center;">${list.cadte}</td>
                                                <td style="text-align:center;">${list.att}</td>
                                                <td style="text-align: center;">${list.pStatus}</td>
                                                <td style="text-align: center;">${list.eStatus}</td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                </table>
                            </logic:present>
                            <br/>


                            <logic:present  name="candiList2">
                                <h3 class="block-head-News1">STUDENT INFO DEPARTMENT SHARED DATA</h3>
                                <br/>
                                <table id="example2" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Father Name</th>
                                            <th>Gender</th>
                                            <th>DOB</th>
                                            <th>Caste</th>
                                            <th>10th HallTicket</th>
                                            <th>10th Passed Year</th>
                                            <th>Joining Date</th>
                                            <th>PH</th>
                                            <th>Identity One</th>

                                            <th>Identity Two</th>
                                            <th>H.NO</th>
                                            <th>Street</th>
                                            <th>City</th>
                                            <th>Mandal</th>
                                            <th>District</th>
                                            <th>PinCode</th>
                                            <th>Mobile</th>
                                            <th>Email</th>
                                            <th>Aadhar</th>

                                            <th>Admission Status</th>
                                            <th>College</th>
                                            <th>Branch</th>
                                            <th>Scheme</th>
                                            <th>Sem.Year</th>
                                            <th>Shift</th>
                                            <th>Section</th>
                                            <th>Active Status</th>
                                            <th>Delete Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <logic:iterate name="candiList2" id="list">
                                            <tr>
                                                <td style="text-align: center;">${list.name}</td>     
                                                <td style="text-align: center;">${list.fname}</td>
                                                <td style="text-align:center;">${list.gender}</td>
                                                <td style="text-align:center;">${list.dob}</td>
                                                <td style="text-align:center;">${list.caste}</td>
                                                <td style="text-align: center;">${list.TenthHallTicket}</td>
                                                <td style="text-align: center;">${list.TenPassYear}</td>
                                                <td style="text-align:center;">${list.joinDate}</td>
                                                <td style="text-align: center;">${list.PH}</td>
                                                <td style="text-align: center;">${list.idOne}</td>

                                                <td style="text-align: left;">${list.idtwo}</td>
                                                <td style="text-align: center;">${list.hno}</td>     
                                                <td style="text-align: center;">${list.streetName}</td>
                                                <td style="text-align:center;">${list.city}</td>
                                                <td style="text-align:center;">${list.mandal}</td>
                                                <td style="text-align: center;">${list.dist}</td>
                                                <td style="text-align: center;">${list.pinCode}</td>
                                                <td style="text-align:center;">${list.mobile}</td>
                                                <td style="text-align: center;">${list.email}</td>
                                                <td style="text-align: center;">${list.aadhar}</td>

                                                <td style="text-align: left;">${list.AddStatus}</td>
                                                <td style="text-align: center;">${list.college}</td>     
                                                <td style="text-align: center;">${list.brach}</td>
                                                <td style="text-align:center;">${list.scheme}</td>
                                                <td style="text-align:center;">${list.semYear}</td>
                                                <td style="text-align: center;">${list.shift}</td>
                                                <td style="text-align: center;">${list.section}</td>
                                                <td style="text-align:center;">${list.active}</td>
                                                <td style="text-align: center;">${list.dStatus}</td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                </table>
                            </logic:present>

                            <br/>

                            <logic:present  name="candiList3">
                                <h3 class="block-head-News1">STUDENT SUBJECT WISE APP DATA</h3>
                                <br/>
                                <table id="example3" class="display nowrap" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>Scheme</th>
                                            <th>Batch</th>
                                            <th>College Name</th>
                                            <th>Branch</th>
                                            <th>Pin</th>
                                            <th>Sem.Year</th>
                                            <th>Category</th>
                                            <th>Result</th>
                                            <th>Total</th>
                                            <th>Passed Year</th>

                                            <th>SUB1</th>
                                            <th>R1</th>
                                            <th>SUB2</th>
                                            <th>R2</th>
                                            <th>SUB3</th>
                                            <th>R3</th>
                                            <th>SUB4</th>
                                            <th>R4</th>
                                            <th>SUB5</th>
                                            <th>R5</th>

                                            <th>SUB6</th>
                                            <th>R6</th>
                                            <th>SUB7</th>
                                            <th>R7</th>
                                            <th>SUB8</th>
                                            <th>R8</th>
                                            <th>SUB9</th>
                                            <th>R9</th>
                                            <th>SUB10</th>
                                            <th>R10</th>

                                            <th>SUB11</th>
                                            <th>R11</th>
                                            <th>Res.Date</th>
                                            <th>F_Status</th>
                                            <th>Eligibility</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <logic:iterate name="candiList3" id="list">
                                            <tr>
                                                <td style="text-align: center;">${list.scheme}</td>     
                                                <td style="text-align: center;">${list.batch}</td>
                                                <td style="text-align:center;">${list.college}</td>
                                                <td style="text-align:center;">${list.brach}</td>
                                                <td style="text-align:center;">${list.pin}</td>
                                                <td style="text-align: center;">${list.semYear}</td>
                                                <td style="text-align: center;">${list.cat}</td>
                                                <td style="text-align:center;">${list.result}</td>
                                                <td style="text-align: center;">${list.total}</td>
                                                <td style="text-align: center;">${list.passsYear}</td>

                                                <td style="text-align: left;">${list.sub1}</td>
                                                <td style="text-align: center;">${list.r1}</td>     
                                                <td style="text-align: center;">${list.sub2}</td>
                                                <td style="text-align:center;">${list.r2}</td>
                                                <td style="text-align:center;">${list.sub3}</td>
                                                <td style="text-align: center;">${list.r3}</td>
                                                <td style="text-align: center;">${list.sub4}</td>
                                                <td style="text-align:center;">${list.r4}</td>
                                                <td style="text-align: center;">${list.sub5}</td>
                                                <td style="text-align: center;">${list.r5}</td>

                                                <td style="text-align: left;">${list.sub6}</td>
                                                <td style="text-align: center;">${list.r6}</td>     
                                                <td style="text-align: center;">${list.sub7}</td>
                                                <td style="text-align:center;">${list.r7}</td>
                                                <td style="text-align:center;">${list.sub8}</td>
                                                <td style="text-align: center;">${list.r8}</td>
                                                <td style="text-align: center;">${list.sub9}</td>
                                                <td style="text-align:center;">${list.r9}</td>
                                                <td style="text-align: center;">${list.sub10}</td>
                                                <td style="text-align: center;">${list.r10}</td>

                                                <td style="text-align: center;">${list.sub11}</td>
                                                <td style="text-align: center;">${list.r11}</td>
                                                <td style="text-align:center;">${list.rdate}</td>
                                                <td style="text-align: center;">${list.fStatus}</td>
                                                <td style="text-align: center;">${list.eligibilty}</td>
                                            </tr>
                                        </logic:iterate>
                                    </tbody>
                                </table>
                            </logic:present>


                        </html:form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>                                       