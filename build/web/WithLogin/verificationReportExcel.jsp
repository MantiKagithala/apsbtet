<%-- 
    Document   : CourseStatusReport
    Created on : Nov 23, 2020, 1:51:42 PM
    Author     : 1582792
--%>

<%@ page contentType="application/vnd.ms-excel"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html>
<html>

    <head>
        <%int i = 0;%>
        <link rel="stylesheet" type="text/css" href="/css/site-examples.css">
        <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">
        <script type="text/javascript" language="javascript" src="js/jquery-1.12.4.js"></script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" language="javascript" src="js/demo.js"></script>
        <script type="text/javascript" class="init">

            $(document).ready(function() {
                $('#example').DataTable({
                    //  "scrollY": 300,
                    "scrollX": true
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #9fa7ff !important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }

            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }
            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"]
            {
                width: 30px !important;
                float: left;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn{
                padding: 6px 12px !important;
                width: auto !important;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row" style="margin: 0px; margin-top: 10px;margin-bottom: 10px;">
                <div  class="main_div" style="background-color:#fff;min-height:435px;box-shadow: 0 0 5px #ccc;">
                    </br>
                    <h3 class="h3_text">TWSH Certificate verification status</h3>
                    <html:form action="/verificationReport" >
                        <html:hidden property="mode"/>
                        <html:hidden property="pgmarksbsc"/>
                        <html:hidden property="dob"/>
                        <html:hidden property="course"/>
                        <html:hidden property="aadhar" styleId="aadhar"/>

                        <logic:present name="result1">
                            <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                            </logic:present>
                            <logic:present name="result">
                            <center> <font color="red" style="font-weight: bold">${result}</font></center>
                            </logic:present>

                        <logic:present  name="candiList">
                            <table id="example" class="display nowrap" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>SNO</th>
                                        <th>Name</th>
                                        <th>Total Assigned</th>
                                        <th>Not Verified</th>
                                        <th>Send To Reupload</th>
                                        <th>Pending for 2nd Verification</th>
                                        <th>Verified</th>
                                        <th>Auto Verified</th>
                                        <th>Rejected</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <logic:iterate name="candiList" id="list">
                                        <tr>
                                            <td style="text-align: center;"><%=++i%></td>
                                            <td style="text-align: left;">${list.name}</td>
                                            <td style="text-align: center;">${list.assignedCount}</td>     
                                            <td style="text-align: center;">${list.notverified}</td>
                                            <td style="text-align:center;">${list.sendToReupload}</td>
                                            <td style="text-align:center;">${list.pending2nd}</td>
                                            <td style="text-align: center;">${list.approved}</td>
                                            <td style="text-align: center;">${list.autoVerified}</td>
                                            <td style="text-align:center;">${list.rejected}</td>
                                        </tr>
                                    </logic:iterate>
                                </tbody>
                                <tr>
                                    <td style="text-align: center;" colspan="2">Total</td>
                                    <td style="text-align: center;">${list.totalAssignedCount}</td>     
                                    <td style="text-align: center;">${list.totalNotverified}</td>
                                    <td style="text-align:center;">${list.totalSendToReupload}</td>
                                    <td style="text-align:center;">${list.totalPending2nd}</td> 
                                    <td style="text-align: center;">${list.totalApproved}</td>
                                    <td style="text-align: center;">${list.totalAutoVerifiedCount}</td>
                                    <td style="text-align:center;">${list.totalRejectedCount}</td> 
                                </tr>
                            </table>
                        </logic:present>
                    </html:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>                                       