<%-- 
    Document   : practicalMarksEntryIS
    Created on : Aug 13, 2021, 12:38:08 PM
    Author     : 1582792
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html>
<html>

    <head>
        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>
     
        <style type="text/css">
           
            @page { size: landscape;}
           
            @media print {
                body {
                    -webkit-print-color-adjust: exact;
                   
                }
            }

            .vendorListHeading th {
                background-color: #1a4567 !important;
                color: white !important;   
            }

            td {
                text-align: center;
            }
        </style>
    <script>
    

function printData(){
   
   setTimeout(function () { window.close(); }, 1000);
    window.print();
   
}
    </script>
</head>
<body padding ="0 !important" onload="printData()" >
   
                    <h3 class="h3_background"><b><center>CCIC Practical Marks Report </center></b></h3>
                    <span></span>

                    <html:form action="/practicalsCCICReport" >
                        <html:hidden property="mode"/>
                        
                            <logic:present name="result1">
                                <br><br>
                                <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                                <br>
                            </logic:present>
                            <logic:present name="result">
                                <br><br>
                                <center> <font color="red" style="font-weight: bold">${result}</font></center>
                                <br>
                            </logic:present>
                            <br>
                            <logic:present name="doculist">
                                
                                    <table border="1" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>
                                                <th rowspan="3">S.No</th>
                                                <th rowspan="3">Application No</th>
                                                <th rowspan="3">Name of Applicant</th>
                                                <th colspan="2">Practicals </th>
                                                <th colspan="18">Sessionals </th>
                                            </tr>
                                            <tr>
                                                <th colspan="2" >${epaper9}</th>

                                                <th colspan="2" >${epaper1}</th>
                                                <th colspan="2" >${epaper2}</th>
                                                <th colspan="2" >${epaper3}</th>
                                                <th colspan="2" >${epaper4}</th>
                                                <th colspan="2" >${epaper5}</th>
                                                <th colspan="2" >${epaper6}</th>
                                                <th colspan="2" >${epaper7}</th>
                                                <th colspan="2">${epaper8}</th>
                                                <th colspan="2" >${epaper9}</th>

                                            </tr>
                                            <tr>
                                                <th>Total<br> Marks</th>
                                                <th>Obtained<br> Marks</th>

                                                <th>Total<br> Marks</th>
                                                <th>Obtained<br> Marks</th>
                                                <th>Total<br> Marks</th>
                                                <th>Obtained<br> Marks</th>
                                                <th>Total<br> Marks</th>
                                                <th>Obtained<br> Marks</th>
                                                <th>Total<br> Marks</th>
                                                <th>Obtained<br> Marks</th>
                                                <th>Total<br> Marks</th>
                                                <th>Obtained<br> Marks</th>
                                                <th>Total<br> Marks</th>
                                                <th>Obtained<br> Marks</th>
                                                <th>Total<br> Marks</th>
                                                <th>Obtained<br> Marks</th>
                                                <th>Total<br> Marks</th>
                                                <th>Obtained<br> Marks</th>
                                                <th>Total<br> Marks</th>
                                                <th>Obtained<br> Marks</th>
                                            </tr>

                                        </thead>
                                        <tbody> 
                                            <%int l = 0;%>
                                            <c:forEach var="list" items="${doculist}" >
                                                <tr>
                                                     <td><%=i++%></td>
                                                    <!--<td><input type="checkbox" class="checkbox" style=" margin: auto;" name="checkid" id="checkid<%=l%>" value="<%=l%>"></td>-->
                                                    <td>
                                                        ${list.applicantid}
                                                        <input type="hidden" name="applicationno<%=l%>" id="applicationno<%=l%>"  value="${list.applicantid}">
                                                    </td>
                                                    <td class="cell expand-maximum-on-hover">${list.applicantname}</td>
                                                    <td>${list.maxMarks9}</td>
                                            <input type="hidden" name="maxMarks<%=l%>" id="maxMarks<%=l%>"  value="${list.maxMarks9}">
                                            <td>
                                                ${list.practicalScored1}
                                                <!--<input type="text" name="obtainedMarks<%=l%>" id="obtainedMarks<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('obtainedMarks<%=l%>', 'maxMarks<%=l%>');"/>-->
                                            </td>

                                            <td>${list.marks}</td>
                                            <input type="hidden" name="sesmaxMarks<%=l%>" id="sesmaxMarks<%=l%>"  value="${list.marks}">
                                            <td>
                                                 ${list.sessionalScored1}
                                                <!--<input type="text" name="sesobtainedMarks<%=l%>" id="sesobtainedMarks<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('sesobtainedMarks<%=l%>', 'sesmaxMarks<%=l%>');"/>-->
                                            </td>
                                            <td>${list.marks2}</td>
                                            <input type="hidden" name="sesmaxMarks2<%=l%>" id="sesmaxMarks2<%=l%>"  value="${list.marks2}">
                                            <td>
                                                   ${list.sessionalScored2}
                                                <!--<input type="text" name="sesobtainedMarks2<%=l%>" id="sesobtainedMarks2<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('sesobtainedMarks2<%=l%>', 'sesmaxMarks2<%=l%>');"/>-->
                                            </td>
                                            <td>${list.marks3}</td>
                                            <input type="hidden" name="sesmaxMarks3<%=l%>" id="sesmaxMarks3<%=l%>"  value="${list.marks3}">
                                            <td>
                                                   ${list.sessionalScored3}
                                                <!--<input type="text" name="sesobtainedMarks3<%=l%>" id="sesobtainedMarks3<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('sesobtainedMarks3<%=l%>', 'sesmaxMarks3<%=l%>');"/>-->
                                            </td>

                                            <td>${list.marks4}</td>
                                            <input type="hidden" name="sesmaxMarks4<%=l%>" id="sesmaxMarks4<%=l%>"  value="${list.marks4}">
                                            <td>
                                                   ${list.sessionalScored4}
                                                <!--<input type="text" name="sesobtainedMarks4<%=l%>" id="sesobtainedMarks4<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('sesobtainedMarks4<%=l%>', 'sesmaxMarks4<%=l%>');"/>-->
                                            </td>

                                            <td>${list.marks5}</td>
                                            <input type="hidden" name="sesmaxMarks5<%=l%>" id="sesmaxMarks5<%=l%>"  value="${list.marks5}">
                                            <td>
                                                   ${list.sessionalScored5}
                                                <!--<input type="text" name="sesobtainedMarks5<%=l%>" id="sesobtainedMarks5<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('sesobtainedMarks5<%=l%>', 'sesmaxMarks5<%=l%>');"/>-->
                                            </td>

                                            <td>${list.marks6}</td>
                                            <input type="hidden" name="sesmaxMarks6<%=l%>" id="sesmaxMarks6<%=l%>"  value="${list.marks6}">
                                            <td>
                                                   ${list.sessionalScored6}
                                                <!--<input type="text" name="sesobtainedMarks6<%=l%>" id="sesobtainedMarks6<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('sesobtainedMarks6<%=l%>', 'sesmaxMarks6<%=l%>');"/>-->
                                            </td>
                                            <td>${list.marks7}</td>
                                            <input type="hidden" name="sesmaxMarks7<%=l%>" id="sesmaxMarks7<%=l%>"  value="${list.marks7}">
                                            <td>
                                                   ${list.sessionalScored7}
                                                <!--<input type="text" name="sesobtainedMarks7<%=l%>" id="sesobtainedMarks7<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('sesobtainedMarks7<%=l%>', 'sesmaxMarks7<%=l%>');"/>-->
                                            </td>
                                            <td>${list.marks8}</td>
                                            <input type="hidden" name="sesmaxMarks8<%=l%>" id="sesmaxMarks8<%=l%>"  value="${list.marks8}">
                                            <td>
                                                   ${list.sessionalScored8}
                                                <!--<input type="text" name="sesobtainedMarks8<%=l%>" id="sesobtainedMarks8<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('sesobtainedMarks8<%=l%>', 'sesmaxMarks8<%=l%>');"/>-->
                                            </td>
                                            <td>${list.marks9}</td>
                                            <input type="hidden" name="sesmaxMarks9<%=l%>" id="sesmaxMarks9<%=l%>"  value="${list.marks9}">
                                            <td>
                                                   ${list.sessionalScored9}
                                                <!--<input type="text" name="sesobtainedMarks9<%=l%>" id="sesobtainedMarks9<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('sesobtainedMarks9<%=l%>', 'sesmaxMarks9<%=l%>');"/>-->
                                            </td>
                                            </tr>
                                        </c:forEach>
                                        </tbody>
                                    </table>
                               
                                <!--<center> <button onclick="return submitData();" class="btn btn-primary">Submit</button></center>-->
                                </logic:present>
                           

                    </html:form>
             
</body>
      

</html>     