<%-- 
    Document   : Teachersprint
    Created on : Jun 5, 2018, 7:39:44 PM
    Author     : 1042564
--%>
<%@ page contentType="application/vnd.ms-excel"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<!DOCTYPE html>
<html>

    <head>
        <%
            int i = 0;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <link rel="stylesheet" type="text/css" href="/css/site-examples.css">
        <!--<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">-->

        <script type="text/javascript" language="javascript" src="js/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/dataTables.fixedColumns.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/demo.js">
        </script>
        <script type="text/javascript" class="init">

            $(document).ready(function() {
                var table = $('#example').DataTable({
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: true,
                    fixedColumns: true
                });
            });
        </script>
        <style type="text/css">
            th, td { white-space: nowrap; }
            div.dataTables_wrapper {
                margin: 0 auto;
            }
            /* 
               div.container {
                   width: 60%;
               }*/
            table.dataTable
            {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.dataTable thead th {
                padding: 10px 18px;
                border: 1px solid #115400 !important;
                color: #fff;
                background-color: #1d7806 !important;
            }
            table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #115400 !important;
            }
            .h3_head{
                background: #337ab7;
                color: #fff;
                font-size: 16px;
                padding: 10px !important;
                text-align: center;
                box-shadow: 5px 5px 5px #ccc;
            }
        </style>
        <style type="text/css">
            #example_wrapper
            {
                width: 70% !important;
            }
        </style>

    </head>

   
</head>
<body>
    <div class="row mainbodyrow">
        <div class="container" style="padding-top: 40px;">
            <div class="col-xs-12">
                <div class="maindodycnt">
                    <h3 class="block-head-News">TWSH OMRData Report</h3>
                    <div class="line-border"></div>
                    <html:form action="/omrDataReport" >
                        <html:hidden property="mode"/>
                        <html:hidden property="dob"/>
                        <html:hidden property="bname"/>
                        <html:hidden property="gender"/>
                        <html:hidden property="caste"/>
                        <html:hidden property="fname"/>
                        <html:hidden property="grade"/>
                        <html:hidden property="hallticket"/>
                        <html:hidden property="aadhar"/>
                        <logic:present name="result1">
                            <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                            </logic:present>
                            <logic:present name="result">
                            <center> <font color="red" style="font-weight: bold">${result}</font></center>
                            </logic:present>
                            <logic:present  name="listData">
                            <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                <thead>
                                     <tr>
                                            <th>SNO</th>
                                            
                                            <th>Center Id</th>
                                            <th>Center Name</th>
                                            <th>Inst Code</th>
                                            <th>Registration No</th>
                                            <th>Name</th>
                                            
                                            <th>Father Name</th>
                                            <th>Date Of Birth</th>

                                            <th>Gender</th>
                                            <th>Grade Code</th>
                                            <th>Day Name</th>
                                            <th>Batch Name</th>
                                            <th>Examination District</th>
                                        </tr>
                                </thead>
                                <tbody>
                                    <logic:iterate name="listData" id="list">
                                          <tr>
                                                <td style="text-align: center;"><%=++i%></td>
                                            
                                                <td style="text-align: center;">${list.centerCode}</td>
                                                <td style="text-align: left;">${list.centerName}</td>     
                                                <td style="text-align: center;">${list.instCode}</td>     
                                                <td style="text-align: center;">${list.regNo}</td>
                                                <td style="text-align: center;">${list.name}</td>
                                                
                                                <td style="text-align: center;">${list.fname}</td>
                                                <td style="text-align: center;">${list.dob}</td>
                                                
                                                <td style="text-align: left;">${list.gender}</td>   
                                                <td style="text-align: center;">${list.gradeCode}</td>
                                                <td style="text-align: center;">${list.dayName}</td>
                                                <td style="text-align: center;">${list.batchName}</td>
                                                <td style="text-align: center;">${list.address}</td>
                                            </tr>
                                    </logic:iterate>
                                        </tbody>
                            </table>
                        </logic:present>
                    </html:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>                                       