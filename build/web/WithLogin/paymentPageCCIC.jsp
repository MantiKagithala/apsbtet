
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>
        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <Style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit
            {
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }
            ul {
                list-style-type: none;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
            });
            function paymentInsert(id) {
                if (id != null) {
                    document.forms[0].mode.value = "payment";
                    document.forms[0].submit();
                }
            }
            function paymentStatus() {
                $("#paydiv").show();
                $("#sampleDiv").hide();
            }
            function backTOSevice() {
                document.forms[0].mode.value = "unspecified";
                document.forms[0].submit();
            }
            function showDiv() {
                if('${status}'=="1"){
                $("#paydiv").hide();
                $("#sampleDiv").show();
                }else{
                      $("#paydiv").show();
                      $("#sampleDiv").hide();
                }
            }
        </script>
    <body onload="showDiv()">
        <div class="page-title title-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <br/>
                        <br/>
                        <h3 class="block-head-News">CCIC Payment Details </h3>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="feedback-form">
        <div class="container">
            <div class="row">
                <div class="line-border"></div>
                <br>
                <div class="row">
                    <div class=" col-md-12">
                        <!--<div class="login-form">-->
                        <div>
                            <html:form action="/registerCCIC"  method="post" enctype="multipart/form-data">
                                <html:hidden property="mode"/>
                                <html:hidden property="course" value="${course}"/>
                                <logic:present name="errMsg">
                                    <p style="text-align: center;color:green;font-weight: bold" > ${errMsg}</p>
                                    <br/>
                                </logic:present>
                            
                                    <logic:present name="result1">
                                            <div id="sampleDiv">
                                        <center> <font color="green" style="font-weight: bold">${result1}</font>
                                            <br/>
                                           <a href="#" onclick="paymentStatus()()"><input type="button" value="PAY" class="btn btn-primary"/></a>&nbsp;&nbsp;&nbsp;
                                            <a href="#" onclick="backTOSevice()"><input type="button" value="HOME" class="btn btn-primary"/></a>
                                        <br/><br/><br/><br/>
                                        <br/><br/><br/><br/>
                                        <br/><br/><br/><br/>
                                        <br/><br/><br/><br/>
                                         </div>
                                    </logic:present>
                               
                                <br/>
                                <!--                                    <h5 class="block-head-News">Personal Details (As per SSC Certificate)</h5>
                                                                   <div class="line-border"></div>-->
                                <div id="paydiv">
                                    <div class="row">

                                          
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Aadhar No  </label>
                                                <div class="col-sm-8">
                                                    <html:text property="fname" styleId="fname" value="  ${aadhaar}" styleClass="form-control" readonly="true"/>
                                                    <html:hidden styleClass="form-control" property="aadhar" styleId="aadhar"  value="${aadhar}"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label"> Name  of the Candidate</label>
                                                <div class="col-sm-8">
                                                    <html:text property="bname" styleId="bname" value="${bname}" readonly="true" styleClass="form-control"/>
                                                    <%--<html:text  styleClass="form-control" property="fname" styleId="fname" maxlength="150" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');"/>--%>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Mobile Number </label>
                                                <div class="col-sm-8">
                                                    <html:text property="mobile" styleId="mobile" value=" ${mobile}"  readonly="true" styleClass="form-control"/>
                                                    <%--<html:text property="dob" styleId="dob" styleClass="dob" readonly="true" />--%>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">

                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Amount<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text property="ecenter" styleId="ecenter" value="  ${amount}"  styleClass="form-control" readonly="true"/>
                                                    <html:hidden property="amount" styleId="amount" value='${amount}' styleClass="form-control"/> 
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">       
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Payment Mode<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <ul>
                                                        <li> <input type="radio" name="paymentmode" id="paymentmode" value="DC" checked="checked"> Debit Card </li>
                                                        <li> <input type="radio" name="paymentmode" id="paymentmode" value="CC" > Credit Card </li>
                                                        <li> <input type="radio" name="paymentmode" id="paymentmode" value="NBS" >Net Banking (SBI) </li>
                                                        <li> <input type="radio" name="paymentmode" id="paymentmode" value="NBI"> Net Banking (ICICI) </li>
                                                        <li> <input type="radio" name="paymentmode" id="paymentmode" value="NBH"> Net Banking (HDFC) </li>
                                                        <li> <input type="radio" name="paymentmode" id="paymentmode" value="EBS"> Net Banking (Others)</li>
                                                        <li> <input type="radio" name="paymentmode" id="paymentmode" value="PTM"> Net Banking (Paytm)</li>
                                                    </ul>

                                                    <!--<textarea rows="3" class="form-control"></textarea>-->

                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    <br/>

                                    <div class="row">
                                        <div class="col-md-12 center">
                                            <!--<center><a class="submit" href="#">Pay</a></center>-->
                                            <center> <input type="submit" value="Pay" class="submit" style="max-width: 96px;font-size:24px;" onclick="paymentInsert('${aadhar}');"/></center>
                                            <br/>
                                            <br/>
                                        </div>
                                    </div>
                                </div>
                            </html:form>
                        </div>
                    </div> 
                </div>
            </div>
        </div>	
    </div>
</body>
</html>
