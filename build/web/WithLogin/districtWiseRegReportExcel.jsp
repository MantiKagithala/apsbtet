<%-- 
    Document   : Teachersprint
    Created on : Jun 5, 2018, 7:39:44 PM
    Author     : 1042564
--%>
<%@ page contentType="application/vnd.ms-excel"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<!DOCTYPE html>
<html>

    <head>
        <%
            int i = 0;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <link rel="stylesheet" type="text/css" href="/css/site-examples.css">
        <!--<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">-->

        <script type="text/javascript" language="javascript" src="js/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/dataTables.fixedColumns.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/demo.js">
        </script>
        <script type="text/javascript" class="init">

            $(document).ready(function() {
                var table = $('#example').DataTable({
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: true,
                    fixedColumns: true
                });
            });
        </script>
        <style type="text/css">
            th, td { white-space: nowrap; }
            div.dataTables_wrapper {
                margin: 0 auto;
            }
            /* 
               div.container {
                   width: 60%;
               }*/
            table.dataTable
            {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.dataTable thead th {
                padding: 10px 18px;
                border: 1px solid #115400 !important;
                color: #fff;
                background-color: #1d7806 !important;
            }
            table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #115400 !important;
            }
            .h3_head{
                background: #337ab7;
                color: #fff;
                font-size: 16px;
                padding: 10px !important;
                text-align: center;
                box-shadow: 5px 5px 5px #ccc;
            }
        </style>
        <style type="text/css">
            #example_wrapper
            {
                width: 70% !important;
            }
        </style>

    </head>


</head>
<body>
    <div class="row mainbodyrow">
        <div class="container" style="padding-top: 40px;">
            <div class="col-xs-12">
                <div class="maindodycnt">
                    <h3 class="block-head-News">TWSH District Wise Abstract</h3>
                    <div class="line-border"></div>
                    <html:form action="/consolidatedReport" >
                        <html:hidden property="mode"/>
                        <html:hidden property="dob"/>
                        <html:hidden property="bname"/>
                        <html:hidden property="gender"/>
                        <html:hidden property="caste"/>
                        <html:hidden property="fname"/>
                        <html:hidden property="grade"/>
                        <html:hidden property="hallticket"/>
                        <html:hidden property="aadhar"/>
                        <logic:present name="result1">
                            <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                            </logic:present>
                            <logic:present name="result">
                            <center> <font color="red" style="font-weight: bold">${result}</font></center>
                            </logic:present>

                        <logic:present  name="instData">
                            <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">


                                <thead>
                                    <tr>
                                        <th rowspan="3">S.NO</th>
                                        <th rowspan="3">District Name</th>
                                        <th colspan="4">Candidates Registered Through Institutes</th>
                                        <th colspan="3" rowspan="2">Private Candidates</th>
                                        <th rowspan="3">Total</th>
                                        <th rowspan="3">Total Paid Applicants</th>
                                        <th rowspan="3">Total UnPaid Applicants</th>
                                    </tr>
                                    <tr>
                                        <th rowspan="2">Total Institutes</th>
                                        <th colspan="3">Total Applicants</th>
                                    </tr>
                                    <tr>
                                        <th>Repeater</th>
                                        <th>Regular</th>
                                        <th>Total</th>
                                        <th>Repeater</th>
                                        <th>Regular</th>
                                        <th>Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <logic:iterate name="instData" id="list">
                                        <tr>
                                            <td style="text-align: center;"><%=++i%></td>
                                            <td style="text-align: left;">${list.districtName}</td>
                                            <td style="text-align: center;">${list.instCount}</td>  
                                            <td style="text-align: center;">${list.repAppCnt}</td>
                                            <td style="text-align: center;">${list.regAppCnt}</td>
                                            <td style="text-align: center;">${list.totalAppCnt}</td>
                                            <td style="text-align: center;">${list.pvtRepCnt}</td>
                                            <td style="text-align: center;">${list.pvtRegCnt}</td>
                                            <td style="text-align: center;">${list.pvttotal}</td>
                                            <td style="text-align: center;">${list.totalCnt}</td>
                                            <td style="text-align: center;">${list.completedPay}</td>
                                            <td style="text-align: center;">${list.pendingPay}</td>
                                        </tr>
                                    </logic:iterate>
                                </tbody>
                                <tr>
                                    <td colspan="2" style="text-align: center;">Total</td>
                                    <td style="text-align: center;">${list.totalInstCount}</td>
                                    <td style="text-align: center;">${list.totalrepAppCnt}</td>
                                    <td style="text-align: center;">${list.totalregAppCnt}</td>
                                    <td style="text-align: center;">${list.totaltotalAppCnt}</td>
                                    <td style="text-align: center;">${list.totalpvtRepCnt}</td>
                                    <td style="text-align: center;">${list.totalpvtRegCnt}</td>
                                    <td style="text-align: center;">${list.totalpvttotal}</td>
                                    <td style="text-align: center;">${list.totaltotalCnt}</td>
                                    <td style="text-align: center;">${list.totalcompletedPay}</td>
                                    <td style="text-align: center;">${list.totalpendingPay}</td>
                                </tr>
                            </table>                        </logic:present>
                    </html:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>                                       