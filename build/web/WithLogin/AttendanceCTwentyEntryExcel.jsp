<%-- 
    Document   : MalpracticeExcel
    Created on : Jul 29, 2021, 11:51:35 PM
    Author     : 1820530
--%>

<%@ page contentType="application/vnd.ms-excel"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>
        <%
            int i = 0;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <link rel="stylesheet" type="text/css" href="/css/site-examples.css">
        <!--<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">-->

        <script type="text/javascript" language="javascript" src="js/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/dataTables.fixedColumns.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/demo.js">
        </script>
        <script type="text/javascript" class="init">

            $(document).ready(function() {
                var table = $('#example').DataTable({
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: true,
                    fixedColumns: true
                });
            });
        </script>
        <style type="text/css">
            th, td { white-space: nowrap; }
            div.dataTables_wrapper {
                margin: 0 auto;
            }
            /* 
               div.container {
                   width: 60%;
               }*/
            table.dataTable
            {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.dataTable thead th {
                padding: 10px 18px;
                border: 1px solid #115400 !important;
                color: #fff;
                background-color: #1d7806 !important;
            }
            table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #115400 !important;
            }
            .h3_head{
                background: #337ab7;
                color: #fff;
                font-size: 16px;
                padding: 10px !important;
                text-align: center;
                box-shadow: 5px 5px 5px #ccc;
            }
        </style>
        <style type="text/css">
            #example_wrapper
            {
                width: 70% !important;
            }
        </style>

    </head>
    <body>


        <h3 class="block-head-News">Attendance Report</h3>

        <logic:present  name="listData">
            <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>S.NO</th>
                        <th>PIN</th>
                        <th>Student Name</th>
                        <th>Academic Year</th>

                        <th>Branch</th>
                        <th>Month</th>
                        <th>Period</th>


                        <th>Scheme Code</th>
                        <th>Section</th>
                        <th>Shift</th>
                        <th>Working Days</th>
                        <th>No.Of Days Attended</th>
                    </tr>
                </thead>
                <tbody>
                    <logic:present  name="listData">
                        <logic:iterate name="listData" id="list">
                            <tr>
                                <td style="text-align: center;"><%=++i%></td>
                                <td style="text-align: left;">${list.pin}</td>
                                <td style="text-align: left;">${list.name}</td>     
                                <td style="text-align: left;">${list.sem}</td>
                                <td style="text-align: left;">${list.br}</td>
                                <td style="text-align: left;">${list.attMonth}</td>     
                                <td style="text-align: left;">${list.attPeriod}</td>
                                <td style="text-align: left;">${list.scheme}</td>
                                <td style="text-align: center;">${list.sec}</td>
                                <td style="text-align: center;">${list.sft}</td>
                                <td style="text-align: center;">${list.workingDays} </td>
                                <td style="text-align: center;">${list.attended}</td>
                            </tr>
                        </logic:iterate>
                    </logic:present>

                </tbody>

            </table>
        </logic:present>   
    </body>
</html>

