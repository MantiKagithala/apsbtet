<%-- 
    Document   : CourseStatusReport
    Created on : Jul 23, 2021, 1:51:42 PM
    Author     : APTOL301294
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<!DOCTYPE html>
<html>
    <head>
        <%int i = 0;%>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  
        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[10, 50, 100, -1], [10, 50, 100, "All"]]
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>
        <script type="text/javascript">
            function getDistInstlist(aadhar) {
                document.forms[0].aadhar.value = aadhar;
                document.forms[0].mode.value = "getDistrictData";
                document.forms[0].submit();
            }
            function getDistInstlistExcel(distCode) {
                document.forms[0].aadhar.value = distCode;
                document.forms[0].mode.value = "getDistrictDataExcel";
                document.forms[0].submit();
            }
            function goBack() {
                document.forms[0].mode.value = "unspecified";
                document.forms[0].submit();
            }
            function validFields() {
                if ($('#grade').val() == "0") {
                    alert("Select Grade");
                    $('#grade').focus();
                    return false;
                } else if ($('#batchCode').val() == "0") {
                    alert("Select Batch");
                    $('#batchCode').focus();
                    return false;
                } 
                else if(($("#blind").val()=="1") &&($('#ecenter').val() == "0")) {
                    alert("Select Examination Center");
                    $('#ecenter').focus();
                    return false;
                } else {
                    document.forms[0].mode.value = "getData";
                    document.forms[0].submit();
                }

            }
            function hideData() {
                $('.DataDiv').hide();
            }
        </script>
    </head>
    <body>
        <div class="row mainbodyrow">
            <div class="container" style="padding-top: 15px;">
                <div class="col-xs-12">
                    <div class="maindodycnt">
                        <h3 class="block-head-News">Typewriting NR Report</h3>
                        <div class="line-border"></div>
                        <html:form action="/nrReport" >
                            <html:hidden property="mode"/>
                            <html:hidden property="dob"/>
                            <html:hidden property="bname"/>
                            <html:hidden property="gender"/>
                            <html:hidden property="caste"/>
                            <html:hidden property="fname"/>
                            <html:hidden property="hallticket"/>
                            <html:hidden property="aadhar"/>
                            <html:hidden property="blind" styleId="blind"/>
                            <html:hidden property="ecenter1" styleId="ecenter1"/>
                            <logic:present name="result1">
                                <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                                </logic:present>
                                <logic:present name="result">
                                <center> <font color="red" style="font-weight: bold">${result}</font></center>
                            </logic:present><br>

                            <div class="row">
                                <div class="col-md-6">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Grade<font color="red">*</font> </label>
                                        <div class="col-sm-8">
                                            <html:select property="grade" styleId="grade" styleClass="form-control">
                                                <html:option value="0">--Select Grade--</html:option> 
                                                <html:optionsCollection property="gradeslist" label="gradename" value="gradecode"/>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Batch <font color="red">*</font></label>
                                        <div class="col-sm-6">
                                            <html:select property="batchCode" styleId="batchCode" styleClass="form-control" onchange="hideData()">
                                                <html:option value="0">--Select--</html:option> 
                                                <html:option value="B1">Batch-I</html:option> 
                                                <html:option value="B2">Batch-II</html:option> 
                                                <html:option value="B3">Batch-III</html:option> 
                                                <html:option value="B4">Batch-IV</html:option> 
                                                <%--<html:optionsCollection property="gradeslist" label="batchname" value="batchcode1"/>--%>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <%if(request.getAttribute("statusflag").toString().equalsIgnoreCase("1")){%>
                                <div class="col-md-6">

                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Examination Center <font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:select property="ecenter" styleId="ecenter" styleClass="form-control">
                                                <html:option value="0">--Select Center--</html:option> 
                                                <html:optionsCollection property="distLists" label="cname" value="ccode"/>
                                            </html:select>
                                        </div>
                                    </div>
                                </div>
                                        <%}%>
                                
                                
                                        <center> <input type="button" onclick="validFields();" value="SUBMIT" class="btn btn-primary"/></center>
                                            
                            </div>
                            <div class="DataDiv">
                                <logic:present  name="listData">
                                    <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                        <a href="./nrReport.xls?mode=downloadExcel&bcode=<%=request.getAttribute("bcode")%>&gcode=<%=request.getAttribute("gcode")%>&ccode=<%=request.getAttribute("ccode")%>"><img src="img/excel.png" style="width: 35px"/></a>
                                        <a href="./nrReport.do?mode=getPdfData&bcode=<%=request.getAttribute("bcode")%>&gcode=<%=request.getAttribute("gcode")%>&ccode=<%=request.getAttribute("ccode")%>"><img src="img/pdf.png" style="width: 30px"/></a>
                                    </div>
                                    <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="80%">
                                        <thead>
                                            <tr>
                                                <th>S.NO</th>
                                                <th>Reg.No/Hall ticket No</th>
                                                <th>Name of the Candidate</th>
                                                <th>Father Name</th>
                                                <th>Date of Birth</th>
                                                <th>Institute Code/Private</th>

                                                <th>Institute Name</th>
                                                <th>Institute Address</th>
                                                <th>Center Code</th>
                                                <th>Center Name</th>
                                                <th>Grade</th>
                                                <th>Batch</th>
                                                <th>Phone.No</th>
                                                <th>Photo</th>
                                                <th>Candidate Signature</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <logic:iterate name="listData" id="list">
                                                <tr>
                                                    <td style="text-align: center;">${list.sno}</td>
                                                    <td style="text-align: left;">${list.instName}</td>
                                                    <td style="text-align: center;">${list.address}</td>     
                                                    <td style="text-align: center;">${list.totalAppli}</td>
                                                    <td style="text-align: center;">${list.totalPaid}</td>
                                                    <td style="text-align: center;">${list.instcode}</td>
                                                    <td style="text-align: center;">${list.totalUnPaid}</td>

                                                    <td style="text-align: left;">${list.add}</td>
                                                    <td style="text-align: left;">${list.centerC}</td>
                                                    <td style="text-align: center;">${list.centerN}</td>     
                                                    <td style="text-align: center;">${list.grade}</td>
                                                    <td style="text-align: center;">${list.batch}</td>
                                                    <td style="text-align: center;">${list.mobile}</td>
                                                    <td><img width="80" height="80" style="float:right;margin-right: 20px;" alt="NO FILE" src="data:image/jpg;base64,${list.photo}" /><br><img width="80" height="80" style="float:right;margin-right: 20px;height:40px;" alt="NO FILE" src="data:image/jpg;base64,${list.sig}" /></td>
                                                    <td style="text-align: center;"></td>
                                                </tr>
                                            </logic:iterate>
                                        </tbody>
                                    </table>
                                </logic:present>
                            </div>

                        </html:form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>                                       