<%-- 
    Document   : practicalMarksEntryIS
    Created on : Aug 13, 2021, 12:38:08 PM
    Author     : 1582792
--%>

<%@page contentType="text/html" pageEncoding="UTF-8" isELIgnored="false" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %> 
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<!DOCTYPE html>
<html>

    <head>
        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>
        <style type="text/css">
           
            @media print {
                body {
                    -webkit-print-color-adjust: exact;
                   
                }
            }

            .vendorListHeading th {
                background-color: #1a4567 !important;
                color: white !important;   
            }

            td {
                text-align: center;
            }
        </style>
      

    <script>
    

function printData(){
   
   setTimeout(function () { window.close(); }, 1000);
    window.print();
   
}
    </script>
</head>
<body padding ="0 !important" onload="printData()" >
  
                    <h3 class="h3_background"><b><center>CCIC Practical Marks Report</center></b></h3>
                    <span></span>

                    <html:form action="/practicalsCCICReport" >
                        <html:hidden property="mode"/>
                        
                      
                        <div class="hideDataDiv">
                            <logic:present name="result1">
                                <br><br>
                                <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                                <br>
                            </logic:present>
                            <logic:present name="result">
                                <br><br>
                                <center> <font color="red" style="font-weight: bold">${result}</font></center>
                                <br>
                            </logic:present>
                            <br>
                            <logic:present name="doculist">
                                <table border="1" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th rowspan="3">S.no</th>
                                            <th rowspan="3">Application No</th>
                                            <th rowspan="3">Name of Applicant</th>
                                            <th colspan="2">Practicals </th>
                                            <th colspan="2">Sessionals </th>
                                        </tr>
                                        <tr>
                                            <th colspan="2" >${epaper}</th>

                                            <th colspan="2">${epaper}</th>

                                        </tr>
                                        <tr>
                                            <th>Total<br> Marks</th>
                                            <th>Obtained<br> Marks</th>
                                            <th>Total<br> Marks</th>
                                            <th>Obtained<br> Marks</th>
                                        </tr>

                                    </thead>
                                    <tbody> 
                                        <%int l = 0;%>
                                        <c:forEach var="list" items="${doculist}" >
                                            <tr>
                                                
                                                <td>
                                                  <%=i++%>
                                                    <!--<input type="checkbox" class="checkbox" style=" margin: auto;" name="checkid" id="checkid<%=l%>" value="<%=l%>">-->
                                                </td>
                                                <td>
                                                    ${list.applicantid}
                                                    <input type="hidden" name="applicationno<%=l%>" id="applicationno<%=l%>"  value="${list.applicantid}">
                                                <td class="cell expand-maximum-on-hover">${list.applicantname}</td>
                                                <td>${list.maxMarks}</td>
                                        <input type="hidden" name="maxMarks<%=l%>" id="maxMarks<%=l%>"  value="${list.maxMarks}">
                                        <td>
                                            ${list.practicalScored}
                                            <!--<input type="text" name="obtainedMarks<%=l%>" id="obtainedMarks<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('obtainedMarks<%=l%>', 'maxMarks<%=l%>');"/>-->
                                        </td>
                                        <td>${list.marks}</td>
                                        <!--<input type="hidden" name="sesmaxMarks<%=l%>" id="sesmaxMarks<%=l%>"  value="${list.marks}">-->
                                        <td>
                                            ${list.sessionalScored}
                                            <!--<input type="text" name="sesobtainedMarks<%=l%>" id="sesobtainedMarks<%=l%>" maxlength="3" onkeypress="return inputLimiter1(event, 'Numbers');" onkeydown="return space(event, this);" onchange="return upTOTen('sesobtainedMarks<%=l%>', 'sesmaxMarks<%=l%>');"/>-->
                                        </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>

                                <!--<center> <button onclick="return submitData();" class="btn btn-primary">Submit</button></center>-->
                                </logic:present>
                           
                        </div>

                    </html:form>
               
</body>

</html>     