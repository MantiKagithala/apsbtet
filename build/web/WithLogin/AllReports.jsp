<%-- 
    Document   : PaymentReconsilationReport
    Created on : Jul 12, 2021, 7:09:24 PM
    Author     : 1820530
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.DateFormat"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<!DOCTYPE html>
<html>
    <head>

        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <link rel="stylesheet" href="css/styles1.css" type="text/css"/>
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  


        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="assets/css/acc.css">	
      
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }
            .tab label
            {
                width: 100% !important;
            }
            .maindodycnt
            {
                overflow: hidden;
            }

        </style>

    </head>
    <body>
        <html:form action="/reportsAll">
            <html:hidden property="mode"/>  
            <div class="row mainbodyrow">
                <div class="container" style="padding-top: 15px;">
                    <div class="col-xs-12">
                        <div class="maindodycnt">
                            <h3 class="block-head-News">Module Wise Reports

                            </h3>
                            <div class="line-border"></div><br>

                            
                            
                            <div class="half">
                                
                                <div class="tab">
                                    <input id="tab-two" type="checkbox" name="tabs">
                                    <label for="tab-two">TWSH</label>
                                    <div class="tab-content">
                                        <ul class="list_type">
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./dashboardReport.do">No. of Candidates Submitted</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./consolidatedReport.do">Consolidated Data</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./verificationReport.do">Certificate Verification Status</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./districtWiseRegReport.do">District Wise Abstract</a></li>
                                            <!--<li><i class="fa fa-link"></i><a target="_blank" href="./paymentReconsilation.do">Payment Reconciliation</a></li>-->
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./nrSHReport.do">NR Short Hand</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./nrReport.do">NR Type Writing</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./presentData.do">Present</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./absentData.do">Absentees</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./malpracticeData.do">MalPractice</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./bufferOMRData.do">Buffer OMRs(SGs)</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./examCentreWiseTW.do">TW Exam Center Wise</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./examCentreWiseSH.do">SH Exam Center Wise</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./instituteWiseTW.do">TW Institution Wise</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./instituteWiseSH.do">SH Institution Wise</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./barcodeReport.do">Barcode Scaning</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./barcodeCenterReport.do">Center Wise Barcode Scaning</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./bundleReport.do">Status of Booklets Bundling</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab">
                                    <input id="tab-one" type="checkbox" name="tabs">
                                    <label for="tab-one">DIPLOMA</label>
                                    <div class="tab-content">
                                        <ul class="list_type">
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./diplomadistrictWiseReport.do">District Wise Applied Candidates</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./nrDiploma.do">NR Report</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./absentDiploma.do">Absentees</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./malpraticeDiploma.do">MalPractice</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./bufferDiploma.do">Buffer OMRs(SGs)</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./examDateWiseReport.do">Date Wise Exam Report</a></li>
                                        </ul>
                                    </div>
                                </div>
                  <%--              <div class="tab">
                                    <input id="tab-three" type="checkbox" name="tabs">
                                    <label for="tab-three">Proforma III</label>
                                    <div class="tab-content">
                                        <ul class="list_type">
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                        </ul>
                                    </div>
                                </div>--%>
                            </div>

                            <div class="half">
                                <div class="tab">
                                    <input id="tab-four" type="checkbox" name="tabs">
                                    <label for="tab-four">CCIC</label>
                                    <div class="tab-content">
                                        <ul class="list_type">
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./dashboardReportCCIC.do">Number Of Candidates Submitted</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./abstractReport.do">Course Wise Abstract</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./verificationReportCCIC.do">Certificate Verification Status</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./ccicNRData.do">NR Data</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./ccicPresentData.do">Present</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./ccicAbsentData.do">Absentees</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./ccicMalpracticeData.do">MalPractice</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./ccicBufferOMRData.do">Buffer OMRs(SGs)</a></li>
                                            <li><i class="fa fa-link"></i><a target="_blank" href="./bundleReportCCIC.do">Booklets Bundling Status</a></li>
                                        </ul>
                                    </div>
                                </div>
                        <%--        <div class="tab">
                                    <input id="tab-five" type="checkbox" name="tabs">
                                    <label for="tab-five">Proforma V</label>
                                    <div class="tab-content">
                                        <ul class="list_type">
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="tab">
                                    <input id="tab-six" type="checkbox" name="tabs">
                                    <label for="tab-six">Proforma VI</label>
                                    <div class="tab-content">
                                        <ul class="list_type">
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet, consectetur adipisicing elit</a></li>
                                            <li><i class="fa fa-link"></i><a href="#">Lorem ipsum dolor sit amet</a></li>
                                        </ul>
                                    </div>
                                </div> --%>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


        </html:form>
    </body>

</html>
