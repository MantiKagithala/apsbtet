<%-- 
    Document   : Teachersprint
    Created on : Jun 5, 2018, 7:39:44 PM
    Author     : 1042564
--%>
<%@ page contentType="application/vnd.ms-excel"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>

<!DOCTYPE html>
<html>

    <head>
        <%
            int i = 0;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <link rel="stylesheet" type="text/css" href="/css/site-examples.css">
        <!--<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.min.css">-->

        <script type="text/javascript" language="javascript" src="js/jquery-1.12.4.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/jquery.dataTables.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/dataTables.fixedColumns.min.js">
        </script>
        <script type="text/javascript" language="javascript" src="js/demo.js">
        </script>
        <script type="text/javascript" class="init">

            $(document).ready(function() {
                var table = $('#example').DataTable({
                    scrollY: "300px",
                    scrollX: true,
                    scrollCollapse: true,
                    paging: true,
                    fixedColumns: true
                });
            });
        </script>
        <style type="text/css">
            th, td { white-space: nowrap; }
            div.dataTables_wrapper {
                margin: 0 auto;
            }
            /* 
               div.container {
                   width: 60%;
               }*/
            table.dataTable
            {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.dataTable thead th {
                padding: 10px 18px;
                border: 1px solid #115400 !important;
                color: #fff;
                background-color: #1d7806 !important;
            }
            table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #115400 !important;
            }
            .h3_head{
                background: #337ab7;
                color: #fff;
                font-size: 16px;
                padding: 10px !important;
                text-align: center;
                box-shadow: 5px 5px 5px #ccc;
            }
        </style>
        <style type="text/css">
            #example_wrapper
            {
                width: 70% !important;
            }
        </style>

    </head>


</head>
<body>
    <div class="row mainbodyrow">
        <div class="container" style="padding-top: 40px;">
            <div class="col-xs-12">
                <div class="maindodycnt">
                    <h3 class="block-head-News">Typewriting NR Report</h3>
                    <div class="line-border"></div>
                    <html:form action="/nrReport">
                        <html:hidden property="mode"/>
                        <html:hidden property="dob"/>
                        <html:hidden property="bname"/>
                        <html:hidden property="gender"/>
                        <html:hidden property="caste"/>
                        <html:hidden property="fname"/>
                        <html:hidden property="grade"/>
                        <html:hidden property="hallticket"/>
                        <html:hidden property="aadhar"/>
                        <logic:present name="result1">
                            <center> <font color="green" style="font-weight: bold">${result1}</font></center>
                            </logic:present>
                            <logic:present name="result">
                            <center> <font color="red" style="font-weight: bold">${result}</font></center>
                            </logic:present>
                            <logic:present  name="listData">
                            <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="80%">
                                <thead>
                                    <tr>
                                        <th>S.NO</th>
                                        <th>Reg.No/Hall ticket No</th>
                                        <th>Name of the Candidate</th>
                                        <th>Father Name</th>
                                        <th>Date of Birth</th>
                                        <th>Institute Code/Private</th>
                                        
                                        <th>Institute Name</th>
                                        <th>Institute Address</th>
                                        <th>Center Code</th>
                                        <th>Center Name</th>
                                        <th>Grade</th>
                                        <th>Batch</th>
                                        <th>Phone.No</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <logic:iterate name="listData" id="list">
                                        <tr>
                                            <td style="text-align: center;">${list.sno}</td>
                                            <td style="text-align: left;">${list.instName}</td>
                                            <td style="text-align: center;">${list.address}</td>     
                                            <td style="text-align: center;">${list.totalAppli}</td>
                                            <td style="text-align: center;">${list.totalPaid}</td>
                                            <td style="text-align: center;">${list.instcode}</td>
                                            <td style="text-align: center;">${list.totalUnPaid}</td>
                                            
                                            <td style="text-align: left;">${list.add}</td>
                                            <td style="text-align: left;">${list.centerC}</td>
                                            <td style="text-align: center;">${list.centerN}</td>     
                                            <td style="text-align: center;">${list.grade}</td>
                                            <td style="text-align: center;">${list.batch}</td>
                                            <td style="text-align: center;">${list.mobile}</td>
                                        </tr>
                                    </logic:iterate>
                                        </tbody>
                            </table>
                        </logic:present>
                    </html:form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>                                       