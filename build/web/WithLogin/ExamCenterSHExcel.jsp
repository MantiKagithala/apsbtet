<%-- 
    Document   : ExamCenterSHExcel
    Created on : Jul 28, 2021, 4:28:57 PM
    Author     : 1820530
--%>

<%@page contentType="application/vnd.ms-excel" %>
<%@taglib uri="/WEB-INF/struts-bean.tld" prefix="bean" %>
<%@taglib uri="/WEB-INF/struts-html.tld" prefix="html" %>
<%@taglib uri="/WEB-INF/struts-logic.tld" prefix="logic" %>
<!DOCTYPE html>
<html>
    <head>
        <%
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>

        <title>Exam Center TW Report</title>

        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[13, -1], [13, "All"]]
                });
                $('#example1').DataTable({
                    "scrollX": true,
                    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]]
                });
            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
            tfoot {
                background-color: #6699ff!important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }
        </style>
        <style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }

        </style>
    </head>
    <body>

        <h3 class="block-head-News">Exam Center Wise Data SHORTHAND</h3>



        <%int count = 1;%>
        <logic:present name="nodata">
        <center><font color="red">${nodata}</font></center>
        </logic:present>

    <logic:present name="ExamCenterShortHandList">

        <br>
        <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                    <thead>
                                        <tr>
                                            <th>S.NO</th>
                                            <th>Exam Center Code</th>
                                            <th>Exam center Name</th>
                                            <th>SEL</th>
                                            <th>SEI</th>
                                            <th>S.E.H</th>
                                            <th>S.E.H.S 150 WPM</th>
                                            <th>S.E.H.S 180 WPM</th>
                                            <th>S.E.H.S 200 WPM</th>
                                            <th>STL</th>
                                            <th>S.T.H.</th>
                                            <th>S.T.H.S 80 WPM</th>
                                            <th>S.T.H.S 100 WPM</th>

                                        </tr>
                                        <bean:define id="total1" value="0"/>
                                        <bean:define id="total2" value="0"/>
                                        <bean:define id="total3" value="0"/>
                                        <bean:define id="total4" value="0"/>
                                        <bean:define id="total5" value="0"/>
                                        <bean:define id="total6" value="0"/>
                                        <bean:define id="total7" value="0"/>
                                        <bean:define id="total8" value="0"/>
                                        <bean:define id="total9" value="0"/>
                                        <bean:define id="total10" value="0"/>
                                    </thead>
                                    <tbody>
                                        <logic:iterate name="ExamCenterShortHandList" id="list">
                                            <tr>
                                                <td style="text-align: center;"><%=count++%></td>
                                                <td style="text-align: left;">${list.centerCode}</td>
                                                <td style="text-align: left;">${list.centerName}</td>     
                                                <td style="text-align: right;">${list.sel}</td>
                                                <td style="text-align: right;">${list.sei}</td>
                                                <td style="text-align: right;">${list.seh}</td>
                                                <td style="text-align: right;">${list.sehs150}</td>
                                                <td style="text-align: right;">${list.sehs180}</td>
                                                <td style="text-align: right;">${list.sehs200}</td>
                                                <td style="text-align: right;">${list.stl}</td>
                                                <td style="text-align: right;">${list.sth}</td>
                                                <td style="text-align: right;">${list.sths80}</td>
                                                <td style="text-align: right;">${list.sths100}</td>
                                                
                                            </tr>
                                            
                                            <bean:define id="total1" value="${total1+list.sel}"/>
                                            <bean:define id="total2" value="${total2+list.sei}"/> 
                                            <bean:define id="total3" value="${total3+list.seh}"/> 
                                            <bean:define id="total4" value="${total4+list.sehs150}"/>
                                            <bean:define id="total5" value="${total5+list.sehs180}"/>
                                            <bean:define id="total6" value="${total6+list.sehs200}"/> 
                                            <bean:define id="total7" value="${total7+list.stl}"/> 
                                            <bean:define id="total8" value="${total8+list.sth}"/>
                                            <bean:define id="total9" value="${total9+list.sths80}"/> 
                                            <bean:define id="total10" value="${total10+list.sths100}"/> 

                                        </logic:iterate>  
                                    </tbody>
                                    
                                   
                                        <tr  style="background-color: white;">
                                            <td style="text-align: center " colspan="3" >
                                                Total
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total1} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total2} </b>
                                            </td> 
                                            <td style="text-align: right ">
                                                <b> ${total3} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total4} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total5} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total6} </b>
                                            </td> 
                                            <td style="text-align: right ">
                                                <b> ${total7} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total8} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total9} </b>
                                            </td>
                                            <td style="text-align: right ">
                                                <b> ${total10} </b>
                                            </td>
                                        </tr>                                       
                                    
                                     

                                </table>
    </logic:present>
</body>
</html>
