<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
    <head>
        <%int i = 0;%>
        <%int checkCount = 0;%>
        <script src="./js/jquery.min.js"></script>
        <!--        <script src="./js/GenerateAadharCardValidation.js"></script>-->
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <!--        <link rel="stylesheet" href="css/style1.css">-->
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  
        <script src="js/onlineExaminationPayment.js" type="text/javascript"></script> 
        <style>
            .ui-datepicker-calendar thead{
                background-color:#2dc1c9 !important;
            }
            .feedback-form {
                padding: 22px 0px;
                min-height: 384px;
            }
        </style>

        <script type="text/javascript">
            $(document).ready(function() {
                $('input[type=text]').attr('autocomplete', 'off');
                $('input[type=text]').bind("cut copy paste", function(e) {
                    alert("Cut copy paste not allowed here");
                    e.preventDefault();
                });
                var seconds = 30;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
                $("body").on("contextmenu", function(e) {
                    return false;
                });
            });

            function SubmitForm(appstatus) {

                if ($("#aadhar1").val() == "") {
                    alert("Please Enter PIN No");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }

                if (appstatus == '0' || appstatus == '2') {
                    if ($("#mobile").val() == "") {
                        $("#mobile").val("");
                        alert("Please Enter  Mobile Number");
                        $("#mobile").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    var mobileNo = $("#mobile").val();
                    var len = mobileNo.length;
                    if (len < 10) {
                        alert("Mobile Number Should be 10 digits");
                        $("#mobile").val("");
                        $("#mobile").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    else if (($("#mobile").val().trim().charAt(0) != '9'
                            && $("#mobile").val().trim().charAt(0) != '8' &&
                            $("#mobile").val().trim().charAt(0) != '7' &&
                            $("#mobile").val().trim().charAt(0) != '6')) {
                        alert("Please Enter valid Mobile Number");
                        $("#mobile").val("");
                        $("#mobile").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    if ($("#email").val() == "") {
                        $("#email").val("");
                        alert("Please Enter Email");
                        $("#email").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    if ('${studentPhoto}' == "") {
                        if ('${ photoFile}' == "") {
                            if ($("#upload1").val() == "") {
                                $("#upload1").val("");
                                alert("Please Upload Photo");
                                $("#upload1").focus().css({'border': '1px solid red'});
                                return false;
                            }
                        }
                    } else {
                        $("#upload1New1").val("${studentPhoto}");
                    }
                    if ($("#ecenter").val() == "0") {

                        alert("Please Select Do You need to change Center");
                        $("#ecenter").focus().css({'border': '1px solid red'});
                        return false;
                    }
                    if ($("#ecenter").val() == "1") {

                        if ($("#distrcit1").val() == "0") {
                            alert("Please Select District Name");
                            $("#distrcit1").focus().css({'border': '1px solid red'});
                            return false;
                        }
                        if ($("#centerCode").val() == "0") {
                            alert("Please Select Center Name");
                            $("#centerCode").focus().css({'border': '1px solid red'});
                            return false;
                        }
                    }
                }


                var checkedBoxes = [];
                var r = false;
                var chkbox = document.getElementsByName('checkid');

                if (chkbox.length > 0) {
                    for (var i = 0, n = chkbox.length; i < n; i++) {
                        if (chkbox [i].checked)
                        {
                            checkedBoxes.push(chkbox [i].value);
                            r = true;
                        }
                    }
                    if (r == false) {
                        alert("please select at least once check box");
                        return false;
                    }
                    if (r) {
//                        alert(checkedBoxes)
                        var returnval = confirm("Please Confirm to Submit the Details");
                        if (returnval == true) {
                            $("#statusresult").val(checkedBoxes);

                            document.forms[0].mode.value = "submitApplicantDetails";
                            document.forms[0].submit();
                        }
                    }
                } else {
                    alert("Your are Not Eligible to pay");
                    return false;
                }
            }

            function mobileDublicateValidation() {
                var data = "mobile=" + $("#mobile").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./onlineExaminationPayment.do?mode=getMobileData&mobile=" + $("#mobile").val().trim(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        var res = response.trim();
                        if (res == "1") {
                            alert("Already Mobile Number is Available");
                            $("#mobile").val("");
                            $("#mobile").focus()
                            return false;
                        } else {
                            return true;
                        }

                    },
                    error: function(e) {
//           // alert('Error: ' + e);
                    }
                });
            }

            function hideCenter() {
                var estatus = $("#ecenter").val();

                if (estatus == "1" || '${samecollege}' == "Yes") {
                    $(".centers").show();
                } else {

                    $(".centers").hide();
                    $("#centerCode").val("0");
                    $("#distrcit1").val("0");

                }
            }
        </script>
    <body>
        <div class="page-title title-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Diploma</h1>
                        <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">
                            <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                            <span>Online Application Form</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feedback-form" >
            <div class="container" >
                <html:form action="/onlineExaminationPayment"  method="post" enctype="multipart/form-data">
                    <html:hidden property="mode"/>
                    <html:hidden property="statusresult" styleId="statusresult"/>
                    <html:hidden property="totalmarks" styleId="totalmarks"/>
                    <html:hidden property="upload1New1" styleId="upload1New1"/>
                    <div class="row">
                        <h3 class="block-head-News">Online Application Form</h3>
                        <div class="line-border"></div>
                        <br>
                        <div class="row">
                            <div class=" col-md-12">


                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Enter PIN<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                            <html:text  styleClass="form-control-plaintext" property="aadhar1" styleId="aadhar1" maxlength="20"  onkeydown="return space(event, this);" onkeypress="return inputLimiter(event,'NameCharactersAndNumbers');" onkeyup="hideData();"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="form-group row">
                                        <div class="col-sm-8">
                                            <center>   <input type="button" onclick="return getFormDetails();" value="GO"  class="btn btn-warning"/></center> 
                                        </div>
                                    </div>
                                </div>
                            </div></div>
                        <div id="hideData">  
                            <logic:present name="result">
                                <br> <br> <br><br><br> <br> <br>
                                <span id="msg"><center><font color="red" style="font-weight: bold">${result}</font></center></span>
                                <br><br> <br><br>
                            </logic:present>
                            <logic:present name="succresult">
                                <br> <br> <br><br><br> <br> <br>
                                <span id="msg"><center><font color="green" style="font-weight: bold">${succresult}</font></center></span>
                                <br><br> <br><br>
                            </logic:present>    

                            <c:if test="${applicationDetails==1}">
                                <br/>
                                <div class="row">
                                    <div class=" col-md-12">
                                        <%if ((request.getAttribute("studentPhoto") != null && request.getAttribute("studentPhoto") != "")) {%>   
                                        <div style="float:right;">
                                            <div class="form-group row">
                                                <div class="col-sm-8" style="float:right;">
                                                    <img width="120" height="120" style="float:right;margin-right: 17px;" alt="NO FILE" src="data:image/jpg;base64,${studentPhoto}" />
                                                </div>
                                            </div>
                                        </div>
                                        <%} else if ((request.getAttribute("photoFile") != null && request.getAttribute("photoFile") != "")) {%>
                                        <div style="float:right;">
                                            <div class="form-group row">
                                                <div class="col-sm-8" style="float:right;">
                                                    <img width="120" height="120" style="float:right;margin-right: 17px;" alt="NO FILE" src="data:image/jpg;base64,${photoFile}" />
                                                </div>
                                            </div>
                                        </div>
                                        <%}%>

                                        <div class="clearfix"></div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name" class="form-label">Student Name</label>

                                                <input type="text" class="form-control-plaintext" value="${stdName}" disabled="true"> 
                                                <html:hidden property="sname" value="${stdName}"/>

                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name" class="form-label">Gender</label>
                                                <div class="">
                                                    <input type="text" value="${gender}" disabled="true">  
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-md-4">
                                            <div class="form-group ">
                                                <label for="name" class="form-label">Father Name</label>
                                                <div class="">
                                                    <input type="text" value="${fname}" disabled="true">  
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name" class="form-label">Branch</label>
                                                <div class="">
                                                    <input type="text" value="${branch}" disabled="true">  
                                                </div>
                                            </div>
                                        </div> 
                                        <div class="col-md-4">
                                            <div class="form-group">
                                                <label for="name" class="form-label">Are you differently abled(PH) </label>
                                                <div class="">
                                                    <input type="text" value="${ph}" disabled="true">  
                                                </div>
                                            </div> 
                                        </div>
                                        <c:if test="${AttendanceFlag==1}">  
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label"></label>
                                                    <div class="col-sm-8">
                                                        <!--<input type="text" value="${attandance} %" disabled="true">-->
                                                    </div>
                                                </div> 
                                            </div>
                                        </c:if>
                                        <c:if test="${AttendanceFlag==0}">  
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name" class="form-label">Attendance</label>
                                                    <div class="">
                                                        <input type="text" value="${attandance} %" disabled="true"> 
                                                    </div>
                                                </div> 
                                            </div>
                                        </c:if>


                                        <div class="clearfix"></div>
                                        <br>
                                        <!--                                            Before Registration Done-->                                         
                                        <c:if test="${appStatus==0 or appStatus==2}">           

                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name" class="form-label">Mobile No<font color="red">*</font></label>
                                                    <div class="">
                                                        <html:text  styleClass="form-control-plaintext" property="mobile"  styleId="mobile" maxlength="10" style="autocomplete:off" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return inputLimiter(this,'Numbers');" onkeydown="return space(event, this);" onchange="mobileDublicateValidation();"/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name" class="form-label">email<font color="red">*</font></label>
                                                    <div class="">
                                                        <html:text property="email" styleId="email"  styleClass="form-control-plaintext" maxlength="50"  onchange="return isEmail();" onkeydown="return space(event, this);"/>
                                                    </div>
                                                </div>
                                            </div>       
                                            <%if ((request.getAttribute("studentPhoto") == null || request.getAttribute("studentPhoto") == "")) {
                                                    if ((request.getAttribute("photoFile") == null || request.getAttribute("photoFile") == "")) {

                                            %>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name" class="form-label">Upload Photo<font color="red">*</font></label>
                                                    <div class="">
                                                        <html:file property="upload1" styleId="upload1"  styleClass="form-control" onchange="return CheckfilePdfOrOther('upload1');"/>
                                                    </div>
                                                </div>
                                            </div>   
                                            <div class="clearfix"></div>
                                            <%}
                                                }%>
                                            <div class="col-md-4" >
                                                <div class="form-group">
                                                    <label for="name" class="form-label">Do you want to change the examination/exam center due to pandemic<font color="red">*</font></label>
                                                    <div class="">
                                                        <html:select property="ecenter"  styleId="ecenter" styleClass="form-control" onchange="hideCenter();">  
                                                            <html:option value="0">---Select---</html:option>
                                                            <html:option value="1">Yes</html:option>
                                                            <html:option value="2">No</html:option>
                                                        </html:select>
                                                    </div>
                                                </div> 
                                            </div>  
                                            <div class="col-md-4 centers">
                                                <div class="form-group">
                                                    <label for="name" class="form-label">District Name<font color="red">*</font></label>
                                                    <div class="">
                                                        <html:select property="distrcit1" styleClass="form-control" styleId="distrcit1" onchange="centersList('0')">  
                                                            <%--<html:option value="0">---Select---</html:option>
                                                            <logic:present name="distLists">
                                                                <html:optionsCollection property="distLists" label="district_Name" value="district_ID"/>
                                                            </logic:present>--%>
                                                              <html:option value="0">--Select--</html:option>
                                            <html:option value="12">ANANTAPUR</html:option>
                                            <html:option value="10">CHITTOOR</html:option>
                                            <html:option value="04">EAST GODAVARI</html:option>
                                            <html:option value="07">GUNTUR</html:option>
                                            <html:option value="11">KADAPA</html:option>
                                            <html:option value="06">KRISHNA</html:option>
                                            <html:option value="13">KURNOOL</html:option>
                                            <html:option value="09">NELLORE</html:option>
                                            <html:option value="08">PRAKASAM</html:option>
                                            <html:option value="01">SRIKAKULAM</html:option>
                                            <html:option value="03">VISAKHAPATNAM</html:option>
                                            <html:option value="02">VIZIANAGARAM</html:option>
                                            <html:option value="05">WEST GODAVARI</html:option>
                                      
                                                        </html:select> 
                                                    </div>
                                                </div> 
                                            </div>     

                                            <div class="col-md-4 centers">
                                                <div class="form-group">
                                                    <label for="name" class="form-label"> Center Name<font color="red">*</font></label>
                                                    <div class="">
                                                        <html:select property="centerCode" styleClass="form-control" styleId="centerCode">  
                                                            <html:option value="0">---Select---</html:option>
                                                        </html:select> 
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                        <!--                                            Before Registration Done-->                                            
                                        <!--                                            After Payment Done-->
                                        <c:if test="${appStatus==1}"> 
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name" class="form-label">Mobile</label>
                                                    <div class="">
                                                        <input type="text" value="${mobile} " disabled="true"> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name" class="form-label">email</label>
                                                    <div class="">
                                                        <input type="text" value="${email} " disabled="true"> 
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group">
                                                    <label for="name" class="form-label">Do you want to change the examination/exam center due to pandemic</label>
                                                    <div class="">
                                                        <input type="text" value="${samecollege} " disabled="true"> 
                                                    </div>
                                                </div> 
                                            </div>
                                            <div class="col-md-4 centers">
                                                <div class="form-group">
                                                    <label for="name" class="form-label">District Name</label>
                                                    <div class="">
                                                        <input type="text" value="${district} " disabled="true"> 
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4 centers">
                                                <div class="form-group">
                                                    <label for="name" class="form-label">Center Code</label>
                                                    <div class="">
                                                        <input type="text" value="${collegecode} " disabled="true"> 
                                                    </div>
                                                </div>
                                            </div>
                                        </c:if>
                                        <!--                       After Payment Done-->
                                    </div> 
                                </div>
                                <div class="clearfix"></div>
                                <br/>
                                <div class="row">
                                    <h3 class="block-head-News">Subject Details</h3>
                                    <div class="line-border"></div>

                                    <table id="example" border="1" cellspacing="1" width="100%">
                                        <thead style="background-color: orange;border:0">
                                            <tr style="background-color: orange;border:0">
                                                <th style="text-align: center;height:15px;">S.No</th>
                                                <th style="text-align: center;">Year/Semester</th>
                                                <th style="text-align: center;">Scheme</th>
                                                <th style="text-align: center;">Subject Code</th>
                                                <th style="text-align: center;">Fee Details</th>
                                                <!-- <th style="text-align: center;">Exam Fee</th>
                                                      <th style="text-align: center;">BackLogs Fee</th>
                                                      <th style="text-align: center;">Certificate Fee</th>
                                                      <th style="text-align: center;">Condonation Fee</th>-->
                                                <th style="text-align: center;">Select</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <logic:present name="listData">
                                                <logic:iterate name="listData" id="list">
                                                    <tr>
                                                        <td style="text-align: center;"><%=++i%></td>
                                                        <td style="text-align: center;">${list.year}</td>
                                                        <td style="text-align: center;">${list.scheme}</td>     
                                                        <td style="text-align: center;">${list.subCode}</td>
                                                         <!--<td style="text-align: center;">Regular - ${list.examFee} , Backlog - ${list.backLogfee}<br> Certificate - ${list.certificatefee} , Condonation - ${list.condonationfee} </td>-->
                                                        <td style="text-align: center;">${list.feeDetails}</td>
<!--                                                    <td style="text-align: center;">${list.examFee}</td>
                                                        <td style="text-align: center;">${list.backLogfee}</td>
                                                        <td style="text-align: center;">${list.certificatefee}</td>
                                                        <td style="text-align: center;">${list.condonationfee}</td>-->
                                                        <c:if test="${list.paymentStatus==1}">
                                                            <td style="text-align: center;"> Paid</td>
                                                        </c:if> 
                                                        <c:if test="${list.paymentStatus==3}">
                                                            <td style="text-align: center;"> Attendance % is Less</td>
                                                        </c:if> 
                                                        <c:if test="${list.paymentStatus==2}">
                                                            <td style="text-align: center;"><input type="checkbox" name="checkid" id="checkid<%=++checkCount%>" value="${list.year}" onchange="showAmtDts()"></td>
                                                    <input type="hidden" id="amount<%=checkCount%>" value="${list.total}">
                                                </c:if> 
                                                </tr>
                                            </logic:iterate>
                                        </logic:present>
                                        </tbody>
                                    </table>
                                    <br/>
                                    <div class="col-md-4 showAmount" style="float:right">
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label"> Amount to pay:</label>
                                            <div class="col-sm-8">
                                                <input type="text"  id="semamount" style="pointer-events: none"/>
                                            </div>
                                        </div>
                                    </div>
                                    <logic:present name="nodata">
                                        <span><center><font color="red" style="font-weight: bold">${nodata}</font></center></span>
                                            </logic:present>
                                    <br/>
                                    <div class="col-md-4 showAmount">
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Payment Mode<font color="red">*</font> </label>
                                            <div class="col-sm-8">
                                                <ul>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="DC" checked="checked"> Debit Card </li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="CC" > Credit Card </li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="NBS" >Net Banking (SBI) </li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="NBI"> Net Banking (ICICI) </li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="NBH"> Net Banking (HDFC) </li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="EBS"> Net Banking (Others)</li>
                                                    <li> <input type="radio" name="paymentmode" id="paymentmode" value="PTM"> Net Banking (Paytm)</li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
                                <br/>
                                <c:if test="${finalStatus==2}">
                                    <center>  <input type="button" onclick="return SubmitForm('${appStatus}');" value="PAY"  class="btn btn-success"></center>
                                    </c:if>
                                </c:if>
                        </div>
                    </div>                  
                </html:form>
            </div>
        </div>
        <script>

            function showData() {

                if ('${appStatus}' == '2') {
                    $("#mobile").val('${mobile}');
                    $("#email").val('${email}');
                    $("#ecenter").val('${samecollegeCode}');
                    $("#distrcit1").val('${districtCode}');
                    centersList('${centerCode}');
                }
            }
            showData();
            showAmtDts();
            hideCenter();
        </script>
    </body>
</html>