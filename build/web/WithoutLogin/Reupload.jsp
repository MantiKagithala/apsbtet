
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>

        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">

  <style>
            .ui-datepicker-calendar thead{
                background-color:#2dc1c9 !important;
            }
        </style>
        <Style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .form-control-plaintext{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit
            {
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $("form").attr('autocomplete', 'off');
            });

            function getData() {
               if ($("#aadhar1").val() == "") {
                    $("#aadhar1").val("");
                    alert("Please Enter Aadhaar Card Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                } else if ($("#aadhar1").val().length != 12) {
                    $("#aadhar1").val("");
                    alert("Aadhaar Card Number should be 12 digit number.");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar1").val() == "999999999999" || $("#aadhar1").val() == "333333333333") {
                    $("#aadhar1").val("");
                    alert("Please Enter Valid Aadhaar Card Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (validateVerhoeff($("#aadhar1").val()) != true) {
                    $("#aadhar1").val("");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#grade1").val() == "0" || $("#grade1").val() == "") {
                    alert("Please Select Grade");
                    $("#grade1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    document.forms[0].mode.value = "getOtp";
                    document.forms[0].submit();
                }
            }

          function validateAdharDetails1() {
                if ($("#regno").val() == ""||$.trim($("#regno").val()).length===0) {
                    $("#regno").val("");
                    alert("Please Enter RegistrationNo");
                    $("#regno").focus().css({'border': '1px solid red'});
                } 
                 else if ($("#dob").val() === undefined || $("#dob").val() === "") {
                    alert("Select  Date of Birth");
                    $("#dob").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    document.forms[0].mode.value = "getOtp";
                    document.forms[0].submit();
                }
            }

                  function validateOtp() {
        if ($("#otp").val() == "") {
            alert("Please Enter OTP Number");
            $("#otp").focus().css({'border': '1px solid red'});
            return false;
        }
        document.forms[0].mode.value = "getData";
        document.forms[0].submit();
    }
 function screenSize() {
                var width = parseInt(screen.width);
              //  $('#scren').text("S.W ->"+width);
                if ((width >= 800)) {
                    $('#aadhaarDiv').show();
                      $('.msg-cce').hide();
                } else {
                      $('#aadhaarDiv').hide();
                    $('#aadhaarDiv1').hide();
                      $('.msg-cce').show();
                }
            }

        </script>
    </head>
    <body onload="screenSize()">
        <div class="page-title title-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Reupload </h1>
                        <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">
                            <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                            <span>REUPLOAD SERVICE </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feedback-form">
            <div class="container">
                <div class="row">
                    <h3 class="block-head-News">REUPLOAD SERVICE </h3>
                    <div class="line-border"></div>
                    <div class="row">
                        <div class=" col-md-12">
                            <html:form action="/resend"  styleId="d" method="post" enctype="multipart/form-data">
                                <html:hidden property="mode"/>
                                <logic:present name="result2">
                                    <center> <font color="green" style="font-weight: bold">${result2}</font>  </center>  
                                    </logic:present>
                                    <logic:present name="result">
                                    <center><font color="red" style="font-weight: bold">${result}</font></center>
                                </logic:present><br>
                                   <logic:present name="form">
                                                                  
                                   <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Registration No<font color="red">*</font></label>
                                            <div class="col-sm-8">
                                                <html:text  styleClass="form-control" property="regno" styleId="regno" maxlength="20"   onkeydown="return space(event, this);" />
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group row">
                                            <label for="name" class="col-sm-4 col-form-label">Date Of Birth<font color="red">*</font></label>
                                            <div class="col-sm-8">
                                                <html:text property="dob" styleId="dob" styleClass="form-control" readonly="true" />
                                                <Script>
                    $(document).ready(function() {
                        var today = new Date();
                        yrRange = '1960' + ":" + '2009';
                        $("#dob").datepicker({
                            dateFormat: 'dd/mm/yy',
                            changeMonth: true,
                            maxDate: new Date(2009, 08 - 1, 01),
                            minDate: new Date(1960, 12 - 04, 1),
                            yearRange: yrRange,
                            changeYear: true
                        });
                    });
                                                </Script>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group row">
                                            <center> <input type="button" onclick="validateAdharDetails1();" value="SUBMIT" class="btn btn-primary"/></center>
                                        </div>
                                    </div>
    </logic:present>
                                 <br>
                 <logic:present name="smsForm">
                          <div class="col-md-4">
                                    <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Mobile No:<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                 <html:text  styleClass="form-control" property="maskmobile" styleId="maskmobile" maxlength="12"   value="${maskmobile}" readonly="true"/>
                                    <html:hidden  property="mobile" styleId="mobile" value="${mobile}"/>
                                    <html:hidden  property="aadhar" styleId="aadhar" value="${aadhar}"/>
                                    <html:hidden  property="grade" styleId="grade" value="${grade}"/>
                                        </div>
                                    </div>
                          </div>
                                          <div class="col-md-6">
                                            <div class="form-group row">
                                        <label for="name" class="col-sm-4 col-form-label">Enter OTP recevied to your registered Mobile No<font color="red">*</font></label>
                                        <div class="col-sm-8">
                                <html:text  property="otp" styleId="otp" maxlength="5" onkeypress='return onlyNumbers(event);' /></td>
                                        </div>
                                            </div>
                                          </div>
                                           <div class="col-md-2">
                           <div class="form-group row">
                                        <center> <input type="button" onclick="validateOtp();" value="SUBMIT" class="btn btn-primary"/></center>
                                    </div>
                                           </div>
                    </logic:present>
                        </div>
                            </html:form>                
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </body>
</html>


