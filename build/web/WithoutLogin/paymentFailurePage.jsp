<%-- 
    Document   : paymentFailurePage
    Created on : Apr 8, 2019, 5:46:27 PM
    Author     : 1582792
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="/WEB-INF/struts-bean.tld" prefix="bean"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Payment Failure Page</title>
        <style>
            .table {
                width: 40%;
                max-width: 100%;
                margin-bottom: 20px;
            }
            .table-responsive table .table {

                width: 40%;
            }

            
            .table-responsive table th {
                text-align:center;
                background-color: #dff6ff;
                color:#000;
                font-weight:bold;
                border-right: 1px solid #3AAFDB !important;
                border-bottom:1px solid #3AAFDB;
                border-top: 1px solid #3AAFDB;
                border-left: 1px solid #3AAFDB;
                font-size: 12px;
                padding: 5px;

            }
            
            .table-responsive table tr.alternaterow1 {
                background:#e7e7e7;
            }
            .table-responsive table tr.alternaterow2 {
                background:#fff;
            }
            .table-responsive table td {
                border: 1px solid #3AAFDB;
                color:#000;
                font-size: 12px;
                padding: 5px;
            }
            .table-responsive.table>thead>tr>th {
                vertical-align:middle;
                border-bottom: 2px solid #3AAFDB !important;
            }
            
            table td {
                border: 1px solid #715235 !important;
            }
            table th {
                    color: #fff !important;
                    vertical-align: middle !important;
                    border: 1px solid #ab8c6e !important;
            }
        </style>
    </head>
    <body>
        <html:form action="/paymentFailurePage" method="post">
            <html:hidden property="mode"/>
            <logic:present name="failed">
                <h3><font color="red">${failed}</font></h3>
            </logic:present>            
        </html:form>
    </body>
</html>