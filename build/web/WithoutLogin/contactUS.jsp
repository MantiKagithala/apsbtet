<%-- 
    Document   : contactUS
    Created on : Nov 23, 2020, 6:27:40 PM
    Author     : 1582792
--%>

<%@page import="java.util.Date"%>
<%@page import="java.text.SimpleDateFormat"%>
<!DOCTYPE html>
<%@ page import="com.ap.WithLoginDAO.LoginDAO" %>

<%
    int visitorCount = LoginDAO.getInstance().visitingTime(request.getRemoteHost().toString());
    response.setHeader("Pragma", "no-cache");
    response.setHeader("Cache-Control", "no-cache,no-store, must-revalidate");
    response.addHeader("Cache-Control", "pre-check=0, post-check=0");
    response.setHeader("Expires", "0");
    response.setDateHeader("Expires", 0);
    response.addHeader("Referrer-Policy", "no-referrer");
    response.addHeader("X-Frame-Options", "SAMEORIGIN");
    response.addHeader("Strict-Transport-Security", " max-age=63072000; includeSubDomains; preload");
    response.addHeader("Content-Security-Policy", "default-src https: 'self' 'unsafe-eval' 'unsafe-inline'  image-src 'self' data:; object-src 'none' ");

%>
<html>
    <link rel="stylesheet" href="css/DSDIGI.css" type="text/css">


    <style type="text/css">
        @media (min-width: 768px){
            .modal-sm {
                width: 500px;
                margin-top: 175px;
            }
        }

        .modal-header {
            min-height: 16.42857143px;
            padding: 6px 16px;
            border-bottom: 1px solid #e5e5e5;
            background: #017100;
            color: #fff;
        }

        .close
        {
            text-shadow: none !important;

            opacity: .6 !important;
        }
    </style>
    <body>
        <div class="row mainbodyrow">
            <div class="container">
                <div class="col-xs-12">
                    <div class="maindodycnt">
                        <div class="row">
                            <div class="col-sm-4 col-xs-12">
                                <div>
                                    <img src="images/ANGRANGA_help.jpg" style="box-shadow: 1px 2px 3px #ccc; border: 1px solid #ccc;" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
      
       
        
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script>
            $('ul.nav li.dropdown').hover(function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
            }, function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
            });
        </script>
    </body>
</html>