
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>

        <%
            int i = 1;
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">
        <link rel="stylesheet" href="css/style1.css">
        <style>
            .ui-datepicker-calendar thead{
                background-color:#2dc1c9 !important;
            }
        </style>
        <style>
            .overlay{
                display: none;
                position: fixed;
                width: 100%;
                height: 100%;
                top: 0;
                left: 0;
                z-index: 999;
                background: rgba(255,255,255,0.8) url("img/LOADER.gif") center no-repeat;
            }
            /* Turn off scrollbar when body element has the loading class */
            body.loading{
                overflow: hidden;   
            }
            /* Make spinner image visible when body element has the loading class */
            body.loading .overlay{
                display: block;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $(document).ajaxStart(function() {
                    $("body").addClass("loading");
                });
                $(document).ajaxComplete(function() {
                    $("body").removeClass("loading");
                });

                var seconds = 5;
                setTimeout(function() {
                    if ($(".msg") !== null)
                        $(".msg").hide();
                }, seconds * 1000);
                $("form").attr('autocomplete', 'off');
                $("#blind").val("NO");
                $("#highdiv").hide();
                $("#lowergradediv").hide();
                $("#sscdiv").hide();
                $("#sscyeardiv").hide();
                $("#lowdiv").hide();
                $("#qualificationdiv").hide();
                $("#interdiv").hide();
                $("#brDiv").show();
                $("#aadharDiv").hide();
                $(".viifile").hide();
                $(".hsfile").hide();
                $('input[type=text], textarea').bind("cut copy paste", function(e) {
                    alert("Cut copy paste not allowed here");
                    e.preventDefault();
                });
                $("#startDiv").show();
                $("#DistrictDiv").show();
                $("#DistrictDiv1").hide();
                $("#MandalDiv").show();
                $("#MandalDiv1").hide();
                $("#yesDiv").hide();
                $("#noDiv").hide();
                $(".blindDiv").hide();
                $(".blindDiv1").hide();
                $(function() {
                    $("#photo").change(function() {
                        $("#myImg").show();
                        if (this.files && this.files[0]) {
                            var reader = new FileReader();
                            reader.onload = imageIsLoaded;
                            reader.readAsDataURL(this.files[0]);
                        }
                    });
                    $("#dialog").dialog({
                        autoOpen: false,
                        modal: true,
                        width: 800,
                        height: 600,
                        closeText: "X"
//                            show: {
//                    effect: "slide",
//                            duration: 1500
//                            },
//                            hide: {
//                    effect: "fade",
//                            duration: 1000
//                            }
                    });
                });
                function imageIsLoaded(e) {
                    $('#myImg').attr('src', e.target.result);
                }
                var angle = 0;
                $(".toggle").click(function() {
                    angle += 90;
                    $('#imageview').css('transform', 'rotate(' + angle + 'deg)');
//                    $("#imageview").toggleClass('flip');
                });
                $('#bname').keyup(function() {
                    this.value = this.value.toUpperCase();
                });

            });
            function fileupload(evt, thisvalue) {

                var files = $('#' + thisvalue).prop("files")
                if (files && files[0]) {
                    var reader = new FileReader();
                    reader.onload = imageIsLoaded1;
                    reader.readAsDataURL(files[0]);
                }
            }
            function imageIsLoaded1(e) {
                $('#imageview').attr('src', e.target.result);
                $("#dialog").dialog("open");
            }
            function space(evt, thisvalue)
            {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    alert("Allows only Numerics");
                    return false;
                }
                return true;
            }
            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }
            function telephoneValidation() {
                var mobileNo = $("#mobile").val();
                var len = mobileNo.length;
                if (len < 10) {
                    alert("Mobile Number Should be 10 digits");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                } else if (($("#mobile").val().trim()) == '9999999999'
                        || ($("#mobile").val().trim()) == '8888888888' ||
                        ($("#mobile").val().trim()) == '7777777777' ||
                        ($("#mobile").val().trim()) == '6666666666') {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#mobile").val().trim().charAt(0) != '9'
                        && $("#mobile").val().trim().charAt(0) != '8' &&
                        $("#mobile").val().trim().charAt(0) != '7' &&
                        $("#mobile").val().trim().charAt(0) != '6')) {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
            }
            function validateAdharDetails() {
                if ($("#aadhar").val() == "") {
                    $("#aadhar").val("");
                    alert("Please Enter Aadhaar Card Number");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                } else if ($("#aadhar").val().length != 12) {
                    $("#aadhar").val("");
                    alert("Aadhaar Card Number should be 12 digit number.");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar").val() == "999999999999" || $("#aadhar").val() == "333333333333") {
                    $("#aadhar").val("");
                    alert("Please Enter Valid Aadhaar Card Number");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (validateVerhoeff($("#aadhar").val()) != true) {
                    $("#aadhar").val("");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                    return false;
                }



            }
            function CheckfilePdfOrOther(myval) {
                var fup = $("#" + myval).val();
                var ext = fup.split('.').pop().toUpperCase();
                if (ext === "JPG")
                {
                    var file = $("#" + myval)[0].files[0];
                    var size = file.size / 1024;
                    if (size < 1001) {
                        return true;
                    } else {
                        alert("File size should be less than 1MB only");
                        $("#" + myval).val('').clone(true);
                        $("#" + myval).focus();
                        return false;
                    }
                } else {
                    alert("Upload JPG Files only & File Size must be less than or Equal to 1MB");
                    $("#" + myval).val('').clone(true);
                    $("#" + myval).focus();
                    return false;
                }
            }
            function CheckfilePdfOrOther40kb(myval) {
                var fup = $("#" + myval).val();
                var ext = fup.split('.').pop().toUpperCase();
                if (ext === "JPG")
                {
                    var file = $("#" + myval)[0].files[0];
                    var size = file.size / 1024;
                    if (size < 41) {
                        return true;
                    } else {
                        alert("File size should be less than 40KB only");
                        $("#" + myval).val('').clone(true);
                        $("#" + myval).focus();
                        return false;
                    }
                } else {
                    alert("Upload JPG Files only & File Size must be less than or Equal to 40KB");
                    $("#" + myval).val('').clone(true);
                    $("#" + myval).focus();
                    return false;
                }
            }
            function showStatus() {
                var statusflag = $("input[name='statusflag']:checked").val();
                if (statusflag == "1") {
                    $("#startDiv").hide();
                    $("#yesDiv").show();
                    $("#brDiv").show();
                    $("#noDiv").hide();
                    $("#aadharDiv").hide();
                } else if (statusflag == "2") {
                    $("#hallticket").val("");
                    $("#startDiv").hide();
                    $("#yesDiv").hide();
                    $("#noDiv").hide();
                    $("#noDiv").hide();
                    $("#aadharDiv").show();
                    $("#brDiv").show();
                }
            }

            function blindStatus() {
                $("#blindno").val("");
                $("#blindflag").val("N");
                if ($("#blind").val() == "YES") {
                    $(".blindDiv").show();
                    $(".blindDiv1").hide();
                } else if ($("#blind").val() == "NO") {
                    $(".blindDiv").hide();
                    $(".blindDiv1").hide();
                }
            }
            function batchStatus() {
                if ($("#grade").val() == "TTL" || $("#grade").val() == "TEL" || $("#grade").val() == "TEH" || $("#grade").val() == "TTH") {
                    if ($("#edate").val() == "DAY-I") {
                        $("#ebatch").empty().append('<option value="0">--Select Batch--</option>\n\ \n\
                            <option value="B1">Batch-I</option>\n\
                            <option value="B2">Batch-II</option>')
                    } else if ($("#edate").val() == "DAY-II") {
                        $("#ebatch").empty().append('<option value="0">--Select Batch--</option>\n\ \n\
                            <option value="B3">Batch-III</option>\n\
                            <option value="B4">Batch-IV</option>')
                    }
                } else if ($("#grade").val() == "TEJ") {
                    if ($("#edate").val() == "DAY-I") {
                        $("#ebatch").empty().append('<option value="0">--Select Batch--</option>\n\ \n\
                            <option value="B1">Batch-I</option>')
                    } else if ($("#edate").val() == "DAY-II") {
                        $("#ebatch").empty().append('<option value="0">--Select Batch--</option>\n\ \n\
                            <option value="B2">Batch-II</option>')
                    }
                }
                else {
                    $("#ebatch").empty().append('<option value="0">--Select Batch--</option>\n\ \n\
                            <option value="B1">Batch-I</option>')
                }
            }
            function statusexam() {
                var data = "district=" + $("#apdistrict").val().trim();
//                $.ajax({
//                    type: "POST",
//                    url: "./register.do?mode=statusexam&aadhar=" + $("#aadhar").val().trim() + "&language=" + $("#language").val() + "&examination=" + $("#examination").val(),
//                    data: data,
//                    cache: true,
//                    contentType: true,
//                    processData: true,
//                    success: function(response) {
//                        if (response == 1) {
//                            alert("Already Applied for one Grade in this Language.Choose Another Language");
//                            $("#grade").val("0")
//                            $("#language").val("0")
//                        }
//
//                    },
//                    error: function(e) {
////            alert('Error: ' + e);
//                    }
//                });
            }
            function fileStatus() {
                $("#upload1").val("");
                $("#upload2").val("");
                $("#upload3").val("");
                $("#upload4").val("");
                if ($("#grade").val() == "TTL" || $("#grade").val() == "THL" || $("#grade").val() == "TUL" || $("#grade").val() == "TEL" || $("#grade").val() == "SEL" || $("#grade").val() == "STL" || $("#grade").val() == "SUL") {
                    $("#lowdiv").show();
                    $("#highdiv").hide();
                    $("#lowergradediv").hide();
                    $("#sscgradediv").hide();
                       if($("#sscyear").val()=="2021"){
                    $('.xfile').hide();
                    $('.viifile').hide();
                    $('.hsfile').hide();
                    $('.highfile3').show();
                    }else{
                            $('.xfile').show();
                    $('.viifile').hide();
                    $('.hsfile').hide();
                    $('.highfile3').hide();
                    }

                } else if ($("#grade").val() == "TEJ") {
                    $("#lowdiv").show();
                    $('.xfile').hide();
                    $('.viifile').show();
                    $('.hsfile').hide();
                    $("#highdiv").hide();
                    $("#lowergradediv").hide();
                    $("#sscgradediv").hide();
                } else if ($("#grade").val() == "TTHS" || $("#grade").val() == "TEHS" || $("#grade").val() == "SEHS150" || $("#grade").val() == "SEHS180" || $("#grade").val() == "SEHS200"
                        || $("#grade").val() == "STHS80" || $("#grade").val() == "STHS100") {
                    $("#lowdiv").show();
                    $('.xfile').hide();
                    $('.viifile').hide();
                    $('.hsfile').show();
                    $("#highdiv").hide();
                    $("#lowergradediv").hide();
                    $("#sscgradediv").hide();
                } else if ($("#grade").val() == "TTH" || $("#grade").val() == "THH" || $("#grade").val() == "TUH" || $("#grade").val() == "TEH") {
                    $("#highdiv").show();
                    $("#lowergradediv").show();
                    $("#sscgradediv").show();
                    $(".highfile").show();
                    $(".highfile1").hide();
                    $("#lowdiv").hide();

                } else if ($("#grade").val() == "STH" || $("#grade").val() == "SEI" || $("#grade").val() == "SUH") {
                    $("#highdiv").hide();
                    $("#lowergradediv").show();
                    $("#sscgradediv").show();
//                    $("#seidiv").hide();
                    $(".highfile").hide();
                    $(".highfile1").show();
                    $("#highspeeddiv").hide();
                    $("#lowdiv").hide();
                } else if ($("#grade").val() == "SEH") {
                    $("#highdiv").show();
                    $("#lowergradediv").show();
                    $("#sscgradediv").show();
                    $(".highfile").hide();
                    $(".highfile1").show();
                    $(".highfile5").show();
                    $("#highspeeddiv").show();
                    $("#lowdiv").hide();
                }

            }
            function examinationDistricts() {
                var data = "district=" + $("#apdistrict").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./register.do?mode=getCentersDistrict&examination=" + $("#examination").val()+"&grade="+$("#grade").val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#apdistrict").empty().append(response);

                    },
                    error: function(e) {
//            alert('Error: ' + e);
                    }
                });
            }
            function centersList() {
                var data = "district=" + $("#apdistrict").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./register.do?mode=getCenters&district=" + $("#apdistrict").val().trim() + "&examination=" + $("#examination").val(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#ecenter").empty().append(response);

                    },
                    error: function(e) {
//            alert('Error: ' + e);
                    }
                });
            }
            function getDistricts() {
                $("#district").val("0");
                $("#mandal").val("0");
                $("#district1").val("");
                $("#mandal1").val("");
                if (($("#state").val() == "Andra Pradesh") || ($("#state").val() == "Telagana")) {
                    $("#DistrictDiv").show();
                    $("#DistrictDiv1").hide();
                    $("#MandalDiv").show();
                    $("#MandalDiv1").hide();
                    var data = "district=" + $("#state").val().trim();
                    $.ajax({
                        type: "POST",
                        url: "./register.do?mode=getDistrictListAt&state=" + $("#state").val().trim(),
                        data: data,
                        cache: true,
                        contentType: true,
                        processData: true,
                        success: function(response) {
                            $("#district").empty().append(response);
                        },
                        error: function(e) {
                        }
                    });
                } else {
                    $("#DistrictDiv").hide();
                    $("#DistrictDiv1").show();
                    $("#MandalDiv").hide();
                    $("#MandalDiv1").show();
                }
            }
            function sadaremstatus() {
                var data = "blindno=" + $("#blindno").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./register.do?mode=sadaremstatus&blindno=" + $("#blindno").val().trim() + "&aadhar=" + $("#aadhar").val().trim(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#blindflag").val("N");
                        $(".blindDiv1").hide();
                        if (response == 0) {
//                        $("#blindno").val("");
                            $("#blindflag").val("N");
                            $(".blindDiv1").show();
                            alert("No data available with this sadarem No")
                        } else if (response == 2) {
                            $("#blindflag").val("N");
                            $("#blindno").val("");
                            alert("SadaremNo tagged to anothher candidate")
                        }
                        else {
                            $("#blindflag").val("Y");
                        }
                    },
                    error: function(e) {
                    }
                });
            }
            function getMandals() {
                var data = "district=" + $("#state").val().trim();
                $.ajax({
                    type: "POST",
                    url: "./register.do?mode=getMandalsAt&state=" + $("#state").val().trim() + "&district=" + $("#district").val().trim(),
                    data: data,
                    cache: true,
                    contentType: true,
                    processData: true,
                    success: function(response) {
                        $("#mandal").empty().append(response);

                    },
                    error: function(e) {
                    }
                });
            }
            function SubmitForm() {
//            adharDuplicatevalidation();
                if (document.getElementById("declaration").checked === false) {
                    alert("Accept Declaration ");
                    $("#declaration").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#bname").val() === undefined || $("#bname").val() === "") {
                    alert("Please enter Name of the applicant");
                    $("#bname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#fname").val() === undefined || $("#fname").val() === "") {
                    alert("Please enter Father Name of the applicant");
                    $("#fname").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#dob").val() === undefined || $("#dob").val() === "") {
                    alert("Select  Date of Birth");
                    $("#dob").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#gender").val() === undefined || $("#gender").val() === "" || $("#gender").val() === "0") {
                    alert("Please Select Gender.");
                    $("#gender").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#caste").val() === undefined || $("#caste").val() === "" || $("#caste").val() === "0") {
                    alert("Select Caste.");
                    $("#caste").focus().css({'border': '1px solid red'});
                    return false;
                }

                else if ($("#blind").val() === undefined || $("#blind").val() === "" || $("#blind").val() === "0") {
                    alert("Select Are you Visually Impaired Candidate?");
                    $("#blind").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#blind").val() == "YES" && (($("#blindflag").val() == "N") && ($("#blindfile").val() === undefined || $("#blindfile").val() === ""))) {
                    alert("Upload Blind Certificate/enter sadarem no");
                    $("#blindfile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#examination").val() === undefined || $("#examination").val() === "" || $("#examination").val() === "0") {
                    alert("Select Examination Appearing.");
                    $("#examination").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#language").val() === undefined || $("#language").val() === "" || $("#language").val() === "0") {
                    alert("Select Language.");
                    $("#language").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#grade").val() === undefined || $("#grade").val() === "" || $("#grade").val() === "0") {
                    alert("Select Grade");
                    $("#grade").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#apdistrict").val() === undefined || $("#apdistrict").val() === "" || $("#apdistrict").val() === "0") {
                    alert("Select Examination District");
                    $("#apdistrict").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#ecenter").val() === undefined || $("#ecenter").val() === "" || $("#ecenter").val() === "0") {
                    alert("Select Examination Center");
                    $("#ecenter").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#edate").val() === undefined || $("#edate").val() === "" || $("#edate").val() === "0") {
                    alert("Select Examination Date");
                    $("#edate").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#ebatch").val() === undefined || $("#ebatch").val() === "" || $("#ebatch").val() === "0") {
                    alert("Select Examination Batch");
                    $("#ebatch").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if ($("#upload1").val() === undefined || $("#upload1").val() === "") {
//                    alert("Upload Certificate.");
//                    $("#upload1").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else if (($("#upload1").val() === undefined || $("#upload1").val() === "") &&
                        ($("#grade").val() == "TTL" || $("#grade").val() == "THL" || $("#grade").val() == "TUL" ||
                                $("#grade").val() == "TEL" || $("#grade").val() == "SEL" || $("#grade").val() == "STL"
                                || $("#grade").val() == "SUL") && ($("#sscflag").val() == "N")) {
                    alert("Upload SSC/Equivalent Certificate.");
                    $("#upload1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#upload1").val() === undefined || $("#upload1").val() === "") && ($("#grade").val() == "TEJ")) {
                    alert("Upload VII Bonafide Certificate.");
                    $("#upload1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#upload1").val() === undefined || $("#upload1").val() === "") && ($("#grade").val() == "TTHS" || $("#grade").val() == "TEHS" || $("#grade").val() == "SEHS150" || $("#grade").val() == "SEHS180" || $("#grade").val() == "SEHS200" || $("#grade").val() == "STHS80" || $("#grade").val() == "STHS100")) {
                    alert("Upload HigherGrade Certificate.");
                    $("#upload1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#upload2").val() === undefined || $("#upload2").val() === "") &&
//                        ($("#upload4").val() === undefined || $("#upload4").val() === "") &&
                        ($("#grade").val() == "TTH" || $("#grade").val() == "THH" || $("#grade").val() == "TUH" ||
                                $("#grade").val() == "TEH") && ($("#qualification").val() == "SSC")) {
                    alert("Upload LowerGrade Certificate");
                    $("#upload2").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (
//                        ($("#upload2").val() === undefined || $("#upload2").val() === "") &&
                        ($("#upload4").val() === undefined || $("#upload4").val() === "") &&
                        ($("#grade").val() == "TTH" || $("#grade").val() == "THH" || $("#grade").val() == "TUH" ||
                                $("#grade").val() == "TEH") && ($("#interflag").val() == "N") && ($("#qualification").val() == "INTER")) {
                    alert("Upload Intermediate/Equivalent Certificate");
                    $("#upload4").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ((($("#upload2").val() != "") && ($("#upload3").val() === undefined || $("#upload3").val() === ""))
                        && ($("#grade").val() == "TTH" || $("#grade").val() == "THH" || $("#grade").val() == "TUH" ||
                        $("#grade").val() == "TEH") && ($("#sscflag").val() == "N")) {
                    alert("Upload SSC/Equivalent Certificate.");
                    $("#upload3").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ((($("#upload2").val() === undefined || $("#upload2").val() === "") && ($("#upload3").val() === undefined || $("#upload3").val() === "")) && ($("#grade").val() == "STH" || $("#grade").val() == "SEI" || $("#grade").val() == "SUH")) {
                    alert("Upload LowerGrade/Graduation Certificate.");
                    $("#upload2").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ((($("#upload2").val() === undefined || $("#upload2").val() === "") && ($("#upload3").val() === undefined || $("#upload3").val() === "") && ($("#upload4").val() === undefined || $("#upload4").val() === "")) && ($("#grade").val() == "SEH")) {
                    alert("Upload LowerGrade/DCCP/Graduation/Vocational certificate.");
                    $("#upload2").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#houseno").val() === undefined || $("#houseno").val() === "" || ($.trim($("#houseno").val()).length === 0)) {
                    alert("Enter Present Address house Number");
                    $("#houseno").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#locality").val() === undefined || $("#locality").val() === "" || ($.trim($("#locality").val()).length === 0)) {
                    alert("Enter Present Address Locality");
                    $("#locality").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#village").val() === undefined || $("#village").val() === "" || ($.trim($("#village").val()).length === 0)) {
                    alert("Enter Present Address Village/Town");
                    $("#village").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#state").val() === undefined || $("#state").val() === "" || $("#state").val() === "0") {
                    alert("Enter  Present Address State");
                    $("#state").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#state").val() == "Andra Pradesh" || $("#state").val() == "Telagana") && ($("#district").val() == "0" || $("#district").val() === ""))
                {
                    alert("Enter Present Address District");
                    $("#district").focus().css({'border': '1px solid red'});
                    return false;
                } else if (($("#state").val() == "Andra Pradesh" || $("#state").val() == "Telagana") && ($("#mandal").val() == "0" || $("#mandal").val() === ""))
                {
                    alert("Enter Present Address Mandal");
                    $("#mandal").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ((($("#state").val() != "Andra Pradesh") && ($("#state").val() != "Telagana")) && ($("#district1").val() == "" || ($.trim($("#district1").val()).length === 0)))
                {
                    alert("Enter Present Address District");
                    $("#district1").focus().css({'border': '1px solid red'});
                    return false;
                } else if ((($("#state").val() != "Andra Pradesh") && ($("#state").val() != "Telagana")) && ($("#mandal1").val() == "" || ($.trim($("#mandal1").val()).length === 0)))
                {
                    alert("Enter Present Address Mandal");
                    $("#mandal1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#pincode").val() === undefined || $("#pincode").val() === "" || ($.trim($("#pincode").val()).length === 0)) {
                    alert("Enter Present Address Pincode");
                    $("#pincode").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#mobile").val() === undefined || ($.trim($("#mobile").val()).length === 0 ||
                        $("#mobile").val().length < 10 || parseInt($("#mobile").val().trim()) < 6000000000)) {
                    alert("Enter Mobile Number");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#email").val() === undefined || $("#email").val() === "" || ($.trim($("#email").val()).length) < 10) {
                    alert("Enter eMail");
                    $("#email").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#aadhar").val() === undefined || $("#aadhar").val() === "" && $("#aadhar").val().length < 12) {
                    alert("Enter  valid  Aadhar Number");
                    $("#aadhar").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#photo").val() === undefined || $("#photo").val() === "") {
                    alert("Upload your Photo.");
                    $("#photo").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#signature").val() === undefined || $("#signature").val() === "") {
                    alert("Upload your Signature.");
                    $("#signature").focus().css({'border': '1px solid red'});
                    return false;
                }

                else {
                    var x = confirm("Do you Want to Proceed?")
                    if (x == false) {
                        return false
                    } else {

                        document.forms[0].mode.value = "submitData";
                        document.forms[0].submit();
                    }
                }
            }
            function isEmail() {
                var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (!regex.test($("#email").val())) {
                    $("#email").val("");
                    alert("Invalid eMail")
                    return true;  //wrong mail
                } else {
                    return false;  //correct mail

                }
            }
            function isPincode() {
                var pincode = $("#pincode").val();
                if (pincode.length < 6) {
                    $("#pincode").val("");
                    alert("Pincode must be 6 digits")
                    return true;  //wrong mail
                } else if (pincode == "000000") {
                    $("#pincode").val("");
                    alert("Invalid Pincode")
                    return true;
                }
                else {
                    return false;

                }
            }
            function getData() {
                if ($("#hallticket").val() == "") {
                    alert("Enter Your Registration Number");
                    $("#hallticket").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#hallticket").val() == "") {
                    alert("Enter Your Registration Number");
                    $("#hallticket").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar2").val().length != 12) {
                    $("#aadhar2").val("");
                    alert("Aadhaar Card Number should be 12 digit number.");
                    $("#aadhar2").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar2").val() == "999999999999" || $("#aadhar2").val() == "333333333333") {
                    $("#aadhar2").val("");
                    alert("Please Enter Valid Aadhaar Card Number");
                    $("#aadhar2").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (validateVerhoeff($("#aadhar2").val()) != true) {
                    $("#aadhar2").val("");
                    $("#aadhar2").focus().css({'border': '1px solid red'});
                    return false;
                }
//                else if ($("#grade2").val() == "0"||$("#grade2").val() == "") {
//                    alert("Select Grade");
//                    $("#grade2").focus().css({'border': '1px solid red'});
//                    return false;
//                }
                else {
                    document.forms[0].mode.value = "editData";
                    document.forms[0].submit();
                }
            }
            function adharDuplicatevalidation() {

                var paraData = "aadhaar=" + $("#aadhaar").val();
                $.ajax({
                    type: "POST",
                    url: "register.do?mode=validatingaadharNum&aadharNum=" + $("#aadhar").val() + "&mobile=" + $("#mobile").val() + "&email=" + $("#email").val(),
                    data: paraData,
                    success: function(response) {
                        if (response == 1) {
                            alert("Mobile Number already Registered for another Aadhar")
                            $("#mobile").val("")
//                            $("#email").val("")

                        } else {
                        }
                    }


                });

            }
            function validateAdharDetails1() {
                if ($("#aadhar1").val() == "") {
                    $("#aadhar1").val("");
                    alert("Please Enter Aadhaar Card Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                } else if ($("#aadhar1").val().length != 12) {
                    $("#aadhar1").val("");
                    alert("Aadhaar Card Number should be 12 digit number.");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar1").val() == "999999999999" || $("#aadhar1").val() == "333333333333") {
                    $("#aadhar1").val("");
                    alert("Please Enter Valid Aadhaar Card Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (validateVerhoeff($("#aadhar1").val()) != true) {
                    $("#aadhar1").val("");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#grade1").val() == "0" || $("#grade1").val() == "") {
                    $("#grade1").val("0");
//                    alert("Please Select Grade");
                    $("#grade1").focus().css({'border': '1px solid red'});
                    return false;
                } else if (($("#grade1").val() == "TEL" || $("#grade1").val() == "TTL" || $("#grade1").val() == "THL" || $("#grade1").val() == "SEL" || $("#grade1").val() == "STL") && ($.trim($("#sschallticket").val()).length === 0)) {
                    $("#sschallticket").val("");
//                    alert("Please Enter SSC Hallticket ");
                    $("#sschallticket").focus().css({'border': '1px solid red'});
                    return false;
                }else if (($("#grade1").val() == "TEL" || $("#grade1").val() == "TTL" || $("#grade1").val() == "THL" || $("#grade1").val() == "SEL" || $("#grade1").val() == "STL") && $("#sscyear").val()== "0") {
                    $("#sscyear").val("0");
//                    alert("Please Enter SSC Hallticket ");
                    $("#sscyear").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#grade1").val() == "TEH" || $("#grade1").val() == "TTH" || $("#grade1").val() == "THH") && ($("#qualification").val() == "0")) {
                    $("#qualification").val("0");
//                    alert("Please select Qualification ");
                    $("#qualification").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#grade1").val() == "TEH" || $("#grade1").val() == "TTH" || $("#grade1").val() == "THH") && ($("#qualification").val() == "SSC") && ($.trim($("#sschallticket").val()).length === 0)) {
                    $("#sschallticket").val("");
//                    alert("Please Enter SSC Hallticket ");
                    $("#sschallticket").focus().css({'border': '1px solid red'});
                    return false;
                } else if (($("#grade1").val() == "TEH" || $("#grade1").val() == "TTH" || $("#grade1").val() == "THH") && ($("#qualification").val() == "INTER") && (($.trim($("#interhallticket").val()).length === 0))) {
                    $("#interhallticket").val("");
//                    alert("Please Enter Inter Hallticket ");
                    $("#interhallticket").focus().css({'border': '1px solid red'});
                    return false;
                } else if (($("#grade1").val() == "TEH" || $("#grade1").val() == "TTH" || $("#grade1").val() == "THH") && ($("#qualification").val() == "INTER") && ($("#intermonth").val() == "0")) {
                    $("#intermonth").val("0");
//                    alert("Please Select Inter Month ");
                    $("#intermonth").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#grade1").val() == "TEH" || $("#grade1").val() == "TTH" || $("#grade1").val() == "THH") && ($("#qualification").val() == "INTER") && ($("#interyear").val() == "0")) {
                    $("#interyear").val("0");
//                    alert("Please Select Inter Year ");
                    $("#interyear").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    var paraData = "aadhaar=" + $("#aadhaar1").val();
                    var aadharNum = $("#aadhar1").val();
                    var grade1 = $("#grade1").val();
                    var qualification = $("#qualification").val();
                    var sschallticket = $("#sschallticket").val();
                    var sscyear = $("#sscyear").val();
                    var url = "";
                    var interhallticket = "";
                    var intermonth = "";
                    var interyear = "";
                    if (qualification == "INTER") {
                        interhallticket = $("#interhallticket").val();
                        intermonth = $("#intermonth").val();
                        interyear = $("#interyear").val();
                        url = "register.do?mode=getStatus&aadharNum=" + $("#aadhar1").val() + "&grade=" + $("#grade1").val() + "&interhallticket=" + $("#interhallticket").val() + "&intermonth=" + $("#intermonth").val() + "&interyear=" + $("#interyear").val() + "&qualification=" + $("#qualification").val();
                    } else {
                        url = "register.do?mode=getStatus&aadharNum=" + $("#aadhar1").val() + "&grade=" + $("#grade1").val() + "&sschallticket=" + $("#sschallticket").val() + "&qualification=0";
                    }
                    var gradename = $("#grade1 option:selected").html();
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: paraData,
                        success: function(response) {
                            $("#highdiv").hide();
                            $("#lowergradediv").hide();
                            $("#sscgradediv").hide();
                            $(".highfile").hide()
                            $(".highfile1").hide()
                            $(".highfile2").hide()
                            $(".highfile5").hide()
                            $("#lowdiv").hide();
                            $("#d")[0].reset()
                            $("#blind").val("NO")
                            $("#sscflag").val("N")
                            $("#interflag").val("N")
                            $("#aadhar1").val(aadharNum)
                            $("#grade1").val(grade1)
                            $("#sschallticket").val(sschallticket)
                            $("#sscyear").val(sscyear)
                            $("#qualification").val(qualification)
                            if (qualification == "INTER") {
                                $("#interhallticket").val(interhallticket);
                                $("#intermonth").val(intermonth);
                                $("#interyear").val(interyear);
                            }
                            var sample = response.trim().split("_")
                            var res = sample[0];
                            var resssc = sample[6];
                            if (res == 0) {
                                var aadhar = sample[1];
                                var examination = sample[2];
                                var language = sample[3];
                                var grade = sample[4];
                                var examinationname = sample[5];
                                $("#myImg").hide();
                                $("#lowergradediv").hide()
                                $("#brDiv").hide();
                                $(".highfile3").hide();
                                $(".blindDiv").hide();
                                $(".blindDiv1").hide();
                                $("#aadharDiv").show();
                                $("#noDiv").show();
                                $('#aadhar').val(aadhar);
                                $('#examination').val(examination);
                                $('#examinationname').val(examinationname);
//                                examinationStatus();
                                $('#language').val(language);
                                $('#languagename').val(language);
//                                languageStatus();
                                $('#grade').val(grade);
                                $('#gradename').val(gradename);
                                $("#gendercaste").show();
                                $("#gendercaste1").hide();
                                $(".dateDiv").show();
                                $(".dateDiv1").hide();
                                examinationDistricts();
                                $("#bname").prop('readonly', false);
                                $("#fname").prop('readonly', false);
                                if ($("#grade1").val() == "TEL" || $("#grade1").val() == "TTL" || $("#grade1").val() == "THL" || $("#grade1").val() == "SEL" || $("#grade1").val() == "STL") {
                                    if (resssc == 1) {
                                        $("#sscflag").val("Y")
                                        $("#bname").val(sample[7])
                                        $("#fname").val(sample[8])
                                        $("#gender1").val(sample[9])
                                        $("#caste1").val(sample[10])
                                        $("#gender").val(sample[9])
                                        $("#caste").val(sample[10])
                                        $("#dob").val(sample[11])
                                        $("#dob1").val(sample[11])
                                        $("#bname").prop('readonly', true);
                                        $("#fname").prop('readonly', true);
                                        $("#gendercaste").hide();
                                        $("#gendercaste1").show();
                                        $(".dateDiv").hide();
                                        $(".dateDiv1").show();
//                                        $("#gender").prop('disabled', true);
//                                        $("#caste").prop('disabled', true);
//                                        $("#dob").prop('readonly', true);
                                        dateChange();
                                        statusexam();
                                    } else if (resssc == 0) {
                                        $("#sscflag").val("N")
                                        fileStatus();
                                        dateChange();
                                        statusexam();
                                    } else if (resssc == 2) {
                                        $("#sscflag").val("N");
                                        $("#brDiv").show("");
                                        $("#noDiv").hide("");
                                        alert("Entered hallticket tagged to another Aadhar")
                                        $("#sschallticket").val("");
                                    }
                                } else {
                                    $("#sscflag").val("N")
                                    $("#gendercaste").show();
                                    $("#gendercaste1").hide();
                                    fileStatus();
                                    dateChange();
                                    statusexam();
                                }
                            } else if (res == 3) {
                                alert("Already Selected One Grade in Respective Language.Choose Another Language")
                                $("#noDiv").hide();
                                $("#brDiv").show();
                                $("#sscdiv").hide();
                                $("#sscyeardiv").hide();
                                $("#interdiv").hide();
                                $("#interhallticket").val("");
                                $("#intermonth").val("0");
                                $("#interyear").val("0");
                                $("#sschallticket").val("");
                                $("#qualification").val("0");
//                                $("#startDiv").show();
                            }
                            else {
                                document.forms[0].statusresult.value = res;
                                document.forms[0].mode.value = "getData";
                                document.forms[0].submit();
                            }
                        }
                    });


                }

            }

            function getTimeStatus() {
                if ($("#grade").val() == "TEL" || $("#grade").val() == "TTL" || $("#grade").val() == "THL" || $("#grade").val() == "TUL" ||
                        $("#grade").val() == "TEH" || $("#grade").val() == "TTH" || $("#grade").val() == "THH" || $("#grade").val() == "TUH" || $("#grade").val() == "TEJ" || $("#grade").val() == "TEHS" || $("#grade").val() == "TTHS") {
                    var paraData = "aadhaar=" + $("#aadhaar1").val();
                    $.ajax({
                        type: "POST",
                        url: "register.do?mode=getTimeStatus&aadharNum=" + $("#aadhar").val() + "&date=" + $("#edate").val() + "&batch=" + $("#ebatch").val() + "&grade=" + $("#grade").val(),
                        data: paraData,
                        success: function(response) {
                            if (response == 1) {
                                alert("Already Applied in this Batch For Other Language Examination.")
                                $("#ebatch").val("0");
                                $("#edate").val("0");
//                                $("#grade").val("0");
                            }
                        }
                    });
                }
            }
            function dateChange() {
                if ($("#grade").val() == "TTL" || $("#grade").val() == "TEL" || $("#grade").val() == "TEH" || $("#grade").val() == "TTH" || $("#grade").val() == "TEJ") {
                    $("#edate").empty().append('<option value="0">--Select Day--</option>\n\
            <option value="DAY-I">DAY-I</option>\n\
            <option value="DAY-II">DAY-II</option>')
                } else {
                    $("#edate").empty().append('<option value="0">--Select Day--</option>\n\
            <option value="DAY-I">DAY-I</option>')
                }
            }
            var angle = 0;
            function rotate() {
                angle += 90;
                $('#imageview').css('transform', 'rotate(' + angle + 'deg)');
            }
            function firstLettterCheck(val, id) {
                var firstChar = val.charAt(0);
                var letters = /^[A-Za-z]+$/;
                if (firstChar.match(letters)) {
                    return true;
                } else {
                    alert("First Character Must be Letter");
                    $("#" + id).val("");
                    return false;
                }
            }
            function getSSCDetails() {
                $("#interflag").val("N")
                $("#sscflag").val("N")
                $("#qualificationdiv").hide();
                $("#sscdiv").hide();
                $("#sscyeardiv").hide();
                $("#interdiv").hide();
                $("#interhallticket").val("");
                $("#intermonth").val("0");
                $("#interyear").val("0");
                $("#sschallticket").val("");
                $("#qualification").val("0");
                $("#sscyear").val("0");
                if ($("#grade1").val() == "TEL" || $("#grade1").val() == "TTL" || $("#grade1").val() == "THL" || $("#grade1").val() == "SEL" || $("#grade1").val() == "STL") {
                    $("#sscdiv").show();
                    $("#sscyeardiv").show();
                    $("#noDiv").hide();
                    $("#brDiv").show();
                } else if ($("#grade1").val() == "TTH" || $("#grade1").val() == "TEH" || $("#grade1").val() == "THH") {
                    $("#qualificationdiv").show();
                    $("#sscdiv").hide();
                    $("#sscyeardiv").hide();
                }
                else {
                    $("#interflag").val("N")
                    $("#sscflag").val("N")
                    $("#qualificationdiv").hide();
                    $("#sscdiv").hide();
                    $("#sscyeardiv").hide();
                    $("#interdiv").hide();
                    $("#interhallticket").val("");
                    $("#intermonth").val("0");
                    $("#interyear").val("0");
                    $("#sschallticket").val("");
                    $("#qualification").val("0");
                    $("#sscyear").val("0");
                }
            }
            function getqualification() {
                if ($("#qualification").val() == "SSC") {
//                   $("#qualificationdiv").show();
                    $("#sscdiv").show();
                    $("#sscyeardiv").show();
                    $("#interdiv").hide();
                    $("#interhallticket").val("");
                    $("#intermonth").val("0");
                    $("#interyear").val("0");
                } else if ($("#qualification").val() == "INTER") {
//                     $("#qualificationdiv").show();
                    $("#interdiv").show();
                    $("#sschallticket").val("");
                    $("#sscyear").val("0");
                    $("#sscdiv").hide();
                    $("#sscyeardiv").hide();
                } else {
                    $("#qualificationdiv").hide();
                    $("#sscdiv").hide();
                    $("#sscyeardiv").hide();
                    $("#interdiv").hide();
                }
            }
              function lowerdivDetails() {
                $("#sscflag").val("N");
                $("#noDiv").hide();
                $("#brDiv").show();
            }
        </script>
    <div class="page-title title-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Registration </h1>
                    <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">

                        <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                        <span>Registration</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br/>
    <body>
        <div class="row mainbodyrow">
            <div class="container">
                <div class="row">

                    <h3 class="block-head-News">TWSH REGISTRATION FORM</h3>
                    <div class="line-border"></div>
                    <br>
                    <div class="row">
                        <div class=" col-md-12">
                            <!--<div class="login-form">-->
                            <div>
                                <html:form action="/register" styleId="d" method="post" enctype="multipart/form-data">
                                    <html:hidden property="mode"/>
                                    <html:hidden property="statusresult"/>
                                    <logic:present name="result2">
                                        <span class="msg"><center> <font color="green" style="font-weight: bold">${result2}</font>  </center> </span>
                                            </logic:present>
                                            <logic:present name="result">
                                        <span class="msg"><center> <font color="red" style="font-weight: bold">${result}</font></center></span>
                                    </logic:present><br>
                                    <div id="startDiv">
                                        <div class="col-md-12">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Did you appear for TWSH in any of Previous sessions?<font color="red">*</font> </label>
                                                <div class="col-sm-8">
                                                    <html:radio styleId="statusflag" property="statusflag" value="1" onclick="showStatus()"/>Yes
                                                    <html:radio styleId="statusflag" property="statusflag" value="2" onclick="showStatus()"/>No
                                                    <!--<input type="text"  class="form-control-plaintext" value="" placeholder="Enter Name">-->
                                                    <br/><br/><br/><br/>
                                                    <br/><br/><br/><br/>
                                                    <br/><br/><br/><br/>
                                                    <br/><br/><br/><br/>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="yesDiv">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Old Registration No<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="hallticket" id="hallticket"  class="form-control"  placeholder="Enter Registration Number" maxlength="20" onkeydown="return space(event, this);">

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Aadhar No<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <input type="text" name="aadhar2" id="aadhar2"  class="form-control"  placeholder="Enter Aadhar Number" maxlength="12"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"/>

                                                    </div>
                                                </div>
                                            </div>

                                            <%-- <div class="col-md-6">
                                                 <div class="form-group row">
                                                     <label for="name" class="col-sm-4 col-form-label">Select Examination Grade<font color="red">*</font></label>
                                                     <div class="col-sm-8">
                                                         <html:select property="grade2" styleId="grade2" styleClass="form-control">
                                                 <html:option value="0">--Select Grade--</html:option> 
                                                 <html:optionsCollection property="gradeslist" label="gname" value="gcode"/>
                                             </html:select>
                                                     </div>
                                                 </div>
                                             </div>--%>
                                            <div class="col-md-2">
                                                <div class="form-group row">
                                                    <input type="button" onclick="getData();" value="SUBMIT" class="btn btn-primary"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="aadharDiv">
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Aadhar Number<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text  styleClass="form-control" property="aadhar1" styleId="aadhar1" maxlength="12"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);" onchange="validateAdharDetails1();"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Examination Grade<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:select property="grade1" styleId="grade1" styleClass="form-control" onchange="getSSCDetails(),validateAdharDetails1();">
                                                        <html:option value="0">--Select Grade--</html:option> 
                                                        <html:optionsCollection property="gradeslist" label="gname" value="gcode"/>
                                                    </html:select>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="qualificationdiv">
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Select Qualification<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:select property="qualification" styleId="qualification" styleClass="form-control" onchange="getqualification(),validateAdharDetails1();">
                                                            <html:option value="0">--Select Qualification--</html:option> 
                                                            <html:option value="SSC">SSC</html:option> 
                                                            <html:option value="INTER">INTERMEDIATE</html:option> 
                                                        </html:select>   
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="sscdiv">
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">SSC<font color="red">(OR)</font>Equivalent HallTicket<font color="red">*</font></label>
                                                    <div class="col-sm-8">
                                                        <html:hidden property="sscflag" styleId="sscflag" value="N"/>
                                                        <html:text  styleClass="form-control" property="sschallticket" styleId="sschallticket" maxlength="12"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);" onchange="lowerdivDetails(),validateAdharDetails1();"/>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="sscyeardiv">
                                            <div class="col-md-4">
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Passout Year<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:select property="sscyear" styleId="sscyear" styleClass="form-control" onchange="lowerdivDetails(),validateAdharDetails1();">
                                                        <html:option value="0">--Select Year--</html:option>
                                                        <html:option value="2021">2021</html:option>
                                                        <html:option value="Previous">Before 2021</html:option>
                                                    </html:select>
                                                </div>
                                            </div>
                                        </div>
                                    <div id="interdiv">
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Intermediate Hallticket<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:text property="interhallticket" styleId="interhallticket" styleClass="form-control"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);" onchange="validateAdharDetails1()"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Passout Month<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:select property="intermonth" styleId="intermonth" styleClass="form-control" onchange="validateAdharDetails1()">
                                                        <option value="0">--Select Month--</option>
                                                        <html:option value="March">March</html:option>
                                                        <html:option value="June">June</html:option>
                                                    </html:select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group row">
                                                <label for="name" class="col-sm-4 col-form-label">Passout Year<font color="red">*</font></label>
                                                <div class="col-sm-8">
                                                    <html:select property="interyear" styleId="interyear" styleClass="form-control" onchange="validateAdharDetails1()">
                                                        <option value="0">--select Year--</option>
                                                        <%
                                                                for (int a = 2020; a > 2010; a--) {%>
                                                        <html:option value="<%=Integer.toString(a)%>"><%=Integer.toString(a)%></html:option>
                                                        <% }
                                                        %>

                                                    </html:select>    
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <!--                                        <div class="col-md-2">
                                    
                                                                                <div class="form-group row">
                                                                                    <center> <input type="button" onclick="validateAdharDetails1();" value="SUBMIT" class="btn btn-primary"/></center>
                                    
                                                                                </div>
                                    
                                                                            </div>-->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="brDiv">
                    <br/><br/><br/><br/>
                    <br/><br/><br/><br/>
                    <br/><br/><br/><br/>
                    <br/><br/><br/><br/>
                </div>
                <div id="noDiv">
                    <p class="p_textred" style="float: right; color: #f00;">All uploads must be in  JPG Format <br/>File size should not be more than 1MB<b>(Except Photo & Signature)</b><br/>
                        Registered  Mobile Number and e-Mail will be used for all future communications </p>
                    <br/>
                    <br/>
                    <h5 class="block-head-News">Personal Details (As per SSC Certificate)</h5>
                    <div class="line-border"></div>
                    <div class="overlay"></div>
                    <div id="dialog" title="View Photo">
                        <center><img class="toggle" id="imageview" src="#"  width="500" height="350"/></center>
                        <br/>
                        <br/>
                        <!--<center> <button class="btn btn-success" style="text-align:center;margin: 10px;" onclick="return rotate();">Rotate <i class="fa fa-rotate-right"></i></button</center>-->


                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Name<font color="red">*</font> </label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="bname" styleId="bname" maxlength="50" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Father Name<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="fname" styleId="fname" maxlength="50" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Date of Birth<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <div class="dateDiv1">
                                        <html:text property="dob1" styleId="dob1" styleClass="form-control" readonly="true" />
                                    </div>
                                    <div class="dateDiv">
                                        <html:text property="dob" styleId="dob" styleClass="form-control" readonly="true" />
                                        <Script>
            $(document).ready(function() {
                var today = new Date();
                yrRange = '1960' + ":" + '2009';
                $("#dob").datepicker({
                    dateFormat: 'dd/mm/yy',
                    changeMonth: true,
                    maxDate: new Date(2009, 08 - 1, 01),
                    minDate: new Date(1960, 12 - 04, 1),
                    yearRange: yrRange,
                    changeYear: true
                });
            });
                                        </Script>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div id="gendercaste">
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Gender<font color="red">*</font> </label>
                                    <div class="col-sm-8">
                                        <html:select property="gender" styleId="gender" styleClass="form-control">
                                            <html:option value="0">--Select Gender--</html:option>
                                            <html:option value="MALE">MALE</html:option>
                                            <html:option value="FEMALE">FEMALE</html:option>
                                            <html:option value="TRANSGENDER">TRANSGENDER</html:option>
                                        </html:select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Community <font color="red">*</font></label>
                                    <div class="col-sm-8">
                                        <html:select property="caste" styleId="caste" styleClass="form-control">
                                            <html:option value="0">--Select Caste--</html:option>
                                            <html:option value="OC">OC</html:option>
                                            <html:option value="OC(EWS)">OC(EWS)</html:option>
                                            <html:option value="BC-A">BC-A</html:option>
                                            <html:option value="BC-B">BC-B</html:option>
                                            <html:option value="BC-C">BC-C</html:option>
                                            <html:option value="BC-D">BC-D</html:option>
                                            <html:option value="BC-E">BC-E</html:option>
                                            <html:option value="SC">SC</html:option>
                                            <html:option value="ST">ST</html:option>
                                        </html:select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="gendercaste1">
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Gender<font color="red">*</font> </label>
                                    <div class="col-sm-8">
                                        <html:text property="gender1" styleId="gender1" styleClass="form-control"  readonly="true"/>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Community <font color="red">*</font></label>
                                    <div class="col-sm-8">
                                        <html:text property="caste1" styleId="caste1" styleClass="form-control" readonly="true"/>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Are you Visually Impaired?<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:select property="blind" styleId="blind" styleClass="form-control" onchange="blindStatus()">
                                        <html:option value="0">--Select Blind Status--</html:option>
                                        <html:option value="YES">YES</html:option>
                                        <html:option value="NO">NO</html:option>
                                    </html:select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="blindDiv" >
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">SADAREM No<font color="red">*</font> </label>
                                    <div class="col-sm-8">
                                        <html:hidden property="blindflag" styleId="blindflag" value="N"/>
                                        <html:text property="blindno" styleId="blindno" styleClass="form-control" maxlength="17" onchange="return sadaremstatus()" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"/>


                                        <!--<textarea rows="3" class="form-control"></textarea>-->

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="blindDiv1" >
                            <div class="col-md-8">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Upload Certificate(should not be more than 1MB)<font color="red">*</font> </label>
                                    <div class="col-sm-8">
                                        <html:file property="blindfile" styleId="blindfile" onchange="return CheckfilePdfOrOther('blindfile');"/><a href="#" onclick="fileupload(event, 'blindfile')"/>View File</a>

                                        <!--<textarea rows="3" class="form-control"></textarea>-->

                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br/>
                    <h5 class="block-head-News">Examination Details</h5>
                    <div class="line-border"></div>
                    <div class="row">
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Institution<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="institute" styleId="institute" value="PrivateCandidate" readonly="true"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Examination Appearing<font color="red">*</font> </label>
                                <div class="col-sm-8">
                                    <html:hidden property="examination" styleId="examination"/>
                                    <html:text property="examinationname" styleId="examinationname" styleClass="form-control" readonly="true"/>
                                    <%-- <html:select property="examination" styleId="examination" styleClass="form-control" onchange="examinationStatus();examinationDistricts()">
                                         <html:option value="0">--Select--</html:option>
                                         <html:option value="TW">Typewriting</html:option>
                                         <html:option value="SH">Shorthand</html:option>
                                     </html:select>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Language<font color="red">*</font> </label>
                                <div class="col-sm-8">
                                    <html:hidden property="language" styleId="language"/>
                                    <html:text property="languagename" styleId="languagename" styleClass="form-control" readonly="true"/>
                                    <%-- <html:select property="language" styleId="language" styleClass="form-control" onchange="languageStatus()">
                                         <html:option value="0">--Select--</html:option>
                                     </html:select>--%>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Grade<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:hidden property="grade" styleId="grade"/>
                                    <html:text property="gradename" styleId="gradename" styleClass="form-control" readonly="true"/>
                                    <%--  <html:select property="grade" styleId="grade" styleClass="form-control" onchange="fileStatus(),dateChange(),statusexam()">
                                          <html:option value="0">--Select--</html:option>
                                      </html:select>--%>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Examination District <font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <%--     <html:hidden property="apdistrict" styleId="apdistrict" value="${sessionScope.distcd}"/>
                                         <html:text property="apdistrict1" styleId="apdistrict1" styleClass="form-control" value="${sessionScope.distname}" readonly="true"/>
                                    --%>
                                    <html:select property="apdistrict" styleId="apdistrict" styleClass="form-control" onchange="centersList();">
                                        <html:option value="0">--Select District--</html:option> 
                                        <html:optionsCollection property="distLists" label="district_Name" value="district_ID"/>
                                    </html:select>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Examination Center<font color="red">*</font> </label>
                                <div class="col-sm-8">
                                    <%-- <html:hidden property="ecenter" styleId="ecenter" value="${sessionScope.ccode}"/>
                                     <html:text property="ecenter1" styleId="ecenter1" styleClass="form-control"  value="${sessionScope.cname}" readonly="true"/>
                                    --%>
                                    <html:select property="ecenter" styleId="ecenter" styleClass="form-control">
                                        <html:option value="0">--Select--</html:option>
                                    </html:select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Examination Date<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:select property="edate" styleId="edate" styleClass="form-control" onchange="batchStatus()">
                                        <html:option value="0">--Select Examination Date--</html:option>
                                        <html:option value="2021-05-01">DAY-I</html:option>
                                        <html:option value="2021-05-02">DAY-II</html:option>
                                    </html:select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Examination Batch<font color="red">*</font> </label>
                                <div class="col-sm-8">
                                    <html:select property="ebatch" styleId="ebatch" styleClass="form-control" onchange="getTimeStatus()">
                                        <html:option value="0">--Select Batch--</html:option> 
                                    </html:select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4" id="lowdiv">

                            <div class="form-group row">
                                <div class="viifile" > <label for="name" class="col-sm-4 col-form-label">Upload VII Bonafide Certificate<font color="red">*</font></label></div>
                                <div class="xfile" > <label for="name" class="col-sm-4 col-form-label">Upload SSC<font color="red">(OR)</font><br/>Equivalent Certificate<font color="red">*</font></label></div>
                                <div class="highfile3" > <label for="name" class="col-sm-4 col-form-label">Upload SSC Bonafide Certificate<font color="red">*</font></label></div>
                                <div class="hsfile" > <label for="name" class="col-sm-4 col-form-label">Upload Higher Grade Certificate<font color="red">*</font></label></div>
                                <div class="col-sm-8">
                                    <html:file property="upload1" styleId="upload1" onchange="return CheckfilePdfOrOther('upload1');"/><a href="#" onclick="fileupload(event, 'upload1')"/>View File</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div id="lowergradediv">
                            <div class="col-md-4">

                                <div class="form-group row">
                                    <label for="name" class="col-sm-4 col-form-label">Upload LowerGrade <div class="highfile5"><font color="red">(OR)</font>DCCP</div> Certificate<font color="red">*</font></label>
                                    <div class="col-sm-8">
                                        <html:file property="upload2" styleId="upload2" onchange="return CheckfilePdfOrOther('upload2');"/><a href="#" onclick="fileupload(event, 'upload2')"/>View File</a>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div id="sscgradediv">
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <div class="highfile"> <label for="name" class="col-sm-4 col-form-label">Upload SSC  Certificate<font color="red">*</font></label></div>
                                    <div class="highfile1"> <label for="name" class="col-sm-4 col-form-label"><font color="red">(OR)</font>Upload Graduation  Certificate<font color="red">*</font></label></div>
                                    <div class="col-sm-8">
                                        <html:file property="upload3" styleId="upload3" onchange="return CheckfilePdfOrOther('upload3');"/><a href="#" onclick="fileupload(event, 'upload3')"/>View File</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div id="highdiv">
                            <div class="col-md-4">
                                <div class="form-group row">
                                    <div class="highfile"> <label for="name" class="col-sm-4 col-form-label"><font color="red">(OR)</font>Upload Intermediate Certificate<font color="red">*</font></label></div>
                                    <div class="highfile2"> <label for="name" class="col-sm-4 col-form-label">Upload Intermediate Certificate<font color="red">*</font></label></div>
                                    <div class="highfile1"><label for="name" class="col-sm-4 col-form-label"><font color="red">(OR)</font>Upload Vocational Certificate<font color="red">*</font></label></div>
                                    <div class="col-sm-8">
                                        <html:file property="upload4" styleId="upload4" onchange="return CheckfilePdfOrOther('upload4');"/><a href="#" onclick="fileupload(event, 'upload4')"/>View File</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <h5 class="block-head-News">Communication Details</h5>
                    <div class="line-border"></div>
                    <div class="row">
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">House No<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="houseno" styleId="houseno" maxlength="250" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Street<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="locality" styleId="locality" maxlength="250" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');" onchange="firstLettterCheck(this.value,this.id)"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Village/Town<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="village" styleId="village" maxlength="100" onkeypress="return inputLimiter(event, 'HouseNo');" onkeyup="this.value = this.value.replace(/[^1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/\-\:]/g, '');" onchange="firstLettterCheck(this.value,this.id)"/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">State<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:select property="state" styleId="state" styleClass="form-control" onchange="getDistricts()">
                                        <html:option value="0">--Select State--</html:option> 
                                        <html:optionsCollection property="stateList" label="sname" value="scode"/>
                                    </html:select>
                                </div>
                            </div>
                        </div>
                        <div id="DistrictDiv" class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">District<font color="red">*</font> </label>
                                <div class="col-sm-8">
                                    <html:select property="district" styleId="district" styleClass="form-control" onchange="getMandals()">
                                        <html:option value="0">--Select--</html:option>
                                    </html:select>
                                </div>
                            </div>
                        </div>
                        <div id="MandalDiv" class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Mandal <font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:select property="mandal" styleId="mandal" styleClass="form-control" >
                                        <html:option value="0">--Select--</html:option>
                                    </html:select>
                                </div>
                            </div>
                        </div>
                        <div id="DistrictDiv1" class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">District<font color="red">*</font> </label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="district1" styleId="district1" maxlength="100" onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                </div>
                            </div>
                        </div>
                        <div id="MandalDiv1" class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Mandal<font color="red">*</font> </label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="mandal1" styleId="mandal1"   maxlength="100"  onkeydown="return space(event, this);" onkeypress="return inputLimiter(event, 'Letters');" onkeyup="this.value = this.value.replace(/[^ ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz]/g, '');"/>
                                </div>
                            </div>
                        </div>


                    </div>
                    <div class="row">
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Pincode<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="pincode" styleId="pincode" maxlength="6" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"  onchange="isPincode()"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Mobile No<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="mobile" styleId="mobile"  onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);"  onchange="telephoneValidation(),adharDuplicatevalidation()" maxlength="10"/>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4">

                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">eMail<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="email" styleId="email"  onkeydown="return space(event, this);" maxlength="100" onchange='return isEmail(this)'/>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Aadhar Number<font color="red">*</font></label>
                                <div class="col-sm-8">
                                    <html:text  styleClass="form-control" property="aadhar" styleId="aadhar" value="${aadhar}" maxlength="12"  onchange="validateAdharDetails(),adharDuplicatevalidation1()" onkeyup="this.value = this.value.replace(/[^0-9]/g, '');" onkeypress="return onlyNumbers(event);" onkeydown="return space(event, this);" readonly="true"/>
                                </div>
                            </div>
                        </div>


                        <div class="col-md-4">
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Upload Photo  (should not be more than 40KB)<font color="red">*</font></label>
                                <div class="col-sm-8">                                           
                                    <html:file property="photo" styleId="photo" onchange="return CheckfilePdfOrOther40kb('photo');"/>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group row">
                                <div class="col-sm-8">                                           
                                    <center>  <img id="myImg" src="#"  width="100" height="70"/></center>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="form-group row">
                                <label for="name" class="col-sm-4 col-form-label">Upload Signature (should not be more than 40KB)<font color="red">*</font></label>
                                <div class="col-sm-8">                                           
                                    <html:file property="signature" styleId="signature" onchange="return CheckfilePdfOrOther40kb('signature');"/><a href="#" onclick="fileupload(event, 'signature')"/>View File</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/>
                    <div>  <input type="checkbox" id="declaration" style="max-width: 15px;max-height: 15px;font-size:24px;"/>&nbsp;&nbsp;&nbsp;I promise to abide by the rules/regulations and the orders of the SBTET, its Authorities and Officers.I do hereby declare that the information furnished in this application is true to the best of my knowledge and belief.
                        I am aware that in the event of any information being found to be false or untrue, I shall be liable to such action by SBTET.
                        <br/>
                    </div>
                    <br/>
                    <br/>
                    <div class="row">
                        <div class="col-md-12 center">

                            <center>   <input type="button" onclick="return SubmitForm();" value="SUBMIT"  class="submit"/></center>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
            </html:form>
        </div>
    </div> 
</div>
</div>
</div>	
</div>
</body>
</html>
