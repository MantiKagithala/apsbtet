<%-- 
    Document   : Teachersprint
    Created on : Jun 5, 2018, 7:39:44 PM
    Author     : 1042564
--%>
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>

<%
    int i = 1;
    String path = request.getContextPath();
    String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

%>
<html>
    <head>
        <script src="./js/jquery.min.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <link rel ="stylesheet" href="css/jquery-ui.css">

        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table.altrowstable1 th {
                background-color: #c1d2e2 !important;
                border: 1px #1e2ab7 solid !important;
                font-size: 13px !important;
                color: #000000 !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;
            }

            table.altrowstable1 td {
                text-align: left;
                border: 1px #1e2ab7 solid !important;
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                background: #e6e8ff;

            }

            input {
                width: 90% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }
            tr.payrad input[type="radio"] {
                float: left;
                text-align: left !important;
                width: 19% !important;
            }
            input[type="radio"], input[type="checkbox"], input[type="submit"]
            {
                width: 88px !important;
                float: center;
            }

            select {
                border-radius: 0;
                margin-bottom: 12px !important;
                border: 1px solid #005396;
                box-shadow: none;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .btn
            {
                padding: 6px 12px !important;
                width: auto !important;
            }
             .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .line-border {
                border-top: 1px solid #0063be;
                width: 91px;
                position: relative;
                top: -21px;
            }
        </style>
        <script type="text/javascript">
            function space(evt, thisvalue)
            {
                var number = thisvalue.value;
                var charCode = (evt.which) ? evt.which : event.keyCode
                if (number.length < 1) {
                    if (evt.keyCode == 32) {
                        return false;
                    }
                }
                return true;
            }
            function telephoneValidation() {

                var mobileNo = $("#mobile").val();
                var len = mobileNo.length;
                if (len < 10) {
                    alert("Mobile Number Should be 10 digits");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (($("#mobile").val().trim().charAt(0) != '9'
                        && $("#mobile").val().trim().charAt(0) != '8' &&
                        $("#mobile").val().trim().charAt(0) != '7' &&
                        $("#mobile").val().trim().charAt(0) != '6')) {
                    alert("Please Enter valid Mobile Number");
                    $("#mobile").val("");
                    $("#mobile").focus().css({'border': '1px solid red'});
                    return false;
                }
            }

            function inputLimiter(e, allow) {
                var AllowableCharacters = '';
                if (allow == 'Letters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'Letters1') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.-/:';
                }
                if (allow == 'Numbers') {
                    AllowableCharacters = '1234567890';
                }
                if (allow == 'landline') {
                    AllowableCharacters = '1234567890-';
                }
                if (allow == 'NameCharacters') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-.\'';
                }
                if (allow == 'NameCharactersAndNumbers') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz-\'/';
                }
                if (allow == 'website') {
                    AllowableCharacters = ' ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz./:';
                }
                if (allow == 'HouseNo') {
                    AllowableCharacters = '1234567890 ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/-:';
                }
                if (allow == 'DistrictExp') {
                    AllowableCharacters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
                }
                if (allow == 'ogpaExp') {
                    AllowableCharacters = '1234567890.';
                }
                var k = document.all ? parseInt(e.keyCode) : parseInt(e.which);
                if (k != 13 && k != 8 && k != 0) {
                    if ((e.ctrlKey == false) && (e.altKey == false)) {
                        return (AllowableCharacters.indexOf(String.fromCharCode(k)) != -1);
                    } else {
                        return true;
                    }
                } else {
                    return true;
                }
            }




            function onlyNumbers(evt) {
                var charCode = (evt.which) ? evt.which : event.keyCode;
                if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                    alert("Allow Numbers Only");
                    return false;
                }
                return true;
            }
            $(document).ready(function() {
                $("form").attr('autocomplete', 'off');
//                $('input[type=text], textarea').bind("cut copy paste", function(e) {
//                    alert("Cut copy paste not allowed here");
//                    e.preventDefault();
//                });
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
                $("body").on("contextmenu", function(e) {
                    return false;
                });
                $("#aadhaarDiv").show();
                $("#dataDiv").hide();
                $("#agricultural").change(function() {
                    if ($("#agricultural").val() == "1") {
                        $("#agriculturalDiv").show();
                    } else {
                        $("#agriculturalDiv").hide();

                    }


                })
            });

        </script>
        <script type="text/javascript">
            function blockSpecialChar(e) {
                var k = e.keyCode;
                if (k == 36 || k == 44 || k == 39 || k == 38) {
                    return false;
                }

            }
            function SubmitData() {
                if ($("#hallticket").val() === undefined || $("#hallticket").val() === "" && $("#hallticket").val().length < 6) {
                    alert("Please Enter Aadhar Number");
                    $("#hallticket").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#dob").val() === undefined || $("#dob").val() === "") {
                    alert("Select  Date of Birth");
                    $("#dob").focus().css({'border': '1px solid red'});
                    return false;
                } else {
                    document.forms[0].mode.value = "otpPage";
                    document.forms[0].submit();
                }
            }
            function screenSize() {
                var width = parseInt(screen.width);
                //  $('#scren').text("S.W ->"+width);
                if ((width >= 800)) {
                    $('#aadhaarDiv').show();
                    $('.msg-cce').hide();
                } else {
                    $('#aadhaarDiv').hide();
                    $('#aadhaarDiv1').hide();
                    $('.msg-cce').show();
                }
            }
            function validateOtp() {
                if ($("#otp").val() == "") {
                    alert("Please Enter OTP Number");
                    $("#otp").focus().css({'border': '1px solid red'});
                    return false;
                }
                document.forms[0].mode.value = "print";
                document.forms[0].submit();
            }
        </script>
    </head>
    <body onload="screenSize();">
        <div class="msg-cce" style="display:none">
            <center>
                <br><br>
                <span style="font-size: 150px; border: 15px solid #a94442; border-radius: 50%; 
                      color: #a94442; padding: 20px; margin: 100px !important;">X</span>
                <br><br>
                <h1 style="color: #a94442; border: none;">          
                    These Services are not Available in Small Screen Devices(Mobiles/Tabs), Please Use Desktop's Only.
                </h1> 
            </center>
            <br><br><br><br><br><br>

        </div>
        <div class="row mainbodyrow">
            <div class="container">
                <div class="col-xs-12">
                    <div class="maindodycnt">
                        <h3 class="block-head-News">Print For Registration Form</h3>
                        <div class="line-border"></div>

                        <html:form action="/registeredFormPrint"  method="post" enctype="multipart/form-data">
                            <html:hidden property="mode"/>

                            <logic:present name="result">
                                <span id="msg"><center><font color="red" style="font-weight: bold">${result}</font></center></span>
                            </logic:present><br>

                            <logic:present name="applicationPage">
                                <div id="aadhaarDiv">
                                    <table align="center" cellpadding="0" cellspacing="0" border="0" width="100%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                                        <tr>
                                            <th>Aadhar Number<font color="red">*</font></th>
                                            <td> <html:text  property="hallticket" styleId="hallticket" maxlength="12" onkeydown="return space(event,this);" onkeypress='return onlyNumbers(event);'/></td>

                                            <th>Date Of Birth<font color="red">*</font></th>
                                            <td><html:text property="dob" styleId="dob" styleClass="dob" readonly="true"/>
                                                <Script>
             $(document).ready(function() {
                 var today = new Date();
                 yrRange = '1998' + ":" + '2010';
                 $(".dob").datepicker({
                     dateFormat: 'dd/mm/yy',
                     changeMonth: true,
                     maxDate: new Date(2010, 08 - 1, 31),
                     minDate: new Date(1998, 12 - 04, 1),
                     yearRange: yrRange,
                     changeYear: true
                 });
             });
                                                </Script>
                                            </td> 

                                            <td><input type="submit" name="SUBMIT1"  id="SUBMIT1" onclick="return SubmitData();" value="SUBMIT" class="btn btn-primary"/></td> 
                                        </tr>
                                    </table>
                                </div>
                            </logic:present>   
                            <logic:present name="otpPage">
                                <table align="center" cellpadding="0" cellspacing="0" border="0" width="50%" class="altrowstable1" id="altrowstable1" style="margin: 0px auto;">
                                    <tr>
                                        <th>Mobile No : <font color="red">*</font></th>
                                        <td>${maskmobile}
                                            <html:hidden  property="mobile" styleId="mobile" value="${mobile}"/>
                                            <html:hidden  property="hallticket" styleId="hallticket" value="${appno}"/>
                                            <html:hidden property="dob" styleId="dob" value="${dob}"/>
                                        </td>
                                    </tr>  
                                    <tr>
                                        <th>Enter OTP recevied to your registered Mobile No<font color="red">*</font></th>
                                        <td><html:text  property="otp" styleId="otp" maxlength="5" onkeypress='return onlyNumbers(event);' /></td>
                                    </tr>
                                    <tr><td colspan="2"><center><input onkeypress='return onlyNumbers(event);' type="submit" onclick="return validateOtp();" value="Print" /></center></td></tr>
                                </table>
                            </logic:present>
                            <br>
                        </div>
                    </html:form>
                </div>
            </div>
        </div>
    </body>
</html>

