
<%@ taglib uri="/WEB-INF/struts-html.tld" prefix="html"%>
<%@ taglib uri="/WEB-INF/struts-logic.tld" prefix="logic"%>
<html>
    <head>


        <script src="./js/jquery.min.js"></script>
        <script src="./js/GenerateAadharCardValidation.js"></script>
        <script src ="./js/jquery.js"></script>
        <Script language="JavaScript" src="./js/jquery-ui.js"></script>
        <!--<link rel ="stylesheet" href="css/jquery-ui.css">-->

        <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css"/>
        <link rel="stylesheet" href="css/bootstrap.css" type="text/css"/>
        <!--<link rel="stylesheet" href="css/styles1.css" type="text/css"/>-->
        <link rel="stylesheet" href="css/dataTables.bootstrap.min.css" type="text/css"/>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 
        <script src="js/jquery.dataTables.min.js" type="text/javascript"></script> 
        <script src="js/dataTables.bootstrap.min.js" type="text/javascript"></script>  

        <script type="text/javascript">

            $(document).ready(function() {
                $('#example').DataTable({
                    "scrollX": true,
                });

            });

        </script>
        <style type="text/css">
            table {
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }
            table thead th {
                background-color: #0865a8!important;
                /*border: 1px #001d31 solid !important;*/
                font-size: 13px !important;
                color: #ffffff !important;
                border-collapse: collapse !important;
                border-spacing: 0px !important;
                text-align: left !important;
                padding: 5px 15px;
                word-break: keep-all;
                white-space: nowrap;

            }
            table tbody td {
                text-align: left;
                /*border: 1px #1e2ab7 solid !important;*/
                vertical-align: middle;
                padding-left: 3px;
                padding-right: 3px;
                font-size: 13px;
                font-family: verdana;
                font-weight: normal;
                height: 20px;
                padding: 7px;
                /*background: #e6e8ff;*/
                border-collapse: collapse !important;
                border-spacing: 0px !important;
            }

            input {
                width: 8% !important;
                padding: 3px !important; 
            }

            tr.payrad td {
                text-align: left !important;
            }

            .form-control{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            #example_wrapper {
                width: 100% !important;
                padding: 20px;
            }
            table.dataTable thead th, table.dataTable tbody td {
                padding: 8px 10px;
                border: 1px solid #ccc;
                font-size: 13px;
            }
            .dataTables_filter{
                padding-right:56px;
            }
        </style>
        <Style>
            .block-head-News {
                position: relative;
                margin: 0 0 20px 0;
                padding: 0;
                border-bottom: 1px #e2e2e2 solid;
                font-weight: normal;
                text-transform: none;
                font-size: 22px;
                transition: all linear 300ms;
                overflow: hidden;
                cursor: pointer;
                padding-bottom: 15px;
                color: #06447d;
                font-weight: 700;
            }
            .form-control-plaintext{
                display: block;
                width: 100% !important;
                height: 34px;
                padding: 6px 12px;
                font-size: 14px;
                line-height: 1.42857143;
                color: #555;
            }
            .submit
            {
                background: #080524;
                padding: 14px 20px;
                color: #fff !important;
                font-size: 20px;
            }
        </style>
        <style type="text/css">
            .navbar-inverse{
                background-color: transparent !important;
                border-color: transparent !important;
            }
            .navbar-brand
            {
                padding: 0px !important;
            }
            .navbar-inverse .navbar-nav > li > a
            {
                color: #0865a8 !important;
            }
            .nav-border
            {
                margin-top: 37px !important;
                position: inherit !important;
            }
            .title-1
            {
                margin-top: 0px !important;
            }
        </style>
        <script type="text/javascript">
            $(document).ready(function() {
                $("form").attr('autocomplete', 'off');
                var seconds = 5;
                setTimeout(function() {
                    if (document.getElementById("msg") !== null)
                        document.getElementById("msg").style.display = "none";
                }, seconds * 1000);
            });

            function getData() {
                if ($("#aadhar1").val() == "") {
                    $("#aadhar1").val("");
                    alert("Please Enter Registration No");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#grade1").val().length != 10) {
                    $("#grade1").val("");
                    alert("Please Enter Valid Registered Mobile Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if ($("#grade1").val() == "0" || $("#grade1").val() == "") {
                    alert("Please Enter Registered Mobile Number");
                    $("#grade1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else {
                    document.forms[0].mode.value = "getDownloadHallticket";
                    document.forms[0].submit();
                }
            }

            function validateAdharDetails1() {
                if ($("#aadhar1").val() == "") {
                    $("#aadhar1").val("");
                    alert("Please Enter Aadhaar Card Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                } else if ($("#aadhar1").val().length != 12) {
                    $("#aadhar1").val("");
                    alert("Aadhaar Card Number should be 12 digit number.");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                } else if ($("#aadhar1").val() == "999999999999" || $("#aadhar1").val() == "333333333333") {
                    $("#aadhar1").val("");
                    alert("Please Enter Valid Aadhaar Card Number");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
                else if (validateVerhoeff($("#aadhar1").val()) != true) {
                    $("#aadhar1").val("");
                    $("#aadhar1").focus().css({'border': '1px solid red'});
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div class="page-title title-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Institutions</h1>
                        <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">
                            <a href="Welcome.do">Home</a><span class="line-separate">/</span>
                            <span>Institutions</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="feedback-form">
            <div class="container">
                <div class="row">
                    <h3 class="block-head-News">Institutions</h3>
                    <div class="line-border"></div>
                    <div class="row">
                        <div class=" col-md-16">
                            <html:form action="/instituteMaster"  styleId="d" method="post" enctype="multipart/form-data">
                                <html:hidden property="mode"/>
                                <logic:present name="result2">
                                    <span id="msg"><center> <font color="green" style="font-weight: bold">${result2}</font></center></span>
                                        </logic:present>
                                        <logic:present name="result1">
                                    <span id="msg"><center><font color="red" style="font-weight: bold">${result1}</font></center></span>
                                </logic:present><br>
                                <%int i = 0;%>
                                <logic:present  name="listData">
                                    <div style="text-align: right; margin-bottom: -8px;padding-right: 36px;">
                                        <a href="./instituteMaster.xls?mode=getDownloadPdf" ><img src="img/pdf.png" style="width: 35px"/></a>
                                    </div>
                                    <table id="example" class="table table-striped table-bordered dataTable no-footer" cellspacing="0" width="100%">
                                        <thead>
                                            <tr>

                                                <th>S.NO</th>
                                                <th>Institute Code</th>
                                                <th>Recog Period</th>
                                                <th>Institute Name</th>
                                                <th>Address</th>
                                                <th>Town</th>
                                                <th>District Name</th>
                                                <th>Pin Code</th>
                                                <th>Principle Name</th>
                                                <th>Mobile</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <logic:iterate name="listData" id="list">
                                                <tr>
                                                    <td style="text-align: center;"><%=++i%></td>
                                                    <td style="text-align: center;">${list.instCode}</td>
                                                    <td style="text-align: center;">${list.regCode}</td>
                                                    <td style="text-align: left;">${list.instName}</td>
                                                    <td style="text-align: left;">${list.address}</td>     
                                                    <td style="text-align: left;">${list.town}</td>
                                                    <td style="text-align: left;">${list.district}</td>
                                                    <td style="text-align: center;">${list.pincode}</td>
                                                    <td style="text-align: left;">${list.principleName}</td>
                                                    <td style="text-align: center;">${list.mobile}</td>
                                                </tr>
                                            </logic:iterate>
                                        </tbody>
                                    </table>
                                </logic:present>
                            </html:form>                
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </body>
</html>