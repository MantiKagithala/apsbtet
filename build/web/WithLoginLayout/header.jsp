<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>  
<%@page import="java.util.Vector"%>
<%@page import="java.util.HashMap"%>
<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="icon" href="assets/img/aptonline.png" type="image/gif" sizes="32x32">
        <title>::APSBTET::</title>

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/style.css">

        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css"> -->
        <!--<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css'>-->
        <!--<link rel="stylesheet" href="assets/css/slider.css">-->
        <style type="text/css">
            .navbar
            {
                margin-top: 0px;
            }
            .navbar-default
            {
                color: #fff;
                background: #0865a8 !important;
                border-color: transparent;
                border-radius: 0px !important;
            }
            .navbar-default .navbar-nav > li > a {
                color: #fff !important;
            }
            .navbar-default .navbar-nav > .active > a, .navbar-default .navbar-nav > .active > a:hover, .navbar-default .navbar-nav > .active > a:focus
            {
                background: #003f6d !important;
            }
            .dropdown-menu
            {
                background: #0865a8 !important;
            }
            .nav-border
            {
                position: absolute !important;
                top: 84px !important;
            }
            .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
    color: #fff;
    background-color: #0865a8 !important;
}
@media (max-width: 767px){
.navbar-default .navbar-nav .open .dropdown-menu > li > a {
    color: #fff !important;
    font-size: 12px !important;
}
}
.dropdown-menu>li>a
{
    font-size: 12px !important;
}
        </style>
    </head>
    <body>
        <div style="background: url(img/head.jpg);">
            <img src="assets/img/logo.png" style="padding: 10px;">
        </div>
        <div class="nav-border"></div>
        <div class="clearfix"></div>

        <nav class="navbar navbar-default">
            <div class="container">

                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div id="navbar" class="navbar-collapse collapse">
                    <ul class="nav navbar-nav">

                        <%
                            Vector serviceList = (Vector) session.getAttribute("services");
                            if (serviceList != null) {
                                ArrayList menuList = (ArrayList) session.getAttribute("mainList");
                                ArrayList subMenuParentIdsList = (ArrayList) session.getAttribute("subMenuParentIdsList");
                                for (int i = 0; i < menuList.size(); i++) {
                                    HashMap menuMap = (HashMap) menuList.get(i);
                                    String mainParentId = menuMap.get("parentId").toString();
                                    String mainTarget = menuMap.get("target_url").toString();
                                    String mainServiceName = menuMap.get("service_name").toString();
                                    String mainserviceId = menuMap.get("serviceId").toString();
                                    if (mainParentId.equals("0") && !subMenuParentIdsList.contains(mainserviceId)) {
                        %>
                        <li><a href="<%=mainTarget%>"><%=mainServiceName%></a></li>
                            <%} else if (mainParentId.equals("0") && subMenuParentIdsList.contains(mainserviceId)) {%>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><%=mainServiceName%><span class="caret"></span></a>
                            <ul class="dropdown-menu">
                                <%    ArrayList subMenuList = (ArrayList) session.getAttribute("subMenuList");
                                    for (int j = 0; j < subMenuList.size(); j++) {
                                        HashMap subMenuMap = (HashMap) subMenuList.get(j);
                                        String subMenuParentId = subMenuMap.get("parentId").toString();
                                        String subMenuServiceName = subMenuMap.get("service_name").toString();
                                        String subMenuTarget = subMenuMap.get("target_url").toString();
                                        String subMenuserviceId = subMenuMap.get("serviceId").toString();
                                        if (mainserviceId.equals(subMenuParentId) && mainParentId.equals("0")) {
                                %>
                                <li><a href="<%=subMenuTarget%>"><%=subMenuServiceName%></a></li>
                                <%}}%></ul>
                                <%}%>
                                <%}%>

                            <%--    <li class="active"><a href="./home.do">Home</a></li>
                                <li><a href="#">About</a></li>
                                <li><a href="#">Contact</a></li>
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Action</a></li>
                                        <li><a href="#">Another action</a></li>
                                        <li><a href="#">Something else here</a></li>
                                        <li><a href="#">Separated link</a></li>
                                        <li><a href="#">One more separated link</a></li>
                                    </ul>
                                </li>

                        <c:forEach items="${menuList}" var="menu">
                            <c:set var="String" value="${menuList}"/> 
                            <c:set var="menuParentId" value="${menu.parentId}"/> 
                            <c:set var="subMenuParentIds" value="${subMenuParentIds}"/> 
                            <c:if test = "${menu.parentId == 0}">

                                <c:if test="${(subMenuParentIds eq menuParentId)}">
                                    <li><a href="${menu.target_url}">${menu.service_name}</a></li>
                                </c:if>

                                <c:if test="${fn:contains(subMenuParentIds, menuParentId)}">
                                    <li class="dropdown">
                                        <a href="${menu.target_url}" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                            ${menu.service_name} <span class="caret"></span></a>
                                        <ul class="dropdown-menu">
                                            <c:forEach items="${subMenuList}" var="subMenu">
                                                <c:if test="${menu.serviceId eq subMenu.parentId}">
                                                    <li><a href="${subMenu.target_url}">${subMenu.service_name}</a></li>
                                                    <li role="separator" class="divider"></li>
                                                    </c:if>
                                                </c:forEach>
                                        </ul>
                                    </li>
                                </c:if>

                            </c:if>

                        </c:forEach>--%>


                            <%                            } else {
                                    response.sendRedirect("./unAuthorised.do");
                                }
                            %>

                    </ul>

                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

        <script src="assets/js/jquery.min.js"></script>
        <script src="assets/js/bootstrap.min.js"></script>	
        <!--<script src="assets/js/fontjs.js"></script>-->
<!--        <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js'></script>
        <script  src="assets/js/slider.js"></script>-->
        <script>
            jQuery(document).ready(function() {
                $(".dropdown").hover(
                        function() {
                            $('.dropdown-menu', this).stop().fadeIn("fast");
                        },
                        function() {
                            $('.dropdown-menu', this).stop().fadeOut("fast");
                        });
            });
        </script>
    </body>
</html>