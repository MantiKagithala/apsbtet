<%@page import="java.util.Vector"%>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">
        <link rel="icon" href="../favicon.ico">
        <title>:: SBTET ::</title>
        <!-- Fontawesome icon CSS -->

        <link rel="stylesheet" href="assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="css/styles1.css">
        
        <!--<link rel="stylesheet" href="assets/css/style.css" type="text/css">-->
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script type="text/javascript" src="./js/menu/BuildMenu.js"></script>
        <script type="text/javascript" src="./js/menu/MenuLayout.js"></script>

        <%

            String Roleid = "";
            if (session.getAttribute("RoleId") != null) {
                Roleid = session.getAttribute("RoleId") + "".trim();
//                System.out.println("Roleid in header jsp : " + Roleid);
            }
            response.setHeader("Pragma", "no-cache");
            response.setHeader("Cache-Control", "no-cache,no-store, must-revalidate");
            response.addHeader("Cache-Control", "pre-check=0, post-check=0");
            response.setHeader("Expires", "0");
            response.setDateHeader("Expires", 0);
            response.addHeader("Referrer-Policy", "no-referrer");
            response.addHeader("X-Frame-Options", "SAMEORIGIN");
            response.addHeader("Strict-Transport-Security", " max-age=63072000; includeSubDomains; preload");
            response.addHeader("Content-Security-Policy", "default-src https: 'self' 'unsafe-eval' 'unsafe-inline'  image-src 'self' data:; object-src 'none' ");

        %>
        <style>
            @import url('https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700');
            body, .toplabel, .middlebox .value
            {
                font-family: 'Roboto Condensed', sans-serif;
            }
            .table-responsive {
                min-height: .01%;
                overflow-x: initial !important;
            }
            #menu_level_0
            {
                position: fixed !important;

            }
            table td img{
                vertical-align: bottom !important;
            }
            td{
                vertical-align: bottom !important;
            }
        </style>


        <script>
            $('ul.nav li.dropdown').hover(function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
            }, function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
            });
        </script>
    </head>
    <body>

        <div class="row toplinksrow" style="display:none;">
            <div class="container">
                <div class="toplinksline">
                    <aside class="col-md-8">
                        <div class="lawyer-right-section">
                        </div>
                    </aside>

                </div>
            </div>
        </div>

        <div class="row topheader">
            <!--            <div class="container">
                            <div class="col-xs-12 col-sm-7">
                                <img src="images/logo.png" style="max-width:100%" />
                            </div>-->
            <!--                 <div class="col-xs-12 col-sm-5 text-right">
                                <img src="images/right_logo.png" style="max-width:100%; margin-top: 5px;" />
                            </div>-->

            <!--</div>-->
            <div class="container">
                <div class="col-xs-12 col-sm-7">
                    <img src="assets/img/logo.png" style="max-width:100%" />
                </div>
                <!--                <div class="col-xs-12 col-sm-5 text-right">
                                    <img src="images/right_logo.png" style="max-width:100%; margin-top: 5px;" />
                                </div>-->
            </div>
            <div class="row menurow">
                <div class="container">

                    <div class="col-xs-12">
                        <nav class="navbar navbar-inverse" role="navigation">
                            <div class="nav-border"></div>
                            <div class="container-fluid" style="padding:0px">


                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1" style="padding:0px">

                                    <%
                                        Vector serviceList = (Vector) session.getAttribute("services");
                                        if (serviceList != null) {
                                            out.println("<script LANGUAGE=\"JavaScript\">");
                                            for (int i = 0; i < serviceList.size(); i++) {
                                                String servicedesc[] = (String[]) serviceList.elementAt(i);
                                                String serviceId = servicedesc[0];
                                                String parentId = servicedesc[1];
                                                String target_url = servicedesc[2];
                                                String service_name = servicedesc[3];
                                                System.out.println(serviceId + " == " + parentId + " " + target_url + " " + service_name);
                                                out.println("AddMenuItem (" + serviceId + ", " + parentId + ", \"" + target_url + "\", \"" + service_name + "\", \"\");");
                                            }
                                        } else {
                                            //out.println("could not get services");
                                            response.sendRedirect("./unAuthorised.do");
                                        }
                                        out.println("</script>");
                                    %>
                                    <script LANGUAGE="JavaScript" type="text/javascript">
                                        DrawMenu();
                                    </script>

                                </div>
                                <!-- /.navbar-collapse -->
                            </div>
                            <!-- /.container-fluid -->
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        <script src="js/jquery-2.1.1.min.js" type="text/javascript"></script> 
        <script src="js/bootstrap.min.js" type="text/javascript"></script> 

        <script>
            $('ul.nav li.dropdown').hover(function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
            }, function() {
                $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
            });
        </script>
    </body>
</html>