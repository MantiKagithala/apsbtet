<%@ taglib uri="http://struts.apache.org/tags-bean" prefix="bean" %>
<%@ taglib uri="http://struts.apache.org/tags-html" prefix="html" %>
<%@ taglib uri="http://struts.apache.org/tags-logic" prefix="logic" %>
<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <link rel="icon" href="assets/img/aptonline.png" type="image/gif" sizes="32x32">
        <title>:: SBTET ::</title>

        <link rel="stylesheet" href="./assets/css/bootstrap.min.css">
        <link rel="stylesheet" href="./assets/css/font-awesome.min.css">
        <link rel="stylesheet" href="./assets/css/style.css">

        <meta http-equiv="X-UA-Compatible" content="IE=edge">


        <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css"> -->
        <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css'>

        <style>
            .captcha {
                letter-spacing: -1px;
                font-size: 21px;
                font-weight: bold;
                color: rgb(33, 70, 11);
                margin-top: 15px;
                width: 85%;
                margin-left: 16px;
                margin-bottom: 0px;
            }
            .feedback-form{
                padding: 0px 0px; 
            }
            .login-form{
                padding: 20px;
            }
    </style>
  <script src="./js/jquery.min.js"></script>
        <script src ="./js/jquery.js"></script>
    <script src="js/md5/md5.js" type="text/javascript"></script>

    <script>
        <%
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";
        %>
        $(document).ready(function() {
           $("form").attr('autocomplete', 'off');
//                $('input[type=text], input[type=password],textarea').bind("cut copy paste", function(e) {
//                    alert("Cut copy paste not allowed here");
//                    e.preventDefault();
//                });
//            $("input").attr('autocomplete', 'off');
        });

        function validateUser() {
            if ($("#userName").val() == "") {
                alert("Please Enter Username");
                $("#userName").focus();
                return false;
            } else if ($("#password").val() == "") {
                alert("Please Enter Password");
                $("#password").focus();
                return false;
            } else if ($("#txtInput").val() == "") {
                alert("Please Enter Captcha Code");
                $("#txtInput").focus();
                return false;
            } else {
                with (document.forms[0]) {
                    password.value = calcMD5(elements["password"].value);
                    password.value = elements["password"].value;
                    document.forms[0].mode.value = "checkDetails";
                    document.forms[0].submit();
                }
            }
        }
        function onlyNumbers(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                alert("Allows Numbers Only");
                return false;
            }
            return true;
        }
        function space(evt, thisvalue) {
            var number = thisvalue.value;
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (number.length < 1) {
                if (evt.keyCode == 32) {
                    return false;
                }
            }
            return true;
        }

    </script>
</head>
<body>
   <!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
      content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	  <link rel="icon" href="assets/img/aptonline.png" type="image/gif" sizes="32x32">
		<title>:: SBTET ::</title>

	  <link rel="stylesheet" href="assets/css/bootstrap.min.css">
		<link rel="stylesheet" href="assets/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/css/style.css">
	  <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css"> -->
<link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/css/swiper.min.css'>
<link rel="stylesheet" href="assets/css/slider.css">

</head>
<body>

<section class="top-content">
	<div class="container">
		<div class="row">
			<div class="col-md-5">
				<!-- <div class="page_social_icons"> -->
								<!-- <a class="fa fa-facebook" href="#" title="Facebook"></a> -->
								<!-- <a class="fa fa-twitter" href="#" title="Twitter"></a> -->
								
								<!-- <a class="fa fa-instagram" href="#" title="Instagram"></a> -->
								<!-- <a class="fa fa-google" href="#" title="Google"></a> -->
							<!-- </div> -->
							
				<div class="guidelines f-l">
					
					
					<ul class="incrs-font">
						<li><a href="">A+</a></li>
						<li><a href="">A</a></li>
						<li><a href="">A-</a></li>
						<li><a href="">A</a></li>
						<li style="background:#2f363c"><a href="">A</a></li>
						
					</ul>&nbsp;&nbsp;&nbsp;
					<a href="">Screen Reader</a>&nbsp;&nbsp;&nbsp;
					<a href="">Skip to Main Content</a>&nbsp;&nbsp;&nbsp;
					
					
				</div>				
			</div>
			<div class="col-md-7">
				<div class="guidelines">
					
					<!--<a href=""><i class="fa fa-commenting" aria-hidden="true"></i>&nbsp; Feed Back</a>&nbsp;&nbsp;&nbsp;-->
					<!--<a href="officialLogin.do" class="login"><i class="fa fa-lock" aria-hidden="true"></i> &nbsp;Login</a>-->
					
				</div>	
			</div>
		</div>
	</div>
</section>

<!-- <section class="page_topline ls section_padding_top_15 section_padding_bottom_15 columns_padding_0"> -->
				<!-- <div class="container-fluid"> -->
					<!-- <div class="row"> -->
						<!-- <div class="col-sm-2 text-center text-sm-left greylinks"> -->
							<!-- <div class="page_social_icons"> -->
								<!-- <a class="social-icon soc-facebook" href="#" title="Facebook"></a> -->
								<!-- <a class="social-icon soc-twitter" href="#" title="Twitter"></a> -->
								
								<!-- <a class="social-icon soc-instagram" href="#" title="Instagram"></a> -->
								<!-- <a class="social-icon soc-google" href="#" title="Google"></a> -->
							<!-- </div> -->
						<!-- </div> -->
						<!-- <div class="col-sm-10 text-right"> -->

							<!-- <a href="">Screen Reader</a>&nbsp;&nbsp;&nbsp; -->
							<!-- <a href="">Skip to Main Content</a> -->
							<!-- <ul class="incrs-font"> -->
								<!-- <li><a href="">A+</a></li> -->
								<!-- <li><a href="">A</a></li> -->
								<!-- <li><a href="">A-</a></li> -->
								<!-- <li><a href="">A</a></li> -->
								<!-- <li style="background:#2f363c"><a href="">A</a></li> -->
							<!-- </ul> -->
						<!-- </div> -->
						
					<!-- </div> -->
				<!-- </div> -->
			<!-- </section> -->




<nav class="navbar navbar-inverse">
		  <div class="container">
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>                        
			  </button>
			  <a class="navbar-brand" href="#"><img src="assets/img/logo.png"></a>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar">
			  <ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="Welcome.do">Home</a></li>
<!--				<li class=""><a href="#">Govt Orders</a></li>
				<li class=""><a href="#"> Publications</a></li>
				<li class="dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown" href="#"> Eligibility & Forms</a>
                                  <ul class="dropdown-menu">
					<li><a href="register.do">TWSH RegistrationForm</a></li>
					<li><a href="formPrint.do">TWSH RegistrationForm Print</a></li>
					
				  </ul>
                                </li>-->
<!--				<li class="dropdown">
				  <a class="dropdown-toggle" data-toggle="dropdown" href="#">ACT'S <span class="caret"></span></a>
				  <ul class="dropdown-menu">
					<li><a href="#">Acts & Rules</a></li>
					<li><a href="#">RTI Act</a></li>
					<li><a href="#">Citizen Chart</a></li>
					
				  </ul>
				</li>-->
				
				<!--<li class="dropdown menu-large ">	<a href="# " class="dropdown-toggle " data-toggle="dropdown ">Other Links <b class="caret "></b></a>-->	
<!--                    <ul class="dropdown-menu megamenu row ">
                        <li class="col-sm-3 ">
                            <ul>
                                
                                <li><a href="# "> Polytechnic In AP</a>

                                </li>
                                <li class="disabled "><a href="# ">AICTE</a>

                                </li>
                                <li><a href="# ">DTEAP</a>

                                </li>
                                
                                <li><a href="# ">Tenders</a>

                                </li>
                                <li><a href="# "> Downloads</a>

                                </li>
                                <li><a href="# ">Anti Raging</a>

                                </li>
                                <li><a href="# ">Media Releases</a>

                                </li>
                            </ul>
                        </li>
                        <li class="col-sm-3 ">
                            <ul>
                                <li><a href="# ">DTE Telangana</a>

                                </li>
                                <li><a href="# ">Polycet Telangana State</a>

                                </li>
                                <li><a href="# ">ITI Bridge Course</a>

                                </li>
                                <li><a href="# ">Diploma Results</a>

                                </li>
                                <li><a href="# ">Status of Biometric Devices In Private Polytechnic</a>

                                </li>
                                <li><a href="# ">Industrial Training Branch Wise Data</a>
                                </li>
								 <li><a href="# ">Requirement for CISCO training Programmes</a>

                                </li>
                               
                               
                            </ul>
                        </li>
                        <li class="col-sm-3 ">
                            <ul>
								 <li><a href="# ">Teaching experience (Subject wise) ? certain infor</a>

                                </li>
                                <li><a href="# ">PRACTICAL SUBJECTS HANDLING STAFF - DETAILS</a>

                                </li>
                                <li><a href="# ">lndustrial training for 2019-20 details of student</a>

                                </li>
                                <li><a href="# ">CISCO Conference Registration Link</a>

                                </li>
                                <li><a href="# ">Industrial Training Branch & Industry wise Details</a>

                                </li>
                            </ul>
                        </li>
                        <li class="col-sm-3 ">
                            <ul>
                                <li><a href="# ">Industrial Training Students Details from the Prls</a>

                                </li>
                                <li><a href="# ">LEARNING MANAGEMENT SYSTEM</a>

                                </li>
                                <li><a href="# ">POLYCET-2020</a>

                                </li>
                                
                            </ul>
                        </li>
                    </ul>-->
                </li>
				<li class=""><a href="#"> Contact</a></li>
                                        <li class=""><a href="officialLogin.do" class="login"> &nbsp;Login<i class="fa fa-lock" aria-hidden="true"></i></a></li>

				<!-- <li class=""><a href="#">Gallery</a></li> -->
				<!-- <li class=""><a href="#">CSR</a></li> -->
				
			  </ul>
			  <!-- <ul class="nav navbar-nav navbar-right"> -->
				<!-- <li><a href="#"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li> -->
				<!-- <li><a href="#"><span class="glyphicon glyphicon-log-in"></span> Login</a></li> -->
			  <!-- </ul> -->
			</div>
		  </div>
		</nav>
    <div class="nav-border"></div>
    <div class="page-title title-1">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h1 class="fx animated fadeInLeft" data-animate="fadeInLeft">Login </h1>
                    <div class="breadcrumbs main-bg fx animated fadeInUp" data-animate="fadeInUp">
                        <!--<a href="Welcome.do">Home</a><span class="line-separate">/</span>-->
                        <span>Login</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="feedback-form">
        <div class="container">
            <div class="row">

                <div class="row">
                    <div class="col-md-8">
                        <img src="assets/img/login.jpg">
                    </div>
                    <div class="col-md-4">

                        <div class="login-form">
                            <html:form action="/officialLogin" method="post"  enctype="multipart/form-data">
                                <html:hidden property="mode"/>
                                <input type="hidden" id="txtCaptcha" value="<%=request.getAttribute("captchaCode")%>"/>
                                <logic:present name="msg">
                                    <center><font size="3" color="red">${msg}</font></center>
                                </logic:present>
                                <div class="form-group">
                                    <label for="exampleInputEmail1">User Name</label>
                                    <!--<input type="text" class="form-control" placeholder="Enter UserName">-->
                                    <input type="text" name="userName" id="userName" autocomplete="off" onkeydown="return space(event, this);"  placeholder="Enter UserName" class="form-control"/>
                                </div>
                                <div class="form-group">
                                    <label for="exampleInputPassword1">Password</label>
                                    <input type="password" name="password" id="password"  autocomplete="off" onkeydown="return space(event, this);" placeholder="Password" class="form-control"/>
                                </div>
                                <div class="form-group" style="margin-bottom:25px;">

                                    <div id="up1">

                                        <table>
                                            <tbody><tr>
                                                    <td>
                                                        <span>

                                                            <input type="text" placeholder="Enter Captcha" name="txtInput" class="form-control" id="txtInput" autocomplete="off" maxlength="5" onkeydown="return space(event, this);" onkeypress='return onlyNumbers(event);'>

                                                        </span>
                                                    </td>
                                                    <td>&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;

                                                    </td>
                                                    <td>
                                                        <p class="captcha">${captchaCode}</p>
                                                    </td>
                                                </tr>
                                            </tbody></table>



                                    </div>
                                </div>


                                <div class="clearfix"></div>
                                <div class="text-sm-center ">

                                    <input onclick="return validateUser();" type="submit" value="Login" class="btn btn-primary" style=" float:right; margin-bottom: 30px;width: 297px;  ">
                                </div>
                            </html:form>

                            <div class="form-group form-check" style="margin-bottom: 15px;
                                 text-align: center;
                                 margin-top: 23px;">
                                <!--<a href="" style="" class="f-l">Forget Password</a>-->
                                <!--||
                                    <a href="" style="" class="f-r">Register Here</a>-->
                            </div>

                        </div> 




                    </div> 
                </div>
            </div>
        </div>	
    </div>

    <footer class="footer-area">
        <div class="container">
            <div class="row">

                <div class="new-footer-links">
                    <!--				  <a href="disclaimer.html">Disclaimer</a> |-->
                    <!-- <a href="privacy_policy.html">Privacy Policy</a> | -->
                    <!-- <a href="termsofuse.html">Terms of Use</a> | -->
                    <!-- <a href="refund_policy.html">Refund &amp; Cancellation Policy</a> | -->
                    <!-- <a href="accessibility.html">Accessibility</a> | -->
                    <!-- <a href="#">FAQ's</a> | -->
                    <!-- <a href="#">Help</a> | -->
                    <!-- <a href="feedback.html">Feedback</a>  -->
                </div>
            </div>
            <div class="copyright-area">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-sm-6 col-md-6">
                        <p> � Copy Rights Reserved with SBTET AP.</p>
                    </div>
                    <div class="col-lg-6 col-sm-6 col-md-6">
                        <p class="text-right">Designed &amp; Developed by <img class="aptonline" src="assets/img/logo-3.png" alt="image"></p>
                    </div>
                </div>
            </div>
        </div>
    </footer>


    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/js/bootstrap.min.js"></script>	
    <script src="assets/js/fontjs.js"></script>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/Swiper/4.3.5/js/swiper.min.js'></script>
    <script  src="assets/js/slider.js"></script>
    <script>
        jQuery(document).ready(function() {
            $(".dropdown").hover(
                    function() {
                        $('.dropdown-menu', this).stop().fadeIn("fast");
                    },
                    function() {
                        $('.dropdown-menu', this).stop().fadeOut("fast");
                    });
        });
    </script>


</body>
</html>
