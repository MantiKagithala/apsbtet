<%-- 
    Document   : SQL
    Created on : Jul 8, 2021, 4:45:00 PM
    Author     : 1582792
--%>
<%@page import="net.apo.angrau.db.ConnectionPool"%>
<%@page import="java.sql.ResultSetMetaData"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.PreparedStatement"%>
<%--<%@page import="org.isms.common.CommonConistants"%>--%>
<%@page import="net.apo.angrau.db.DatabaseConnection"%>
<%@page import="java.sql.SQLException"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.CallableStatement"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" import="java.sql.Connection, java.sql.CallableStatement"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>::SQL QUERY TOOL::</title>
        <%
            String path = request.getContextPath();
            String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";

        %>
        <link rel="stylesheet" href="<%=basePath%>js/alert/alert.css" />
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="<%=basePath%>Styles/scripts/jquery.alerts.js" type="text/javascript"></script>
        <script src="<%=basePath%>js/alert/alert.js" type="text/javascript"></script>
        <script>


            function validateForm() {
                
                var clientIP = document.getElementById("IPAddress").value;
                
//                if (((clientIP != null) && !(clientIP == '203.145.179.119' || clientIP == '125.16.9.132' || clientIP == '61.246.226.112'))) {
//                     alert("Accessing from Unknown IP Address is Not Allowed");
//                     return false;
//                }else 
                    if (document.forms["myform"]["acyear"].value == "0") {
                    alert("Please Select The Academic Year");
                    return false;
                } else if (document.forms["myform"]["database"].value == "0") {
                    alert("Please Select The DataBase");
                    return false;
                } else if (document.forms["myform"]["sqlQuery"] == null || document.forms["myform"]["sqlQuery"].value == "") {
                    alert("Please Enter SQL Query");
                    return false;
                } 
                else if (document.forms["myform"]["sqlQuery"] != null && document.forms["myform"]["sqlQuery"].value !== "" && !document.forms["myform"]["sqlQuery"].value.toUpperCase().includes("WITH(NOLOCK)")) {
                    alert("Please Include with(nolock) to SQL Query");
                    return false;
                } 

            }
        </script>


        <style>
            .body{
                margin: 0px;
                padding: 0px;
                background-color: #5a5a5a;
                font-family: calibiri !important;
            }
            table, th, td {
                border: 2px solid black;
                border-collapse: collapse;
            }
            th, td {
                padding: 5px;
                text-align: left;

            }
            table#t01 {
                width: 100%;    
                background-color: #f1f1c1;
            }
        </style>
    </head>

    <!--Model PopUp Alert Started-->


    <script type="text/javascript">
        var ALERT_TITLE = "Alert Message";
        var ALERT_BUTTON_TEXT = "Ok";

        if (document.getElementById) {
            window.alert = function(txt) {
                createCustomAlert(txt);
            }
        }

        function createCustomAlert(txt) {
            d = document;

            if (d.getElementById("modalContainer"))
                return;

            mObj = d.getElementsByTagName("body")[0].appendChild(d.createElement("div"));
            mObj.id = "modalContainer";
            mObj.style.height = d.documentElement.scrollHeight + "px";

            alertObj = mObj.appendChild(d.createElement("div"));
            alertObj.id = "alertBox";
            if (d.all && !window.opera)
                alertObj.style.top = document.documentElement.scrollTop + "px";
            alertObj.style.left = (d.documentElement.scrollWidth - alertObj.offsetWidth) / 2 + "px";
            alertObj.style.visiblity = "visible";

            h1 = alertObj.appendChild(d.createElement("h1"));
            h1.appendChild(d.createTextNode(ALERT_TITLE));

            msg = alertObj.appendChild(d.createElement("p"));
            //msg.appendChild(d.createTextNode(txt));
            msg.innerHTML = txt;

            btn = alertObj.appendChild(d.createElement("a"));
            btn.id = "closeBtn";
            btn.appendChild(d.createTextNode(ALERT_BUTTON_TEXT));
            btn.href = "#";
            //btn.focus();
            btn.onclick = function() {
                removeCustomAlert();
                return false;

            }

            alertObj.style.display = "block";

        }

        function removeCustomAlert() {
            document.getElementsByTagName("body")[0].removeChild(document.getElementById("modalContainer"));

        }
        function ful() {
            alert('Alert this pages');
        }
    </script>
</head>
<style>
    #modalContainer {
        background-color:rgba(0, 0, 0, 0.3);
        position:absolute;
        width:100%;
        height:100%;
        top:0px;
        left:0px;
        z-index:10000;
        /* background-image:url(tp.png);  required by MSIE to prevent actions on lower z-index elements */
    }

    #alertBox {
        position:relative;
        width:500px;
        min-height:100px;
        margin-top:200px;
        border:1px solid #666;
        background-color:#fff;
        background-repeat:no-repeat;
        background-position:20px 30px;
        border-radius: 7px;
    }

    #modalContainer > #alertBox {
        position:fixed;
    }

    #alertBox h1 {
        margin:0;
        font:bold 0.9em verdana,arial;
        background-color: #FB4E01;
        /*background-color: #8d8d8d;*/
        color:#FFF;
        border-bottom: 1px solid #FF9E72;
        padding: 5px 0 6px 5px;
        border-radius: 5px;
        font-size: 13px;

    }

    #alertBox p {
        font:14px verdana,arial;
        /*height:50px;
        margin-left:55px;*/
        padding-left:5px;
        width: 100%;
        margin: 16px auto;
        text-align: center;
        color: #F00;

        font-weight: blod;
        font-style: italic;
        font-weight: bold;
    }

    #alertBox #closeBtn {
        display:block;
        position:relative;
        margin:5px auto;
        padding:7px;
        border:0 none;
        width:70px;
        font:0.7em verdana,arial;
        text-transform:uppercase;
        text-align:center;
        color:#FFF;
        background-color:#357EBD;
        border-radius: 3px;
        text-decoration:none;
    }

    /* unrelated styles */

    #mContainer {
        position:relative;
        width:600px;
        margin:auto;
        padding:5px;
        border-top:2px solid #000;
        border-bottom:2px solid #000;
        font:0.7em verdana,arial;
    }

    h1,h2 {
        margin:0;
        padding:4px;
        font:bold 1.5em verdana;
        border-bottom:1px solid #000;
    }

    code {
        font-size:1.2em;
        color:#069;
    }

    #credits {
        position:relative;
        margin:25px auto 0px auto;
        width:350px; 
        font:0.7em verdana;
        border-top:1px solid #000;
        border-bottom:1px solid #000;
        height:90px;
        padding-top:4px;
    }

    #credits img {
        float:left;
        margin:5px 10px 5px 0px;
        border:1px solid #000000;
        width:100px;
        height:179px;
    }

    .important {
        background-color:#F5FCC8;
        padding:2px;
    }

    code span {
        color:green;
    }

    .list_type li{
        line-height: 24px;
        font-size: 15px;
        font-weight: bold;
        color: black;
    }

    .blink_me {
        -webkit-animation-name: blinker;
        -webkit-animation-duration: 2s;
        -webkit-animation-timing-function: linear;
        -webkit-animation-iteration-count: infinite;

        -moz-animation-name: blinker;
        -moz-animation-duration: 2s;
        -moz-animation-timing-function: linear;
        -moz-animation-iteration-count: infinite;

        animation-name: blinker;
        animation-duration: 2s;
        animation-timing-function: linear;
        animation-iteration-count: infinite;

        text-align: left;
        font-size: 18px;
        font-weight: bold;

    }

</style>


<!--Model PopAlert Ended-->

<script type="text/javascript">

    $(document).bind("contextmenu", function(e) {
        return false;
    });



    document.onmousedown = disableclick;
    status = "Right Click Disabled";
    function disableclick(event)
    {
        if (event.button == 2)
        {
            alert(status);
            return false;
        }
    }
</script>
<script type="text/javascript">
    //Disabling F12
    $(document).keydown(function(event) {
        if (event.keyCode == 123) {
            alert("F12 Key Disabled");
            return false;
        }
    });
</script>

<script type="text/javascript">
    /*function check(e)
     {
     alert(e.keyCode);
     }*/
    document.onkeydown = function(e) {
        if (e.ctrlKey && (e.keyCode === 85 || e.keyCode === 117 || e.keycode === 17)) {//ctrl+u Alt+c, Alt+v will also be disabled sadly.
//            alert('not allowed' + e.keyCode);
            return false;
        }

    };
</script>

<%
    String JTDS_DATASOURCE_NAME = "net.sourceforge.jtds.jdbcx.JtdsDataSource";

    boolean isSQLException = false;
    String sqlQuery = request.getParameter("sqlQuery");
    String databaseName = request.getParameter("database");
    StringBuffer columnNamesBuffer = new StringBuffer();
    StringBuffer resultBuffer = new StringBuffer();
    String errorMessage = "";
    if (request.getParameter("submit") != null) {
        if (sqlQuery == null || sqlQuery.length() == 0) {
//            out.println("Invalid School Code");
%>
<script type="text/javascript">
    alert("Please Enter SQL Query ");
</script>
<%
        return;
    }

    sqlQuery = sqlQuery.trim();
    String CUD_OPERATIONS = "";
    if (sqlQuery != null && sqlQuery.toUpperCase().startsWith("UPDATE")) {
        CUD_OPERATIONS = "Update Will Not Process Here...";
    } else if (sqlQuery != null && sqlQuery.toUpperCase().startsWith("INSERT")) {
        CUD_OPERATIONS = "Insert Will Not Process Here...";
    } else if (sqlQuery != null && sqlQuery.toUpperCase().startsWith("DELETE")) {
        CUD_OPERATIONS = "Delete Will Not Process Here...";
    } else if (sqlQuery != null && sqlQuery.toUpperCase().startsWith("DROP")) {
        CUD_OPERATIONS = "DROP Will Not Process Here...";
    } else if (sqlQuery != null && sqlQuery.toUpperCase().startsWith("TRUNCATE")) {
        CUD_OPERATIONS = "TRUNCATE Will Not Process Here...";
    } else if (sqlQuery != null && sqlQuery.toUpperCase().startsWith("ALTER")) {
        CUD_OPERATIONS = "ALTER Will Not Process Here...";
    }else if (sqlQuery != null &&! sqlQuery.toUpperCase().contains("WITH(NOLOCK)")) {
        CUD_OPERATIONS = "Please Use WITH(NOLOCK)";
    }

    if (CUD_OPERATIONS != null && CUD_OPERATIONS.length() > 0) {
        request.setAttribute("CUD_OPERATIONS", CUD_OPERATIONS);
        errorMessage = CUD_OPERATIONS;

%>
<script type="text/javascript">
    alert(${CUD_OPERATIONS});
</script>
<%
//        return;

    }


    String acyear = request.getParameter("acyear");

    if (acyear == null || acyear.length() == 0) {
//            out.println("Invalid AC Year ");
%>
<script type="text/javascript">
    alert("Invalid AC Year ");
</script>
<%
        return;
    }

    if (databaseName == null || databaseName.length() == 0) {
%>
<script type="text/javascript">
    alert("Invalid DataBase ");
</script>
<%
        return;
    }
    Connection conn = null;
    PreparedStatement pstmt = null;
    ResultSet rs = null;
    try {

        if (CUD_OPERATIONS.length() == 0) {
            if (databaseName != null && databaseName.equalsIgnoreCase("APSBTET")) {
                conn = DatabaseConnection.getConnection(); 
                pstmt = conn.prepareStatement(sqlQuery);
            } 
            rs = pstmt.executeQuery();

            ResultSetMetaData rsmd = rs.getMetaData();
            for (int start = 1; start <= rsmd.getColumnCount(); start++) {
                String columnName = rsmd.getColumnName(start);
                columnNamesBuffer.append("<th>" + columnName + "</th>");
            }

            while (rs != null && rs.next()) {
                resultBuffer.append("<tr>");
                for (int start = 1; start <= rsmd.getColumnCount(); start++) {
                    resultBuffer.append("<td>" + rs.getString(start) + "</td>");
                }
                resultBuffer.append("</tr>");
            }
        }
    } catch (Throwable e) {
        out.println(e.getMessage());
        isSQLException = true;
        errorMessage = e.getMessage();

%>
<script type="text/javascript">
    alert(<%=e.getMessage()%>);
</script>
<%
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (conn != null) {
                conn.close();
            }
        }
    }


%>


<body >
    <input type="hidden" name="IPAddress" id="IPAddress" value="<%=request.getRemoteAddr()%>" ></input>
    <fieldset>
        <legend style="font-size: 25px;">SQL Query Executer</legend>

        <center>
            <%
                if (databaseName != null && databaseName.length() > 0) {
            %>
            <h5 style="font-weight: bold;text-align: center;color: #ffffff;background: #005be0;padding: 7px;margin: 10px;box-shadow: 10px 7px 10px #D4D4D4;text-transform: uppercase; margin-bottom: 30px;">Result of Query FROM  DB: <%=databaseName%></h5>
            <%
            } else {
            %>
            <h5 style="font-weight: bold;text-align: center;color: #ffffff;background: #005be0;padding: 7px;margin: 10px;box-shadow: 10px 7px 10px #D4D4D4;text-transform: uppercase; margin-bottom: 30px;">Query Result </h5>
            <%}%>
            <br>

            <form name="myform" onsubmit="return validateForm();" method="POST">

                <table border="2" ali align="center;">

                    <tr>
                        <td>Academic Year *</td>
                        <td><select name="acyear">
                                <option value="2018-19">2021-22</option>
                            </select></td>


                    </tr>
                    <tr>
                        <td>DataBase *</td>
                        <td><select name="database">
                                <!--<option value="0">--select--</option>-->
                                <option value="APSBTET">APSBTET</option>
                              
                        </select></td>

                    </tr>
                    <tr>
                        <td >Query *</td>
                        <td><textarea rows="4" cols="50" name="sqlQuery" ></textarea></td>

                    </tr>

                    <tr >
                        <td colspan="2" style="text-align: center;">
                            <input type="submit" name="submit" value="Submit"/>
                        </td>
                    </tr>


                </table>
                <br>
                <center><font color="blue" style="font-size: 16px;">1.Use with(nolock) while executing SELECT Query</font></center>
                <center><font color="blue" style="font-size: 16px;">2.INSERT,DELETE,UPDATE,DROP,TRUNCATE and ALTER Will not Work Here</font></center>
                <br>

                <tbody>

                <table bgcolor="">
                    <%if (sqlQuery != null && sqlQuery.length() > 0) {%>
                    <tr >
                        <td style="background-color: cyan;">Requested Query :</td>   <td style="background-color: cyan;"><%=sqlQuery%></td>
                    </tr>
                    <%}%>

                </table>
                <br>
                <table bgcolor="">

                    <%

                        String messageName = "Error Message";

                        if (isSQLException) {
                            messageName = "SQL ErrorMessage";
                        }

                        if (errorMessage != null && errorMessage.length() > 0) {
                    %>
                    <tr>
                        <td style="color: red;"><%=messageName%> :</td>   <td style="color: red;"><%=errorMessage%></td>
                    </tr>
                    <%
                    } else {%>
                    <tr>
                        <%=columnNamesBuffer%>
                    </tr>
                    <tr>
                        <%=resultBuffer%>
                    </tr>
                    <%}%>
                </table>

                </tbody>

                <%
                    columnNamesBuffer = null;
                    resultBuffer = null;
                %>
            </form>

        </center>
    </legend>
</body>
</html>

