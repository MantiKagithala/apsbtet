
package ws.pg.org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rquestId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="clientId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="paymentmode" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="baseamount" type="{http://www.w3.org/2001/XMLSchema}double"/>
 *         &lt;element name="checksumkey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rquestId",
    "clientId",
    "serviceid",
    "paymentmode",
    "baseamount",
    "checksumkey"
})
@XmlRootElement(name = "CheckSumforRequest")
public class CheckSumforRequest {

    protected String rquestId;
    protected String clientId;
    protected String serviceid;
    protected String paymentmode;
    protected double baseamount;
    protected String checksumkey;

    /**
     * Gets the value of the rquestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRquestId() {
        return rquestId;
    }

    /**
     * Sets the value of the rquestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRquestId(String value) {
        this.rquestId = value;
    }

    /**
     * Gets the value of the clientId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getClientId() {
        return clientId;
    }

    /**
     * Sets the value of the clientId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setClientId(String value) {
        this.clientId = value;
    }

    /**
     * Gets the value of the serviceid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceid() {
        return serviceid;
    }

    /**
     * Sets the value of the serviceid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceid(String value) {
        this.serviceid = value;
    }

    /**
     * Gets the value of the paymentmode property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPaymentmode() {
        return paymentmode;
    }

    /**
     * Sets the value of the paymentmode property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPaymentmode(String value) {
        this.paymentmode = value;
    }

    /**
     * Gets the value of the baseamount property.
     * 
     */
    public double getBaseamount() {
        return baseamount;
    }

    /**
     * Sets the value of the baseamount property.
     * 
     */
    public void setBaseamount(double value) {
        this.baseamount = value;
    }

    /**
     * Gets the value of the checksumkey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChecksumkey() {
        return checksumkey;
    }

    /**
     * Sets the value of the checksumkey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChecksumkey(String value) {
        this.checksumkey = value;
    }

}
