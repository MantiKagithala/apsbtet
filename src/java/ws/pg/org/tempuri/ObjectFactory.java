
package ws.pg.org.tempuri;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the org.tempuri package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: org.tempuri
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link EncryptResponse }
     * 
     */
    public EncryptResponse createEncryptResponse() {
        return new EncryptResponse();
    }

    /**
     * Create an instance of {@link DecryptResponse }
     * 
     */
    public DecryptResponse createDecryptResponse() {
        return new DecryptResponse();
    }

    /**
     * Create an instance of {@link Decrypt }
     * 
     */
    public Decrypt createDecrypt() {
        return new Decrypt();
    }

    /**
     * Create an instance of {@link CheckSumforRequestResponse }
     * 
     */
    public CheckSumforRequestResponse createCheckSumforRequestResponse() {
        return new CheckSumforRequestResponse();
    }

    /**
     * Create an instance of {@link CheckSumforResponse }
     * 
     */
    public CheckSumforResponse createCheckSumforResponse() {
        return new CheckSumforResponse();
    }

    /**
     * Create an instance of {@link CheckSumforResponseResponse }
     * 
     */
    public CheckSumforResponseResponse createCheckSumforResponseResponse() {
        return new CheckSumforResponseResponse();
    }

    /**
     * Create an instance of {@link CheckSumforRequest }
     * 
     */
    public CheckSumforRequest createCheckSumforRequest() {
        return new CheckSumforRequest();
    }

    /**
     * Create an instance of {@link Encrypt }
     * 
     */
    public Encrypt createEncrypt() {
        return new Encrypt();
    }

}
