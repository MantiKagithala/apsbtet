
package ws.pg.org.tempuri;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="rquestId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="PgRefNo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="baseamount" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="servcieid" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="checksumkey" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "rquestId",
    "pgRefNo",
    "baseamount",
    "servcieid",
    "checksumkey"
})
@XmlRootElement(name = "CheckSumforResponse")
public class CheckSumforResponse {

    protected String rquestId;
    @XmlElement(name = "PgRefNo")
    protected String pgRefNo;
    protected String baseamount;
    protected String servcieid;
    protected String checksumkey;

    /**
     * Gets the value of the rquestId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRquestId() {
        return rquestId;
    }

    /**
     * Sets the value of the rquestId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRquestId(String value) {
        this.rquestId = value;
    }

    /**
     * Gets the value of the pgRefNo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPgRefNo() {
        return pgRefNo;
    }

    /**
     * Sets the value of the pgRefNo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPgRefNo(String value) {
        this.pgRefNo = value;
    }

    /**
     * Gets the value of the baseamount property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBaseamount() {
        return baseamount;
    }

    /**
     * Sets the value of the baseamount property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBaseamount(String value) {
        this.baseamount = value;
    }

    /**
     * Gets the value of the servcieid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServcieid() {
        return servcieid;
    }

    /**
     * Sets the value of the servcieid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServcieid(String value) {
        this.servcieid = value;
    }

    /**
     * Gets the value of the checksumkey property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getChecksumkey() {
        return checksumkey;
    }

    /**
     * Sets the value of the checksumkey property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setChecksumkey(String value) {
        this.checksumkey = value;
    }

}
