
/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ps.isms.msgs;

/**
 *
 * @author APTOL301535
 */
public class MessageOutboxDto {    
    
    public String MsgId;
    public String applicationNumber;
    public String msgPurpose;
    public String message;
    public int MsgNoOfTries=0;
    public String mobileNumber; 
    public String SmsStatus; 
    public String MsgRemarks;
    public String emailId; 
    public String MailNoOfTries;  
    public String MailStatus;  
    public String msgService;  
    public String MailRemarks; 
    public String CreatedDate; 
    public String createdBy;
    public String UpdatedDate;
     public String fullName;
    

    public String getMsgId() {
        return MsgId;
    }

    public void setMsgId(String MsgId) {
        this.MsgId = MsgId;
    }

    public String getApplicationNumber() {
        return applicationNumber;
    }

    public void setApplicationNumber(String ApplicationNumber) {
        this.applicationNumber = ApplicationNumber;
    }

    public String getMsgPurpose() {
        return msgPurpose;
    }

    public void setMsgPurpose(String MsgPurpose) {
        this.msgPurpose = MsgPurpose;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String Message) {
        this.message = Message;
    }

    public int getMsgNoOfTries() {
        return MsgNoOfTries;
    }

    public void setMsgNoOfTries(int MsgNoOfTries) {
        this.MsgNoOfTries = MsgNoOfTries;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getSmsStatus() {
        return SmsStatus;
    }

    public void setSmsStatus(String SmsStatus) {
        this.SmsStatus = SmsStatus;
    }

    public String getMsgRemarks() {
        return MsgRemarks;
    }

    public void setMsgRemarks(String MsgRemarks) {
        this.MsgRemarks = MsgRemarks;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }


    public String getMailNoOfTries() {
        return MailNoOfTries;
    }

    public void setMailNoOfTries(String MailNoOfTries) {
        this.MailNoOfTries = MailNoOfTries;
    }

    public String getMailStatus() {
        return MailStatus;
    }

    public void setMailStatus(String MailStatus) {
        this.MailStatus = MailStatus;
    }

    public String getMsgService() {
        return msgService;
    }

    public void setMsgService(String MsgService) {
        this.msgService = MsgService;
    }

    public String getMailRemarks() {
        return MailRemarks;
    }

    public void setMailRemarks(String MailRemarks) {
        this.MailRemarks = MailRemarks;
    }

    public String getCreatedDate() {
        return CreatedDate;
    }

    public void setCreatedDate(String CreatedDate) {
        this.CreatedDate = CreatedDate;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    

    public String getUpdatedDate() {
        return UpdatedDate;
    }

    public void setUpdatedDate(String UpdatedDate) {
        this.UpdatedDate = UpdatedDate;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    
    
    
} 
