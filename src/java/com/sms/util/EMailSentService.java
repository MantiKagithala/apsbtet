/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sms.util;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import org.apache.log4j.Logger;

/**
 *
 * @author 1629028
 */
public class EMailSentService {
//static Logger LOG = Logger.getLogger(LoginAction.class.getName());

    private static EMailSentService serviceObj = null;

    public static EMailSentService getInstance() {
        if (serviceObj == null) {
            serviceObj = new EMailSentService();
        }
        return serviceObj;
    }

    public boolean SendEmail(
            ArrayList<InternetAddress> ToMailsList,
            ArrayList<InternetAddress> CCMailsList,
            ArrayList<InternetAddress> BCCMailsList,
            String subject, String message) {
		final String username = ""; //our GMail Account
		final String password = "D$e@2020";
//        final String username = "support-sed@telangana.gov.in"; //our GMail Account
//        final String password = "D$e@2020";

       
        Boolean Msg = true;
        try {
            Properties properties = new Properties();
            properties.put("mail.smtp.host", "10.3.3.206");
            properties.put("mail.smtp.port", "25");
            properties.put("mail.smtp.starttls.enable", "true");
            Session session = Session.getInstance(properties, new Authenticator() {
                protected PasswordAuthentication getPasswordAuthentication() {
                    return new PasswordAuthentication(username, password);
                }
            });
            Message msg = new MimeMessage(session);
            //msg.setFrom(new InternetAddress("MeesevaDevTeam(no-reply@meeseva.ap.gov.in)"));
            msg.setFrom(new InternetAddress(username + "(No Reply Private Schools)"));

            InternetAddress[] iaarrayTo = new InternetAddress[ToMailsList.size()];
            InternetAddress[] iaarrayCC = new InternetAddress[CCMailsList.size()];
            InternetAddress[] iaarrayBCC = new InternetAddress[BCCMailsList.size()];

            int count = 0;
            for (InternetAddress ia : ToMailsList) {
                iaarrayTo[count] = ia;
                count++;
            }

            count = 0;
            for (InternetAddress ia : CCMailsList) {
                iaarrayCC[count] = ia;
                count++;
            }

            count = 0;
            for (InternetAddress ia : BCCMailsList) {
                iaarrayBCC[count] = ia;
                count++;
            }

            msg.setRecipients(Message.RecipientType.TO, iaarrayTo);
            msg.setRecipients(Message.RecipientType.CC, iaarrayCC);
            msg.setRecipients(Message.RecipientType.BCC, iaarrayBCC);

            msg.setSubject(subject);
            // msg.setText(message);
            msg.setContent(mailHTML(message), "text/html");
            session.setDebug(true);
            Transport tr = session.getTransport("smtp");
            tr.connect();
            msg.saveChanges(); // don't forget this
            tr.sendMessage(msg, msg.getAllRecipients());
            tr.close();
        } catch (Exception e) {
            e.printStackTrace();
            Msg = false;
        }
        return Msg;
    }

    private static String mailHTML(String msg) {
        String body = "<div style='background-color: #f7f7f7;font-family: Helvetica,Arial,sans-serif;font-size: 11px;color: #8c8c8c;'> <br> <br> "
                + ".&emsp;<table align='center' border='0' cellpadding='10' cellspacing='0' width='80%'><tr><td bgcolor='#ffffff'><center>"
                + "<img src='https://schooledu.telangana.gov.in/ISMS/img/logo_left.png' width='600px'></center><hr style='width: 95%'>"
                + "</td></tr><tr><td bgcolor='#ffffff'><DIV style='font-family: Helvetica,Arial,sans-serif; font-size: 13px;color: #666666; "
                + "padding: 60px; padding-top:0; padding-bottom:10 '></p> "
                + "<br><p><b>" + msg + "</b></p> <br><p style='float: right;'> Sincerely, <br>ISMS Telangana.</p> <br> "
                + "<br></DIV></td></tr><tr><td bgcolor='#f7f7f7'><center>"
                + "<p style='font-family: Helvetica,Arial,sans-serif; font-size: 11px;color: #8c8c8c;'> "
                + "Please do not forward this email to others. This URL belongs to your account access only we not accepts sharing."
                + "The system accepts only one response for each unique URL. Please do not reply to this email. To get in touch with us, "
                + "click <a href='#' style='text-decoration: none'>Help & Contact.</a> <BR> <br> Copyright &copy; 2019 ISMS Telangana. "
                + "All rights reserved.</p></center></td></tr></table></div>";

        return body;
    }
}