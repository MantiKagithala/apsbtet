/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginDAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import net.apo.angrau.db.DatabaseConnection;

/**
 *
 * @author 1820530
 */
public class PaymentReconsilationDAO {
    
     private static PaymentReconsilationDAO paymentObj = null;

    private PaymentReconsilationDAO() {
    }

    public static PaymentReconsilationDAO getInstance() {

        if (paymentObj == null) {
            paymentObj = new PaymentReconsilationDAO();
        }
        return paymentObj;
    }

    public ArrayList getPaymentReconsilationDetials(String fromDate, String toDate, int flag) {
        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        ArrayList paymentList = new ArrayList();
        Map map = null; 
        
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Payment_Reconsilation_Date (?,?,?)}");
            cstmt.setString(1, fromDate);
            cstmt.setString(2, toDate);
            System.out.println("toDate"+toDate);
              System.out.println("fromDate"+fromDate);
            cstmt.setInt(3, flag);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("pgRefNo", rs.getString(1));
                    map.put("requestId", rs.getString(2));
                    map.put("baseAmount", rs.getString(3));
                    map.put("amountRetained", rs.getString(4));
                    map.put("amountRemitted", rs.getString(5));
                    map.put("convineanceCharge", rs.getString(6)); 
                    map.put("gst", rs.getString(7)); 
                    map.put("customerName", rs.getString(8)); 
                    map.put("paymentMode", rs.getString(9)); 
                    map.put("phoneNum", rs.getString(10)); 
                    map.put("createdDate", rs.getString(11));                  
                      map.put("course", rs.getString(12)); 
                        map.put("semesters", rs.getString(13)); 
                    paymentList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
       
        return paymentList;
    }
    
}
