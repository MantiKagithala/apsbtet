/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginDAO;

import com.ap.WithLoginForm.LoginForm;
import net.apo.angrau.db.DatabaseConnection;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Vector;
//import java.util.concurrent.Semaphore;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * This Class is having all login related queries
 *
 * @author 1451421
 */
public class LoginDAO {

    private static LoginDAO loginDAO = null;

    public static LoginDAO getInstance() {

        if (loginDAO == null) {
            loginDAO = new LoginDAO();
        }
        return loginDAO;
    }

    private LoginDAO() {
    }

    public String getLoginFlag(LoginForm LoginForm) {
        String flag = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String query = null;
        String password = null;
        String userEncPwd = null;
        try {
            password = LoginForm.getPassword().substring(5, LoginForm.getPassword().length() - 5);
            userEncPwd = passwordEncrypt(password);

            con = DatabaseConnection.getConnection();

            query = "select LoginFlag from LoginDetails where username='" + LoginForm.getUserName() + "'\n"
                    + "and EncryptedPassword='" + userEncPwd + "' and status='Active'";

            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    flag = rs.getString(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return flag;
        }
    }

    public ArrayList getUserInfoBasedOnLoginUser(LoginForm loginForm, HttpServletRequest request) {
        Connection con = null;
        PreparedStatement cstmt = null;
        ResultSet rs = null;
        ArrayList userList = null;
        HttpSession session = null;
        String loginUser = loginForm.getUserName();
        // System.out.println("loginUser===="+loginUser);
        try {
            session = request.getSession();
            con = DatabaseConnection.getConnection();

            cstmt = con.prepareStatement("select username,roleid from LoginDetails where username = ?");
            cstmt.setString(1, loginUser);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    userList = new ArrayList();
                    userList.add(rs.getString(1));
                    userList.add(rs.getString(2));
//                    userList.add(rs.getString(3));
//                    userList.add(rs.getString(4));

                    session.setAttribute("userName", rs.getString(1));
                    session.setAttribute("RoleId", rs.getString(2));
//                    session.setAttribute("districtId", rs.getString(3));                    
//                   session.setAttribute("colleageName", rs.getString(4));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("userList DAO==" + userList);
        return userList;
    }

    public String passwordEncrypt(String password) throws NoSuchAlgorithmException {
        String sr = null;
        MessageDigest md = MessageDigest.getInstance("MD5");
        byte[] dataBytes = password.getBytes();
        md.update(dataBytes);
        byte[] mdbytes = md.digest();

        //convert the byte to hex format method 1
        StringBuffer sb = new StringBuffer();
        for (int j = 0; j < mdbytes.length; j++) {
            sb.append(Integer.toString((mdbytes[j] & 0xff) + 0x100, 16).substring(1));
        }
        sr = sb.toString();

        return sr;
    }

    public String getPasswordForChange(String userName, String password) {
        String pwd = null;
        Connection con = null;
        Statement st = null;
        ResultSet rs = null;
        String sql = null;
        try {
            con = DatabaseConnection.getConnection();
            st = con.createStatement();
            sql = "select encryptedPassword from logindetails(nolock) where userName='" + userName + "' and encryptedPassword='" + password + "' and status='Active'";
            rs = st.executeQuery(sql);
            if (rs != null && rs.next()) {
                pwd = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return pwd;
    }

//    public String getPassword(String userName) {
//        String password = null;
//        password = DatabaseConnection.getString("select encryptedpassword from logindetails(nolock) where username='" + userName + "' and status='Active'", CommonConistants.KEY);
////
////
//        return password;
//    }
    public int visitingTime(String systemIp) {
        Connection con = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        int count = 0;
        int count2 = 0;
        int count3 = 0;
        int visitorCount = 0;
        try {
            con = DatabaseConnection.getConnection();
            String query = "insert into VisitingTime (IpAddress,VisitorDate) values('" + systemIp + "',getDate())";
            stmt = con.prepareStatement(query);
            int count1 = stmt.executeUpdate();
            query = "select Count(*),visitorcount from visitorcount where status='Active'  group by VisitorCount";
            stmt = con.prepareStatement(query);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    String val = rs.getString(1);
                    visitorCount = rs.getInt(2);
                    if (!val.equals("")) {
                        count = rs.getInt(1);
                    }
                }
            }
            if (count == 0) {
                query = "insert into visitorcount (VisitorCount,Status) values('0','Active')";
                stmt = con.prepareStatement(query);
                count2 = stmt.executeUpdate();
            } else {
                if (count > 0) {
                    query = "update visitorcount set VisitorCount=VisitorCount+1 where status='Active'";
                    stmt = con.prepareStatement(query);
                    count3 = stmt.executeUpdate();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return visitorCount;
    }

    public int getLoginCount(LoginForm LoginForm) {
        int flag = 0;
        ResultSet rs = null;
        String query = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        try {

            con = DatabaseConnection.getConnection();

            query = "select count(*) from LoginDetails where username='" + LoginForm.getUserName() + "'\n"
                    + "and Password='" + LoginForm.getOldPassword() + "' and status='Active'";

            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    flag = rs.getInt(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return flag;
        }
    }

    public int updatePassword(LoginForm LoginForm) {
        int flag = 0;
        ResultSet rs = null;
        String query = null;
        Connection con = null;
        String password = null;
        String userEncPwd = null;
        PreparedStatement pstmt = null;
        try {

            password = LoginForm.getNewNormPassword().substring(5, LoginForm.getNewNormPassword().length() - 5);
            userEncPwd = passwordEncrypt(password);

            con = DatabaseConnection.getConnection();

            query = "update LoginDetails set Password='" + LoginForm.getNewPassword() + "', EncryptedPassword='" + userEncPwd + "',"
                    + " UpdatedDate=getDate() where username='" + LoginForm.getUserName() + "' and status='Active'";

            pstmt = con.prepareStatement(query);
            flag = pstmt.executeUpdate();

            if (rs != null) {
                while (rs.next()) {
                    flag = rs.getInt(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return flag;
        }
    }

    public int updatePasswordBeforeLogin(LoginForm LoginForm) {
        int flag = 0;
        ResultSet rs = null;
        String query = null;
        Connection con = null;
        String password = null;
        String userEncPwd = null;
        PreparedStatement pstmt = null;
        try {

            password = LoginForm.getPasswordNew().substring(5, LoginForm.getPasswordNew().length() - 5);
            userEncPwd = passwordEncrypt(password);

            con = DatabaseConnection.getConnection();

            query = "update LoginDetails set Password='" + LoginForm.getNewPassword() + "', EncryptedPassword='" + userEncPwd + "',"
                    + " UpdatedDate=getDate(),LoginFlag='Y' where username='" + LoginForm.getUserName() + "' and status='Active'";

            pstmt = con.prepareStatement(query);
            flag = pstmt.executeUpdate();

            if (rs != null) {
                while (rs.next()) {
                    flag = rs.getInt(1);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return flag;
        }
    }

    public Vector getUserServices(LoginForm loginForm, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Statement st = null;
        Connection con = null;
        HttpSession session = null;
        Vector services = new Vector();
        String serviceToUserSQL = null;
        try {
            con = DatabaseConnection.getConnection();
            st = con.createStatement();
            session = request.getSession(true);
            serviceToUserSQL = "select serviceid,parentid,targeturl,servicename,priority from services(nolock) "
                    + "where serviceid in (select serviceid from roles_services(nolock) where roleid = '" + session.getAttribute("RoleId") + "' and status='Active') "
                    + " and status='Active' order by parentid,priority";
//            System.out.println("serviceToUserSQL "+serviceToUserSQL);
            rs = st.executeQuery(serviceToUserSQL);
            if (rs != null) {
                while (rs.next()) {
                    String servicedesc[] = new String[5];
                    servicedesc[0] = rs.getString(1);
                    servicedesc[1] = rs.getString(2);
                    servicedesc[2] = rs.getString(3);
                    servicedesc[3] = rs.getString(4);
                    services.addElement(servicedesc);
                }
            }
        } catch (NullPointerException n) {
            throw new Exception("Problem for getting the Services");
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return services;
    }
      public int getDetails(LoginForm LoginForm,HttpServletRequest request) {
        int flag = 0;
        ResultSet rs = null;
        String query = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        HttpSession session=request.getSession();
        try {
            con = DatabaseConnection.getConnection();
            query = "select a.Centre_Code,B.Centre_name,a.DISTCD,DISTNAMe,INSTITUTION_NAME from TWSH_INSTITUTE_MASTER a with(nolock)  left join TWSH_CENTER_MASTER b with(nolock)   on a.CENTRE_CODE=b.CENTRE_CODE  where INST_CODE=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1,LoginForm.getUserName());
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    session.setAttribute("ccode",rs.getString(1));
                    session.setAttribute("cname",rs.getString(2));
                    session.setAttribute("distcd",rs.getString(3));
                    session.setAttribute("distname",rs.getString(4));
                    session.setAttribute("institute",rs.getString(5));
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            return flag;
        }
    }
      public int getInstituteDetails(LoginForm LoginForm, HttpServletRequest request) throws Exception {
        int flag = 0;
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HttpSession session = request.getSession();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_ExamCenterDetailsbyInstID_Get(?)}");
            cstmt.setString(1, LoginForm.getUserName());
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    session.setAttribute("ccode", rs.getString(5));
                    session.setAttribute("cname", rs.getString(6));
                    session.setAttribute("distcd", rs.getString(1));
                    session.setAttribute("distname", rs.getString(2));
                    session.setAttribute("instituteCode", rs.getString(3));
                    session.setAttribute("instituteName", rs.getString(4));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return flag;
    }
}
