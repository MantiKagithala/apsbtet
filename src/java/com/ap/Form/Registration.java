/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.Form;

/**
 *
 * @author APTOL301655
 */
public class Registration {

    private String aadhar;
    private String bname;
    private String parentdet;
    private String fname;
    private String mname;
    private String inservice;
    private String designation;
    private String office;
    private String gname;
    private String grelation;
    private String dob;
    private String age;
    private String gender;
    private String state;
    private String district;
    private String mandal;
    private String village;
    private String religion;
    private String nationality;
    private String caste;
    private String subcaste;
    private String socialstatus;
    private String cwsn;
    private String catcwsn;
    private String mobile;
    private String landLine;
    private String email;
    private String houseno;
    private String locality;
    private String addmandal;
    private String adddistrcit;
    private String addstate;
    private String pincode;
    private String phouseno;
    private String plocality;
    private String paddmandal;
    private String padddistrcit;
    private String pstate;
    private String ppincode;
    private String course;
    private String subjectgroup;
    private String specialization;
    private String passoutyear;
    private String university;
    private String bscinstitute;
    private String marksbsc;
    private String ogpa;
    private String totalpoints;
    private String inap;
    private String IVacyear;
    private String IVnonlocal;
    private String IVdistrict;
    private String IIIacyear;
    private String IIInonlocal;
    private String IIIdistrict;
    private String IIacyear;
    private String IInonlocal;
    private String IIdistrict;
    private String Iacyear;
    private String Inonlocal;
    private String Idistrict;
    private String intersenioracyear;
    private String interseniornonlocal;
    private String interseniordistrict;
    private String interjunioracyear;
    private String interjuniornonlocal;
    private String interjuniordistrict;
    private String Xacyear;
    private String Xnonlocal;
    private String Xdistrict;
    private String VIIIacyear;
    private String VIIInonlocal;
    private String VIIIdistrict;
    private String VIIacyear;
    private String VIInonlocal;
    private String VIIdistrict;
    private String VIacyear;
    private String VInonlocal;
    private String VIdistrict;
    private String nonresidance;
    private String fromdate;
    private String todate;
    private String nonresdentialfile;
    private String residancerelationship;
    private String organization;
    private String exam;
    private String applicationno;
    private String rank;
    private String eeamarks;
    private String distanceeducation;
    private String nameofcourse;
    private String pfinstitute;
    private String pftime;
    private String pfyear;
    private String nccinternatiaonal;
    private String nccinterstate;
    private String nccuniversity;
    private String nssintermnational;
    private String nssinterstate;
    private String nssuniversity;
    private String sportsintermnational;
    private String sportsinterstate;
    private String sportsuniversity;
    private String gamesintermnational;
    private String gamesinterstate;
    private String gamesuniversity;
    private String cultintermnational;
    private String cultinterstate;
    private String cultuniversity;
    private String literalinterstate;
    private String literalintermnational;
    private String literaluniversity;
    private String eeatotalmarks;

    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getParentdet() {
        return parentdet;
    }

    public void setParentdet(String parentdet) {
        this.parentdet = parentdet;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getInservice() {
        return inservice;
    }

    public void setInservice(String inservice) {
        this.inservice = inservice;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getOffice() {
        return office;
    }

    public void setOffice(String office) {
        this.office = office;
    }

    public String getGname() {
        return gname;
    }

    public void setGname(String gname) {
        this.gname = gname;
    }

    public String getGrelation() {
        return grelation;
    }

    public void setGrelation(String grelation) {
        this.grelation = grelation;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getMandal() {
        return mandal;
    }

    public void setMandal(String mandal) {
        this.mandal = mandal;
    }

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public String getReligion() {
        return religion;
    }

    public void setReligion(String religion) {
        this.religion = religion;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getSubcaste() {
        return subcaste;
    }

    public void setSubcaste(String subcaste) {
        this.subcaste = subcaste;
    }

    public String getSocialstatus() {
        return socialstatus;
    }

    public void setSocialstatus(String socialstatus) {
        this.socialstatus = socialstatus;
    }

    public String getCwsn() {
        return cwsn;
    }

    public void setCwsn(String cwsn) {
        this.cwsn = cwsn;
    }

    public String getCatcwsn() {
        return catcwsn;
    }

    public void setCatcwsn(String catcwsn) {
        this.catcwsn = catcwsn;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getLandLine() {
        return landLine;
    }

    public void setLandLine(String landLine) {
        this.landLine = landLine;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getHouseno() {
        return houseno;
    }

    public void setHouseno(String houseno) {
        this.houseno = houseno;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getAddmandal() {
        return addmandal;
    }

    public void setAddmandal(String addmandal) {
        this.addmandal = addmandal;
    }

    public String getAdddistrcit() {
        return adddistrcit;
    }

    public void setAdddistrcit(String adddistrcit) {
        this.adddistrcit = adddistrcit;
    }

    public String getAddstate() {
        return addstate;
    }

    public void setAddstate(String addstate) {
        this.addstate = addstate;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getPhouseno() {
        return phouseno;
    }

    public void setPhouseno(String phouseno) {
        this.phouseno = phouseno;
    }

    public String getPlocality() {
        return plocality;
    }

    public void setPlocality(String plocality) {
        this.plocality = plocality;
    }

    public String getPaddmandal() {
        return paddmandal;
    }

    public void setPaddmandal(String paddmandal) {
        this.paddmandal = paddmandal;
    }

    public String getPadddistrcit() {
        return padddistrcit;
    }

    public void setPadddistrcit(String padddistrcit) {
        this.padddistrcit = padddistrcit;
    }

    public String getPstate() {
        return pstate;
    }

    public void setPstate(String pstate) {
        this.pstate = pstate;
    }

    public String getPpincode() {
        return ppincode;
    }

    public void setPpincode(String ppincode) {
        this.ppincode = ppincode;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getSubjectgroup() {
        return subjectgroup;
    }

    public void setSubjectgroup(String subjectgroup) {
        this.subjectgroup = subjectgroup;
    }

    public String getSpecialization() {
        return specialization;
    }

    public void setSpecialization(String specialization) {
        this.specialization = specialization;
    }

    public String getPassoutyear() {
        return passoutyear;
    }

    public void setPassoutyear(String passoutyear) {
        this.passoutyear = passoutyear;
    }

    public String getUniversity() {
        return university;
    }

    public void setUniversity(String university) {
        this.university = university;
    }

    public String getBscinstitute() {
        return bscinstitute;
    }

    public void setBscinstitute(String bscinstitute) {
        this.bscinstitute = bscinstitute;
    }

    public String getMarksbsc() {
        return marksbsc;
    }

    public void setMarksbsc(String marksbsc) {
        this.marksbsc = marksbsc;
    }

    public String getOgpa() {
        return ogpa;
    }

    public void setOgpa(String ogpa) {
        this.ogpa = ogpa;
    }

    public String getTotalpoints() {
        return totalpoints;
    }

    public void setTotalpoints(String totalpoints) {
        this.totalpoints = totalpoints;
    }

    public String getInap() {
        return inap;
    }

    public void setInap(String inap) {
        this.inap = inap;
    }

    public String getIVacyear() {
        return IVacyear;
    }

    public void setIVacyear(String IVacyear) {
        this.IVacyear = IVacyear;
    }

    public String getIVnonlocal() {
        return IVnonlocal;
    }

    public void setIVnonlocal(String IVnonlocal) {
        this.IVnonlocal = IVnonlocal;
    }

    public String getIVdistrict() {
        return IVdistrict;
    }

    public void setIVdistrict(String IVdistrict) {
        this.IVdistrict = IVdistrict;
    }

    public String getIIIacyear() {
        return IIIacyear;
    }

    public void setIIIacyear(String IIIacyear) {
        this.IIIacyear = IIIacyear;
    }

    public String getIIInonlocal() {
        return IIInonlocal;
    }

    public void setIIInonlocal(String IIInonlocal) {
        this.IIInonlocal = IIInonlocal;
    }

    public String getIIIdistrict() {
        return IIIdistrict;
    }

    public void setIIIdistrict(String IIIdistrict) {
        this.IIIdistrict = IIIdistrict;
    }

    public String getIIacyear() {
        return IIacyear;
    }

    public void setIIacyear(String IIacyear) {
        this.IIacyear = IIacyear;
    }

    public String getIInonlocal() {
        return IInonlocal;
    }

    public void setIInonlocal(String IInonlocal) {
        this.IInonlocal = IInonlocal;
    }

    public String getIIdistrict() {
        return IIdistrict;
    }

    public void setIIdistrict(String IIdistrict) {
        this.IIdistrict = IIdistrict;
    }

    public String getIacyear() {
        return Iacyear;
    }

    public void setIacyear(String Iacyear) {
        this.Iacyear = Iacyear;
    }

    public String getInonlocal() {
        return Inonlocal;
    }

    public void setInonlocal(String Inonlocal) {
        this.Inonlocal = Inonlocal;
    }

    public String getIdistrict() {
        return Idistrict;
    }

    public void setIdistrict(String Idistrict) {
        this.Idistrict = Idistrict;
    }

    public String getIntersenioracyear() {
        return intersenioracyear;
    }

    public void setIntersenioracyear(String intersenioracyear) {
        this.intersenioracyear = intersenioracyear;
    }

    public String getInterseniornonlocal() {
        return interseniornonlocal;
    }

    public void setInterseniornonlocal(String interseniornonlocal) {
        this.interseniornonlocal = interseniornonlocal;
    }

    public String getInterseniordistrict() {
        return interseniordistrict;
    }

    public void setInterseniordistrict(String interseniordistrict) {
        this.interseniordistrict = interseniordistrict;
    }

    public String getInterjunioracyear() {
        return interjunioracyear;
    }

    public void setInterjunioracyear(String interjunioracyear) {
        this.interjunioracyear = interjunioracyear;
    }

    public String getInterjuniornonlocal() {
        return interjuniornonlocal;
    }

    public void setInterjuniornonlocal(String interjuniornonlocal) {
        this.interjuniornonlocal = interjuniornonlocal;
    }

    public String getInterjuniordistrict() {
        return interjuniordistrict;
    }

    public void setInterjuniordistrict(String interjuniordistrict) {
        this.interjuniordistrict = interjuniordistrict;
    }

    public String getXacyear() {
        return Xacyear;
    }

    public void setXacyear(String Xacyear) {
        this.Xacyear = Xacyear;
    }

    public String getXnonlocal() {
        return Xnonlocal;
    }

    public void setXnonlocal(String Xnonlocal) {
        this.Xnonlocal = Xnonlocal;
    }

    public String getXdistrict() {
        return Xdistrict;
    }

    public void setXdistrict(String Xdistrict) {
        this.Xdistrict = Xdistrict;
    }

    public String getVIIIacyear() {
        return VIIIacyear;
    }

    public void setVIIIacyear(String VIIIacyear) {
        this.VIIIacyear = VIIIacyear;
    }

    public String getVIIInonlocal() {
        return VIIInonlocal;
    }

    public void setVIIInonlocal(String VIIInonlocal) {
        this.VIIInonlocal = VIIInonlocal;
    }

    public String getVIIIdistrict() {
        return VIIIdistrict;
    }

    public void setVIIIdistrict(String VIIIdistrict) {
        this.VIIIdistrict = VIIIdistrict;
    }

    public String getVIIacyear() {
        return VIIacyear;
    }

    public void setVIIacyear(String VIIacyear) {
        this.VIIacyear = VIIacyear;
    }

    public String getVIInonlocal() {
        return VIInonlocal;
    }

    public void setVIInonlocal(String VIInonlocal) {
        this.VIInonlocal = VIInonlocal;
    }

    public String getVIIdistrict() {
        return VIIdistrict;
    }

    public void setVIIdistrict(String VIIdistrict) {
        this.VIIdistrict = VIIdistrict;
    }

    public String getVIacyear() {
        return VIacyear;
    }

    public void setVIacyear(String VIacyear) {
        this.VIacyear = VIacyear;
    }

    public String getVInonlocal() {
        return VInonlocal;
    }

    public void setVInonlocal(String VInonlocal) {
        this.VInonlocal = VInonlocal;
    }

    public String getVIdistrict() {
        return VIdistrict;
    }

    public void setVIdistrict(String VIdistrict) {
        this.VIdistrict = VIdistrict;
    }

    public String getNonresidance() {
        return nonresidance;
    }

    public void setNonresidance(String nonresidance) {
        this.nonresidance = nonresidance;
    }

    public String getFromdate() {
        return fromdate;
    }

    public void setFromdate(String fromdate) {
        this.fromdate = fromdate;
    }

    public String getTodate() {
        return todate;
    }

    public void setTodate(String todate) {
        this.todate = todate;
    }

    public String getNonresdentialfile() {
        return nonresdentialfile;
    }

    public void setNonresdentialfile(String nonresdentialfile) {
        this.nonresdentialfile = nonresdentialfile;
    }

    public String getResidancerelationship() {
        return residancerelationship;
    }

    public void setResidancerelationship(String residancerelationship) {
        this.residancerelationship = residancerelationship;
    }

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getExam() {
        return exam;
    }

    public void setExam(String exam) {
        this.exam = exam;
    }

    public String getApplicationno() {
        return applicationno;
    }

    public void setApplicationno(String applicationno) {
        this.applicationno = applicationno;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getEeamarks() {
        return eeamarks;
    }

    public void setEeamarks(String eeamarks) {
        this.eeamarks = eeamarks;
    }

    public String getDistanceeducation() {
        return distanceeducation;
    }

    public void setDistanceeducation(String distanceeducation) {
        this.distanceeducation = distanceeducation;
    }

    public String getNameofcourse() {
        return nameofcourse;
    }

    public void setNameofcourse(String nameofcourse) {
        this.nameofcourse = nameofcourse;
    }

    public String getPfinstitute() {
        return pfinstitute;
    }

    public void setPfinstitute(String pfinstitute) {
        this.pfinstitute = pfinstitute;
    }

    public String getPftime() {
        return pftime;
    }

    public void setPftime(String pftime) {
        this.pftime = pftime;
    }

    public String getPfyear() {
        return pfyear;
    }

    public void setPfyear(String pfyear) {
        this.pfyear = pfyear;
    }

    public String getNccinternatiaonal() {
        return nccinternatiaonal;
    }

    public void setNccinternatiaonal(String nccinternatiaonal) {
        this.nccinternatiaonal = nccinternatiaonal;
    }

    public String getNccinterstate() {
        return nccinterstate;
    }

    public void setNccinterstate(String nccinterstate) {
        this.nccinterstate = nccinterstate;
    }

    public String getNccuniversity() {
        return nccuniversity;
    }

    public void setNccuniversity(String nccuniversity) {
        this.nccuniversity = nccuniversity;
    }

    public String getNssintermnational() {
        return nssintermnational;
    }

    public void setNssintermnational(String nssintermnational) {
        this.nssintermnational = nssintermnational;
    }

    public String getNssinterstate() {
        return nssinterstate;
    }

    public void setNssinterstate(String nssinterstate) {
        this.nssinterstate = nssinterstate;
    }

    public String getNssuniversity() {
        return nssuniversity;
    }

    public void setNssuniversity(String nssuniversity) {
        this.nssuniversity = nssuniversity;
    }

    public String getSportsintermnational() {
        return sportsintermnational;
    }

    public void setSportsintermnational(String sportsintermnational) {
        this.sportsintermnational = sportsintermnational;
    }

    public String getSportsinterstate() {
        return sportsinterstate;
    }

    public void setSportsinterstate(String sportsinterstate) {
        this.sportsinterstate = sportsinterstate;
    }

    public String getSportsuniversity() {
        return sportsuniversity;
    }

    public void setSportsuniversity(String sportsuniversity) {
        this.sportsuniversity = sportsuniversity;
    }

    public String getGamesintermnational() {
        return gamesintermnational;
    }

    public void setGamesintermnational(String gamesintermnational) {
        this.gamesintermnational = gamesintermnational;
    }

    public String getGamesinterstate() {
        return gamesinterstate;
    }

    public void setGamesinterstate(String gamesinterstate) {
        this.gamesinterstate = gamesinterstate;
    }

    public String getGamesuniversity() {
        return gamesuniversity;
    }

    public void setGamesuniversity(String gamesuniversity) {
        this.gamesuniversity = gamesuniversity;
    }

    public String getCultintermnational() {
        return cultintermnational;
    }

    public void setCultintermnational(String cultintermnational) {
        this.cultintermnational = cultintermnational;
    }

    public String getCultinterstate() {
        return cultinterstate;
    }

    public void setCultinterstate(String cultinterstate) {
        this.cultinterstate = cultinterstate;
    }

    public String getCultuniversity() {
        return cultuniversity;
    }

    public void setCultuniversity(String cultuniversity) {
        this.cultuniversity = cultuniversity;
    }

    public String getLiteralinterstate() {
        return literalinterstate;
    }

    public void setLiteralinterstate(String literalinterstate) {
        this.literalinterstate = literalinterstate;
    }

    public String getLiteralintermnational() {
        return literalintermnational;
    }

    public void setLiteralintermnational(String literalintermnational) {
        this.literalintermnational = literalintermnational;
    }

    public String getLiteraluniversity() {
        return literaluniversity;
    }

    public void setLiteraluniversity(String literaluniversity) {
        this.literaluniversity = literaluniversity;
    }
    
    
    
}
