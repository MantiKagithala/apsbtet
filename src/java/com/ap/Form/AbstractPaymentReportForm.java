/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.Form;

import org.apache.struts.action.ActionForm;

/**
 *
 * @author 1820530
 */
public class AbstractPaymentReportForm extends ActionForm{
    
    private String mode;
    private String courseCode;
    

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCourseCode() {
        return courseCode;
    }

    public void setCourseCode(String courseCode) {
        this.courseCode = courseCode;
    }
    
    
    
    
}
