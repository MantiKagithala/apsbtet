/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.payment;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

/**
 *
 * @author 1582792
 */
public class PaymentWSAccess {
    
    private static String webServiceInit(String xmlInput) {
        String responseString = "";
        String outputString = "";
        HttpURLConnection httpConn = null;
        try {
            System.getProperties().setProperty("https.protocols", "TLSv1.2,TLSv1.1,TLSv1,SSLv3");
            String strURL = "https://aptonline.in/ThirdPartyPaymentPG/EncryptDEcrypt.asmx?wsdl";
//             String strURL = "https://uat.aponline.gov.in:9443/ThirdPartyPaymentPG/EncryptDEcrypt.asmx?WSDL";

            URL url = new URL(strURL);

            URLConnection connection = url.openConnection();
            connection.setRequestProperty("content-type", "text/xml");
            httpConn = (HttpURLConnection) connection;
            ByteArrayOutputStream bout = new ByteArrayOutputStream();



            byte[] buffer = new byte[xmlInput.length()];
            buffer = xmlInput.getBytes();
            bout.write(buffer);
            byte[] b = bout.toByteArray();

            httpConn.setRequestMethod("POST");
            httpConn.setDoOutput(true);
            httpConn.setDoInput(true);
            OutputStream out = httpConn.getOutputStream();
            out.write(b);
            out.close();

            InputStreamReader isr = new InputStreamReader(httpConn.getInputStream());
             BufferedReader in =null;
            try{
                 in = new BufferedReader(isr); 
                while ((responseString = in.readLine()) != null) {
                outputString = outputString + responseString;
            }
            }catch(Exception e){}finally{if(in!=null)in.close();}
           
            
            httpConn.disconnect();
            System.out.println("outputString " + outputString);
            return outputString;
        } catch (Exception e) {
            e.printStackTrace();
            return e + "";
        }

    }

    public static String encryptPayData(String data)  throws Exception{
        System.out.println("data "+data);
        String xmlInput = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://tempuri.org/\">"
                + " <SOAP-ENV:Body> "
                + "<ns1:Encrypt>"
                + " <ns1:request>"+data+"</ns1:request>"
                + " </ns1:Encrypt> "
                + "</SOAP-ENV:Body> "
                + "</SOAP-ENV:Envelope>";

        String xmlOutput = webServiceInit(xmlInput);
        Document doc = parseXML(xmlOutput);
        String out = doc.getElementsByTagName("EncryptResult").item(0).getTextContent();


        return out;

    }

    public static String checkSum(String rquestId,String clientId,String serviceid,
            String paymentmode,String baseamount,String checksumkey)  throws Exception{

        String xmlInput = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://tempuri.org/\">"
                + " <SOAP-ENV:Body> "
                + "<ns1:CheckSumforRequest> "
                + "<ns1:rquestId>"+rquestId+"</ns1:rquestId>"
                + " <ns1:clientId>"+clientId+"</ns1:clientId>"
                + " <ns1:serviceid>"+serviceid+"</ns1:serviceid>"
                + " <ns1:paymentmode>"+paymentmode+"</ns1:paymentmode> "
                + "<ns1:baseamount>"+baseamount+"</ns1:baseamount>"
                + " <ns1:checksumkey>"+checksumkey+"</ns1:checksumkey> "
                + "</ns1:CheckSumforRequest> "
                + "</SOAP-ENV:Body> "
                + "</SOAP-ENV:Envelope>";

        String xmlOutput = webServiceInit(xmlInput);
        Document doc = parseXML(xmlOutput);
        String out = doc.getElementsByTagName("CheckSumforRequestResult").item(0).getTextContent();


        return out;

    }

    public static String decryptPayData(String data)  throws Exception{

        String xmlInput = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://tempuri.org/\"> "
                + "<SOAP-ENV:Body>"
                + " <ns1:Decrypt> <ns1:request>"+data+"</ns1:request>"
                + " </ns1:Decrypt>"
                + " </SOAP-ENV:Body> "
                + "</SOAP-ENV:Envelope>";

        String xmlOutput = webServiceInit(xmlInput);
        Document doc = parseXML(xmlOutput);
        String out = doc.getElementsByTagName("DecryptResult").item(0).getTextContent();


        return out;

    }

    private static Document parseXML(String inString) throws Exception{
            DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
            DocumentBuilder db = dbf.newDocumentBuilder();
            InputSource is = new InputSource(new StringReader(inString));
            return db.parse(is);     
    }

 public static String decrypt(String request) throws Exception {

        String xmlInput = "<?xml version=\"1.0\" encoding=\"UTF-8\"?> <SOAP-ENV:Envelope xmlns:SOAP-ENV=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:ns1=\"http://tempuri.org/\"> "
                + "<SOAP-ENV:Body>"
                + " <ns1:Decrypt> <ns1:request>" + request + "</ns1:request>"
                + " </ns1:Decrypt>"
                + " </SOAP-ENV:Body> "
                + "</SOAP-ENV:Envelope>";

        String xmlOutput = PaymentWSAccess.webServiceInit(xmlInput);
        Document doc = parseXML(xmlOutput);
        String out = doc.getElementsByTagName("DecryptResult").item(0).getTextContent();


        return out;

    }
   
}
