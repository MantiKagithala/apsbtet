/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.payment;

import com.ap.payment.PayFlow;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author APTOL301535
 */
@WebServlet(name = "Redirect", urlPatterns = {"/redirect"})
public class Redirect extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        PrintWriter out = response.getWriter();
        String payLoad = request.getParameter("payLoad");
        String sum = request.getParameter("sum");
        System.out.println("payLoad "+payLoad);
        System.out.println("sum "+sum);
        try {
             response.setHeader("Cache-Control", "no-cache");
            response.setHeader("Cache-Control", "no-store");
            response.setHeader("Pragma", "no-cache");
            response.setDateHeader("Expires", 0);
            String noBack = "<SCRIPT type=\"text/javascript\">\n"
                    + "    window.history.forward();\n"
                    + "    function noBack() { window.history.forward(); }\n"
                    + "</SCRIPT>";
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>"+noBack);
            out.println("<title>Redirecting to PaymentGateWay</title>");
            out.println("</head>");
            out.println("<body onload='noBack();' onpageshow='if (event.persisted) noBack();' onunload=''> ");
            out.println("<h1>&nbsp;</h1>");
            out.println("<center>");
            out.println("<br><img src='loader.gif' />");
            out.println("<h1>Redirecting to Payment Gateway</h1>");
            out.println("<p>Please don't press Refresh or Back button or close window</p>");
            out.println("<br></center>");
            out.println("</body>");
            out.println("</html>");
            
            //Payment
            
            String payGRedirect = PayFlow.initPayRequest(payLoad, sum);
            if(payGRedirect.equals("PG_ERROR")){
                response.sendRedirect(request.getContextPath()+"/requestFailed");
            }else{
                response.sendRedirect(payGRedirect);
            }
            
//            ServletContext context = getServletContext();
//            RequestDispatcher rd = context.getRequestDispatcher("/process");
//            rd.forward(request, response);
        }catch(Exception e){e.printStackTrace();} finally {
            out.close();
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
