/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginAction;

import com.ap.WithLoginDAO.LoginDAO;
import com.ap.WithLoginForm.LoginForm;
import java.io.PrintWriter;
import java.util.Random;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.log4j.Logger;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class ChangePasswordAfterLoginAction extends DispatchAction {

    private static Logger log = Logger.getLogger(ChangePasswordAfterLoginAction.class);

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return mapping.findForward("success");
    }

    public ActionForward changePassword(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        try {

            LoginForm loginForm = (LoginForm) form;
            HttpSession httpSession = request.getSession();
            String userName = httpSession.getAttribute("userName").toString();
            loginForm.setUserName(userName);
            int loginChecking = LoginDAO.getInstance().getLoginCount(loginForm);
            if (loginChecking > 0) {
                loginChecking = LoginDAO.getInstance().updatePassword(loginForm);
                if (loginChecking > 0) {
                    target = "success";
                    //httpSession.invalidate();
                    loginForm.setOldPassword("");
                    loginForm.setNewPassword("");
                    loginForm.setConfirmPassword("");
                    request.setAttribute("result1", "Password Changed successfully Re-login");
                } else {
                    loginForm.setOldPassword("");
                    loginForm.setNewPassword("");
                    loginForm.setConfirmPassword("");
                    request.setAttribute("result", "Password Failed to update please re-try");
                }
            } else {
                loginForm.setOldPassword("");
                loginForm.setNewPassword("");
                loginForm.setConfirmPassword("");
                request.setAttribute("result", "Please Enter Valid Cridentials");
            }


        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return mapping.findForward(target);
    }
}
