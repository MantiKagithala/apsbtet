/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginAction;

import com.ap.WithLoginDAO.CommonExamDataDAO;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1820530
 */
public class InstituteWiseTWAction extends DispatchAction{
    
     private static final String SUCCESS = "success";

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
      
        CommonExamDataDAO commonExamDataDAO = CommonExamDataDAO.getInstance();
        String roleId = (String) session.getAttribute("RoleId");
         ArrayList InstituteTWList = new ArrayList();
               
        InstituteTWList = CommonExamDataDAO.getInstituteTWList();
       
        System.out.println("InstituteTWList :" + InstituteTWList);
        if (InstituteTWList != null && InstituteTWList.size() > 0) {
            request.setAttribute("InstituteTWList", InstituteTWList);
        } else {
            request.setAttribute("nodata", "No details found");
        }
        
        System.out.println("hi unspecified");
        return mapping.findForward(SUCCESS);
    }
    
    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
    CommonExamDataDAO commonExamDataDAO = CommonExamDataDAO.getInstance();

        
         System.out.println("hi excel");
         ArrayList InstituteTWList = new ArrayList();
               
        InstituteTWList = CommonExamDataDAO.getInstituteTWList();
       
        System.out.println("InstituteTWList :" + InstituteTWList);
        if (InstituteTWList != null && InstituteTWList.size() > 0) {
            request.setAttribute("InstituteTWList", InstituteTWList);
        } else {
            request.setAttribute("nodata", "No details found");
        }
        

        return mapping.findForward("InstituteTWReportExcel");
    }
    
}
