/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginAction;

import com.ap.WithLoginDAO.LoginDAO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionForm;
import org.apache.log4j.Logger;


/**
 *
 * @author 484898
 */
public class LogOutAction extends org.apache.struts.action.Action {

    static Logger log = Logger.getLogger(LoginAction.class.getName());
    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    @Override
    public ActionForward execute(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        
        LoginDAO loginDAO = LoginDAO.getInstance();

        HttpSession session = null;
        String roleId = "";
        session = request.getSession(true);
        String userName = "";
        
        if (session != null && session.getAttribute("userName") != null
                && session.getAttribute("RoleId") != null) {

            userName = session.getAttribute("userName").toString();
            roleId = session.getAttribute("RoleId").toString();

            try {
               session.removeAttribute("userName");
                session.removeAttribute("RoleId");
                session.invalidate();

            } catch (Exception ex) {
                log.error(ex.getMessage(), ex);
            }
        }
        return mapping.findForward(SUCCESS);
    }
}
