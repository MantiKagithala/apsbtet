/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginAction;

import com.ap.Form.AbstractPaymentReportForm;
import com.ap.WithLoginDAO.AbstractPaymentReportDAO;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1820530
 */
public class AbstractPaymentReport extends DispatchAction{
    
    private static final String SUCCESS = "success";

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        AbstractPaymentReportForm form1 = (AbstractPaymentReportForm) form;
        AbstractPaymentReportDAO abstractPaymentReportDAO = AbstractPaymentReportDAO.getInstance();
        String roleId = (String) session.getAttribute("RoleId");
         ArrayList paymentList = new ArrayList();
               
        paymentList = abstractPaymentReportDAO.getPaymentAbstractReport();
        System.out.println("paymentList :" + paymentList);
        if (paymentList != null && paymentList.size() > 0) {
            request.setAttribute("paymentAbstractReport", paymentList);
        } else {
            request.setAttribute("nodata", "No details found");
        }
        
        System.out.println("hi unspecified");
        return mapping.findForward(SUCCESS);
    }
    
    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
      AbstractPaymentReportDAO abstractPaymentReportDAO = AbstractPaymentReportDAO.getInstance();

        
         System.out.println("hi excel");
        ArrayList paymentList = new ArrayList();
        paymentList = abstractPaymentReportDAO.getPaymentAbstractReport();
        System.out.println("paymentList :" + paymentList);
        if (paymentList != null && paymentList.size() > 0) {
            request.setAttribute("paymentAbstractReport", paymentList);
        } else {
            request.setAttribute("nodata", "No details found");
        }

        return mapping.findForward("AbstractPaymentReportExcel");
    }
    
     public ActionForward getSubReport(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        AbstractPaymentReportDAO abstractPaymentReportDAO = AbstractPaymentReportDAO.getInstance();        
         ArrayList paymentSubList = new ArrayList();
        
        String statusCode = request.getParameter("statusId");
        String courseCode = request.getParameter("courseCode");
      
        System.out.println("statusCode :" + statusCode);
        System.out.println("courseCode :" + courseCode);
        request.setAttribute("statusCode",statusCode);
        request.setAttribute("courseCode",courseCode);
        paymentSubList = abstractPaymentReportDAO.getPaymentAbstractSubReport(statusCode,courseCode);
        System.out.println("paymentList :" + paymentSubList);
        if (paymentSubList != null && paymentSubList.size() > 0) {
            request.setAttribute("paymentSubList", paymentSubList);
        } else {
            request.setAttribute("nodata", "No details found");
        }
               
        return mapping.findForward(SUCCESS);
        
       
     }
     
      public ActionForward downloadSubReportExcel(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
          
          HttpSession session = request.getSession();
        AbstractPaymentReportDAO abstractPaymentReportDAO = AbstractPaymentReportDAO.getInstance();        
         ArrayList paymentSubList = new ArrayList();
        
        String statusCode = request.getParameter("statusCodeExcel");
        String courseCode = request.getParameter("courseCodeExcel");
      
        System.out.println("statusCode :" + statusCode);
        System.out.println("courseCode :" + courseCode);
        
        paymentSubList = abstractPaymentReportDAO.getPaymentAbstractSubReport(statusCode,courseCode);
        System.out.println("paymentList :" + paymentSubList);
        if (paymentSubList != null && paymentSubList.size() > 0) {
            request.setAttribute("paymentSubList", paymentSubList);
        } else {
            request.setAttribute("nodata", "No details found");
        }
          
          return mapping.findForward("AbstractPaymentReportExcel");    
        
     }
     
      
    
}
