/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginAction;

import com.ap.Action.*;
import com.ap.DAO.CourseStatusReportDao;
import com.ap.WithLoginForm.AvailableSeatsForm;
import com.ap.Form.RegisterFormPhD;
import com.ap.payment.PaymentGatewayIntegrationPHD;
import net.apo.angrau.db.DatabaseConnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sms.util.SMSSendService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import sun.misc.BASE64Decoder;

/**
 *
 * @author APTOL301655
 */
public class HelpDesk extends DispatchAction {

    private static final String SUCCESS = "success";
    public static final String PDF = "PGCET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "failure";
        AvailableSeatsForm myform = (AvailableSeatsForm) form;
        try {
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("success");
    }

   
    public ActionForward getData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        AvailableSeatsForm myform = (AvailableSeatsForm) form;
        HashMap map = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList<HashMap> lstDetails = new ArrayList();
        try {
            System.out.println("entered "+myform.getProgrammeName()+ " ==== "+myform.getProgrammeId());
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Proc_ApplStatus_Check(?,?)}");
            cstmt.setString(1, myform.getProgrammeName());
            cstmt.setString(2, myform.getProgrammeId());
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("HALLTICKET", rs.getString(1));
                    map.put("name", rs.getString(2));
                    map.put("mjS", rs.getString(3));
                    map.put("Registration", rs.getString(4));
                    map.put("payment", rs.getString(5));
                    lstDetails.add(map);
                    map = null;
                }
            }
            if (lstDetails != null && !lstDetails.isEmpty()) {
                request.setAttribute("masterData", lstDetails);
                myform.setProgrammeList(lstDetails);
            } else {
                request.setAttribute("result", "No Details Found");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("success");
    }
}
