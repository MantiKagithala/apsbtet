/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginAction;

import com.ap.Form.ReconsilationForm;
import com.ap.WithLoginDAO.PaymentReconsilationDAO;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1820530
 */
public class PaymentReconsilationAction extends DispatchAction {

    private static final String SUCCESS = "success";

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        String roleId = (String) session.getAttribute("RoleId");

        System.out.println("hi unspecified");
        return mapping.findForward(SUCCESS);
    }

    public ActionForward getReconsilationReport(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        ReconsilationForm form1 = (ReconsilationForm) form;
        PaymentReconsilationDAO paymentReconsilationDAO = PaymentReconsilationDAO.getInstance();
        ArrayList paymentList = new ArrayList();

        String fromDate = "2019-06-20"; 
        String x= form1.getFromDate();
        String fday = x.split("/")[0];
        String fmonth = x.split("/")[1];
        String fyear = x.split("/")[2];
        System.out.println("from date :" + x);
        System.out.println("to date :" + form1.getToDate());
        String toDate = "2020-12-01"; //
        String tday = form1.getToDate().toString().split("/")[0];
        String tmonth = form1.getToDate().toString().split("/")[1];
        String tyear = form1.getToDate().toString().split("/")[2];
        int flag = 3;
        
//        String fdate = fyear+"-"+fmonth+"-"+fday;
//        String tdate = tyear+"-"+tmonth+"-"+tday;
        
        
        String fdate = fyear+"-"+fday+"-"+fmonth;
        String tdate = tyear+"-"+tday+"-"+tmonth;
      //  System.out.println("form1.getFromDate()=="+form1.getFromDate());
        
        System.out.println("todate "+form1.getToDate());
        request.setAttribute("fromDate", form1.getFromDate());
        request.setAttribute("toDate", form1.getToDate());
        request.setAttribute("flag", flag);
       
        paymentList = paymentReconsilationDAO.getPaymentReconsilationDetials(fdate, tdate, flag);
        System.out.println("paymentList :" + paymentList);
        if (paymentList != null && paymentList.size() > 0) {
            request.setAttribute("paymentReconsilationReport", paymentList);
        } else {
            request.setAttribute("nodata", "No details found");
        }
form1.setFromDate(x);
form1.setToDate(tday);
        return mapping.findForward(SUCCESS);
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        PaymentReconsilationDAO paymentReconsilationDAO = PaymentReconsilationDAO.getInstance();

        String fromDate = request.getParameter("submitFrom");
        String toDate = request.getParameter("submitTo");
        int flag = Integer.parseInt(request.getParameter("flag"));
        
         System.out.println("hi excel");
        ArrayList paymentList = new ArrayList();
        paymentList = paymentReconsilationDAO.getPaymentReconsilationDetials(fromDate, toDate, flag);
        System.out.println("paymentList :" + paymentList);
        if (paymentList != null && paymentList.size() > 0) {
            request.setAttribute("paymentReconsilationReport", paymentList);
        } else {
            request.setAttribute("nodata", "No details found");
        }

        return mapping.findForward("PaymentReconsilationReportExcel");
    }
}
