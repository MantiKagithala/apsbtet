/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.Action;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author aptol301305
 */
public class DownloadAdmissionPDF extends DispatchAction {

    /* forward name="success" path="" */
    private static final String SUCCESS = "success";

    /**
     * This is the action called from the Struts framework.
     *
     * @param mapping The ActionMapping used to select this instance.
     * @param form The optional ActionForm bean for this request.
     * @param request The HTTP Request we are processing.
     * @param response The HTTP Response we are processing.
     * @throws java.lang.Exception
     * @return
     */
    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        return mapping.findForward(SUCCESS);
    }

    public ActionForward downloadPDFFileold(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        String filename = (String) request.getParameter("filename");
        request.setAttribute("filename", filename);
        return mapping.findForward("pgadmissionformpdf");
    }

    public ActionForward downloadPDFFile(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String rowId = null;
        try {
            if (request.getParameter("filename") != null && request.getParameter("filename").toString().length() > 0) {


                BufferedInputStream in = null;
//                File dir = new File("D:\\" + request.getParameter("filename"));
                File dir = new File("E:\\Download\\" + request.getParameter("filename"));
                FileInputStream fin = new FileInputStream(dir);

                in = new BufferedInputStream(fin);
                response.setContentType("application/pdf");
                ServletOutputStream out = response.getOutputStream();
                String filename = (String) request.getParameter("filename");

                request.setAttribute("filename", filename);
//                response.setContentType("application/force-download");
//                response.addHeader("Content-Disposition", "attachment; filename=\"" + dir + "\"");

                byte[] buffer = new byte[4 * 1024];

                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }

                out.flush();
                out.close();

            } else {
                request.setAttribute("msg", "Selected Go Not Available");
            }
        } catch (FileNotFoundException e) {
//                target = "success";
            request.setAttribute("errorMsg", " >>>>>>>>>>>>> Requested Document Was Not Available. <<<<<<<<<<<<<");
        } catch (Exception e) {
            e.printStackTrace();
        }
        //   unspecified(mapping, form, request, response);


        return mapping.findForward("pgadmissionformpdf");

    }
}
