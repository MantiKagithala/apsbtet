/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.Action;

/**
 *
 * @author APTOL301294
 */
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.ap.DAO.HallticketDownloadLoginDao;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.RegisterBeforeLoginPrintDao;
import static net.apo.angrau.action.RegisterPrintPdfAction.getDrivePath;

public class DownloadHallticketLoginAction extends DispatchAction {

    HallticketDownloadLoginDao dao = new HallticketDownloadLoginDao();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        try {
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("success");
    }

    public ActionForward getDownloadHallticket(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        File directorytemp = null;
        String temporaryfolderPth = "";
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session = request.getSession();
        try {
            String url = request.getRequestURL().toString();
            String baseURL = url.substring(0, url.length() - request.getRequestURI().length()) + request.getContextPath() + "/";
            String remoteAddress = request.getRemoteAddr();
            rForm.setGrade2(remoteAddress);
            rForm.setGrade(rForm.getGrade1().trim());
            rForm.setGradename(baseURL);
            String uname = session.getAttribute("userName").toString();
            rForm.setUserName(uname);
            ArrayList listOfMap = dao.getApplicationStatus(rForm);
            System.out.println("");
            if (listOfMap.size() > 0) {
                HashMap map = (HashMap) listOfMap.get(0);
                String recordstatus = map.get("key").toString();
                if (recordstatus.equals("4")) {
                    ArrayList list = dao.getData(rForm);
                    System.out.println("list size ---1 " + list.size());
                    if (list.size() > 0) {
                        temporaryfolderPth = getDrivePath() + PDF;
                        if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                            directorytemp = new File(temporaryfolderPth);
                            if (!directorytemp.exists()) {
                                directorytemp.mkdirs();
                            }
                        }
                        String filename = rForm.getAadhar1() + "_Hallticket" + ".pdf";
                        dao.getPdfDataNew(temporaryfolderPth, filename, list);
                        boolean downloadstatus = false;
                        downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
                        if (downloadstatus == true) {
                            dao.deleteFile(temporaryfolderPth + filename);
                            rForm.setAadhar("");
                        }
                        target = "success";
                    } else {
                        request.setAttribute("result1", "No data found with the given input");
                        target = "success";
                    }
                } else {
                    request.setAttribute("result1", map.get("keyValue").toString());
                    target = "success";
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "E://";
        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;
            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }
}