/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.Action;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;

/**
 *
 * @author ci96067
 */
public class PaymentGatewayFailureActionPHD {
    
    String target = "failure";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String res = "";
        try {
            res = request.getParameter("resp").toString();
            request.setAttribute("failed", res);
            target = "failure";

        } catch (Exception e) {
            request.setAttribute("failed", "Your transaction was declained due to Server Failure. Money will be credited your bank account within 7 working days");
            e.printStackTrace();

        }
        return mapping.findForward(target);
    }
}
