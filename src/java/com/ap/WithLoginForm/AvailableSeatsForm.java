/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginForm;

import com.ap.Form.*;
import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author APTOL301655
 */
public class AvailableSeatsForm extends ActionForm {
    private String mode; 
    private String programmeId; 
    private String programmeName;
    private ArrayList programmeList = new ArrayList();
    
    private String majorSubjectId; 
    private String majorSubjectName;
    private ArrayList majorSubjectList = new ArrayList();

    public String getMajorSubjectId() {
        return majorSubjectId;
    }

    public void setMajorSubjectId(String majorSubjectId) {
        this.majorSubjectId = majorSubjectId;
    }

    public String getMajorSubjectName() {
        return majorSubjectName;
    }

    public void setMajorSubjectName(String majorSubjectName) {
        this.majorSubjectName = majorSubjectName;
    }

    public ArrayList getMajorSubjectList() {
        return majorSubjectList;
    }

    public void setMajorSubjectList(ArrayList majorSubjectList) {
        this.majorSubjectList = majorSubjectList;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getProgrammeId() {
        return programmeId;
    }

    public void setProgrammeId(String programmeId) {
        this.programmeId = programmeId;
    }
    
    public String getProgrammeName() {
        return programmeName;
    }

    public void setProgrammeName(String programmeName) {
        this.programmeName = programmeName;
    }

    public ArrayList getProgrammeList() {
        return programmeList;
    }

    public void setProgrammeList(ArrayList programmeList) {
        this.programmeList = programmeList;
    }
   
}