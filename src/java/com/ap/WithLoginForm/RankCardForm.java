/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.WithLoginForm;

import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import org.apache.struts.action.ActionErrors;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.action.ActionMessage;
import org.apache.struts.upload.FormFile;
/**
 *
 * @author 1582792
 */
public class RankCardForm extends org.apache.struts.action.ActionForm {

    private String mode;
    private String district;
    private String applicationNo;
    private String gdpoints;
    private String intpoints;

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getApplicationNo() {
        return applicationNo;
    }

    public void setApplicationNo(String applicationNo) {
        this.applicationNo = applicationNo;
    }

    public String getGdpoints() {
        return gdpoints;
    }

    public void setGdpoints(String gdpoints) {
        this.gdpoints = gdpoints;
    }

    public String getIntpoints() {
        return intpoints;
    }

    public void setIntpoints(String intpoints) {
        this.intpoints = intpoints;
    }
    
    
    
}
