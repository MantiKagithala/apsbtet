/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.Form;

/**
 *
 * @author 1820530
 */
public class ReconsilationForm extends org.apache.struts.action.ActionForm {
    
    private String mode;
    private String fromDate;
    private String toDate;
    
    private String submitFrom;

    public String getSubmitFrom() {
        return submitFrom;
    }

    public void setSubmitFrom(String submitFrom) {
        this.submitFrom = submitFrom;
    }
    
    
    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getFromDate() {
        return fromDate;
    }

    public void setFromDate(String fromDate) {
        this.fromDate = fromDate;
    }

    public String getToDate() {
        return toDate;
    }

    public void setToDate(String toDate) {
        this.toDate = toDate;
    }

    @Override
    public String toString() {
        return "ReconsilationForm{" + "mode=" + mode + ", fromDate=" + fromDate + ", toDate=" + toDate + '}';
    }
    
    
    
    
    
}
