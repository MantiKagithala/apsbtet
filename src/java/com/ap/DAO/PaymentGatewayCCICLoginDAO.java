/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ap.DAO;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import net.apo.angrau.db.DatabaseConnection;

/**
 *
 * @author 1582792
 */
public class PaymentGatewayCCICLoginDAO {
        public String paymentSuccess(String reqId,String appId,String PgRefNo,String baseAmt,String Conv_Charges,
            String gst,String serviceId,String checksum,String customerName,String serviceCharges,String userName){

        
        Connection con = null;
        PreparedStatement pstmt = null;
        String query = "";
        int insert = 0;
        String status = "";
        try {
            con = DatabaseConnection.getConnection();
                    query = "update PaymentDetailsCCIC set PGRefNo=?,Base_amount=?,ServiceID=?,"
                    + "check_sum=?,Customer_Name=?,Updated_Date=getdate() where Request_Id=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, PgRefNo);
                System.out.println("reqId in DAO"+reqId);
            pstmt.setString(2, baseAmt);
            pstmt.setString(3, serviceId);
            pstmt.setString(4, checksum);
            pstmt.setString(5, customerName);
            pstmt.setString(6, reqId);
            System.out.println("userName"+userName);
            insert = pstmt.executeUpdate();
            
            
            if (insert > 0) {
                status = "Inserted Successfully";
            } else {
                status = "Error in Inserting..";
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return status;
    }

  
        
          public ArrayList getDataAfterPayment(String reqId) {

        Connection con = null;
        PreparedStatement pstmt = null;
        String query = "";
        ArrayList list = new ArrayList();
        ResultSet rs = null;
        Map map = null;
        CallableStatement stmt = null;
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareCall("{call Usp_PaymentStatus_CCIC(?,?,?)}");
            stmt.setString(1, "1");
            stmt.setString(2, reqId);
            stmt.setString(3, "");
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {

                    map = new HashMap<String, String>();
                    map.put("customerName", rs.getString(1));
                    map.put("gender", rs.getString(2));
                    map.put("caste", rs.getString(3));
                    map.put("dob", rs.getString(4));
                    map.put("mobile", rs.getString(5));
                    map.put("ph", rs.getString(6));
                    map.put("serviceCharges", rs.getString(7));
                    map.put("PgRefNo", rs.getString(8));
                    map.put("gst", rs.getString(9));
                    map.put("baseAmt", rs.getString(10));
                    map.put("reqId", rs.getString(11));
//                    map.put("aadhar", rs.getString(12));
                    map.put("aadhar",  "XXXXXXXX"+rs.getString(12).substring(8, 12));
                    map.put("email", rs.getString(13));
                    map.put("grade", rs.getString(14));

                    list.add(map);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
    
}
