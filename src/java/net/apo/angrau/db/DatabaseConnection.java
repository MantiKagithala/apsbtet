/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.db;

import java.sql.Connection;

/**
 *
 * @author 1441677
 */
public class DatabaseConnection {

    public static Connection getConnection() {
        Connection con = null;
        try {
                  Class.forName("net.sourceforge.jtds.jdbcx.JtdsDataSource");
     ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/01hw436572/APSBTET", "sa", "sa@12345");
//                         ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.96.64.53/APSBTET", "sa", "sa@12345");

// ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.100.101.95/APRGUKTCET;instance=MSSQL2012", "ANGRAU", "3edc#EDC");
       
                         //UAT
//                         ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.96.64.55/APSBTET", "sa", "sa@12345");
//                            ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.96.64.53/APSBTET_Diploma", "sa", "sa@12345");
    con = ConnectionPool.getInstance().checkout();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }
     public static Connection getConnectionDiploma() {
        Connection con = null;
        try {
                  Class.forName("net.sourceforge.jtds.jdbcx.JtdsDataSource");
     ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/01hw436572/APSBTET_Diploma", "sa", "sa@12345");
//                         ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.96.64.53/APSBTET_Diploma", "sa", "sa@12345");

// ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.100.101.95/APRGUKTCET;instance=MSSQL2012", "ANGRAU", "3edc#EDC");
       
                         //UAT
//                         ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.96.64.55/APSBTET", "sa", "sa@12345");
//                            ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.96.64.55/APSBTET_Diploma", "sa", "sa@12345");
    con = ConnectionPool.getInstance().checkout();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }
     public static Connection getConnectionDiplomaC20() {
        Connection con = null;
        try {
            Class.forName("net.sourceforge.jtds.jdbcx.JtdsDataSource");
//            ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/01hw436572/Diploma_C20", "sa", "sa@12345");
//            ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.96.64.55/Diploma_C20", "sa", "sa@12345");
//            ConnectionPool.getInstance().createConnectionPool("jdbc:jtds:sqlserver:/10.96.64.53/Diploma_C20", "sa", "sa@12345");
            con = ConnectionPool.getInstance().checkout();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return con;
    }
}
