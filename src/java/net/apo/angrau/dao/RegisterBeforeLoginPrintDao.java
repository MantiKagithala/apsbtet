/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.sms.util.MessagePropertiesUtil;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.forms.RegisterForm;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author APTOL301294
 */
public class RegisterBeforeLoginPrintDao {
    
    public ArrayList getApplicationStatus(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map =null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_AplicationStatus (?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim()); ///Reg No
            cstmt.setString(2, rform.getGrade().trim()); ///Mobile No
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                map = new HashMap<String, String>();
                map.put("key", rs.getString(1));
                map.put("keyValue", rs.getString(2));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public String getStatus(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        String result = null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_HallticketStatus (?,?,?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim());
            cstmt.setString(2, rform.getGrade().trim());
            cstmt.setString(3, rform.getExamination().trim());
            cstmt.setString(4, rform.getLanguage().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return result;
    }

    public HashMap<String, String> getData(RegisterForm rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        RegisterForm regform = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {

            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_TWSH_Print_REGNO(?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim());
            cstmt.setString(2, rform.getGrade().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("name", rs.getString(1));
                request.setAttribute("fname", rs.getString(2));
//                request.setAttribute("dob", rform.getDob());
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("caste", rs.getString(5));

                request.setAttribute("visImp", rs.getString(6));
                request.setAttribute("institution", rs.getString(7));
                request.setAttribute("examinationAppearing", rs.getString(8));
                request.setAttribute("language", rs.getString(9));
                request.setAttribute("grade", rs.getString(10));
                request.setAttribute("examinationDistrict", rs.getString(11));

                request.setAttribute("examinationCenter", rs.getString(12));
                request.setAttribute("examinationDate", rs.getString(13));
                request.setAttribute("examinationBatch", rs.getString(14));
                request.setAttribute("hNo", rs.getString(15));
                request.setAttribute("street", rs.getString(16));

                request.setAttribute("village", rs.getString(17));
                request.setAttribute("state", rs.getString(18));
                request.setAttribute("district", rs.getString(19));
                request.setAttribute("mandal", rs.getString(20));
                request.setAttribute("pincode", rs.getString(21));

                request.setAttribute("mobile", rs.getString(22));
                request.setAttribute("email", rs.getString(23));
                request.setAttribute("aadhar", rs.getString(24));

                request.setAttribute("RegNo", rs.getString(26));
                request.setAttribute("paymentRefNo", rs.getString(28));
                request.setAttribute("paymentAmount", rs.getString(29));
                request.setAttribute("paymentDate", rs.getString(30));
                Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(31));
                String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                request.setAttribute("dob", dob);
                request.setAttribute("date", new Date());
                map.put("status", rs.getString(40).toString());
                String filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(32) + "\\" + rs.getString(24) + "\\" + rs.getString(25);
                File file = new File(filepath);
                  byte[] inFileName = FileUtils.readFileToByteArray(file);
   String b64 = DatatypeConverter.printBase64Binary(inFileName);
//                BufferedImage image = ImageIO.read(file);
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                ImageIO.write(image, "jpg", baos);
//                baos.flush();
//                byte[] imageInByteArray = baos.toByteArray();
//                String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                request.setAttribute("photo", b64);
                
                String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(32) + "\\" + rs.getString(24) + "\\" + rs.getString(41);
                File file1 = new File(filepath1);
                byte[] inFileName1 = FileUtils.readFileToByteArray(file1);
                String sigb64 = DatatypeConverter.printBase64Binary(inFileName1);
//                BufferedImage image1 = ImageIO.read(file1);
//                ByteArrayOutputStream baos1 = new ByteArrayOutputStream();
//                ImageIO.write(image1, "jpg", baos1);
//                baos1.flush();
//                byte[] imageInByteArray1 = baos1.toByteArray();
                request.setAttribute("signature", sigb64);
                
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return map;
    }

    public HashMap<String, String> getDataOLD(RegisterForm rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        RegisterForm regform = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            Date todate1 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate1);

            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_Print(?,?)}");
            cstmt.setString(1, rform.getHallticket());
            cstmt.setString(2, dob);

            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("hall", rs.getString(1));
                request.setAttribute("bname", rs.getString(2));
                request.setAttribute("fname", rs.getString(4));
                request.setAttribute("dob", rform.getDob());
                request.setAttribute("mothername", rs.getString(5));
                request.setAttribute("gender", rs.getString(6));
                request.setAttribute("caste", rs.getString(7));
                request.setAttribute("ews", rs.getString(8));
                request.setAttribute("fatheroccu", rs.getString(9));
                request.setAttribute("income", rs.getString(10));
                request.setAttribute("mobile", rs.getString(11));
                request.setAttribute("email", rs.getString(12));
                request.setAttribute("areaStudied", rs.getString(13));
                request.setAttribute("address", rs.getString(14) + "," + rs.getString(15) + "," + rs.getString(82) + "," + rs.getString(16) + "," + rs.getString(17) + "," + rs.getString(18) + "," + rs.getString(19));
                //request.setAttribute("address1",rs.getString(17)+","+rs.getString(18)+","+rs.getString(19));

                request.setAttribute("cap", rs.getString(20));
                request.setAttribute("ph", rs.getString(21));
                request.setAttribute("ncc", rs.getString(22));
                request.setAttribute("sports", rs.getString(23));

                request.setAttribute("marks", rs.getString(26));

                request.setAttribute("tenClass", rs.getString(29));
                request.setAttribute("tenYear", rs.getString(30));
                request.setAttribute("tenState", rs.getString(31));
                request.setAttribute("tenDistrict", rs.getString(32));
                request.setAttribute("tenArea", rs.getString(33));

                request.setAttribute("nineClass", rs.getString(34));
                request.setAttribute("nineYear", rs.getString(35));
                request.setAttribute("nineState", rs.getString(36));
                request.setAttribute("nineDistrict", rs.getString(37));
                request.setAttribute("nineArea", rs.getString(38));

                request.setAttribute("eightClass", rs.getString(39));
                request.setAttribute("eightYear", rs.getString(40));
                request.setAttribute("eightState", rs.getString(41));
                request.setAttribute("eightDistrict", rs.getString(42));
                request.setAttribute("eightArea", rs.getString(43));

                request.setAttribute("sevenClass", rs.getString(44));
                request.setAttribute("sevenYear", rs.getString(45));
                request.setAttribute("sevenState", rs.getString(46));
                request.setAttribute("sevenDistrict", rs.getString(47));
                request.setAttribute("sevenArea", rs.getString(48));

                request.setAttribute("sixClass", rs.getString(49));
                request.setAttribute("sixYear", rs.getString(50));
                request.setAttribute("sixState", rs.getString(51));
                request.setAttribute("sixDistrict", rs.getString(52));
                request.setAttribute("sixArea", rs.getString(53));

                request.setAttribute("fiveClass", rs.getString(54));
                request.setAttribute("fiveYear", rs.getString(55));
                request.setAttribute("fiveState", rs.getString(56));
                request.setAttribute("fiveDistrict", rs.getString(57));
                request.setAttribute("fiveArea", rs.getString(58));

                request.setAttribute("fourClass", rs.getString(59));
                request.setAttribute("fourYear", rs.getString(60));
                request.setAttribute("fourState", rs.getString(61));
                request.setAttribute("fourDistrict", rs.getString(62));
                request.setAttribute("fourArea", rs.getString(63));

                request.setAttribute("thirdClass", rs.getString(64));
                request.setAttribute("thirdYear", rs.getString(65));
                request.setAttribute("thirdState", rs.getString(66));
                request.setAttribute("thirdDistrict", rs.getString(67));
                request.setAttribute("thirdArea", rs.getString(68));

                request.setAttribute("secondClass", rs.getString(69));
                request.setAttribute("secondYear", rs.getString(70));
                request.setAttribute("secondState", rs.getString(71));
                request.setAttribute("secondDistrict", rs.getString(72));
                request.setAttribute("secondArea", rs.getString(73));

                request.setAttribute("firstClass", rs.getString(74));
                request.setAttribute("firstYear", rs.getString(75));
                request.setAttribute("firstState", rs.getString(76));
                request.setAttribute("firstDistrict", rs.getString(77));
                request.setAttribute("firstArea", rs.getString(78));


                request.setAttribute("sportsinternational", rs.getString(79));
                request.setAttribute("sportsnational", rs.getString(80));
                request.setAttribute("sportsstate", rs.getString(81));


                request.setAttribute("village", rs.getString(82));
                request.setAttribute("localStatus", rs.getString(83));

                request.setAttribute("lastUpdatedDate", rs.getString(84));
                request.setAttribute("paymentRefNo", rs.getString(85));
                request.setAttribute("paymentAmount", rs.getString(86));
                request.setAttribute("paymentDate", rs.getString(87));
                request.setAttribute("mat", rs.getString(88));
                request.setAttribute("phy", rs.getString(89));
                request.setAttribute("bio", rs.getString(90));
                request.setAttribute("date", new Date());
                try {
                    String filepath = MessagePropertiesUtil.FIlE_PATH2 + "\\" + rs.getString(91) + "\\" + rs.getString(91) + "_Photo.jpg";
                    File file = new File(filepath);
                    if (file.isFile()) {
                        BufferedImage image = ImageIO.read(file);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ImageIO.write(image, "jpg", baos);
                        baos.flush();
                        byte[] imageInByteArray = baos.toByteArray();
                        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                        request.setAttribute("photo", b64);
                    } else {
                        String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(1) + "\\" + rs.getString(1) + "_PHOTO.jpg";
                        File file1 = new File(filepath1);
                        BufferedImage image = ImageIO.read(file1);
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        ImageIO.write(image, "jpg", baos);
                        baos.flush();
                        byte[] imageInByteArray = baos.toByteArray();
                        String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                        request.setAttribute("photo", b64);
                    }
                } catch (Exception e) {
                    String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(1) + "\\" + rs.getString(1) + "_PHOTO.jpg";
                    File file1 = new File(filepath1);
                    BufferedImage image = ImageIO.read(file1);
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    ImageIO.write(image, "jpg", baos);
                    baos.flush();
                    byte[] imageInByteArray = baos.toByteArray();
                    String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
                    request.setAttribute("photo", b64);
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }

    public String getvalidatingAadharNumWithApplication(String appNO, String dob) throws Exception {
        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String mb = "";
        String mm = "";
        try {
            String[] mb1 = dob.split("/");
            mm = mb1[2].toString() + "-" + mb1[1].toUpperCase() + "-" + mb1[0].toString();
            con = DatabaseConnection.getConnection();
            query = "select mobile from Diploma_Registration with(nolock)  where HallTicket='" + appNO + "' and DOB='" + mm + "'";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    mb = rs.getString(1);
                }
            } else {
                mb = "";
            }
        } catch (Exception e) {
            mb = "";
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mb;
    }

    public int SaveOTP(String Appno, String mobile, String otp, String systemIp) throws Exception {
        int count = 0;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call proc_OTP_Details_Insert(?,?,?,?)}");
            cstmt.setString(1, Appno);
            cstmt.setString(2, mobile);
            cstmt.setString(3, otp);
            cstmt.setString(4, systemIp);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    public String getvalidatingOTP(String Appno, String mobile) throws Exception {
        String otp = "";
        ResultSet rs = null;
        String query = null;
        Connection con = null;
        PreparedStatement st = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select OTP  from OTP_Details where Mobile_Num='" + mobile + "' and Aadhar_Id='" + Appno + "'";
            //System.out.println("query "+query);
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    otp = rs.getString(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return otp;
    }

    public ArrayList getGradesList() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select Grade,GRADE_DESC from TWSH_GRADE_MASTER with(nolock) order by RowID asc";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("gcode", rs.getString(1));
                map.put("gname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }
}
