/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author APTOL301461
 */
public class OnlineExaminationCenterChangeDAO {

    public ArrayList getMasterData(String pin, HttpServletRequest request) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        ArrayList centerList = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call USP_Diploma_StudentCollegeNameDetails_Get (?)}");
            cstmt.setString(1, pin); ///Reg No
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    request.setAttribute("flagCode", rs.getString(1));
                    request.setAttribute("flag", rs.getString(2));
                    request.setAttribute("collegeCode", rs.getString(3));
                    request.setAttribute("collegeName", rs.getString(4));
                    request.setAttribute("DistrictId", rs.getString(5));
                    request.setAttribute("DistrictName", rs.getString(6));
                    centerList.add("1");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return centerList;

    }
    
    public String getUpdateData(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        String centerList = "0";
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call USP_Diploma_StudentCollegeNameDetails_Update(?,?,?,?,?)}");
            cstmt.setString(1, rform.getAadhar1()); ///Pin
            cstmt.setString(2, rform.getCenterCode());
            cstmt.setString(3, rform.getEcenter());//Pandamic Yes/No
            cstmt.setString(4, rform.getDistrcit1());
            cstmt.setString(5, rform.getBlind());//SystemIp
//            System.out.println("1-"+rform.getAadhar1());
//            System.out.println("2-"+rform.getCenterCode());
//            System.out.println("3-"+rform.getEcenter());
//            System.out.println("4-"+rform.getDistrcit1());
//            System.out.println("5-"+rform.getBlind());
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    centerList = rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return centerList;

    }

    public ArrayList getCenterDetails(String district)  throws Exception {
        HashMap map = null;
        Connection con = null;
        ResultSet rs = null;
        CallableStatement cstmt = null;
        ArrayList centerList = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Usp_DiplomaStudent_Get ?,?}");
            cstmt.setString(1, district); ///Reg No
            cstmt.setString(2, "3");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("id", rs.getString(1));
                    map.put("name", rs.getString(2));
                    centerList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return centerList;

    }
    
    public ArrayList getDistrictDetails() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnectionDiploma();
            query = "SELECT * FROM STEPS_DISTRICT with(nolock)";
            cstmt = con.prepareCall("{Call Usp_DiplomaStudent_Get (?,?)}");
            cstmt.setString(1, ""); ///Reg No
            cstmt.setString(2, "5");
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return list;
    }
   
}
