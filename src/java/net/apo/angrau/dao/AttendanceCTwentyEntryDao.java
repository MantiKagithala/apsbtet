/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.forms.RegisterForm;
import org.apache.log4j.Logger;

/**
 *
 * @author APTOL301294
 */
public class AttendanceCTwentyEntryDao {
    
    private static final Logger logger = Logger.getLogger(AttendanceCTwentyEntryDao.class);

    public ArrayList getGradeList(String CenterId) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiplomaC20();
            cstmt = con.prepareCall("{Call Diploma_Attendance_Entry_Selection_Get(?,?,?,?,?,?,?)}");
            cstmt.setString(1, CenterId);
            cstmt.setString(2, "0");
            cstmt.setString(3, "0");
            cstmt.setString(4, "0");
            cstmt.setString(5, "0");
            cstmt.setString(6, "0");
            cstmt.setString(7, "1");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("gradecode", rs.getString(1));
                    map.put("gradename", rs.getString(1));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    
    public ArrayList getBrachList(String yearORSem,String centerId) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiplomaC20();
            
            cstmt = con.prepareCall("{Call Diploma_Attendance_Entry_Selection_Get(?,?,?,?,?,?,?)}");
            cstmt.setString(1, centerId);
            cstmt.setString(2, yearORSem);
            cstmt.setString(3, "0");
            cstmt.setString(4, "0");
            cstmt.setString(5, "0");
            cstmt.setString(6, "0");
            cstmt.setString(7, "2");
//            System.out.println("getBrachList-centerId "+centerId);
//            System.out.println("getBrachList-yearORSem "+yearORSem);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("braC", rs.getString(1));
                    map.put("braN", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getSubjectList(String yearSem,String branchC,String centerId) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiplomaC20();
            
            cstmt = con.prepareCall("{Call Diploma_Attendance_Entry_Selection_Get(?,?,?,?,?,?,?)}");
            cstmt.setString(1, centerId);
            cstmt.setString(2, yearSem);
            cstmt.setString(3, branchC);
            cstmt.setString(4, "0");
            cstmt.setString(5, "0");
            cstmt.setString(6, "0");
            cstmt.setString(7, "3");
            
//            System.out.println("getSubjectList-centerId "+centerId);
//            System.out.println("getSubjectList-yearORSem "+yearSem);
//            System.out.println("getSubjectList-branchC "+branchC);
            
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("subC", rs.getString(1));
                    map.put("subN", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    
    public ArrayList getMonths(String yearSem ,String branchC ,String centerId) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiplomaC20();
            
            cstmt = con.prepareCall("{Call Diploma_Attendance_Entry_Selection_Get(?,?,?,?,?,?,?)}");
            cstmt.setString(1, centerId);
            cstmt.setString(2, yearSem);
            cstmt.setString(3, branchC);
            cstmt.setString(4, "0");
            cstmt.setString(5, "0");
            cstmt.setString(6, "0");
            cstmt.setString(7, "3");
            
//            System.out.println("getMonths-centerId "+centerId);
//            System.out.println("getMonths-yearORSem "+yearSem);
//            System.out.println("getMonths-branchC "+branchC);
            
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("examC", rs.getString(1));
                    map.put("examN", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    
    public ArrayList getPeriod(String yearSem ,String branchC ,String centerId,String month) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiplomaC20();
            
            cstmt = con.prepareCall("{Call Diploma_Attendance_Entry_Selection_Get(?,?,?,?,?,?,?)}");
            cstmt.setString(1, centerId);
            cstmt.setString(2, yearSem);
            cstmt.setString(3, branchC);
            cstmt.setString(4, month);
            cstmt.setString(5, "0");
            cstmt.setString(6, "0");
            cstmt.setString(7, "4");
            
//            System.out.println("getPeriod-centerId "+centerId);
//            System.out.println("getPeriod-yearORSem "+yearSem);
//            System.out.println("getPeriod-branchC "+branchC);
//            System.out.println("getPeriod-month "+month);
            
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("priC", rs.getString(1));
                    map.put("priN", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    
    

    public ArrayList getCenteBasicData(String userName, String yearSem, String collegeWorkingDays, String month,String branch,String period) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiplomaC20();
            cstmt = con.prepareCall("{Call Diploma_Attendance_Entry_Selection_Get(?,?,?,?,?,?,?)}");
            cstmt.setString(1, userName);
            cstmt.setString(2, yearSem);
            cstmt.setString(3, branch);
            cstmt.setString(4, month);
            cstmt.setString(5, period);
            cstmt.setString(6, collegeWorkingDays);
            cstmt.setString(7, "5");
//            System.out.println("getCenteBasicData--userName "+userName+" branch "+branch+ "  yearSem "+yearSem+ " collegeWorkingDays "+collegeWorkingDays+ " month "+month + " period : "+period);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("pin", rs.getString(1));
                    map.put("name", rs.getString(2));
                    map.put("sem", rs.getString(3));
                    map.put("scheme", rs.getString(4));
                    map.put("sec", rs.getString(5));
                    map.put("sft", rs.getString(6));
                    map.put("workingDays", rs.getString(7));
                    map.put("attended", rs.getString(8));
                    
                    map.put("br", rs.getString(9));
                    map.put("attMonth", rs.getString(10));
                    map.put("attPeriod", rs.getString(11));
                    map.put("submitStatus", rs.getString(13));
                    
                    map.put("editEnable", rs.getString(12));
                    
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            logger.info("getting data info : " + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    
    public ArrayList getSubmitData(RegisterForm rForm) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            String pinList = rForm.getSschallticket();
            String nameList = rForm.getBname();
            String schemeList = rForm.getOccupation();
            String noOfWorkingList = rForm.getMaxMarks();
            String shiftList = rForm.getMarks();
            String sectionList = rForm.getMaskmobile();
            String attendedList = rForm.getAttendance();
            
            if (rForm.getSschallticket().startsWith("~")) {
                pinList = pinList.substring(1);
            }
            if (rForm.getSschallticket().endsWith("~")) {
                pinList = pinList.substring(0, pinList.length() - 1);
            }
            
            if (rForm.getBname().startsWith("~")) {
                nameList = nameList.substring(1);
            }
            if (rForm.getBname().endsWith("~")) {
                nameList = nameList.substring(0, nameList.length() - 1);
            }
            
            if (rForm.getOccupation().startsWith("~")) {
                schemeList = schemeList.substring(1);
            }
            if (rForm.getOccupation().endsWith("~")) {
                schemeList = schemeList.substring(0, schemeList.length() - 1);
            }
            
            if (rForm.getMaxMarks().startsWith("~")) {
                noOfWorkingList = noOfWorkingList.substring(1);
            }
            if (rForm.getMaxMarks().endsWith("~")) {
                noOfWorkingList = noOfWorkingList.substring(0, noOfWorkingList.length() - 1);
            }
            
            if (rForm.getMaskmobile().startsWith("~")) {
                sectionList = sectionList.substring(1);
            }
            if (rForm.getMaskmobile().endsWith("~")) {
                sectionList = sectionList.substring(0, sectionList.length() - 1);
            }
            
            if (rForm.getMarks().startsWith("~")) {
                shiftList = shiftList.substring(1);
            }
            if (rForm.getMarks().endsWith("~")) {
                shiftList = shiftList.substring(0, shiftList.length() - 1);
            }
            
            if (rForm.getAttendance().startsWith("~")) {
                attendedList = attendedList.substring(1);
            }
            if (rForm.getAttendance().endsWith("~")) {
                attendedList = attendedList.substring(0, attendedList.length() - 1);
            }
            
            String buttonType = rForm.getVstatus();
            
            String userName = rForm.getUserName();
            String remoteHost = rForm.getIncome();
            String yearSem = rForm.getGrade();
            String month = rForm.getEdate();
            String barnch = rForm.getBatchCode();
            String period = rForm.getEpaper();
            String collegeWorkingDays = rForm.getSubjectCode();
            
//            System.out.println("yearSem -- submit "+yearSem);
//            System.out.println("pinList "+pinList);
//            System.out.println("nameList "+nameList);
//            
//            System.out.println("schemeList "+schemeList);
//            System.out.println("sectionList "+sectionList);
//            System.out.println("shiftList "+shiftList);
//            System.out.println("noOfWorkingList "+noOfWorkingList);
//            System.out.println("attendedList "+attendedList);
            
            con = DatabaseConnection.getConnectionDiplomaC20();
            cstmt = con.prepareCall("{Call Diploma_Attendance_Entry_Save(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, pinList);
            cstmt.setString(2, nameList);
            cstmt.setString(3, schemeList);
            cstmt.setString(4, sectionList);
            cstmt.setString(5, shiftList);
            cstmt.setString(6, noOfWorkingList);
            cstmt.setString(7, attendedList);
            
            cstmt.setString(8, buttonType);
            cstmt.setString(9, yearSem);
            cstmt.setString(10, barnch);
            cstmt.setString(11, month);
            cstmt.setString(12, period);
            cstmt.setString(13, collegeWorkingDays);
            cstmt.setString(14, remoteHost); //Ipaddress
            cstmt.setString(15, userName);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    HashMap map = new HashMap();
                    map.put("successId",rs.getString(1));
                    map.put("successMsg",rs.getString(2));
                    lstDetails.add(map);
                }
            }
            
        } catch (Exception e) {
            logger.info("submit info : " + e.getMessage());
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getHallticktesList(String userName, String yearSem, String subjectID, String examDate,String branchCode, String flag) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call USP_DiplomaAttendanceSheet_Report(?,?,?,?,?,?)}");
            cstmt.setString(1, userName);
            cstmt.setString(2, branchCode);
            cstmt.setString(3, yearSem);
            cstmt.setString(4, subjectID);
            cstmt.setString(5, flag);
            cstmt.setString(6, examDate);
            //System.out.println("userName "+userName+ " branchCode "+branchCode+ " yearSem "+yearSem+" subjectID "+subjectID+" flag"+flag+" examDate"+examDate );
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    lstDetails.add(rs.getString(1));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public void getPdfDataTW(String filepath, String filename, String userName, String yearSem, String branchCode, String subjectID, String examDate) throws SQLException {
        createPDFTW(filepath, filename, userName, yearSem, branchCode, subjectID, examDate);
    }

    private void createPDFTW(String filepath, String filename, String userName, String yearSem, String branchCode, String subjectID, String examDate) {

        Document doc = new Document();
        PdfWriter docWriter = null;

        DecimalFormat df = new DecimalFormat("0.00");

        try {

            List<HashMap> list1 = getCenteBasicData(userName, yearSem, subjectID, examDate,branchCode,"");
            HashMap map = (HashMap) list1.get(0);
            String ccode = map.get("ccode").toString();
            String cname = map.get("cname").toString();
            String gradecode = map.get("gradecode").toString();
            String eedate = map.get("eedate").toString();
            String etime = map.get("etime").toString();
            String subject =map.get("subject").toString();
            String branch =map.get("branchC").toString();



//            String ccode = "1038";
//            String cname = "MRA GR GOVT. POLYTECHNIC, PHOOLBAGH, VIZIANAGARAM";
//            String gradecode = "AC-Auto Cad";
//            String subject ="AC - PAPER I (THEORY)";
//            String eedate = "08-08-2021";
//            String etime = "9:30AM TO 10:30AM";
            ArrayList presentList = getHallticktesList(userName, yearSem, subjectID, examDate,branchCode, "2");
            ArrayList abstList = getHallticktesList(userName, yearSem, subjectID, examDate,branchCode, "3");
            ArrayList malList = getHallticktesList(userName, yearSem, subjectID, examDate,branchCode, "4");
            ArrayList buffList = getHallticktesList(userName, yearSem, subjectID, examDate,branchCode, "5");

            
//            presentList.add("18270-EC-014");
//            presentList.add("16208-EC-021");
//            presentList.add("19208-EC-006");
//            presentList.add("18270-EC-042");
//            presentList.add("18270-EC-004");
//            presentList.add("19021-EC-256");
//
//            malList.add("18270-EC-004");
//            malList.add("18008-EC-018");
//
//            buffList.add("19208-EC-011");
//            buffList.add("18270-EC-020");

            //special font sizes
            Font heading1 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.NORMAL, new BaseColor(0, 0, 0));
            Font heading = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
            Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
            Font f5 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, new BaseColor(0, 0, 0)); //MAGENTA -Pink
            Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 9.5f);

            //file path
            String path = filepath + "/" + filename;

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

            doc.setPageSize(PageSize.A4.rotate());

            //open document
            doc.open();


            String exam = "DIPLOMA EXAMINATIONS, AUGUST/SEPTEMBER - 2021";

            Paragraph proParagraph = new Paragraph("PROFORMA T.E-1");
            proParagraph.setAlignment(Element.ALIGN_RIGHT);
            proParagraph.setSpacingAfter(7f);

            Paragraph paragraph = new Paragraph(""
                    + "                    "
                    + "STATE BOARD OF TECHNICAL EDUCTION & TRAINING           "
                    + "                                      "
                    + "                                      "
                    + "                                      "
                    + "                          "
                    + " ANDHRA PRADESH - VIJAYAWADA"
                    + "                          "
                    + "                                        "
                    + "                                        "
                    + "                                        "
                    + "                                        "
                    + "" + exam, heading);


            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setSpacingAfter(20f);

            float[] attcolumnWidths = {2.6f, 2.2f, 2.8f, 1f, 2.0f, 1f};
            PdfPTable attTable1 = new PdfPTable(attcolumnWidths);


            PdfPCell attTable = null;
            attTable = new PdfPCell(new Paragraph("", heading));
            attTable.setBorder(Rectangle.NO_BORDER);
            attTable1.addCell(attTable);

            attTable = new PdfPCell(new Paragraph("", heading));
            attTable.setBorder(Rectangle.NO_BORDER);
            attTable1.addCell(attTable);

            attTable = new PdfPCell(new Paragraph("ATTENDANCE SHEET", heading));
            attTable.setHorizontalAlignment(Element.ALIGN_CENTER);
            attTable.setBorderColor(new BaseColor(0, 0, 0));
            attTable.setBorderWidthTop(0.2f);
            attTable.setBorderWidthLeft(0.2f);
            attTable.setBorderWidthBottom(0.2f);
            attTable.setBorderWidthRight(0.2f);
            attTable.setFixedHeight(22f);
            attTable1.addCell(attTable);

            attTable = new PdfPCell(new Paragraph("", heading));
            attTable.setBorder(Rectangle.NO_BORDER);
            attTable1.addCell(attTable);

            attTable = new PdfPCell(new Paragraph("", heading));
            attTable.setBorder(Rectangle.NO_BORDER);
            attTable.setFixedHeight(22f);
            attTable1.addCell(attTable);

            attTable = new PdfPCell(new Paragraph("", heading));
            attTable.setBorder(Rectangle.NO_BORDER);
            attTable.setFixedHeight(22f);
            attTable1.addCell(attTable);

            Paragraph paragraph7 = new Paragraph("              (To be kept in seperate cover and sent along with the Answer Scripts)", heading);
            paragraph7.setAlignment(Element.ALIGN_CENTER);
            paragraph7.setSpacingBefore(5f);
            paragraph7.setSpacingAfter(15f);

            float[] attcolumnWidths1 = {0.1f, 0.1f, 0f, 0f, 0.6f, 0.6f};
            PdfPTable attTable11 = new PdfPTable(attcolumnWidths);
            attTable11.setWidthPercentage(99);

            PdfPCell attTable111 = null;
            attTable111 = new PdfPCell(new Paragraph("", heading));
            attTable111.setBorder(Rectangle.NO_BORDER);
            attTable111.setColspan(4);
            attTable11.addCell(attTable111);

            attTable111 = new PdfPCell(new Paragraph("CENTER CODE", heading));
            attTable111.setHorizontalAlignment(Element.ALIGN_CENTER);
            attTable111.setBorderColor(new BaseColor(0, 0, 0));
            attTable111.setPadding(2);
            attTable111.setBorderWidthTop(0.4f);
            attTable111.setBorderWidthLeft(0.4f);
            attTable111.setBorderWidthBottom(0.4f);
            attTable111.setBorderWidthRight(0.2f);
            attTable111.setFixedHeight(22f);
            attTable11.addCell(attTable111);

            attTable111 = new PdfPCell(new Paragraph(ccode, heading));
            attTable111.setHorizontalAlignment(Element.ALIGN_CENTER);
            attTable111.setBorderColor(new BaseColor(0, 0, 0));
            attTable111.setBorderWidthTop(0.4f);
            attTable111.setBorderWidthLeft(0.2f);
            attTable111.setBorderWidthBottom(0.4f);
            attTable111.setBorderWidthRight(0.4f);
            attTable111.setFixedHeight(22f);
            attTable11.addCell(attTable111);



            Chunk cname1 = new Chunk(cname, bfBold12);
            Chunk gradecode1 = new Chunk(gradecode, bfBold12);
            Chunk edate1 = new Chunk(eedate, bfBold12);
            Chunk etime1 = new Chunk(etime, bfBold12);
            Chunk subject1 = new Chunk(subject, bfBold12);
            Chunk branch1 = new Chunk(branch, bfBold12);
            Paragraph p = new Paragraph("");
            p.setSpacingBefore(30f);
            Chunk p1 = new Chunk("Name of the examination Center  ", heading1);
            p.add(p1);
            p.add(cname1);
            p.add(new Chunk("   Year/Semester ", heading1));
            p.add(gradecode1);
            p.add(new Chunk("   Branch ", heading1));
            p.add(branch1);
            p.add(new Chunk("  Subject ", heading1));
            p.add(subject1);
            p.add(new Chunk("    Date of Examination ", heading1));
            p.add(edate1);
            p.add(new Chunk("   Time of Examination ", heading1));
            p.add(etime1);


            Paragraph paragraph2 = new Paragraph("NO. OF CANDIDATES PRESENT WITH HALL TICKETS: " + presentList.size(), f5);
            paragraph2.setAlignment(Element.ALIGN_CENTER);
            paragraph2.setSpacingBefore(30f);


            float[] columnWidths = {6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f};
            PdfPTable table = new PdfPTable(columnWidths);
            table.setWidthPercentage(100f);
            for (int x = 0; x < presentList.size(); x++) {
                int j = x;
                for (j = x; j < x + 10; j++) {
                    if (j < presentList.size()) {
                        insertCell(table, presentList.get(j) + "", Element.ALIGN_CENTER, 1, bf12);
                    } else {
                        insertCell(table, "", Element.ALIGN_CENTER, 1, bf12);
                    }
                }
                x = j - 1;
            }
            if (presentList.size() == 0) {
                insertCell(table, "NIL", Element.ALIGN_CENTER, 10, bf12);
            }
            paragraph2.add(table);

            Paragraph paragraph3 = new Paragraph("NO. OF CANDIDATES ABSENT : " + abstList.size(), f5);
            paragraph3.setAlignment(Element.ALIGN_CENTER);

            float[] columnWidths3 = {6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f};
            PdfPTable table3 = new PdfPTable(columnWidths3);
            table3.setWidthPercentage(100f);
            for (int x = 0; x < abstList.size(); x++) {
                int j = x;
                for (j = x; j < x + 10; j++) {
                    if (j < abstList.size()) {
                        insertCell(table3, abstList.get(j) + "", Element.ALIGN_CENTER, 1, bf12);
                    } else {
                        insertCell(table3, "", Element.ALIGN_CENTER, 1, bf12);
                    }
                }
                x = j - 1;
            }
            if (abstList.size() == 0) {
                insertCell(table3, "NIL", Element.ALIGN_CENTER, 10, bf12);
            }
            paragraph3.add(table3);


            Paragraph paragraph5 = new Paragraph("NO. OF MALPRACTICES BOOKED : " + malList.size(), f5);
            paragraph5.setAlignment(Element.ALIGN_CENTER);

            float[] columnWidths5 = {6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f};
            PdfPTable table5 = new PdfPTable(columnWidths5);
            table5.setWidthPercentage(100f);
            for (int x = 0; x < malList.size(); x++) {
                int j = x;
                for (j = x; j < x + 10; j++) {
                    if (j < malList.size()) {
                        insertCell(table5, malList.get(j) + "", Element.ALIGN_CENTER, 1, bf12);
                    } else {
                        insertCell(table5, "", Element.ALIGN_CENTER, 1, bf12);
                    }
                }
                x = j - 1;
            }
            if (malList.size() == 0) {
                insertCell(table5, "NIL", Element.ALIGN_CENTER, 10, bf12);
            }
            paragraph5.add(table5);


            //Paragraph paragraph4 = new Paragraph("NO. OF SG'S (BUFFER OMR'S) USED : " + buffList.size(), f5);
            Paragraph paragraph4 = new Paragraph("NO. OF BUFFER OMR'S USED : " + buffList.size(), f5);
            paragraph4.setAlignment(Element.ALIGN_CENTER);

            float[] columnWidths4 = {6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f};
            PdfPTable table4 = new PdfPTable(columnWidths4);
            table4.setWidthPercentage(100f);
            for (int x = 0; x < buffList.size(); x++) {
                int j = x;
                for (j = x; j < x + 10; j++) {
                    if (j < buffList.size()) {
                        insertCell(table4, buffList.get(j) + "", Element.ALIGN_CENTER, 1, bf12);
                    } else {
                        insertCell(table4, "", Element.ALIGN_CENTER, 1, bf12);
                    }
                }
                x = j - 1;
            }
            if (buffList.size() == 0) {
                insertCell(table4, "NIL", Element.ALIGN_CENTER, 10, bf12);
            }
            paragraph4.add(table4);


            Paragraph paragraph6 = new Paragraph("Date :                                            "
                    + "                                                                              "
                    + "                                                                              "
                    + "                                                                              "
                    //+ "                                                                              "
                    + "Signature:", f5);
            paragraph6.setSpacingBefore(20f);

            doc.add(proParagraph);
            doc.add(paragraph);
            doc.add(attTable1);
            doc.add(paragraph7);
            doc.add(attTable11);
            doc.add(p);
            doc.add(paragraph2);
            doc.add(paragraph3);
            doc.add(paragraph5);
            doc.add(paragraph4);
            doc.add(paragraph6);



        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    private void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
//                    response.getOutputStream().flush();
//                    response.getOutputStream().close();
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
