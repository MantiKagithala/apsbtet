/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.sms.util.MessagePropertiesUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;
import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;
import com.sms.util.SendSMSDTO;
/**
 *
 * @author APTOL301655
 */
public class DtoofficersDAO {
       private static final Logger logger = Logger.getLogger(DtoofficersDAO.class);
   public String getvalidatingHallTicketNum(String aadhar,String hallticket) throws Exception {
        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String mb = "";
        String mm = "";

        try {
         query = "select mobile from TBL_TWSH_Registration with(nolock) where Aadhar=? and Treasuryid=? ";
          con = DatabaseConnection.getConnection();
            st = con.prepareStatement(query);
            st.setString(1,aadhar);
            st.setString(2,hallticket);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    mb = rs.getString(1);
                }
            } else {
                mb = "";
            }
        } catch (Exception e) {
            mb = "";
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mb;
    }
 public int SaveOTP(String hallticket, String mobile, String otp, String systemIp) throws Exception {
        int count = 0;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call proc_OTP_Details_Insert(?,?,?,?)}");
            cstmt.setString(1, hallticket);
            cstmt.setString(2, mobile);
            cstmt.setString(3, otp);
            cstmt.setString(4, systemIp);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }
 public String getvalidatingOTP(String hallticket, String mobile) throws Exception {

        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String otp = "";

        try {
            con = DatabaseConnection.getConnection();
            query = "select top(1) OTP  from OTP_Details with(nolock) where Mobile_Num='" + mobile + "' and Aadhar_Id='" + hallticket + "' order by insertDate desc";
            System.out.println("query " + query);
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    otp = rs.getString(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return otp;
    }
public ArrayList getStateDetails() throws Exception {

        String query = null;
        ArrayList list = new ArrayList();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select * from StateMaster with(nolock) order by StateName";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("scode", rs.getString(2));
                    map.put("sname", rs.getString(2));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {


                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;

    }
public ArrayList getDistrictDetails() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_DISTRICT with(nolock)";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return list;
    }

    public ArrayList getMandalDetails(String district) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_BLOCK with(nolock) where LEFT(BLKCD,4)=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("mandal_ID", rs.getString(1));
                map.put("mandal_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }
    public ArrayList getDistrictDetailsAt() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_DISTRICT_TS with(nolock)";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return list;
    }

    public ArrayList getMandalDetailsAt(String district) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_BLOCK_TS with(nolock) where LEFT(BLKCD,4)=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("mandal_ID", rs.getString(1));
                map.put("mandal_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }
  public ArrayList getcenterDetails(String district) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_MasterExaminationCenters_Get_DTO(?)}");
            cstmt.setString(1, district);
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("ccode", rs.getString(1));
                map.put("cname", rs.getString(2));
                list.add(map);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }
 public String insertFiles(RegisterForm rform, String remoteAddress) {
        Connection con = null;
        PreparedStatement cstmt = null;
        ResultSet rs = null;
        String result = "";

        String fileName1 = "";
        String fileName2 = "";
        String fileName3 = "";
        String fileName4 = "";
        String subjectCode = "ALL";
        boolean flag = false;

        String hallticket = rform.getAadhar();
        String course = rform.getCourse();

        if (rform.getCategory().equalsIgnoreCase("Backlog")) {
            subjectCode = rform.getSchoolcode();
        } else {
            subjectCode = "ALL";
        }

        if (rform.getPhoto() != null && rform.getPhoto().toString().length() > 0) {
            flag = uploadDocs(rform.getPhoto(), hallticket + "_PHOTO.jpg", hallticket, course);
            if (flag == true) {
                fileName3 = hallticket + "_PHOTO.jpg";
            }
        }
        if (rform.getSignature() != null && rform.getSignature().toString().length() > 0) {
            flag = uploadDocs(rform.getSignature(), hallticket + "_SIGNATURE.jpg", hallticket, course);
            if (flag == true) {
                fileName4 = hallticket + "_SIGNATURE.jpg";
            }
        }
        if (rform.getUpload1() != null && rform.getUpload1().toString().length() > 0) {
            flag = uploadDocs(rform.getUpload1(), hallticket + "_SSC.jpg", hallticket, course);
            if (flag == true) {
                fileName1 = hallticket + "_SSC.jpg";
            }
        }
        if (rform.getUpload2() != null && rform.getUpload2().toString().length() > 0) {
            flag = uploadDocs(rform.getUpload2(), hallticket + "_SERVICE.jpg", hallticket, course);
            if (flag == true) {
                fileName2 = hallticket + "_SERVICE.jpg";
            }
        }

        try {
            Date todate2 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate2);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_ApplicantRegistrationDetails_Insert_Pivate(?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, rform.getBname());
            cstmt.setString(2, rform.getFname());
            cstmt.setString(3, dob);
            cstmt.setString(4, rform.getGender());
            cstmt.setString(5, rform.getAadhar());
            cstmt.setString(6, rform.getCategory());
            cstmt.setString(7, rform.getApdistrict());
            cstmt.setString(8, rform.getInstitute());
            cstmt.setString(9, course);
            cstmt.setString(10, subjectCode);
            cstmt.setString(11, rform.getEcenter());
            cstmt.setString(12, fileName1);
            cstmt.setString(13, rform.getHouseno());
            cstmt.setString(14, rform.getLocality());
            cstmt.setString(15, rform.getVillage());
            cstmt.setString(16, rform.getState());
            if ((rform.getState().equalsIgnoreCase("Andra Pradesh")) || (rform.getState().equalsIgnoreCase("Telagana"))) {
                cstmt.setString(17, rform.getDistrict());
                cstmt.setString(18, rform.getMandal());
            } else {
                cstmt.setString(17, rform.getDistrict1());
                cstmt.setString(18, rform.getMandal1());
            }
            cstmt.setString(19, rform.getPincode());
            cstmt.setString(20, rform.getMobile());
            cstmt.setString(21, rform.getEmail());
            cstmt.setString(22, fileName4);
            cstmt.setString(23, fileName3);
            cstmt.setString(24, rform.getHallticket());
            cstmt.setString(25, remoteAddress);
            cstmt.setString(26, "9999");
            cstmt.setString(27, fileName2);
            cstmt.setString(28, rform.getSschallticket());
            cstmt.setString(29, rform.getSscyear());
            cstmt.setString(30, rform.getSscflag());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getString(1);
            }
        } catch (Exception ex) {
            logger.info("Error================" + ex.getMessage());
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return result;
    }
public boolean uploadDocs(FormFile uploadFileName, String fileName, String aadhar, String grade) {
        boolean flag = false;
        try {
            String strDirectoytemp = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + grade + "\\" + aadhar;
            if (strDirectoytemp != null && !"".equals(strDirectoytemp) && strDirectoytemp.length() > 0) { // If directory is not exists it will create
                File directorytemp = new File(strDirectoytemp);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
                File fileToCreatetemp = new File(strDirectoytemp, fileName); // Copy the file into directory
                FileOutputStream fileOutStreamtemp = new FileOutputStream(fileToCreatetemp); // Write the file content into buffer
                if (uploadFileName.getFileSize() > 0) {
                    fileOutStreamtemp.write(uploadFileName.getFileData());
                    fileOutStreamtemp.flush();
                    fileOutStreamtemp.close();
                    flag = true;
                } else {
                    flag = false;
                }
            } else {
                flag = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }
  public HashMap<String, String> getDataForPaymentDob(RegisterForm rform, HttpServletRequest request) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
//        PreparedStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        String dob = null;
        try {


            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_Get_CCIC (?,?,?)}");
//            cstmt = con.prepareStatement("select * from studentinfo where rgukt_rollno=? and dob=?");
            cstmt.setString(1, rform.getAadhar().trim());
            cstmt.setString(2, rform.getCourse().trim());
            cstmt.setString(3, "9999");
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("gender", rs.getString(2));
                request.setAttribute("caste", rs.getString(3));
                request.setAttribute("aadhaar", "XXXXXXXX" + rs.getString(6).substring(8, 12));
                request.setAttribute("mobile", rs.getString(3));
                request.setAttribute("email", rs.getString(4));
                request.setAttribute("amount", rs.getDouble(5));
                request.setAttribute("aadhar", rs.getString(6));
                request.setAttribute("grade", rs.getString(7));
                map.put("child", "9866747477");
//                list.add(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }

        return map;
    }
  public ArrayList getDataForPaymentSuccess(RegisterForm rform) {

        Connection con = null;
        PreparedStatement pstmt = null;
        String query = "";
        ArrayList list = new ArrayList();
        ResultSet rs = null;
        Map map = null;
        CallableStatement stmt = null;
        try {
            con = DatabaseConnection.getConnection();
//            query = "select  Request_Id,App_Id,PGRefNo,Base_amount,Convience_Charges,GST,Customer_Name from MSc_PaymentDetails (nolock) where Request_Id = '"+reqId+"' ";
//                System.out.println("query - query"+query);
//            pstmt = con.prepareStatement(query);
//           
//             rs = pstmt.executeQuery();

            stmt = con.prepareCall("{call Usp_PaymentStatus_CCIC(?,?,?,?)}");
            stmt.setString(1, "2");
            stmt.setString(2, rform.getAadhar());
            stmt.setString(3, rform.getCourse());
            stmt.setString(4, "9999");
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {

                    map = new HashMap<String, String>();
                    map.put("customerName", rs.getString(1));
                    map.put("gender", rs.getString(2));
                    map.put("caste", rs.getString(3));
                    map.put("dob", rs.getString(4));
                    map.put("mobile", rs.getString(5));
                    map.put("ph", rs.getString(6));
                    map.put("serviceCharges", rs.getString(7));
                    map.put("PgRefNo", rs.getString(8));
                    map.put("gst", rs.getString(9));
                    map.put("baseAmt", rs.getString(10));
                    map.put("reqId", rs.getString(11));
                    map.put("aadhar", "XXXXXXXX" + rs.getString(12).substring(8, 12));
                    list.add(map);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
  
  public int getStatus(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        int result = 0;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_HallticketStatus_CCIC (?,?)}");
            cstmt.setString(1, rform.getAadhar().trim());
            cstmt.setString(2, "OA");
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return result;
    }
   public HashMap<String, String> getSSCDetails(String hallticket,String Aadhar,HttpServletRequest request) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
//        PreparedStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        String dob = null;
        try {


            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_Get_SSCDETAILS_CCIC(?,?)}");
//            cstmt = con.prepareStatement("select * from studentinfo where rgukt_rollno=? and dob=?");
            cstmt.setString(1, hallticket);
            cstmt.setString(2, Aadhar);
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                 request.setAttribute("sschallticket", hallticket);
                 request.setAttribute("aadhar", Aadhar);
                 request.setAttribute("bname", rs.getString(2));
                 request.setAttribute("fname", rs.getString(3));
                 request.setAttribute("gender", rs.getString(4));
               request.setAttribute("caste", rs.getString(5));
                request.setAttribute("dob", rs.getString(6));
                request.setAttribute("aadharcount", rs.getString(8));
                request.setAttribute("Examinationstatus", rs.getString(9).toUpperCase());
                map.put("child","123456");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }
    public String insertSmsLogDetails(SendSMSDTO sendSMSDTO) throws Exception {
        Connection con = null;
//        Statement st = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String mobiles = "";
        String query = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "insert into SmsLog(MessageDescription,Messageto,LoginId,Systemip,senddate,sendstatus,Recevername,Receverdesignation,Receverdepartment) values('" + sendSMSDTO.getSms() + "',"
                    + "'" + sendSMSDTO.getMobileNumber() + "','" + sendSMSDTO.getLoginId() + "','10.100.102.80',getDate(),"
                    + "'" + sendSMSDTO.getSendStatus() + "','Aponline','Teacher','Teacher') ";
            ps = con.prepareStatement(query);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {

                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mobiles;
    }
}
