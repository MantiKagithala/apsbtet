/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;
import org.apache.log4j.Logger;

/**
 *
 * @author APTOL301655
 */
public class BundleDAO {
  private static final Logger logger = Logger.getLogger(BundleDAO.class);
    public HashMap<String, String> getDetails(String grade, String paper, String barcode,String examinationname) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_BARCODEBUNDLE_Get_TWSH(?,?,?,?)}");
            cstmt.setString(1, grade);
            cstmt.setString(2, paper);
            cstmt.setString(3, barcode);
            cstmt.setString(4, examinationname);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("status", rs.getString(1));
                    map.put("total", rs.getString(2));
                    map.put("regno", rs.getString(3));
                    map.put("bundlecount", rs.getString(4));
                    map.put("scannedmismatchcount", rs.getString(5));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }
    public HashMap<String, String> getDetailsStatus(String bundlecode) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        PreparedStatement cstmt = null;
        HashMap map = null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareStatement("select count(*) from TWSH_OMR_Bundling  with(nolock) where BundleId=?");
            cstmt.setString(1, bundlecode);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("status", rs.getString(1));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return map;
    }

    public ArrayList getGradesList() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select Grade,GRADE_DESC from TWSH_GRADE_MASTER with(nolock) where rowid not in('22','23','10','11') order by RowID asc";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("gcode", rs.getString(1));
                map.put("gname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
             try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }

    public HashMap<String,String> submitDetails(HttpServletRequest req, String username,RegisterForm rForm) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap<String,String> map = new HashMap<String,String>();
        try {
            String bundle = req.getParameter("bundle");
            String bundlecode = req.getParameter("bundlecode");
            String sytemIp = req.getRemoteAddr();
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_TWSH_OMR_Bundling(?,?,?,?,?,?,?)}");
            cstmt.setString(1, bundle.substring(0,bundle.length()-1));
            cstmt.setString(2, bundlecode);
            cstmt.setString(3, username);
            cstmt.setString(4, sytemIp);
            cstmt.setString(5, rForm.getGrade());
            cstmt.setString(6, rForm.getEcenter());
            cstmt.setString(7, rForm.getExaminationname());
            rs = cstmt.executeQuery();
            if (rs != null && rs.next()) {
                 map.put("status",rs.getString(1));
             map.put("statusmsg",rs.getString(2));
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                logger.info("e====================="+e.getMessage());
                e.printStackTrace();
            }
        }
        //  System.out.println("resDDD===" + res);
        return map;
    }

}
