/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author APTOL301294
 */
public class CenterExamReportDao {
    
    public ArrayList getGradeList() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            st = con.prepareStatement("select GRADE,GRADE_DESC from TWSH_GRADE_MASTER (nolock)");
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("gradecode", rs.getString(1));
                    map.put("gradename", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    
    public ArrayList getBatchList() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_ExamBatchTypeDetails_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("batchcode1", rs.getString(1));
                    map.put("batchname", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getCenteBasicData(String userName, String gradeCode, String batchCode, String paper, String examDate) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_AttendanceSheet_Report(?,?,?,?,?,?)}");
            cstmt.setString(1, userName);
            cstmt.setString(2, gradeCode);
            cstmt.setString(3, batchCode);
            cstmt.setString(4, paper);
            cstmt.setString(5, "1");
            cstmt.setString(6, examDate);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("ccode", rs.getString(1));
                    map.put("cname", rs.getString(2));
                    map.put("gradecode", rs.getString(3));
                    map.put("paper", rs.getString(4));
                    map.put("batch", rs.getString(5));
                    map.put("eedate", rs.getString(6));
                    map.put("etime", rs.getString(7));
                    map.put("totalCount", rs.getString(8));
                    map.put("abCount", rs.getString(9));
                    map.put("malCount", rs.getString(10));
                    map.put("attCount", rs.getString(11));
                    map.put("bufferCount", rs.getString(12));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getHallticktesList(String userName, String gradeCode, String batchCode, String paper, String examDate, String flag) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_AttendanceSheet_Report(?,?,?,?,?,?)}");
            cstmt.setString(1, userName);
            cstmt.setString(2, gradeCode);
            cstmt.setString(3, batchCode);
            cstmt.setString(4, paper);
            cstmt.setString(5, flag);
            cstmt.setString(6, examDate);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    lstDetails.add(rs.getString(1));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public void getPdfDataTW(String filepath, String filename, String userName, String gradeCode, String batchCode, String paper, String examDate) throws SQLException {
        createPDFTW(filepath, filename, userName, gradeCode, batchCode, paper, examDate);
    }

    private void createPDFTW(String filepath, String filename, String userName, String gradeCode, String batchCode, String paper, String examDate) {

        Document doc = new Document();
        PdfWriter docWriter = null;

        DecimalFormat df = new DecimalFormat("0.00");

        try {

            List<HashMap> list1 = getCenteBasicData(userName, gradeCode, batchCode, paper, examDate);
            HashMap map = (HashMap) list1.get(0);
            String ccode = map.get("ccode").toString();
            String cname = map.get("cname").toString();
            String gradecode = map.get("gradecode").toString();
            String batch = map.get("batch").toString();
            String eedate = map.get("eedate").toString();
            String etime = map.get("etime").toString();
            String totalCount = map.get("totalCount").toString();
            String abCount = map.get("abCount").toString();
            String malCount = map.get("malCount").toString();
            String attCount = map.get("attCount").toString();
            String bufferCount = map.get("bufferCount").toString();
            String paperNumber = map.get("paper").toString();

            ArrayList presentList = getHallticktesList(userName, gradeCode, batchCode, paper, examDate, "2");
            ArrayList abstList = getHallticktesList(userName, gradeCode, batchCode, paper, examDate, "3");
            ArrayList malList = getHallticktesList(userName, gradeCode, batchCode, paper, examDate, "4");
            ArrayList buffList = getHallticktesList(userName, gradeCode, batchCode, paper, examDate, "5");

            //special font sizes
            Font heading = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
            Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 16, Font.BOLD, new BaseColor(0, 0, 0));
            Font f5 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, new BaseColor(0, 0, 0)); //MAGENTA -Pink
            Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 9.5f);

            //file path
            String path = filepath + "/" + filename;

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

            doc.setPageSize(PageSize.A4.rotate());

            //open document
            doc.open();


            String exam = "";
            if (gradecode.startsWith("T")) {
                exam = "TYPEWRITING EXAMINATION";
            } else if (gradecode.startsWith("S")) {
                exam = "SHORTHAND EXAMINATION";
            }
            
            Paragraph proParagraph = new Paragraph("PROFORMA T.E-1");
            proParagraph.setAlignment(Element.ALIGN_RIGHT);
            proParagraph.setSpacingAfter(7f);

            Paragraph paragraph = new Paragraph(""
                    + "                    "
                    + "STATE BOARD OF TECHNICAL EDUCTION & TRAINING           "
                    + "                                      "
                    + "                                      "
                    + "                                      "
                    + "                          "
                    + " ANDHRA PRADESH - VIJAYAWADA"
                    + "                          "
                    + "                                        "
                    + "                                        "
                    + "                                        "
                    + "                                        "
                    + "" + exam + " – JULY/AUGUST, 2021", heading);


            paragraph.setAlignment(Element.ALIGN_CENTER);
            paragraph.setSpacingAfter(20f);

            float[] attcolumnWidths = {2.6f, 2.2f, 2.8f, 1f, 2.0f, 1f};
            PdfPTable attTable1 = new PdfPTable(attcolumnWidths);


            PdfPCell attTable = null;
            attTable = new PdfPCell(new Paragraph("", heading));
            attTable.setBorder(Rectangle.NO_BORDER);
            attTable1.addCell(attTable);

            attTable = new PdfPCell(new Paragraph("", heading));
            attTable.setBorder(Rectangle.NO_BORDER);
            attTable1.addCell(attTable);

            attTable = new PdfPCell(new Paragraph("ATTENDANCE SHEET", heading));
            attTable.setHorizontalAlignment(Element.ALIGN_CENTER);
            attTable.setBorderColor(new BaseColor(0, 0, 0));
            attTable.setBorderWidthTop(0.2f);
            attTable.setBorderWidthLeft(0.2f);
            attTable.setBorderWidthBottom(0.2f);
            attTable.setBorderWidthRight(0.2f);
            attTable.setFixedHeight(22f);
            attTable1.addCell(attTable);

            attTable = new PdfPCell(new Paragraph("", heading));
            attTable.setBorder(Rectangle.NO_BORDER);
            attTable1.addCell(attTable);

            attTable = new PdfPCell(new Paragraph("", heading));
            attTable.setBorder(Rectangle.NO_BORDER);
            attTable.setFixedHeight(22f);
            attTable1.addCell(attTable);

            attTable = new PdfPCell(new Paragraph("", heading));
            attTable.setBorder(Rectangle.NO_BORDER);
            attTable.setFixedHeight(22f);
            attTable1.addCell(attTable);

            Paragraph paragraph7 = new Paragraph("                (To be kept in seperate cover and sent along with the Answer Scripts)", heading);
            paragraph7.setAlignment(Element.ALIGN_CENTER);
            paragraph7.setSpacingBefore(5f);
            paragraph7.setSpacingAfter(15f);

            float[] attcolumnWidths1 = {0.1f, 0.1f, 0f, 0f, 0.6f, 0.6f};
            PdfPTable attTable11 = new PdfPTable(attcolumnWidths);
            attTable11.setWidthPercentage(99);

            PdfPCell attTable111 = null;
            attTable111 = new PdfPCell(new Paragraph("", heading));
            attTable111.setBorder(Rectangle.NO_BORDER);
            attTable111.setColspan(4);
            attTable11.addCell(attTable111);

            attTable111 = new PdfPCell(new Paragraph("CENTER CODE", heading));
            attTable111.setHorizontalAlignment(Element.ALIGN_CENTER);
            attTable111.setBorderColor(new BaseColor(0, 0, 0));
            attTable111.setPadding(2);
            attTable111.setBorderWidthTop(0.4f);
            attTable111.setBorderWidthLeft(0.4f);
            attTable111.setBorderWidthBottom(0.4f);
            attTable111.setBorderWidthRight(0.2f);
            attTable111.setFixedHeight(22f);
            attTable11.addCell(attTable111);

            attTable111 = new PdfPCell(new Paragraph(ccode, heading));
            attTable111.setHorizontalAlignment(Element.ALIGN_CENTER);
            attTable111.setBorderColor(new BaseColor(0, 0, 0));
            attTable111.setBorderWidthTop(0.4f);
            attTable111.setBorderWidthLeft(0.2f);
            attTable111.setBorderWidthBottom(0.4f);
            attTable111.setBorderWidthRight(0.4f);
            attTable111.setFixedHeight(22f);
            attTable11.addCell(attTable111);
            
            
            
            
            
//            float[] examWidth = {0.1f, 0.1f, 0f, 0f, 0.6f, 0.6f};
//            PdfPTable examWidthTable11 = new PdfPTable(examWidth);
//            examWidthTable11.setWidthPercentage(99);
//
//            PdfPCell examWidthTable11Pdf = null;
//            examWidthTable11Pdf = new PdfPCell(new Paragraph("Name of the examination Center ", heading));
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setColspan(3);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//
//            examWidthTable11Pdf = new PdfPCell(new Paragraph(cname, heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setColspan(3);
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//
//            examWidthTable11Pdf = new PdfPCell(new Paragraph("Subject/Grade", heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11Pdf.setFixedHeight(22f);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//            
//            examWidthTable11Pdf = new PdfPCell(new Paragraph(gradecode, heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setColspan(2);
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//            
//            
//            examWidthTable11Pdf = new PdfPCell(new Paragraph("Paper", heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11Pdf.setFixedHeight(22f);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//            
//            examWidthTable11Pdf = new PdfPCell(new Paragraph(paper, heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setColspan(2);
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//            
//            
//            
//            examWidthTable11Pdf = new PdfPCell(new Paragraph("Bach No", heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11Pdf.setFixedHeight(22f);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//            
//            examWidthTable11Pdf = new PdfPCell(new Paragraph(batch, heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setBorderColor(new BaseColor(0, 0, 0));
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//            
//            
//            examWidthTable11Pdf = new PdfPCell(new Paragraph("Day & Date", heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11Pdf.setFixedHeight(22f);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//            
//            examWidthTable11Pdf = new PdfPCell(new Paragraph(eedate, heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setBorderColor(new BaseColor(0, 0, 0));
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//            
//            
//            examWidthTable11Pdf = new PdfPCell(new Paragraph("Time of Examination", heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11Pdf.setFixedHeight(22f);
//            examWidthTable11.addCell(examWidthTable11Pdf);
//            
//            examWidthTable11Pdf = new PdfPCell(new Paragraph(etime, heading));
//            examWidthTable11Pdf.setHorizontalAlignment(Element.ALIGN_LEFT);
//            examWidthTable11Pdf.setBorder(Rectangle.NO_BORDER);
//            examWidthTable11.addCell(examWidthTable11Pdf);
            
            
            



            Chunk cname1 = new Chunk(cname, bfBold12);
            Chunk batch1 = new Chunk(batch, bfBold12);
            Chunk gradecode1 = new Chunk(gradecode, bfBold12);
            Chunk edate1 = new Chunk(eedate, bfBold12);
            Chunk etime1 = new Chunk(etime, bfBold12);
            Paragraph p = new Paragraph("");
            p.setSpacingBefore(30f);
            Chunk p1 = new Chunk("Name of the examination Center ", heading);
            p.add(p1);
            p.add(cname1);
            p.add(new Chunk("   Subject/Grade ", heading));
            p.add(gradecode1);
            
            Chunk paper1 = new Chunk(paper, bfBold12);
            Paragraph pp1 = new Paragraph("");
            pp1.add(new Chunk("Paper ", heading));
            pp1.add(" "+paper1);
            pp1.add(new Chunk(" Bach No  ", heading));
            pp1.add(batch1);
            pp1.add(new Chunk("   Day & Date ", heading));
            pp1.add(edate1);
            pp1.add(new Chunk("   Time of Examination ", heading));
            pp1.add(etime1);


            Paragraph paragraph2 = new Paragraph("NO. OF CANDIDATES PRESENT WITH HALL TICKETS: " + presentList.size(), f5);
            paragraph2.setAlignment(Element.ALIGN_CENTER);
            paragraph2.setSpacingBefore(30f);

            float[] columnWidths = {6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f};
            PdfPTable table = new PdfPTable(columnWidths);
            table.setWidthPercentage(100f);
            for (int x = 0; x < presentList.size(); x++) {
                int j = x;
                for (j = x; j < x + 10; j++) {
                    if (j < presentList.size()) {
                        insertCell(table, presentList.get(j) + "", Element.ALIGN_CENTER, 1, bf12);
                    } else {
                        insertCell(table, "", Element.ALIGN_CENTER, 1, bf12);
                    }
                }
                x = j - 1;
            }
            if (presentList.size() == 0) {
                insertCell(table, "NIL", Element.ALIGN_CENTER, 10, bf12);
            }
            paragraph2.add(table);

            Paragraph paragraph3 = new Paragraph("NO. OF CANDIDATES ABSENT : " + abstList.size(), f5);
            paragraph3.setAlignment(Element.ALIGN_CENTER);

            float[] columnWidths3 = {6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f};
            PdfPTable table3 = new PdfPTable(columnWidths3);
            table3.setWidthPercentage(100f);
            for (int x = 0; x < abstList.size(); x++) {
                int j = x;
                for (j = x; j < x + 10; j++) {
                    if (j < abstList.size()) {
                        insertCell(table3, abstList.get(j) + "", Element.ALIGN_CENTER, 1, bf12);
                    } else {
                        insertCell(table3, "", Element.ALIGN_CENTER, 1, bf12);
                    }
                }
                x = j - 1;
            }
            if (abstList.size() == 0) {
                insertCell(table3, "NIL", Element.ALIGN_CENTER, 10, bf12);
            }
            paragraph3.add(table3);


            Paragraph paragraph5 = new Paragraph("NO. OF MALPRACTICES BOOKED : " + malList.size(), f5);
            paragraph5.setAlignment(Element.ALIGN_CENTER);

            float[] columnWidths5 = {6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f};
            PdfPTable table5 = new PdfPTable(columnWidths5);
            table5.setWidthPercentage(100f);
            for (int x = 0; x < malList.size(); x++) {
                int j = x;
                for (j = x; j < x + 10; j++) {
                    if (j < malList.size()) {
                        insertCell(table5, malList.get(j) + "", Element.ALIGN_CENTER, 1, bf12);
                    } else {
                        insertCell(table5, "", Element.ALIGN_CENTER, 1, bf12);
                    }
                }
                x = j - 1;
            }
            if (malList.size() == 0) {
                insertCell(table5, "NIL", Element.ALIGN_CENTER, 10, bf12);
            }
            paragraph5.add(table5);


            Paragraph paragraph4 = new Paragraph("NO. OF SG'S (BUFFER OMR'S) USED : " + buffList.size(), f5);
            paragraph4.setAlignment(Element.ALIGN_CENTER);

            float[] columnWidths4 = {6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f, 6f};
            PdfPTable table4 = new PdfPTable(columnWidths4);
            table4.setWidthPercentage(100f);
            for (int x = 0; x < buffList.size(); x++) {
                int j = x;
                for (j = x; j < x + 10; j++) {
                    if (j < buffList.size()) {
                        insertCell(table4, buffList.get(j) + "", Element.ALIGN_CENTER, 1, bf12);
                    } else {
                        insertCell(table4, "", Element.ALIGN_CENTER, 1, bf12);
                    }
                }
                x = j - 1;
            }
            if (buffList.size() == 0) {
                insertCell(table4, "NIL", Element.ALIGN_CENTER, 10, bf12);
            }
            paragraph4.add(table4);


            Paragraph paragraph6 = new Paragraph("Date :                                            "
                    + "                                                                              "
                    + "                                                                              "
                    + "                                                                              "
                    //+ "                                                                              "
                    + "Signature:", f5);
            paragraph6.setSpacingBefore(50f);

            doc.add(proParagraph);
            doc.add(paragraph);
            doc.add(attTable1);
            doc.add(paragraph7);
            doc.add(attTable11);
            doc.add(p);
            doc.add(pp1);
            //doc.add(examWidthTable11);
            doc.add(paragraph2);
            doc.add(paragraph3);
            doc.add(paragraph5);
            doc.add(paragraph4);
            doc.add(paragraph6);



        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    private void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
//                    response.getOutputStream().flush();
//                    response.getOutputStream().close();
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
