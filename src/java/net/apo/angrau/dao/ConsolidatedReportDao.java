/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.sms.util.MessagePropertiesUtil;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.apo.angrau.forms.RegisterForm;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.action.RegisterFileUpload;
/**
 *
 * @author APTOL301294
 */
public class ConsolidatedReportDao {
    
    
    public List<HashMap> getDistrictWiseReport() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        int totalTotalAppli=0;
        int totalTotalPaid=0;
        int totalTotalUnPaid=0;
        int totalInstCount=0;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_ConsolidatedReport_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("instCode", rs.getString(1));
                    map.put("instName", rs.getString(2));
                    map.put("address", rs.getString(3));
                    map.put("totalAppli", rs.getString(4));
                    map.put("totalPaid", rs.getString(5));
                    map.put("totalUnPaid", rs.getString(6));
                    totalInstCount = totalInstCount + Integer.parseInt(rs.getString(3));
                    totalTotalAppli = totalTotalAppli + Integer.parseInt(rs.getString(4));
                    totalTotalPaid = totalTotalPaid + Integer.parseInt(rs.getString(5));
                    totalTotalUnPaid = totalTotalUnPaid + Integer.parseInt(rs.getString(6));
                    map.put("totalInstCount", totalInstCount);
                    map.put("totalTotalAppli", totalTotalAppli);
                    map.put("totalTotalPaid", totalTotalPaid);
                    map.put("totalTotalUnPaid", totalTotalUnPaid);
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getCandidateList(RegisterForm rForm) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        int totalTotalAppli=0;
        int totalTotalPaid=0;
        int totalTotalUnPaid=0;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_ConsolidatedDetailsReport_Get(?)}");
            cstmt.setString(1, rForm.getAadhar());//DistrictCode
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("instCode", rs.getString(1));
                    map.put("instName", rs.getString(2));
                    map.put("address", rs.getString(3));
                    map.put("totalAppli", rs.getString(4));
                    map.put("totalPaid", rs.getString(5));
                    map.put("totalUnPaid", rs.getString(6));
                    totalTotalAppli = totalTotalAppli + Integer.parseInt(rs.getString(4));
                    totalTotalPaid = totalTotalPaid + Integer.parseInt(rs.getString(5));
                    totalTotalUnPaid = totalTotalUnPaid + Integer.parseInt(rs.getString(6));
                    map.put("totalTotalAppli", totalTotalAppli);
                    map.put("totalTotalPaid", totalTotalPaid);
                    map.put("totalTotalUnPaid", totalTotalUnPaid);
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public String getUpdateCandidateInfo(RegisterForm rForm) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        String updateStatus = null;
        System.out.println("hiiiiiiii DAO");
        Date todate1 = new SimpleDateFormat("dd/mm/yyyy").parse(rForm.getDob());
        String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate1);

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Admin_Studdent_Info_Update(?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, rForm.getAadhar());
            cstmt.setString(2, rForm.getGrade());
            cstmt.setString(3, rForm.getBname());
            cstmt.setString(4, rForm.getFname());
            cstmt.setString(5, dob);
            cstmt.setString(6, rForm.getGender());
            cstmt.setString(7, rForm.getCaste());
            cstmt.setString(8, rForm.getUserName());
            cstmt.setString(9, rForm.getCwsn());//SystemIP
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    updateStatus = rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return updateStatus;
    }

    public HashMap<String, String> getData1(RegisterForm rform) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        RegisterForm regform = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {


            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_TWSH_Print(?,?)}");
            cstmt.setString(1, rform.getAadhar());
            cstmt.setString(2, rform.getGrade());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                map.put("name", rs.getString(1));
                map.put("fname", rs.getString(2));
//                map.put("dob", rform.getDob());
                map.put("gender", rs.getString(4));
                map.put("caste", rs.getString(5));

                map.put("visImp", rs.getString(6));
                map.put("institution", rs.getString(7));
                map.put("examinationAppearing", rs.getString(8));
                map.put("language", rs.getString(9));
                map.put("grade", rs.getString(10));
                map.put("examinationDistrict", rs.getString(11));

                map.put("examinationCenter", rs.getString(12));
                map.put("examinationDate", rs.getString(13));
                map.put("examinationBatch", rs.getString(14));
                map.put("hNo", rs.getString(15));
                map.put("street", rs.getString(16));

                map.put("village", rs.getString(17));
                map.put("state", rs.getString(18));
                map.put("district", rs.getString(19));
                map.put("mandal", rs.getString(20));
                map.put("pincode", rs.getString(21));

                map.put("mobile", rs.getString(22));
                map.put("email", rs.getString(23));
                map.put("aadhar", rs.getString(24));

                map.put("RegNo", rs.getString(26));
                map.put("paymentRefNo", rs.getString(28));
                map.put("paymentAmount", rs.getString(29));
                map.put("paymentDate", rs.getString(30));
                Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(31));
                String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                map.put("dob", dob);
                map.put("filepath", MessagePropertiesUtil.FIlE_PATH + "\\" + rform.getGrade().trim() + "\\" + rs.getString(24) + "\\" + rs.getString(25));
                map.put("date", new Date().toString());

            }


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }
    public ArrayList getcenterDistDetails(String examination) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            if (examination.equals("TW")) {
                query = "select DISTCD,DISTNAME from  STEPS_DISTRICT with(nolock) where DISTCD in(select DISTCD from  TWSH_CENTER_MASTER with(nolock) where TW_COURSE='Y') order by DISTCD";
            } else if (examination.equals("SH")) {
                query = "select DISTCD,DISTNAME from  STEPS_DISTRICT with(nolock) where DISTCD in(select DISTCD from  TWSH_CENTER_MASTER with(nolock) where  SH_COURSE='Y') order by DISTCD";
            }
            pstmt = con.prepareStatement(query);
//            pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }
    public ArrayList getStateDetails() throws Exception {

        String query = null;
        ArrayList list = new ArrayList();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select * from StateMaster order by StateName";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("scode", rs.getString(2));
                    map.put("sname", rs.getString(2));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {


                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;
    }
    public HashMap<String, String> getData(RegisterForm rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        RegisterForm regform = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        try {

            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_TWSH_Print(?,?)}");
            System.out.println("aadhar======================"+ rform.getAadhar());
            System.out.println("grade======================"+ rform.getGrade());
            cstmt.setString(1, rform.getAadhar().trim());
            cstmt.setString(2, rform.getGrade().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("fname", rs.getString(2));
//                request.setAttribute("dob", rform.getDob());
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("caste", rs.getString(5));

                request.setAttribute("blind", rs.getString(6));
                request.setAttribute("institution", rs.getString(7));
                request.setAttribute("examinationname", rs.getString(8));
                request.setAttribute("language", rs.getString(9));
                request.setAttribute("gradename", rs.getString(10));
                request.setAttribute("apdistrict1", rs.getString(11));

                request.setAttribute("ecenter1", rs.getString(12));
                request.setAttribute("edate1", rs.getString(13));
                request.setAttribute("ebatch1", rs.getString(14));
                request.setAttribute("houseno", rs.getString(15));
                request.setAttribute("street", rs.getString(16));

                request.setAttribute("village", rs.getString(17));
                request.setAttribute("state", rs.getString(18));
                request.setAttribute("district1", rs.getString(19));
                request.setAttribute("mandal1", rs.getString(20));
                request.setAttribute("pincode", rs.getString(21));

                request.setAttribute("mobile", rs.getString(22));
                request.setAttribute("email", rs.getString(23));
                request.setAttribute("aadhar", rs.getString(24));
                request.setAttribute("grade", rs.getString(32));
                request.setAttribute("examination", rs.getString(33));
                request.setAttribute("district", rs.getString(34));
                request.setAttribute("mandal", rs.getString(35));
                request.setAttribute("apdistrict", rs.getString(36));
                request.setAttribute("ecenter", rs.getString(37));
                   request.setAttribute("edate", rs.getString(38));
                request.setAttribute("ebatch", rs.getString(39));

//                request.setAttribute("RegNo", rs.getString(26));
//                request.setAttribute("paymentRefNo", rs.getString(28));
//                request.setAttribute("paymentAmount", rs.getString(29));
//                request.setAttribute("paymentDate", rs.getString(30));
                Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(31));
                String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                request.setAttribute("dob", dob);
                request.setAttribute("date", new Date());
//                String filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + rform.getGrade().trim() + "\\" + rs.getString(24) + "\\" + rs.getString(25);
//                File file = new File(filepath);
//                BufferedImage image = ImageIO.read(file);
//                ByteArrayOutputStream baos = new ByteArrayOutputStream();
//                ImageIO.write(image, "jpg", baos);
//                baos.flush();
//                byte[] imageInByteArray = baos.toByteArray();
//                String b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
//                request.setAttribute("photo", b64);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return map;
    }
    
    public String insertFiles(RegisterForm rform, String remoteAddress, String uname) {
        Connection con = null;
        //   CallableStatement cstmt = null;
        PreparedStatement cstmt = null;
        ResultSet rs = null;
        HashMap hashMap = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        String fileName1 = "";
        String fileName2 = "";
        String fileName3 = "";
        String fileName4 = "";
        String fileName5 = "";
        String fileName6 = "";
        String fileName7 = "";
        String fileName8 = "";

        RegisterFileUpload rf = new RegisterFileUpload();
        boolean flag = false;


        String hallticket = rform.getAadhar();
        String grade = rform.getGrade();
        if (rform.getPhoto() != null && rform.getPhoto().toString().length() > 0) {
            flag = rf.uploadDocs(rform.getPhoto(), hallticket + "_PHOTO1.jpg", hallticket, grade);
            if (flag == true) {
                fileName1 = hallticket + "_PHOTO1.jpg";
            }
        }
        if (rform.getSignature() != null && rform.getSignature().toString().length() > 0) {
            flag = rf.uploadDocs(rform.getSignature(), hallticket + "_SIGNATURE1.jpg", hallticket, grade);
            if (flag == true) {
                fileName3 = hallticket + "_SIGNATURE1.jpg";
            }
        }
        if (rform.getBlind().equalsIgnoreCase("YES")) {
            if (rform.getBlindfile() != null && rform.getBlindfile().toString().length() > 0) {
                flag = rf.uploadDocs(rform.getBlindfile(), hallticket + "_BLIND1.jpg", hallticket, grade);
                if (flag == true) {
                    fileName2 = hallticket + "_BLIND1.jpg";
                }
            }
        }
        if (rform.getUpload1() != null && rform.getUpload1().toString().length() > 0) {
            flag = rf.uploadDocs(rform.getUpload1(), hallticket + "_UPLOAD11.jpg", hallticket, grade);
            if (flag == true) {
                fileName4 = hallticket + "_UPLOAD11.jpg";
            }
        }
        if (rform.getUpload2() != null && rform.getUpload2().toString().length() > 0) {
            flag = rf.uploadDocs(rform.getUpload2(), hallticket + "_UPLOAD21.jpg", hallticket, grade);
            if (flag == true) {
                fileName5 = hallticket + "_UPLOAD21.jpg";
            }
        }
        if (rform.getUpload3() != null && rform.getUpload3().toString().length() > 0) {
            flag = rf.uploadDocs(rform.getUpload3(), hallticket + "_UPLOAD31.jpg", hallticket, grade);
            if (flag == true) {
                fileName6 = hallticket + "_UPLOAD31.jpg";
            }
        }
        if (rform.getUpload4() != null && rform.getUpload4().toString().length() > 0) {
            flag = rf.uploadDocs(rform.getUpload4(), hallticket + "_UPLOAD41.jpg", hallticket, grade);
            if (flag == true) {
                fileName7 = hallticket + "_UPLOAD41.jpg";
            }
        }
        if (rform.getUpload5() != null && rform.getUpload5().toString().length() > 0) {
            flag = rf.uploadDocs(rform.getUpload5(), hallticket + "_Hallticket1.jpg", hallticket, grade);
            if (flag == true) {
                fileName8 = hallticket + "_Hallticket1.jpg";
            }
        }
        try {
            Date todate2 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate2);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_Registraion_Insert1(?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?)}");
            cstmt.setString(1, rform.getBname());
            cstmt.setString(2, rform.getFname());
            cstmt.setString(3, dob);
            cstmt.setString(4, rform.getGender());
            cstmt.setString(5, rform.getCaste());
            cstmt.setString(6, rform.getBlind());
            cstmt.setString(7, fileName2);
            cstmt.setString(8, rform.getInstitute());
            cstmt.setString(9, rform.getExamination());
            cstmt.setString(10, rform.getLanguage());
            cstmt.setString(11, rform.getGrade());
            cstmt.setString(12, rform.getApdistrict());
            cstmt.setString(13, rform.getEcenter());
            cstmt.setString(14, rform.getEdate());
            cstmt.setString(15, rform.getEbatch());
            cstmt.setString(16, rform.getHouseno());
            cstmt.setString(17, rform.getLocality());
            cstmt.setString(18, rform.getVillage());
            cstmt.setString(19, rform.getState());
            if ((rform.getState().equalsIgnoreCase("Andra Pradesh"))||(rform.getState().equalsIgnoreCase("Telagana"))) {
                cstmt.setString(20, rform.getDistrict());
                cstmt.setString(21, rform.getMandal());
            } else {
               cstmt.setString(20, rform.getDistrict1());
                cstmt.setString(21, rform.getMandal1());
            }
            cstmt.setString(22, rform.getPincode());
            cstmt.setString(23, rform.getMobile());
            cstmt.setString(24, rform.getEmail());
            cstmt.setString(25, rform.getAadhar());
            cstmt.setString(26, fileName1);
            cstmt.setString(27, remoteAddress);
            cstmt.setString(28, uname);
            cstmt.setString(29, fileName3);
            cstmt.setString(30, fileName4);
            cstmt.setString(31, fileName5);
            cstmt.setString(32, fileName6);
            cstmt.setString(33, fileName7);
            cstmt.setString(34, rform.getHallticket());
            cstmt.setString(35, fileName8);
            cstmt.setString(36, "");
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        System.out.println("result===================" + result);
        return result;
    }
}
