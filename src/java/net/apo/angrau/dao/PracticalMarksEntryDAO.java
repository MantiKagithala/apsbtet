/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author 1582792
 */
public class PracticalMarksEntryDAO {

    public List<HashMap> getColleageDetails(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "1");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("caste", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));
                map.put("maxMarks", rs.getString(6));// Practicals
                map.put("epaper", rs.getString(7));
                req.setAttribute("epaper", rs.getString(7));
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getStudentDetailsIS(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "4");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("caste", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));
                map.put("maxMarks", rs.getString(6));//Practicals IS-109
                map.put("marks", rs.getString(7));//Sessional IS-109
                map.put("epaper", rs.getString(8));// PaperCodes

                req.setAttribute("epaper", rs.getString(8));
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
      //  System.out.println("lstDetails==" + lstDetails);
        return lstDetails;
    }

    public List<HashMap> getDetailsFP(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "5");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("caste", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));
                map.put("maxMarks", rs.getString(6));//Practicals IS-109
                map.put("marks", rs.getString(7));//Sessional IS-109
                map.put("epaper", rs.getString(8));// PaperCodes

                map.put("maxMarks2", rs.getString(9));//Practicals IS-109
                map.put("marks2", rs.getString(10));//Sessional IS-109
                map.put("epaper2", rs.getString(11));// PaperCodes

                map.put("maxMarks3", rs.getString(12));//Practicals IS-109
                map.put("marks3", rs.getString(13));//Sessional IS-109
                map.put("epaper3", rs.getString(14));// PaperCodes

                req.setAttribute("epaper1", rs.getString(8));
                req.setAttribute("epaper2", rs.getString(11));
                req.setAttribute("epaper3", rs.getString(14));
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("lstDetails=="+lstDetails);
        return lstDetails;
    }

    public List<HashMap> getDetailsID(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "6");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("caste", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));
                map.put("maxMarks", rs.getString(6));//Practicals IS-109
                map.put("marks", rs.getString(7));//Sessional IS-109
                map.put("epaper", rs.getString(8));// PaperCodes

                map.put("maxMarks2", rs.getString(9));//Practicals IS-109
                map.put("marks2", rs.getString(10));//Sessional IS-109
                map.put("epaper2", rs.getString(11));// PaperCodes

                map.put("maxMarks3", rs.getString(12));//Practicals IS-109
                map.put("marks3", rs.getString(13));//Sessional IS-109
                map.put("epaper3", rs.getString(14));// PaperCodes

                map.put("maxMarks4", rs.getString(15));//Practicals IS-109
                map.put("marks4", rs.getString(16));//Sessional IS-109
                map.put("epaper4", rs.getString(17));// PaperCodes

                map.put("maxMarks5", rs.getString(18));//Practicals IS-109
                map.put("marks5", rs.getString(19));//Sessional IS-109
                map.put("epaper5", rs.getString(20));// PaperCodes

                map.put("maxMarks6", rs.getString(21));//Practicals IS-109
                map.put("marks6", rs.getString(22));//Sessional IS-109
                map.put("epaper6", rs.getString(23));// PaperCodes

                req.setAttribute("epaper1", rs.getString(8));
                req.setAttribute("epaper2", rs.getString(11));
                req.setAttribute("epaper3", rs.getString(14));
                req.setAttribute("epaper4", rs.getString(17));
                req.setAttribute("epaper5", rs.getString(20));
                req.setAttribute("epaper6", rs.getString(23));
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("lstDetails=="+lstDetails);
        return lstDetails;
    }

    public ArrayList getcourses(String username) throws Exception {
        String query = null;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement pstmt = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
        try {
            con = DatabaseConnection.getConnection();
//            query = "select * from MSc_CourseMaster";
//            query = "select CourseID,COURSEName from CCIC_MasterDistrictWiseInstitutionWiseCourseDetails (nolock) where InstitutionID='" + username + "'";
//            pstmt = con.prepareStatement(query);
//            rs = pstmt.executeQuery();


            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get(?,?,?)}");
            cstmt.setString(1, username);
            cstmt.setString(2, "");
            cstmt.setString(3, "2");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("courseId", rs.getString(1));
                map.put("courseName", rs.getString(2));
                list.add(map);
                map = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // System.out.println("courselist======" + list);
        return list;
    }

    public String submitOfficeDetails(HttpServletRequest req, String username, String remoteAddress) {

        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String res = "0";
        try {

            String applicantid = req.getParameter("applicationdts");
            String maxMarks = req.getParameter("maxmarksdts");
            String obtainedMarks = req.getParameter("obtaineddts");
            String paper = req.getParameter("stype");
            String course = req.getParameter("coursetype");
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Insert (?,?,?,?,?,?,?)}");

            cstmt.setString(1, applicantid.substring(0, applicantid.length() - 1));
            cstmt.setString(2, course.substring(0, course.length() - 1));
            cstmt.setString(3, paper.substring(0, paper.length() - 1));
            cstmt.setString(4, maxMarks.substring(0, maxMarks.length() - 1));
            cstmt.setString(5, obtainedMarks.substring(0, obtainedMarks.length() - 1));
            cstmt.setString(6, remoteAddress);
            cstmt.setString(7, username);
            rs = cstmt.executeQuery();

            if (rs != null && rs.next()) {
                res = rs.getString(1);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("resDDD===" + res);
        return res;
    }

    public String submitIndustryDetails(HttpServletRequest req, String username, String remoteAddress) {
      //  System.out.println("submitOfficeDetails===");
        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String res = "0";
        try {

            String applicantid = req.getParameter("applicationdts");
            String maxMarks = req.getParameter("maxmarksdts");
            String obtainedMarks = req.getParameter("obtaineddts");
            String sesmaxMarks = req.getParameter("sesmaxmarksdts");
            String sesobtainedMarks = req.getParameter("sesobtaineddts");
            String paper = req.getParameter("stype");
            String course = req.getParameter("coursetype");


//            System.out.println("applicantid ====" + applicantid);
//            System.out.println("maxMarks ====" + maxMarks);
//            System.out.println("obtainedMarks ====" + obtainedMarks);
//            System.out.println("sesmaxMarks ====" + sesmaxMarks);
//            System.out.println("sesobtainedMarks ====" + sesobtainedMarks);
//            System.out.println("paper ====" + paper);
//            System.out.println("course ====" + course);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Insert_SePr (?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setString(1, applicantid.substring(0, applicantid.length() - 1));
            cstmt.setString(2, course.substring(0, course.length() - 1));
            cstmt.setString(3, paper.substring(0, paper.length() - 1));
            cstmt.setString(4, maxMarks.substring(0, maxMarks.length() - 1));
            cstmt.setString(5, obtainedMarks.substring(0, obtainedMarks.length() - 1));
            cstmt.setString(6, paper.substring(0, paper.length() - 1));
            cstmt.setString(7, sesmaxMarks.substring(0, sesmaxMarks.length() - 1));
            cstmt.setString(8, sesobtainedMarks.substring(0, sesobtainedMarks.length() - 1));
            cstmt.setString(9, remoteAddress);
            cstmt.setString(10, username);
            rs = cstmt.executeQuery();

            if (rs != null && rs.next()) {
                res = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public String submitOtherDetails(HttpServletRequest req, String username, String remoteAddress) {

        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String res = "0";
        try {

            String applicantid = req.getParameter("applicationdts");
            String maxMarks = req.getParameter("maxmarksdts");
            String obtainedMarks = req.getParameter("obtaineddts");
            String maxMarks2 = req.getParameter("maxmarksdts2");
            String obtainedMarks2 = req.getParameter("obtaineddts2");
            String maxMarks3 = req.getParameter("maxmarksdts3");
            String obtainedMarks3 = req.getParameter("obtaineddts3");

            String sesmaxMarks = req.getParameter("sesmaxmarksdts");
            String sesobtainedMarks = req.getParameter("sesobtaineddts");
            String sesmaxMarks2 = req.getParameter("sesmaxmarksdts2");
            String sesobtainedMarks2 = req.getParameter("sesobtaineddts2");
            String sesmaxMarks3 = req.getParameter("sesmaxmarksdts3");
            String sesobtainedMarks3 = req.getParameter("sesobtaineddts3");
            String paper = req.getParameter("stype");
            String paper2 = req.getParameter("stype2");
            String paper3 = req.getParameter("stype3");
            String course = req.getParameter("coursetype");

//            System.out.println("applicationdts===" + applicantid);
//            System.out.println("course===" + course);
//
//            System.out.println("==========================================");
//            System.out.println("paper===" + paper);
//            System.out.println("maxMarks===" + maxMarks);
//            System.out.println("obtainedMarks===" + obtainedMarks);
//
//            System.out.println("==========================================");
//            System.out.println("paper===" + paper2);
//            System.out.println("maxMarks===" + maxMarks2);
//            System.out.println("obtainedMarks===" + obtainedMarks2);
//
//            System.out.println("==========================================");
//            System.out.println("paper===" + paper3);
//            System.out.println("maxMarks===" + maxMarks3);
//            System.out.println("obtainedMarks===" + obtainedMarks3);
//
//            System.out.println("==========================================");

            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Insert_SePr_Tripple (?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?)}");

            cstmt.setString(1, applicantid.substring(0, applicantid.length() - 1));
            cstmt.setString(2, course.substring(0, course.length() - 1));
            cstmt.setString(3, paper.substring(0, paper.length() - 1));
            cstmt.setString(4, maxMarks.substring(0, maxMarks.length() - 1));
            cstmt.setString(5, obtainedMarks.substring(0, obtainedMarks.length() - 1));

            cstmt.setString(6, paper2.substring(0, paper2.length() - 1));
            cstmt.setString(7, maxMarks2.substring(0, maxMarks2.length() - 1));
            cstmt.setString(8, obtainedMarks2.substring(0, obtainedMarks2.length() - 1));

            cstmt.setString(9, paper3.substring(0, paper.length() - 1));
            cstmt.setString(10, maxMarks3.substring(0, maxMarks3.length() - 1));
            cstmt.setString(11, obtainedMarks3.substring(0, obtainedMarks3.length() - 1));


            cstmt.setString(12, paper.substring(0, paper.length() - 1));
            cstmt.setString(13, sesmaxMarks.substring(0, sesmaxMarks.length() - 1));
            cstmt.setString(14, sesobtainedMarks.substring(0, sesobtainedMarks.length() - 1));

            cstmt.setString(15, paper2.substring(0, paper2.length() - 1));
            cstmt.setString(16, sesmaxMarks2.substring(0, sesmaxMarks2.length() - 1));
            cstmt.setString(17, sesobtainedMarks2.substring(0, sesobtainedMarks2.length() - 1));

            cstmt.setString(18, paper3.substring(0, paper3.length() - 1));
            cstmt.setString(19, sesmaxMarks3.substring(0, sesmaxMarks3.length() - 1));
            cstmt.setString(20, sesobtainedMarks3.substring(0, sesobtainedMarks3.length() - 1));
            cstmt.setString(21, remoteAddress);
            cstmt.setString(22, username);
            rs = cstmt.executeQuery();

            if (rs != null && rs.next()) {
                res = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public String submitIDDetails(HttpServletRequest req, String username, String remoteAddress) {

        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String res = "0";
        try {

            String applicantid = req.getParameter("applicationdts");
            String maxMarks = req.getParameter("maxmarksdts");
            String obtainedMarks = req.getParameter("obtaineddts");
            String maxMarks2 = req.getParameter("maxmarksdts2");
            String obtainedMarks2 = req.getParameter("obtaineddts2");
            String maxMarks3 = req.getParameter("maxmarksdts3");
            String obtainedMarks3 = req.getParameter("obtaineddts3");
            String maxMarks4 = req.getParameter("maxmarksdts4");
            String obtainedMarks4 = req.getParameter("obtaineddts4");
            String maxMarks5 = req.getParameter("maxmarksdts5");
            String obtainedMarks5 = req.getParameter("obtaineddts5");
            String maxMarks6 = req.getParameter("maxmarksdts6");
            String obtainedMarks6 = req.getParameter("obtaineddts6");

            String sesmaxMarks = req.getParameter("sesmaxmarksdts");
            String sesobtainedMarks = req.getParameter("sesobtaineddts");
            String sesmaxMarks2 = req.getParameter("sesmaxmarksdts2");
            String sesobtainedMarks2 = req.getParameter("sesobtaineddts2");
            String sesmaxMarks3 = req.getParameter("sesmaxmarksdts3");
            String sesobtainedMarks3 = req.getParameter("sesobtaineddts3");
            String sesmaxMarks4 = req.getParameter("sesmaxmarksdts4");
            String sesobtainedMarks4 = req.getParameter("sesobtaineddts4");
            String sesmaxMarks5 = req.getParameter("sesmaxmarksdts5");
            String sesobtainedMarks5 = req.getParameter("sesobtaineddts5");
            String sesmaxMarks6 = req.getParameter("sesmaxmarksdts6");
            String sesobtainedMarks6 = req.getParameter("sesobtaineddts6");
            String paper = req.getParameter("stype");
            String paper2 = req.getParameter("stype2");
            String paper3 = req.getParameter("stype3");
            String paper4 = req.getParameter("stype4");
            String paper5 = req.getParameter("stype5");
            String paper6 = req.getParameter("stype6");
            String course = req.getParameter("coursetype");
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Insert_SePr_Six (?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?)}");

            cstmt.setString(1, applicantid.substring(0, applicantid.length() - 1));
            cstmt.setString(2, course.substring(0, course.length() - 1));
            cstmt.setString(3, paper.substring(0, paper.length() - 1));
            cstmt.setString(4, maxMarks.substring(0, maxMarks.length() - 1));
            cstmt.setString(5, obtainedMarks.substring(0, obtainedMarks.length() - 1));

            cstmt.setString(6, paper2.substring(0, paper2.length() - 1));
            cstmt.setString(7, maxMarks2.substring(0, maxMarks2.length() - 1));
            cstmt.setString(8, obtainedMarks2.substring(0, obtainedMarks2.length() - 1));

            cstmt.setString(9, paper3.substring(0, paper3.length() - 1));
            cstmt.setString(10, maxMarks3.substring(0, maxMarks3.length() - 1));
            cstmt.setString(11, obtainedMarks3.substring(0, obtainedMarks3.length() - 1));

            cstmt.setString(12, paper4.substring(0, paper4.length() - 1));
            cstmt.setString(13, maxMarks4.substring(0, maxMarks4.length() - 1));
            cstmt.setString(14, obtainedMarks4.substring(0, obtainedMarks4.length() - 1));

            cstmt.setString(15, paper5.substring(0, paper5.length() - 1));
            cstmt.setString(16, maxMarks5.substring(0, maxMarks5.length() - 1));
            cstmt.setString(17, obtainedMarks5.substring(0, obtainedMarks5.length() - 1));

            cstmt.setString(18, paper6.substring(0, paper6.length() - 1));
            cstmt.setString(19, maxMarks6.substring(0, maxMarks6.length() - 1));
            cstmt.setString(20, obtainedMarks6.substring(0, obtainedMarks6.length() - 1));

            cstmt.setString(21, paper.substring(0, paper.length() - 1));
            cstmt.setString(22, sesmaxMarks.substring(0, sesmaxMarks.length() - 1));
            cstmt.setString(23, sesobtainedMarks.substring(0, sesobtainedMarks.length() - 1));

            cstmt.setString(24, paper2.substring(0, paper2.length() - 1));
            cstmt.setString(25, sesmaxMarks2.substring(0, sesmaxMarks2.length() - 1));
            cstmt.setString(26, sesobtainedMarks2.substring(0, sesobtainedMarks2.length() - 1));

            cstmt.setString(27, paper3.substring(0, paper3.length() - 1));
            cstmt.setString(28, sesmaxMarks3.substring(0, sesmaxMarks3.length() - 1));
            cstmt.setString(29, sesobtainedMarks3.substring(0, sesobtainedMarks3.length() - 1));

            cstmt.setString(30, paper4.substring(0, paper4.length() - 1));
            cstmt.setString(31, sesmaxMarks4.substring(0, sesmaxMarks4.length() - 1));
            cstmt.setString(32, sesobtainedMarks4.substring(0, sesobtainedMarks4.length() - 1));

            cstmt.setString(33, paper5.substring(0, paper5.length() - 1));
            cstmt.setString(34, sesmaxMarks5.substring(0, sesmaxMarks5.length() - 1));
            cstmt.setString(35, sesobtainedMarks5.substring(0, sesobtainedMarks5.length() - 1));

            cstmt.setString(36, paper6.substring(0, paper6.length() - 1));
            cstmt.setString(37, sesmaxMarks6.substring(0, sesmaxMarks6.length() - 1));
            cstmt.setString(38, sesobtainedMarks6.substring(0, sesobtainedMarks6.length() - 1));
            cstmt.setString(39, remoteAddress);
            cstmt.setString(40, username);
            rs = cstmt.executeQuery();

            if (rs != null && rs.next()) {
                res = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }
     public String submitIntegritySafetyDetails(HttpServletRequest req, String username, String remoteAddress) {

        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String res = "0";
        try {

            String applicantid = req.getParameter("applicationdts");
            String maxMarks = req.getParameter("maxmarksdts");
            String obtainedMarks = req.getParameter("obtaineddts");

            String sesmaxMarks = req.getParameter("sesmaxmarksdts");
            String sesobtainedMarks = req.getParameter("sesobtaineddts");
            String sesmaxMarks2 = req.getParameter("sesmaxmarksdts2");
            String sesobtainedMarks2 = req.getParameter("sesobtaineddts2");
            String sesmaxMarks3 = req.getParameter("sesmaxmarksdts3");
            String sesobtainedMarks3 = req.getParameter("sesobtaineddts3");
            String sesmaxMarks4 = req.getParameter("sesmaxmarksdts4");
            String sesobtainedMarks4 = req.getParameter("sesobtaineddts4");
            String sesmaxMarks5 = req.getParameter("sesmaxmarksdts5");
            String sesobtainedMarks5 = req.getParameter("sesobtaineddts5");
            String sesmaxMarks6 = req.getParameter("sesmaxmarksdts6");
            String sesobtainedMarks6 = req.getParameter("sesobtaineddts6");

            String sesmaxMarks7 = req.getParameter("sesmaxmarksdts7");
            String sesobtainedMarks7 = req.getParameter("sesobtaineddts7");
            String sesmaxMarks8 = req.getParameter("sesmaxmarksdts8");
            String sesobtainedMarks8 = req.getParameter("sesobtaineddts8");
            String sesmaxMarks9 = req.getParameter("sesmaxmarksdts9");
            String sesobtainedMarks9 = req.getParameter("sesobtaineddts9");


            String paper = req.getParameter("stype");
            String paper2 = req.getParameter("stype2");
            String paper3 = req.getParameter("stype3");
            String paper4 = req.getParameter("stype4");
            String paper5 = req.getParameter("stype5");
            String paper6 = req.getParameter("stype6");

            String paper7 = req.getParameter("stype7");
            String paper8 = req.getParameter("stype8");
            String paper9 = req.getParameter("stype9");

            String course = req.getParameter("coursetype");
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Insert_SePr_IS (?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?)}");

            cstmt.setString(1, applicantid.substring(0, applicantid.length() - 1));
            cstmt.setString(2, course.substring(0, course.length() - 1));

            cstmt.setString(3, paper.substring(0, paper.length() - 1));
            cstmt.setString(4, sesmaxMarks.substring(0, sesmaxMarks.length() - 1));
            cstmt.setString(5, sesobtainedMarks.substring(0, sesobtainedMarks.length() - 1));

            cstmt.setString(6, paper2.substring(0, paper2.length() - 1));
            cstmt.setString(7, sesmaxMarks2.substring(0, sesmaxMarks2.length() - 1));
            cstmt.setString(8, sesobtainedMarks2.substring(0, sesobtainedMarks2.length() - 1));

            cstmt.setString(9, paper3.substring(0, paper3.length() - 1));
            cstmt.setString(10, sesmaxMarks3.substring(0, sesmaxMarks3.length() - 1));
            cstmt.setString(11, sesobtainedMarks3.substring(0, sesobtainedMarks3.length() - 1));

            cstmt.setString(12, paper4.substring(0, paper4.length() - 1));
            cstmt.setString(13, sesmaxMarks4.substring(0, sesmaxMarks4.length() - 1));
            cstmt.setString(14, sesobtainedMarks4.substring(0, sesobtainedMarks4.length() - 1));

            cstmt.setString(15, paper5.substring(0, paper5.length() - 1));
            cstmt.setString(16, sesmaxMarks5.substring(0, sesmaxMarks5.length() - 1));
            cstmt.setString(17, sesobtainedMarks5.substring(0, sesobtainedMarks5.length() - 1));

            cstmt.setString(18, paper6.substring(0, paper6.length() - 1));
            cstmt.setString(19, sesmaxMarks6.substring(0, sesmaxMarks6.length() - 1));
            cstmt.setString(20, sesobtainedMarks6.substring(0, sesobtainedMarks6.length() - 1));

            cstmt.setString(21, paper7.substring(0, paper7.length() - 1));
            cstmt.setString(22, sesmaxMarks7.substring(0, sesmaxMarks7.length() - 1));
            cstmt.setString(23, sesobtainedMarks7.substring(0, sesobtainedMarks7.length() - 1));

            cstmt.setString(24, paper8.substring(0, paper8.length() - 1));
            cstmt.setString(25, sesmaxMarks8.substring(0, sesmaxMarks8.length() - 1));
            cstmt.setString(26, sesobtainedMarks8.substring(0, sesobtainedMarks8.length() - 1));

            cstmt.setString(27, paper9.substring(0, paper9.length() - 1));
            cstmt.setString(28, sesmaxMarks9.substring(0, sesmaxMarks9.length() - 1));
            cstmt.setString(29, sesobtainedMarks9.substring(0, sesobtainedMarks9.length() - 1));

            cstmt.setString(30, paper9.substring(0, paper9.length() - 1));
            cstmt.setString(31, maxMarks.substring(0, maxMarks.length() - 1));
            cstmt.setString(32, obtainedMarks.substring(0, obtainedMarks.length() - 1));

            cstmt.setString(33, remoteAddress);
            cstmt.setString(34, username);
            rs = cstmt.executeQuery();

            if (rs != null && rs.next()) {
                res = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    public String submitYoDetails(HttpServletRequest req, String username, String remoteAddress) {

        Connection con = null;
        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String res = "0";
        try {

            String applicantid = req.getParameter("applicationdts");
            String maxMarks = req.getParameter("maxmarksdts");
            String obtainedMarks = req.getParameter("obtaineddts");

            String sesmaxMarks = req.getParameter("sesmaxmarksdts");
            String sesobtainedMarks = req.getParameter("sesobtaineddts");
            String sesmaxMarks2 = req.getParameter("sesmaxmarksdts2");
            String sesobtainedMarks2 = req.getParameter("sesobtaineddts2");
            String sesmaxMarks3 = req.getParameter("sesmaxmarksdts3");
            String sesobtainedMarks3 = req.getParameter("sesobtaineddts3");
            String sesmaxMarks4 = req.getParameter("sesmaxmarksdts4");
            String sesobtainedMarks4 = req.getParameter("sesobtaineddts4");



            String paper = req.getParameter("stype");
            String paper2 = req.getParameter("stype2");
            String paper3 = req.getParameter("stype3");
            String paper4 = req.getParameter("stype4");


            String course = req.getParameter("coursetype");
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Insert_SePr_YO (?,?,?,?,?,?,?,?,?,?,"
                     + "?,?,?,?,?,?,?,?,?)}");

            cstmt.setString(1, applicantid.substring(0, applicantid.length() - 1));
            cstmt.setString(2, course.substring(0, course.length() - 1));

            cstmt.setString(3, paper.substring(0, paper.length() - 1));
            cstmt.setString(4, sesmaxMarks.substring(0, sesmaxMarks.length() - 1));
            cstmt.setString(5, sesobtainedMarks.substring(0, sesobtainedMarks.length() - 1));

            cstmt.setString(6, paper2.substring(0, paper2.length() - 1));
            cstmt.setString(7, sesmaxMarks2.substring(0, sesmaxMarks2.length() - 1));
            cstmt.setString(8, sesobtainedMarks2.substring(0, sesobtainedMarks2.length() - 1));

            cstmt.setString(9, paper3.substring(0, paper3.length() - 1));
            cstmt.setString(10, sesmaxMarks3.substring(0, sesmaxMarks3.length() - 1));
            cstmt.setString(11, sesobtainedMarks3.substring(0, sesobtainedMarks3.length() - 1));

            cstmt.setString(12, paper4.substring(0, paper4.length() - 1));
            cstmt.setString(13, sesmaxMarks4.substring(0, sesmaxMarks4.length() - 1));
            cstmt.setString(14, sesobtainedMarks4.substring(0, sesobtainedMarks4.length() - 1));

            cstmt.setString(15, paper4.substring(0, paper4.length() - 1));
            cstmt.setString(16, maxMarks.substring(0, maxMarks.length() - 1));
            cstmt.setString(17, obtainedMarks.substring(0, obtainedMarks.length() - 1));

            cstmt.setString(18, remoteAddress);
            cstmt.setString(19, username);
            rs = cstmt.executeQuery();

            if (rs != null && rs.next()) {
                res = rs.getString(1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return res;
    }


 public List<HashMap> getDetailsIntegritySafety(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "7");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("dob", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));

                //map.put("maxMarks", rs.getString(6));//Practicals IS-109
                map.put("marks", rs.getString(6));//Sessional IS-109
                map.put("epaper", rs.getString(7));// PaperCodes

                // map.put("maxMarks2", rs.getString(9));//Practicals IS-109
                map.put("marks2", rs.getString(8));//Sessional IS-109
                map.put("epaper2", rs.getString(9));// PaperCodes

                // map.put("maxMarks3", rs.getString(12));//Practicals IS-109
                map.put("marks3", rs.getString(10));//Sessional IS-109
                map.put("epaper3", rs.getString(11));// PaperCodes

                // map.put("maxMarks4", rs.getString(15));//Practicals IS-109
                map.put("marks4", rs.getString(12));//Sessional IS-109
                map.put("epaper4", rs.getString(13));// PaperCodes

                // map.put("maxMarks5", rs.getString(18));//Practicals IS-109
                map.put("marks5", rs.getString(14));//Sessional IS-109
                map.put("epaper5", rs.getString(15));// PaperCodes

                // map.put("maxMarks6", rs.getString(21));//Practicals IS-109
                map.put("marks6", rs.getString(16));//Sessional IS-109
                map.put("epaper6", rs.getString(17));// PaperCodes

                map.put("marks7", rs.getString(18));//Sessional IS-109
                map.put("epaper7", rs.getString(19));// PaperCodes

                map.put("marks8", rs.getString(20));//Sessional IS-109
                map.put("epaper8", rs.getString(21));// PaperCodes

                map.put("maxMarks9", rs.getString(22));//Practicals IS-109
                map.put("marks9", rs.getString(23));//Sessional IS-109
                map.put("epaper9", rs.getString(24));// PaperCodes

                req.setAttribute("epaper1", rs.getString(7));
                req.setAttribute("epaper2", rs.getString(9));
                req.setAttribute("epaper3", rs.getString(11));
                req.setAttribute("epaper4", rs.getString(13));
                req.setAttribute("epaper5", rs.getString(15));
                req.setAttribute("epaper6", rs.getString(17));
                req.setAttribute("epaper7", rs.getString(19));
                req.setAttribute("epaper8", rs.getString(21));
                req.setAttribute("epaper9", rs.getString(24));
                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("lstDetails=="+lstDetails);
        return lstDetails;
    }

    public List<HashMap> getDetailsYo(RegisterForm rform, HttpServletRequest req) {

        Connection con = null;

        CallableStatement cstmt = null;
        PreparedStatement psmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_PracticalMarks_Get(?,?,?)}");
            cstmt.setString(1, rform.getCourse());
            cstmt.setString(2, rform.getUserName());
            cstmt.setString(3, "8");
            rs = cstmt.executeQuery();

            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("applicantid", rs.getString(1));
                map.put("applicantname", rs.getString(2));
                map.put("gender", rs.getString(3));
                map.put("dob", rs.getString(4));//DateofBirth
                map.put("course", rs.getString(5));

                //map.put("maxMarks", rs.getString(6));//Practicals IS-109
                map.put("marks", rs.getString(6));//Sessional IS-109
                map.put("epaper", rs.getString(7));// PaperCodes

                // map.put("maxMarks2", rs.getString(9));//Practicals IS-109
                map.put("marks2", rs.getString(8));//Sessional IS-109
                map.put("epaper2", rs.getString(9));// PaperCodes

                // map.put("maxMarks3", rs.getString(12));//Practicals IS-109
                map.put("marks3", rs.getString(10));//Sessional IS-109
                map.put("epaper3", rs.getString(11));// PaperCodes

                map.put("maxMarks4", rs.getString(12));//Practicals IS-109
                map.put("marks4", rs.getString(13));//Sessional IS-109
                map.put("epaper4", rs.getString(14));// PaperCodes




                req.setAttribute("epaper1", rs.getString(7));
                req.setAttribute("epaper2", rs.getString(9));
                req.setAttribute("epaper3", rs.getString(11));
                req.setAttribute("epaper4", rs.getString(14));

                lstDetails.add(map);
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("lstDetails=="+lstDetails);
        return lstDetails;
    }
}
