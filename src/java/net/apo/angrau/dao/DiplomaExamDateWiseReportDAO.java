/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.Base64;
import com.sms.util.MessagePropertiesUtil;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.db.DatabaseConnection;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author 1820530
 */
public class DiplomaExamDateWiseReportDAO {

    private static DiplomaExamDateWiseReportDAO commonObj = null;

    public ArrayList getBrachList(String yearOrSem, String userName) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Diploma_ExamDatesbySemister_Get(?)}");
            cstmt.setString(1, yearOrSem);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("braC", rs.getString(2));
                    map.put("braN", rs.getString(1));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    private DiplomaExamDateWiseReportDAO() {
    }

    public static DiplomaExamDateWiseReportDAO getInstance() {

        if (commonObj == null) {
            commonObj = new DiplomaExamDateWiseReportDAO();
        }
        return commonObj;
    }

    public ArrayList getGradeList() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_GradeDetails_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("gradecode", rs.getString(1));
                    map.put("gradename", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getBatchList() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_ExamBatchTypeDetails_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("batchcode1", rs.getString(1));
                    map.put("batchname", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;

    }

    public ArrayList getCenterList() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_CentersUserDetails_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("centercode", rs.getString(1));
                    map.put("centerName", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;

    }

    public List<HashMap> getApplicationStatusReport(String userName, String gradeCode, String batchCode, String status) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_NRDATA_ApplicantStatusReport_Get(?,?,?,?)}");
            cstmt.setString(1, userName);
            cstmt.setString(2, gradeCode);
            cstmt.setString(3, batchCode);
            cstmt.setString(4, status);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sno", rs.getString(1));
                    map.put("instName", rs.getString(2));
                    map.put("address", rs.getString(3));
                    map.put("totalAppli", rs.getString(4));
                    map.put("totalPaid", rs.getString(5));
                    map.put("totalUnPaid", rs.getString(6));

                    map.put("add", rs.getString(7));
                    map.put("centerC", rs.getString(8));
                    map.put("centerN", rs.getString(9));
                    map.put("grade", rs.getString(10));
                    map.put("batch", rs.getString(11));
                    map.put("mobile", rs.getString(12));

                    map.put("instcode", rs.getString(13));
                    map.put("phhot", rs.getString(14));
                    map.put("sig", rs.getString(15));
                    map.put("remarks", rs.getString(17));
                    map.put("examDate", rs.getString(18));

                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getApplicationStatusCCICReport(String userName, String roleId, String status) {

        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;

        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_NRDATA_ApplicantStatusReport_Get(?,?,?)}");
            cstmt.setString(1, userName);
            cstmt.setString(2, roleId);
            cstmt.setString(3, status);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sno", rs.getString(1));
                    map.put("regNo", rs.getString(2));
                    map.put("name", rs.getString(3)); //name
                    map.put("fatherName", rs.getString(4)); // fname
                    map.put("dob", rs.getString(5)); //dob
                    map.put("instName", rs.getString(6));//institute name

                    map.put("instAddress", rs.getString(7)); // institute address
                    map.put("centerC", rs.getString(8));//center code
                    map.put("centerN", rs.getString(9)); // centre name                
                    map.put("mobile", rs.getString(10));// mobile.no

                    map.put("instcode", rs.getString(11)); // institute code
                    map.put("phhot", rs.getString(12));
                    map.put("sig", rs.getString(13));
                    map.put("remarks", rs.getString(15));
                    map.put("examDate", rs.getString(16));
                    map.put("courseId", rs.getString(17));
                    map.put("subject", rs.getString(18));
                    map.put("category", rs.getString(19));
                    String filepath = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(17) + "\\" + rs.getString(14) + "\\" + rs.getString(12);
//                    String filepath = MessagePropertiesUtil.FIlE_PATH + "\\SEH"+"\\449585831514"+"\\449585831514_PHOTO.jpg";
                    File file = new File(filepath);
                    map.put("photopath", filepath);
                    String b64 = "";
                    if (file.exists() && !file.isDirectory()) {
                        try {
                            byte[] inFileName = FileUtils.readFileToByteArray(file);
                            b64 = DatatypeConverter.printBase64Binary(inFileName);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    map.put("photo", b64);

                    String filepath1 = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(17) + "\\" + rs.getString(14) + "\\" + rs.getString(13);
//                    String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\SEH" + "\\449585831514" + "\\449585831514_SIGNATURE.jpg";
                    map.put("sigpath", filepath1);
                    File file1 = new File(filepath1);
                    String sigb64 = "";
                    if (file1.exists() && !file1.isDirectory()) {
                        try {
                            byte[] inFileName1 = FileUtils.readFileToByteArray(file1);
                            sigb64 = DatatypeConverter.printBase64Binary(inFileName1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    map.put("sig", sigb64);

                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getNRReport(String userName, String yearOrSem, String examDate, String centerCode) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Diploma_ApplicantAttendanceStatusReport_Get(?,?,?)}");
            cstmt.setString(1, userName);
            cstmt.setString(2, yearOrSem);
            cstmt.setString(3, examDate);
//            System.out.println("userName : " + userName);
//            System.out.println("course : " + course);
//            System.out.println("centerCode : " + centerCode);
//            System.out.println("subCode : " + subCode);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("centerC", rs.getString(1));
                    map.put("branchC", rs.getString(2));
                    map.put("subName", rs.getString(3));
                    map.put("rollNo", rs.getString(4));
                    map.put("year", rs.getString(5));
                    map.put("examDate", rs.getString(6));
                    map.put("status", rs.getString(7));
                    lstDetails.add(map);
                    map = null;
                }
            }
//            map = new HashMap();
//            map.put("centerC", "1");
//            map.put("branchC", "2");
//            map.put("subName", "3");
//            map.put("rollNo", "4");
//            map.put("year", "5");
//            map.put("examDate", "6");
//            map.put("status", "7");
//            lstDetails.add(map);
//            map = null;
//            map = new HashMap();
//            map.put("centerC", "A");
//            map.put("branchC", "B");
//            map.put("subName", "C");
//            map.put("rollNo", "D");
//            map.put("year", "E");
//            map.put("examDate", "F");
//            map.put("status", "G");
//            lstDetails.add(map);
//            map = null;
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
//                    response.getOutputStream().flush();
//                    response.getOutputStream().close();
                    out.flush();
                    out.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getPdfDataNR(String filepath, String filename, String userName, String course, String subCode, String centerCode) throws SQLException {
        createPDFNR(filepath, filename, userName, course, subCode, centerCode);
    }

    private void createPDFNR(String filepath, String filename, String userName, String course, String subCode, String centerCode) {

        Document doc = new Document();
        PdfWriter docWriter = null;

        DecimalFormat df = new DecimalFormat("0.00");

        try {

            List<HashMap> list = getNRReport(userName, course, subCode, centerCode);
            //special font sizes
            Font heading = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
            Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, new BaseColor(0, 0, 0));
            Font f5 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD, new BaseColor(119, 41, 4)); //MAGENTA -Pink
            Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 9.5f);

            //file path
            String path = filepath + "/" + filename;

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

            doc.setPageSize(PageSize.A4.rotate());

            //open document
            doc.open();

            //create a paragraph

            Paragraph paragraph3 = new Paragraph(" Report : Date Wise Exam Report", bfBold12);

            //specify column widths
            float[] columnWidths = {2f, 5f, 4f, 6f, 6f, 4f, 4f, 6f}; //8
            //create PDF table with the given widths
            PdfPTable table = new PdfPTable(columnWidths);
            // set table width a percentage of the page width
            table.setWidthPercentage(100f);

            //insert column headings
            insertCell(table, "SNO", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Center Code", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Branch Code", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Subject", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "PIN Number", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Year/Semester", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Exam Date", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Attendance Status", Element.ALIGN_CENTER, 1, bfBold12);
            table.setHeaderRows(1);
            int j = 0;
            for (int x = 0; x < list.size(); x++) {
                HashMap map = list.get(x);
                insertCell(table, String.valueOf(++j) + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("centerC") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("branchC") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("subName") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("rollNo") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("year") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("examDate") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("status") + "", Element.ALIGN_CENTER, 1, bf12);
                map = null;

            }
            paragraph3.add(table);
            doc.add(paragraph3);

        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    private void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

    public ArrayList getSubjectList(String courseId) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_CCIC_MasterCourseSubjectDetails_Get(?)}");
            cstmt.setString(1, courseId);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("subC", rs.getString(3));
                    map.put("subN", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getCourseList(String userName) {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Diploma_NRSemisterDetails_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("ccd", rs.getString(1));
                    map.put("courseName", rs.getString(1));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getccicCenterList(String courseId, String subject) {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call USP_CENTERNameDetails_Get(?,?)}");
            cstmt.setString(1, courseId);
            cstmt.setString(2, subject);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("centercode", rs.getString(3));
                    map.put("centerName", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public String getImageInBase64Format(String pin, String filename) throws Exception {

        //   String path = "\\\\10.3.37.204\\E$\\ISMS\\UDISE\\";
        //   String path = "\\\\10.138.133.8\\D$\\Diploma\\";
        String path = "\\E$\\Diploma\\";

        String filepath = null;
        String b64 = "";
        ByteArrayOutputStream baos = null;
        BufferedImage image = null;
        try {
            filepath = path + pin + "\\" + filename;
            File file = new File(filepath);
            if (!file.exists()) {
                return "";
            }
            image = ImageIO.read(file);
            baos = new ByteArrayOutputStream();
            int lastIndex = file.getName().lastIndexOf('.');

            String extenstion = file.getName().substring(lastIndex);
            if (extenstion != null && extenstion.equalsIgnoreCase(".jpg")) {
                ImageIO.write(image, "jpg", baos);
            } else {
                ImageIO.write(image, "png", baos);
            }

            baos.flush();
            byte[] imageInByteArray = baos.toByteArray();
            b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                baos.close();
            }

            if (image != null) {
            }
        }
        return b64;
    }
}
