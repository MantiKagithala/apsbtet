/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sms.util.MessagePropertiesUtil;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.db.DatabaseConnection;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author 1820530
 */
public class ApplicationStatusDAO {

    private static ApplicationStatusDAO commonObj = null;

    private ApplicationStatusDAO() {
    }

    public static ApplicationStatusDAO getInstance() {

        if (commonObj == null) {
            commonObj = new ApplicationStatusDAO();
        }
        return commonObj;
    }

    public ArrayList getGradeList() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_GradeDetails_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("gradecode", rs.getString(1));
                    map.put("gradename", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getBatchList() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_TWSH_ExamBatchTypeDetails_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("batchcode1", rs.getString(1));
                    map.put("batchname", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;

    }

    public ArrayList getCenterList() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_CentersUserDetails_Get}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("centercode", rs.getString(1));
                    map.put("centerName", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;

    }

    public List<HashMap> getApplicationStatusReport(String userName, String gradeCode, String batchCode, String status) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_NRDATA_ApplicantStatusReport_Get(?,?,?,?)}");
            cstmt.setString(1, userName);
            cstmt.setString(2, gradeCode);
            cstmt.setString(3, batchCode);
            cstmt.setString(4, status);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sno", rs.getString(1));
                    map.put("instName", rs.getString(2));
                    map.put("address", rs.getString(3));
                    map.put("totalAppli", rs.getString(4));
                    map.put("totalPaid", rs.getString(5));
                    map.put("totalUnPaid", rs.getString(6));

                    map.put("add", rs.getString(7));
                    map.put("centerC", rs.getString(8));
                    map.put("centerN", rs.getString(9));
                    map.put("grade", rs.getString(10));
                    map.put("batch", rs.getString(11));
                    map.put("mobile", rs.getString(12));

                    map.put("instcode", rs.getString(13));
                    map.put("phhot", rs.getString(14));
                    map.put("sig", rs.getString(15));
                    map.put("remarks", rs.getString(17));
                    map.put("examDate", rs.getString(18));
                    String filepath = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(10) + "\\" + rs.getString(16) + "\\" + rs.getString(14);
//                    String filepath = MessagePropertiesUtil.FIlE_PATH + "\\SEH"+"\\449585831514"+"\\449585831514_PHOTO.jpg";
                    File file = new File(filepath);
                    map.put("photopath", filepath);
                    String b64 = "";
                    if (file.exists() && !file.isDirectory()) {
                        try {
                            byte[] inFileName = FileUtils.readFileToByteArray(file);
                            b64 = DatatypeConverter.printBase64Binary(inFileName);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    map.put("photo", b64);

                    String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\" + rs.getString(10) + "\\" + rs.getString(16) + "\\" + rs.getString(15);
//                    String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\SEH" + "\\449585831514" + "\\449585831514_SIGNATURE.jpg";
                    map.put("sigpath", filepath1);
                    File file1 = new File(filepath1);
                    String sigb64 = "";
                    if (file1.exists() && !file1.isDirectory()) {
                        try {
                            byte[] inFileName1 = FileUtils.readFileToByteArray(file1);
                            sigb64 = DatatypeConverter.printBase64Binary(inFileName1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    map.put("sig", sigb64);

                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getApplicationStatusCCICReport(String userName, String roleId, String status) {

        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;

        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call CCIC_NRDATA_ApplicantStatusReport_Get(?,?,?)}");
            cstmt.setString(1, userName);
            cstmt.setString(2, roleId);
            cstmt.setString(3, status);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sno", rs.getString(1));
                    map.put("regNo", rs.getString(2));
                    map.put("name", rs.getString(3)); //name
                    map.put("fatherName", rs.getString(4)); // fname
                    map.put("dob", rs.getString(5)); //dob
                    map.put("instName", rs.getString(6));//institute name

                    map.put("instAddress", rs.getString(7)); // institute address
                    map.put("centerC", rs.getString(8));//center code
                    map.put("centerN", rs.getString(9)); // centre name                
                    map.put("mobile", rs.getString(10));// mobile.no

                    map.put("instcode", rs.getString(11)); // institute code
                    map.put("phhot", rs.getString(12));
                    map.put("sig", rs.getString(13));
                    map.put("remarks", rs.getString(15));
                    map.put("examDate", rs.getString(16));
                    map.put("courseId", rs.getString(17));
                    map.put("subject", rs.getString(18));
                    map.put("category", rs.getString(19));
                    String filepath = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(17) + "\\" + rs.getString(14) + "\\" + rs.getString(12);
//                    String filepath = MessagePropertiesUtil.FIlE_PATH + "\\SEH"+"\\449585831514"+"\\449585831514_PHOTO.jpg";
                    File file = new File(filepath);
                    map.put("photopath", filepath);
                    String b64 = "";
                    if (file.exists() && !file.isDirectory()) {
                        try {
                            byte[] inFileName = FileUtils.readFileToByteArray(file);
                            b64 = DatatypeConverter.printBase64Binary(inFileName);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    map.put("photo", b64);

                    String filepath1 = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(17) + "\\" + rs.getString(14) + "\\" + rs.getString(13);
//                    String filepath1 = MessagePropertiesUtil.FIlE_PATH + "\\SEH" + "\\449585831514" + "\\449585831514_SIGNATURE.jpg";
                    map.put("sigpath", filepath1);
                    File file1 = new File(filepath1);
                    String sigb64 = "";
                    if (file1.exists() && !file1.isDirectory()) {
                        try {
                            byte[] inFileName1 = FileUtils.readFileToByteArray(file1);
                            sigb64 = DatatypeConverter.printBase64Binary(inFileName1);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    map.put("sig", sigb64);

                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public List<HashMap> getNRReport(String userName, String course, String subCode, String centerCode) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_CCIC_NRDataDetailsReport_Get(?,?,?)}");
            cstmt.setString(1, userName);
            cstmt.setString(2, course);
            cstmt.setString(3, centerCode);
//            System.out.println("userName : "+userName);
//            System.out.println("course : "+course);
//            System.out.println("subCode : "+subCode);
//            System.out.println("centerCode : "+centerCode);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("sno", rs.getString(1));
                    map.put("rollNo", rs.getString(2));
                    map.put("name", rs.getString(3));
                    map.put("fatherName", rs.getString(4));
                    map.put("dob", rs.getString(5));
                    map.put("instName", rs.getString(6));

                    map.put("instAddress", rs.getString(7));
                    map.put("category", rs.getString(8));
                    map.put("subName", rs.getString(9));
                    map.put("centerC", rs.getString(10));
                    map.put("centerN", rs.getString(11));
                    map.put("courseCode", rs.getString(12));
                    map.put("courseName", rs.getString(13));
                    map.put("mobile", rs.getString(14));

                    map.put("instcode", rs.getString(15));
                    map.put("phhot", rs.getString(16));
                    map.put("sig", rs.getString(17));

                    String filepath = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(12) + "\\" + rs.getString(18) + "\\" + rs.getString(16);
                    map.put("photopath", filepath);
                    map.put("sigpath", filepath);
                    File file = new File(filepath);
                    String b64 = "No Image";
                    if (file.exists() && !file.isDirectory()) {
                        byte[] inFileName = FileUtils.readFileToByteArray(file);
                        b64 = DatatypeConverter.printBase64Binary(inFileName);
                    }
                    map.put("photo", b64);

                    String filepath1 = MessagePropertiesUtil.FIlE_PATH_CCIC + "\\" + rs.getString(12) + "\\" + rs.getString(18) + "\\" + rs.getString(17);
                    map.put("sigpath", filepath1);
                    String sigb64 = "No Image";
                    File file1 = new File(filepath1);
                    if (file1.exists() && !file1.isDirectory()) {
                        byte[] inFileName1 = FileUtils.readFileToByteArray(file1);
                        sigb64 = DatatypeConverter.printBase64Binary(inFileName1);
                    }
                    map.put("sig", sigb64);

                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }

                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
//                    response.getOutputStream().flush();
//                    response.getOutputStream().close();
                    out.flush();
                    out.close();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getPdfDataNR(String filepath, String filename, String userName, String course, String subCode, String centerCode) throws SQLException {
        createPDFNR(filepath, filename, userName, course, subCode, centerCode);
    }

    private void createPDFNR(String filepath, String filename, String userName, String course, String subCode, String centerCode) {

        Document doc = new Document();
        PdfWriter docWriter = null;

        DecimalFormat df = new DecimalFormat("0.00");

        try {

            List<HashMap> list = getNRReport(userName, course, subCode, centerCode);
            //special font sizes
            Font heading = new Font(Font.FontFamily.TIMES_ROMAN, 12, Font.BOLD, new BaseColor(0, 0, 0));
            Font bfBold12 = new Font(Font.FontFamily.TIMES_ROMAN, 10, Font.BOLD, new BaseColor(0, 0, 0));
            Font f5 = new Font(Font.FontFamily.TIMES_ROMAN, 7, Font.BOLD, new BaseColor(119, 41, 4)); //MAGENTA -Pink
            Font bf12 = new Font(Font.FontFamily.TIMES_ROMAN, 9.5f);

            //file path
            String path = filepath + "/" + filename;

            docWriter = PdfWriter.getInstance(doc, new FileOutputStream(path));

            doc.setPageSize(PageSize.A4.rotate());

            //open document
            doc.open();

            //create a paragraph

            Paragraph paragraph = new Paragraph(""
                    + "                    "
                    + "STATE BOARD OF TECHNICAL EDUCTION & TRAINING           "
                    + "                                      "
                    + "                                      "
                    + "                                      "
                    + "                          "
                    + " ANDHRA PRADESH - VIJAYAWADA"
                    + "                          "
                    + "                                        "
                    + "                                        "
                    + "                                        "
                    + "                                        "
                    + "CCIC, CRAFT AND SHORT TERM CERTIFICATION COURCES EXAMINATIONS,AUGUST - 2021"
                    + "                          "
                    + "                                        "
                    // + "                                        "
                    + "NR REPORT", heading);
            paragraph.setAlignment(Element.ALIGN_CENTER);


            //specify column widths
            float[] columnWidths = {2f, 6f, 6f, 6f, 4f, 3f, 4f, 4f, 5f, 5f, 4f, 3f, 5f, 6f}; //15
            //create PDF table with the given widths
            PdfPTable table = new PdfPTable(columnWidths);
            // set table width a percentage of the page width
            table.setWidthPercentage(100f);

            //insert column headings
            insertCell(table, "SNO", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Reg.No/ \n Hall ticket No", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Name of the Candidate", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Father Name", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Date of Birth", Element.ALIGN_CENTER, 1, bfBold12);

            insertCell(table, "Institute Code/ \n Private", Element.ALIGN_CENTER, 1, bfBold12);
//            insertCell(table, "Institute Name", Element.ALIGN_CENTER, 1, bfBold12);
//            insertCell(table, "Institute Address", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Center Code", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Center Name", Element.ALIGN_CENTER, 1, bfBold12);

            insertCell(table, "Course", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Subject", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Category", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Phone.No", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Photo", Element.ALIGN_CENTER, 1, bfBold12);
            insertCell(table, "Candidate Signature", Element.ALIGN_CENTER, 1, bfBold12);

            table.setHeaderRows(1);


            for (int x = 0; x < list.size(); x++) {
                HashMap map = list.get(x);

                insertCell(table, map.get("sno") + "", Element.ALIGN_CENTER, 1, bf12);
                insertCell(table, map.get("rollNo") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("name") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("fatherName") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("dob") + "", Element.ALIGN_LEFT, 1, bf12);

                insertCell(table, map.get("instcode") + "", Element.ALIGN_LEFT, 1, bf12);
//                insertCell(table, map.get("instName") + "", Element.ALIGN_LEFT, 1, bf12);
//                insertCell(table, map.get("instAddress") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("centerC") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("centerN") + "", Element.ALIGN_LEFT, 1, bf12);

                insertCell(table, map.get("courseName") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("subName") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("category") + "", Element.ALIGN_LEFT, 1, bf12);
                insertCell(table, map.get("mobile") + "", Element.ALIGN_LEFT, 1, bf12);

                float[] columnWidths1 = {2f};
                PdfPTable photostable = new PdfPTable(columnWidths1);
                photostable.getDefaultCell().setBorder(PdfPCell.NO_BORDER);

                photostable.getRowHeight(150);
                String Photo = map.get("photopath").toString();
                String sign = map.get("sigpath").toString();;
                File myFile = new File(Photo);
                if (myFile.exists() && !myFile.isDirectory()) {
                    Image img = Image.getInstance(Photo);
                    img.scaleToFit(80f, 80f);
                    img.setAlignment(Element.ALIGN_CENTER);
                    PdfPCell pcell1 = new PdfPCell(img);
                    pcell1.setFixedHeight(80f);
                    photostable.addCell(pcell1);
                } else {
                    Paragraph p1 = new Paragraph("            No image       ", bf12);
                    photostable.addCell(p1);
                }
                File myFile1 = new File(sign);
                if (myFile1.exists() && !myFile1.isDirectory()) {
                    Image img1 = Image.getInstance(sign);
                    img1.scaleToFit(65f, 40f);
                    img1.setAlignment(Element.ALIGN_CENTER);
                    PdfPCell pcell2 = new PdfPCell(img1);

                    photostable.addCell(pcell2);
                } else {
                    Paragraph p1 = new Paragraph("             No image            ", bf12);
                    photostable.addCell(p1);
                }
                table.addCell(photostable);

                insertCell(table, "", Element.ALIGN_LEFT, 1, bf12);

                map = null;

            }
            //add the PDF table to the paragraph 
            paragraph.add(table);
            // add the paragraph to the document
            doc.add(paragraph);

        } catch (DocumentException dex) {
            dex.printStackTrace();
        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            if (doc != null) {
                //close the document
                doc.close();
            }
            if (docWriter != null) {
                //close the writer
                docWriter.close();
            }
        }
    }

    private void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

    public ArrayList getSubjectList(String courseId) throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_CCIC_MasterCourseSubjectDetails_Get(?)}");
            cstmt.setString(1, courseId);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("subC", rs.getString(3));
                    map.put("subN", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getCourseList(String userName) {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_CCIC_MasterCourseDetails_Get(?)}");
            cstmt.setString(1, userName);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("ccd", rs.getString(1));
                    map.put("courseName", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public ArrayList getccicCenterList(String courseId) {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            //cstmt = con.prepareCall("{Call USP_CCIC_ExamCenterDetails_Get}");
            cstmt = con.prepareCall("{Call USP_CCIC_ExamCenterDetailsBySubject_Get(?)}");
            cstmt.setString(1, courseId);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("centercode", rs.getString(1));
                    map.put("centerName", rs.getString(2));
                    lstDetails.add(map);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
}
