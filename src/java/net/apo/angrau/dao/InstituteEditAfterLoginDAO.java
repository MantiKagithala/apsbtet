/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;
import com.sms.util.SendSMSDTO;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import org.apache.struts.upload.FormFile;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.action.RegisterFileUpload;
import org.apache.commons.lang3.StringUtils;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringEscapeUtils;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import javax.imageio.ImageIO;
import com.sms.util.MessagePropertiesUtil;
import java.io.File;
import java.io.FileOutputStream;
import javax.xml.bind.DatatypeConverter;

/**
 *
 * @author APTOL301655
 */
public class InstituteEditAfterLoginDAO {
  
    public List<HashMap> getCandidateList(String uname) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_Institute_Edit_Report(?)}");
            cstmt.setString(1,uname);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(2));
                    String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                    map.put("dob", dob);
                    map.put("name", rs.getString(1));
                    map.put("mobile", rs.getString(3));
                    map.put("caste", rs.getString(4));
                    map.put("examination", rs.getString(5));
                    map.put("grade", rs.getString(6));
                    map.put("regno", rs.getString(7));
                    map.put("aadhar", rs.getString(8));
                    map.put("gender", rs.getString(9));
                    map.put("fname", rs.getString(10));
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

   
    public HashMap<String, String> getData(RegisterForm rform, HttpServletRequest request) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        CallableStatement cstmt = null;
        HashMap<String, String> map = new HashMap<String, String>();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_Institute_Edit_Get_Data(?,?,?)}");
            cstmt.setString(1, rform.getHallticket().trim());
            cstmt.setString(2, rform.getAadhar().trim());
            cstmt.setString(3, rform.getGrade().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("fname", rs.getString(2));
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("caste", rs.getString(5));

                request.setAttribute("blind", rs.getString(6));
                request.setAttribute("institution", rs.getString(7));
                request.setAttribute("examinationname", rs.getString(8));
                request.setAttribute("language", rs.getString(9));
                request.setAttribute("gradename", rs.getString(10));
                request.setAttribute("apdistrict1", rs.getString(11));

                request.setAttribute("ecenter1", rs.getString(12));
                request.setAttribute("edate1", rs.getString(13));
                request.setAttribute("ebatch1", rs.getString(14));
                request.setAttribute("houseno", rs.getString(15));
                request.setAttribute("street", rs.getString(16));

                request.setAttribute("village", rs.getString(17));
                request.setAttribute("state", rs.getString(18));
                request.setAttribute("district1", rs.getString(19));
                request.setAttribute("mandal1", rs.getString(20));
                request.setAttribute("pincode", rs.getString(21));

                request.setAttribute("mobile", rs.getString(22));
                request.setAttribute("email", rs.getString(23));
                request.setAttribute("aadhar", rs.getString(24));
                request.setAttribute("regno", rs.getString(26));
                request.setAttribute("grade", rs.getString(32));
                request.setAttribute("examination", rs.getString(33));
                request.setAttribute("district", rs.getString(34));
                request.setAttribute("mandal", rs.getString(35));
                request.setAttribute("apdistrict", rs.getString(36));
                request.setAttribute("ecenter", rs.getString(37));
                request.setAttribute("edate", rs.getString(38));
                request.setAttribute("ebatch", rs.getString(39));

                Date todate1 = new SimpleDateFormat("yyyy-mm-dd").parse(rs.getString(31));
                String dob = new SimpleDateFormat("dd/mm/yyyy").format(todate1);
                request.setAttribute("dob", dob);
                request.setAttribute("date", new Date());
                request.setAttribute("blindno", rs.getString(46));
                request.setAttribute("oldRegNo", rs.getString(47));
                request.setAttribute("hallticketFile", rs.getString(48));
                //New 
                if(rs.getString(51)!=null && rs.getString(51).length()>0){
                    request.setAttribute("qualification", "INTER");
                }else{
                     request.setAttribute("qualification", "SSC");
                }
                if((rs.getString(41).length()>0)&& rs.getString(41).contains("SSCBONAFIDE")){
                    request.setAttribute("sscyear", "2021");
                }else{
                     request.setAttribute("sscyear", "Previous");
                }
                 if(rs.getString(47)!=null  && rs.getString(47).length()>0){
                    request.setAttribute("hallticketflag", "Y");
                }else{
                     request.setAttribute("hallticketflag", "N");
                }
                request.setAttribute("sschallticket", rs.getString(49));
                request.setAttribute("sscflag", rs.getString(50));
                request.setAttribute("interhallticket", rs.getString(51));
                request.setAttribute("intermonth", rs.getString(52));
                request.setAttribute("interyear", rs.getString(53));
                request.setAttribute("interflag", rs.getString(54));
                request.setAttribute("blindflag", rs.getString(55));
                request.setAttribute("lowerreghallticket", rs.getString(56));
                request.setAttribute("lowerregflag", rs.getString(57));

            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        System.out.println("map=================="+map);
        return map;
    }

    public HashMap<String, String> getDataForPayment(RegisterForm rform, HttpServletRequest request) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        try {
            Date todate1 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate1);
//System.out.println("dob=================="+dob);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_student_Get (?,?)}");

            cstmt.setString(1, rform.getHallticket().trim());
            cstmt.setString(2, dob);
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("caste", rs.getString(5));
                request.setAttribute("aadhaar", "XXXXXXXX" + rs.getString(6).substring(8, 12));
                request.setAttribute("mobile", rs.getString(9));
                request.setAttribute("ph", rs.getString(55));
                request.setAttribute("amount", rs.getDouble(52));
                request.setAttribute("sschallticket", rs.getString(16));
                request.setAttribute("hallticket", rs.getString(16));

                map.put("child", "9866747477");
//                list.add(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }

    public int getStatus(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        int result = 0;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_HallticketStatus (?,?,?,?)}");
            cstmt.setString(1, rform.getAadhar().trim());
            cstmt.setString(2, rform.getGrade().trim());
            cstmt.setString(3, rform.getExamination().trim());
            cstmt.setString(4, rform.getLanguage().trim());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getInt(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return result;
    }

    public String getvalidatingOTP(String aadharNum, String mobile) throws Exception {

        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String otp = "";

        try {
            con = DatabaseConnection.getConnection();
            query = "select OTP  from OTP_Details where Mobile_Num='" + mobile + "' and Aadhar_Id='" + aadharNum + "'";
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    otp = rs.getString(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return otp;
    }

    public int SaveOTP(String aadharNum, String mobile, String otp, String systemIp) throws Exception {
        int count = 0;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call proc_OTP_Details_Insert(?,?,?,?)}");
            cstmt.setString(1, aadharNum);
            cstmt.setString(2, mobile);
            cstmt.setString(3, otp);
            cstmt.setString(4, systemIp);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    public String insertFiles(RegisterForm rform, String remoteAddress, String uname) {
        Connection con = null;
        //   CallableStatement cstmt = null;
        PreparedStatement cstmt = null;
        ResultSet rs = null;
        HashMap hashMap = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        String fileName1 = "";
        String fileName2 = "";
        String fileName3 = "";
        String fileName4 = "";
        String fileName5 = "";
        String fileName6 = "";
        String fileName7 = "";
        String fileName8 = "";
        
        String fileName9="";
        String fileName10="";
        String fileName11="";
        String fileName12="";

        RegisterFileUpload rf = new RegisterFileUpload();
        boolean flag = false;


        String hallticket = rform.getAadhar();
        String grade = rform.getGrade();
        if (rform.getPhoto() != null && rform.getPhoto().toString().length() > 0) {
            flag = rf.uploadDocs(rform.getPhoto(), hallticket + "_PHOTO_Edit.jpg", hallticket, grade);
            if (flag == true) {
                fileName1 = hallticket + "_PHOTO_Edit.jpg";
            }
        }
        if (rform.getSignature() != null && rform.getSignature().toString().length() > 0) {
            flag = rf.uploadDocs(rform.getSignature(), hallticket + "_SIGNATURE_Edit.jpg", hallticket, grade);
            if (flag == true) {
                fileName3 = hallticket + "_SIGNATURE_Edit.jpg";
            }
        }
        if (rform.getBlind().equalsIgnoreCase("YES")) {
            if (rform.getBlindfile() != null && rform.getBlindfile().toString().length() > 0) {
                flag = rf.uploadDocs(rform.getBlindfile(), hallticket + "_BLIND_Edit.jpg", hallticket, grade);
                if (flag == true) {
                    fileName2 = hallticket + "_BLIND_Edit.jpg";
                }
            }
        }
        if (rform.getUpload1() != null && rform.getUpload1().toString().length() > 0) {
           if (rform.getGrade().equalsIgnoreCase("TTL")|| rform.getGrade().equalsIgnoreCase("TEL")
                                                  || rform.getGrade().equalsIgnoreCase("THL")
                                                  || rform.getGrade().equalsIgnoreCase("TUL")
                                                  || rform.getGrade().equalsIgnoreCase("STL")
                                                  || rform.getGrade().equalsIgnoreCase("SEL")
                                                  || rform.getGrade().equalsIgnoreCase("SUL")) {
                if(rform.getSscyear().equalsIgnoreCase("2021")){
               fileName9=hallticket + "_SSCBONAFIDE_Edit.jpg";
               }else{
                  fileName9=hallticket + "_SSC_Edit.jpg";  
               }
           }else if (rform.getGrade().equalsIgnoreCase("TTHS")
                                                            || rform.getGrade().equalsIgnoreCase("TEHS")
                                                            || rform.getGrade().equalsIgnoreCase("SEHS150")
                                                            || rform.getGrade().equalsIgnoreCase("SEHS180")
                                                            || rform.getGrade().equalsIgnoreCase("SEHS200")
                                                            || rform.getGrade().equalsIgnoreCase("STHS80")
                                                            || rform.getGrade().equalsIgnoreCase("STHS100")) {
                  fileName9=hallticket + "_HIGHERGRADE_Edit.jpg";
           }else if (rform.getGrade().equalsIgnoreCase("TEJ")) {
                 fileName9=hallticket + "_VII_Edit.jpg";
           }else{
                fileName9=hallticket + "_UPLOAD1_Edit.jpg";
           }
            flag = rf.uploadDocs(rform.getUpload1(), fileName9, hallticket, grade);
            if (flag == true) {
                fileName4 = fileName9;
            }
        }
        
        
        if (rform.getUpload2() != null && rform.getUpload2().toString().length() > 0) {
            if (rform.getGrade().equalsIgnoreCase("TTH")
                                                            || rform.getGrade().equalsIgnoreCase("TEH")
                                                            || rform.getGrade().equalsIgnoreCase("TUH")
                                                            || rform.getGrade().equalsIgnoreCase("THH")
                                                            || rform.getGrade().equalsIgnoreCase("STH")
                                                            || rform.getGrade().equalsIgnoreCase("SUH")
                                                            || rform.getGrade().equalsIgnoreCase("SEI")
                                                            || rform.getGrade().equalsIgnoreCase("SEH")) {
            fileName10=hallticket + "_LOWERGRADE_Edit.jpg";
            }else{
                  fileName10=hallticket + "_UPLOAD2_Edit.jpg";
            }
            flag = rf.uploadDocs(rform.getUpload2(), fileName10, hallticket, grade);
            if (flag == true) {
                fileName5 = fileName10;
            }
        }
        
        
        if (rform.getUpload3() != null && rform.getUpload3().toString().length() > 0) {
            if (rform.getGrade().equalsIgnoreCase("TTH")
                                                            || rform.getGrade().equalsIgnoreCase("TEH")
                                                            || rform.getGrade().equalsIgnoreCase("TUH")
                                                            || rform.getGrade().equalsIgnoreCase("THH")) {
                fileName11=hallticket + "_SSC_Edit.jpg";
            }else if (rform.getGrade().equalsIgnoreCase("STH")
                                                            || rform.getGrade().equalsIgnoreCase("SUH")
                                                            || rform.getGrade().equalsIgnoreCase("SEI")
                                                            || rform.getGrade().equalsIgnoreCase("SEH")) {
                 fileName11=hallticket + "_GRADUATION_Edit.jpg";
                
            }else{
                  fileName11=hallticket + "_UPLOAD3_Edit.jpg";
            }
            flag = rf.uploadDocs(rform.getUpload3(), fileName11, hallticket, grade);
            if (flag == true) {
                fileName6 = fileName11;
            }
        }
        
        if (rform.getUpload4() != null && rform.getUpload4().toString().length() > 0) {
            
            if (rform.getGrade().equalsIgnoreCase("TTH")
                                                            || rform.getGrade().equalsIgnoreCase("TEH")
                                                            || rform.getGrade().equalsIgnoreCase("TUH")
                                                            || rform.getGrade().equalsIgnoreCase("THH")) {
                fileName12=hallticket + "_INTERMEDIATE_Edit.jpg";
            }else if (rform.getGrade().equalsIgnoreCase("SEH")) {
                  fileName12=hallticket + "_VOCATIONAL_Edit.jpg";
            }else{
                  fileName12=hallticket + "_UPLOAD4_Edit.jpg";
            }
            flag = rf.uploadDocs(rform.getUpload4(),fileName12 , hallticket, grade);
            if (flag == true) {
                fileName7 = fileName12;
            }
        }
        if (rform.getUpload5() != null && rform.getUpload5().toString().length() > 0) {
            flag = rf.uploadDocs(rform.getUpload5(), hallticket + "_Hallticket_Edit.jpg", hallticket, grade);
            if (flag == true) {
                fileName8 = hallticket + "_Hallticket_Edit.jpg";
            }
        }
        try {
            Date todate2 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate2);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_TWSH_Registration_Edit_Institute(?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?,?,?,?,"
                    + "?,?,?,?,?,?,?)}");
            cstmt.setString(1, rform.getBname());
            cstmt.setString(2, rform.getFname());
            cstmt.setString(3, dob);
            cstmt.setString(4, rform.getGender());
            cstmt.setString(5, rform.getCaste());
            cstmt.setString(6, rform.getBlind());
            cstmt.setString(7, fileName2);
            cstmt.setString(8, rform.getInstitute());
            cstmt.setString(9, rform.getExamination());
            cstmt.setString(10, rform.getLanguage());
            cstmt.setString(11, rform.getGrade());
            cstmt.setString(12, rform.getApdistrict());
            cstmt.setString(13, rform.getEcenter());
            cstmt.setString(14, rform.getEdate());
            cstmt.setString(15, rform.getEbatch());
            cstmt.setString(16, rform.getHouseno());
            cstmt.setString(17, rform.getLocality());
            cstmt.setString(18, rform.getVillage());
            cstmt.setString(19, rform.getState());
            if ((rform.getState().equalsIgnoreCase("Andra Pradesh"))||(rform.getState().equalsIgnoreCase("Telagana"))) {
                cstmt.setString(20, rform.getDistrict());
                cstmt.setString(21, rform.getMandal());
            } else {
               cstmt.setString(20, rform.getDistrict1());
                cstmt.setString(21, rform.getMandal1());
            }
            cstmt.setString(22, rform.getPincode());
            cstmt.setString(23, rform.getMobile());
            cstmt.setString(24, rform.getEmail());
            cstmt.setString(25, rform.getAadhar());
            cstmt.setString(26, fileName1);
            cstmt.setString(27, remoteAddress);
            cstmt.setString(28, uname);
            cstmt.setString(29, fileName3);
            cstmt.setString(30, fileName4);
            cstmt.setString(31, fileName5);
            cstmt.setString(32, fileName6);
            cstmt.setString(33, fileName7);
            cstmt.setString(34, rform.getHallticket());
            cstmt.setString(35, fileName8);
            cstmt.setString(36, rform.getBlindno());
            cstmt.setString(37, rform.getSschallticket());
            cstmt.setString(38, rform.getSscflag());
            cstmt.setString(39, rform.getInterhallticket());
            cstmt.setString(40, rform.getIntermonth());
            cstmt.setString(41, rform.getInteryear());
            cstmt.setString(42, rform.getInterflag());
            cstmt.setString(43, rform.getBlindflag());
            cstmt.setString(44, rform.getRegno());
            cstmt.setString(45, rform.getLowerreghallticket());
            cstmt.setString(46, rform.getLowerregflag());
            cstmt.setString(47, "InstituteEdit");
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result = rs.getString(1);
            }
        } catch (Exception ex) {
              System.out.println("result===================" +  ex.getMessage());
//            logger.info("Error================" + ex.getMessage());
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("result===================" + result);
        return result;
    }
//
//    public String insertSmsLogDetails(SendSMSDTO sendSMSDTO) throws Exception {
//        Connection con = null;
////        Statement st = null;
//        PreparedStatement pstmt = null;
//        ResultSet rs = null;
//        String mobiles = "";
//        String query = null;
//        PreparedStatement ps = null;
//        try {
//            con = DatabaseConnection.getConnection();
//            query = "insert into SmsLog(MessageDescription,Messageto,LoginId,Systemip,senddate,sendstatus,Recevername,Receverdesignation,Receverdepartment) values('" + sendSMSDTO.getSms() + "',"
//                    + "'" + sendSMSDTO.getMobileNumber() + "','" + sendSMSDTO.getLoginId() + "','10.100.102.80',getDate(),"
//                    + "'" + sendSMSDTO.getSendStatus() + "','Aponline','Teacher','Teacher') ";
//            ps = con.prepareStatement(query);
//            ps.executeUpdate();
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//
//                if (rs != null) {
//                    rs.close();
//                }
//                if (pstmt != null) {
//                    pstmt.close();
//                }
//                if (con != null) {
//                    con.close();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return mobiles;
//    }
//
//    public ArrayList getStudentDetailsForPayment(String hallTicketNumber) throws Exception {
//        PreparedStatement stmt = null;
//        ArrayList studentList = new ArrayList();
//        Connection con = null;
//        ResultSet rs = null;
//        Map map = null;
//        try {
//            con = DatabaseConnection.getConnection();
//            stmt = con.prepareStatement("{call Usp_PaymentStatus(?,?)}");
//            stmt.setString(1, "3");
//            stmt.setString(2, hallTicketNumber);
//            rs = stmt.executeQuery();
//            if (rs != null) {
//                while (rs.next()) {
//                    map = new HashMap();
//
//                    map.put("appNoPay", rs.getString(1));
//                    map.put("mobilePay", rs.getString(2));
//                    map.put("appNamePay", rs.getString(3));
//                    map.put("amount", rs.getString(4));
//                    map.put("motherName", rs.getString(5));
//                    map.put("caste", rs.getString(7));
//                    map.put("cwsn", rs.getString(8));
//                    studentList.add(map);
//                }
//                map = null;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//
//            try {
//                if (rs != null) {
//                    rs.close();
//                }
//                if (stmt != null) {
//                    stmt.close();
//                }
//                if (con != null) {
//                    con.close();
//                }
//
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return studentList;
//    }

    public ArrayList getDataForPaymentSuccess(RegisterForm rform,String uname) {

        Connection con = null;
        PreparedStatement pstmt = null;
        String query = "";
        ArrayList list = new ArrayList();
        ResultSet rs = null;
        Map map = null;
        CallableStatement stmt = null;
        try {
            con = DatabaseConnection.getConnection();
//            query = "select  Request_Id,App_Id,PGRefNo,Base_amount,Convience_Charges,GST,Customer_Name from MSc_PaymentDetails (nolock) where Request_Id = '"+reqId+"' ";
//                System.out.println("query - query"+query);
//            pstmt = con.prepareStatement(query);
//           
//             rs = pstmt.executeQuery();

            stmt = con.prepareCall("{call Usp_PaymentStatus(?,?,?,?)}");
            stmt.setString(1, "2");
            stmt.setString(2, rform.getAadhar());
            stmt.setString(3, rform.getGrade());
            stmt.setString(4, uname);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {

                    map = new HashMap<String, String>();
                    map.put("customerName", rs.getString(1));
                    map.put("gender", rs.getString(2));
                    map.put("caste", rs.getString(3));
                    map.put("dob", rs.getString(4));
                    map.put("mobile", rs.getString(5));
                    map.put("ph", rs.getString(6));
                    map.put("serviceCharges", rs.getString(7));
                    map.put("PgRefNo", rs.getString(8));
                    map.put("gst", rs.getString(9));
                    map.put("baseAmt", rs.getString(10));
                    map.put("reqId", rs.getString(11));
                    map.put("aadhar", "XXXXXXXX" + rs.getString(12).substring(8, 12));
                    list.add(map);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return list;
    }
//      public int getvalidatingAadharNum(String aadharNum) throws Exception {
//
//        String query = null;
//        PreparedStatement st = null;
//        Connection con = null;
//        ResultSet rs = null;
//        int count = 0;
//        CallableStatement stmt = null;
//        try {
//            con = DatabaseConnection.getConnection();
//            stmt = con.prepareCall("{call Usp_AadhaarStatusCheck(?)}");
//            stmt.setString(1, aadharNum);
//            rs = stmt.executeQuery();
//            if (rs != null) {
//                while (rs.next()) {
//                    count = rs.getInt(1);
//                }
//            }
//
//        } catch (Exception e) {
//
//            e.printStackTrace();
//        } finally {
//            try {
//                if (rs != null) {
//                    rs.close();
//                }
//                if (stmt != null) {
//                    stmt.close();
//                }
//                if (con != null) {
//                    con.close();
//                }
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
//        return count;
//    }

    public HashMap<String, String> getDataForPaymentDob(RegisterForm rform, HttpServletRequest request,String uname) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
//        PreparedStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        String dob = null;
        try {


            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_Get_TWSH (?,?,?)}");
//            cstmt = con.prepareStatement("select * from studentinfo where rgukt_rollno=? and dob=?");
            cstmt.setString(1, rform.getAadhar().trim());
            cstmt.setString(2, rform.getGrade().trim());
            cstmt.setString(3, uname);
//            cstmt.setString(1, "795640259613");
//            cstmt.setString(2, "1999-12-01");
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("gender", rs.getString(2));
                request.setAttribute("caste", rs.getString(3));
                request.setAttribute("aadhaar", "XXXXXXXX" + rs.getString(7).substring(8, 12));
                request.setAttribute("mobile", rs.getString(4));
                request.setAttribute("email", rs.getString(5));
                request.setAttribute("amount", rs.getDouble(6));
                request.setAttribute("aadhar", rs.getString(7));
                request.setAttribute("registration", rs.getString(8));
                request.setAttribute("grade", rs.getString(9));

                map.put("child", "9866747477");
//                list.add(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }

        return map;
    }

    public ArrayList getDistrictDetails() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_DISTRICT with(nolock)";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return list;
    }

    public ArrayList getMandalDetails(String district) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_BLOCK with(nolock) where LEFT(BLKCD,4)=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("mandal_ID", rs.getString(1));
                map.put("mandal_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public ArrayList getcenterDetails(String district, String examination,String uname) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            if (examination.equals("TW")) {
                query = "select a.Centre_Code,B.Centre_name from TWSH_INSTITUTE_MASTER a with(nolock)  left join TWSH_CENTER_MASTER b with(nolock) on a.CENTRE_CODE=b.CENTRE_CODE  where INST_CODE=?";
          pstmt = con.prepareStatement(query);
            pstmt.setString(1, uname);
            } else if (examination.equals("SH")) {
                query = "select CENTRE_CODE,CENTRE_NAME from  TWSH_CENTER_MASTER with(nolock) where DISTCD=? and SH_COURSE='Y'";
             pstmt = con.prepareStatement(query);
            pstmt.setString(1, district);
            }
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("ccode", rs.getString(1));
                map.put("cname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }
   public ArrayList getcenterDistDetails(String examination,String grade,String instcode) throws Exception {
        String query = null;
        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
           if (examination.equals("TW")) {
                query = "select DISTCD,DISTNAME from  TWSH_INSTITUTE_Master with(nolock) where INST_code=?";
                 pstmt = con.prepareStatement(query);
                   pstmt.setString(1,instcode);
            } else if (examination.equals("SH")) {
                query = "select DISTCD,DISTNAME from  STEPS_DISTRICT with(nolock) where DISTCD in(select DISTCD from  TWSH_CENTER_MASTER with(nolock) where  SH_COURSE='Y' and AVL_SH_GRADES like ?) order by DISTCD";
                pstmt = con.prepareStatement(query);
          pstmt.setString(1,"%"+grade+"%");
            }
//            pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public ArrayList getStateDetails() throws Exception {

        String query = null;
        ArrayList list = new ArrayList();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        Map map = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select * from StateMaster order by StateName";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("scode", rs.getString(2));
                    map.put("sname", rs.getString(2));
                    list.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {


                if (rs != null) {
                    rs.close();
                }
                if (pstmt != null) {
                    pstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return list;

    }

    public ArrayList getvillageDetails(String district) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM steps_village with(nolock) where LEFT(VILCD,6)=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("vcode", rs.getString(1));
                map.put("vname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public ArrayList getDistrictDetailsAt() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_DISTRICT_TS with(nolock)";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (pstmt != null) {
                pstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return list;
    }

    public ArrayList getMandalDetailsAt(String district) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "SELECT * FROM STEPS_BLOCK_TS with(nolock) where LEFT(BLKCD,4)=?";
            pstmt = con.prepareStatement(query);
            pstmt.setString(1, district);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("mandal_ID", rs.getString(1));
                map.put("mandal_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public int getvalidatingAadharNum(String mobile, String aadhar) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select count (distinct  aadhar)  from TBL_TWSH_Registration with(nolock) where  mobile=? and aadhar not in (?)");
            stmt.setString(1, mobile);
            stmt.setString(2, aadhar);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return count;
    }

    public HashMap<String,String> getDetails(RegisterForm rForm,HttpServletRequest request) throws Exception {

        String query = null;
        CallableStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        String count = "0";
        HashMap<String,String> pevexamdetails=new HashMap<String,String>();
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareCall("{call USP_TWSH_PreviousRegistrationDetails_Get (?,?)}");
            stmt.setString(1, rForm.getHallticket());
            stmt.setString(2, rForm.getAadhar());
            rs = stmt.executeQuery();
            if (rs != null) {
                if (rs.next()==true) {
                     request.setAttribute("bname", rs.getString(1).trim());
                request.setAttribute("fname", rs.getString(2).trim());
                request.setAttribute("dob", rs.getString(3).trim());
                request.setAttribute("gender", rs.getString(4).trim());
                request.setAttribute("caste", rs.getString(5).trim());
                request.setAttribute("examination", rs.getString(6).trim());
                request.setAttribute("language", rs.getString(7).trim());
                request.setAttribute("examinationname", rs.getString(8).trim());
                request.setAttribute("grade", rs.getString(9).trim());
                request.setAttribute("ExaminationStatus", rs.getString(10).trim());
                request.setAttribute("gradename", rs.getString(11).trim());
                request.setAttribute("fieldstatus", rs.getString(12).trim());
                
                request.setAttribute("hallticket", rForm.getHallticket().trim());
                request.setAttribute("aadhar", rForm.getAadhar2().trim());
                
                pevexamdetails.put("ExaminationStatus", rs.getString(10).trim());
                pevexamdetails.put("grade", rs.getString(9).trim());
                pevexamdetails.put("examination", rs.getString(6).trim());
                pevexamdetails.put("language", rs.getString(7).trim());
                pevexamdetails.put("hallticketstatus", rs.getString(13).trim());
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return pevexamdetails;
    }

    public int getTimeStatus(String Aadhar, String date, String batch, String grade) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;
        try {
            con = DatabaseConnection.getConnection();
            if (grade.equalsIgnoreCase("TEL") || grade.equalsIgnoreCase("TTL") || grade.equalsIgnoreCase("TUL") || grade.equalsIgnoreCase("THL")) {
                stmt = con.prepareStatement("select count(*)  from TBL_TWSH_Registration with(nolock) where Aadhar=? and ExaminationDate=? and ExaminationBatch=? and grade in('TEL','TTL','TUL','THL')");
            } else if (grade.equalsIgnoreCase("TEH") || grade.equalsIgnoreCase("TTH") || grade.equalsIgnoreCase("TUH") || grade.equalsIgnoreCase("THH") || grade.equalsIgnoreCase("TEJ")) {
                stmt = con.prepareStatement("select count(*)  from TBL_TWSH_Registration with(nolock) where Aadhar=? and ExaminationDate=? and ExaminationBatch=? and grade in('TEH','TTH','TUH','THH','TEJ')");

            } else if (grade.equalsIgnoreCase("TTHS") || grade.equalsIgnoreCase("TEHS")) {
                stmt = con.prepareStatement("select count(*)  from TBL_TWSH_Registration with(nolock) where Aadhar=? and ExaminationDate=? and ExaminationBatch=? and grade in('TEHS','TTHS')");

            }
            stmt.setString(1, Aadhar);
            stmt.setString(2, date);
            stmt.setString(3, batch);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return count;
    }

    public int getStatusExam(String Aadhar, String examination, String language) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select count(*)  from TBL_TWSH_Registration with(nolock) where Aadhar=? and Examination=? and language=?");
            stmt.setString(1, Aadhar.trim());
            stmt.setString(2, examination.trim());
            stmt.setString(3, language.trim());
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    public ArrayList getGradesList() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select Grade,GRADE_DESC from TWSH_GRADE_MASTER with(nolock) where rowid not in('22','23','10','11') order by RowID asc";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("gcode", rs.getString(1));
                map.put("gname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

    public String getGrade(RegisterForm rForm) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        String grade = "";
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select GRADE_DESC from TWSH_GRADE_MASTER with(nolock) where GRADE=?");
            stmt.setString(1, rForm.getGrade());
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    grade = rs.getString(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return grade;
    }
    public HashMap<String, String> getSSCDetails(String hallticket,String Aadhar) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
//        PreparedStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        String dob = null;
        try {


            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_Student_Get_SSCDETAILS(?,?)}");
//            cstmt = con.prepareStatement("select * from studentinfo where rgukt_rollno=? and dob=?");
            cstmt.setString(1, hallticket);
            cstmt.setString(2, Aadhar);
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                 map.put("bname", rs.getString(2));
                 map.put("fname", rs.getString(3));
                 map.put("gender", rs.getString(4));
                map.put("caste", rs.getString(5));
                map.put("dob", rs.getString(6));
                map.put("aadharcount", rs.getString(8));
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }
    
    public int getLowerExamDetails(String lhallticket,String Aadhar) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("{call Usp_Student_Get_LOWERGADEDETAILS(?,?)}");
            stmt.setString(1, lhallticket);
            stmt.setString(2, Aadhar);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    int lowercount = rs.getInt(1);
                    String examcount = rs.getString(2);
                    if(lowercount==1 && examcount.equalsIgnoreCase("Pass")){
                        count=2;
                    }else if(lowercount==0 && examcount.equalsIgnoreCase("Pass")){
                        count=1;
                    }else if(examcount.equalsIgnoreCase("Fail")){
                        count=3;
                    }
                    else{
                        count=0;
                    }
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return count;
    }
     public int getSadaremcount(String aadhar,String sadaremno) throws Exception {

        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select count (distinct  aadhar)  from TBL_TWSH_Registration with(nolock) where  Sadaremno=? and aadhar not in (?)");
            stmt.setString(1, sadaremno);
            stmt.setString(2, aadhar);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }
     public int getIntercount(String aadhar,String interhallticket) throws Exception {
        String query = null;
        PreparedStatement stmt = null;
        Connection con = null;
        ResultSet rs = null;
        int count = 0;
        try {
            con = DatabaseConnection.getConnection();
            stmt = con.prepareStatement("select count (distinct  aadhar)  from TBL_TWSH_Registration with(nolock) where  interhallticket=? and aadhar not in (?)");
            stmt.setString(1, interhallticket);
            stmt.setString(2, aadhar);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }

        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        System.out.println("count================="+count);
        return count;
    }
}
