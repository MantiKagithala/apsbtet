/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.sms.util.MessagePropertiesUtil;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import javax.imageio.ImageIO;
import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.io.FileOutputStream;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;

import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.forms.RegisterForm;
import org.apache.commons.io.FileUtils;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author APTOL301461
 */
public class OnlineExaminationEntryDAO {

  //  private String path = "\\\\10.3.37.204\\E$\\ISMS\\UDISE\\";
  //  private String path = "\\\\10.138.133.8\\D$\\Diploma\\";
      private String path = "\\\\10.96.64.51\\E$\\Diploma\\";
//        private String path = "\\E$\\Diploma\\";

    public String getApplicationDetails(RegisterForm rform, HttpServletRequest req) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        String res = "0";

        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Usp_DiplomaStudent_Get (?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim()); ///Reg No
            cstmt.setString(2, "1");
            rs = cstmt.executeQuery();
            if (rs != null && rs.next() == true) {
                if (rs.getString(1) != null && rs.getString(1).length() != 1) {
                    req.setAttribute("Pin", rs.getString(1));
                    req.setAttribute("stdName", rs.getString(2));
                    req.setAttribute("fname", rs.getString(3));
                    req.setAttribute("gender", rs.getString(4));
                    req.setAttribute("center", rs.getString(5));
                    req.setAttribute("branch", rs.getString(6));
                    req.setAttribute("ph", rs.getString(7));
                    req.setAttribute("attandance", rs.getString(8));
                    req.setAttribute("finalStatus", rs.getString(9));

                    req.setAttribute("mobile", rs.getString(10));
                    req.setAttribute("email", rs.getString(11));
                    req.setAttribute("samecollege", rs.getString(12));
                    req.setAttribute("district", rs.getString(13));
                    req.setAttribute("collegecode", rs.getString(14));

                    
                    if (rs.getBytes(15) != null && rs.getBytes(15).length > 0) {
                        req.setAttribute("studentPhoto", DatatypeConverter.printBase64Binary(rs.getBytes(15)).trim());
                    } else {
                        req.setAttribute("studentPhoto", "");
                    }
                    if (rs.getString(16) != null && rs.getString(16)!="") {
                       req.setAttribute("photoFile", getImageInBase64Format(rform.getAadhar1().trim(),rs.getString(16)));
                     } else {
                        req.setAttribute("photoFile", "");
                    }
                     req.setAttribute("AttendanceFlag", rs.getString(17));
//                     System.out.println("AAAAAAAAAAAA"+rs.getString(17));
                     
                    req.setAttribute("samecollegeCode", rs.getString(18));
                    req.setAttribute("districtCode", rs.getString(19));
                    req.setAttribute("centerCode", rs.getString(20));

                    
                    res = "1";
                } else {
                    res = rs.getString(1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return res;
    }

    public ArrayList getCenterDetails(String district) {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        ArrayList centerList = new ArrayList();
        HashMap map = null;

        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Usp_DiplomaStudent_Get ?,?}");
            cstmt.setString(1, district); ///Reg No
            cstmt.setString(2, "3");


            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("id", rs.getString(1));
                    map.put("name", rs.getString(2));

                    centerList.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try{
            if (rs != null) {
                  rs.close();
            }
            if (cstmt != null) {
                  cstmt.close();
            }
            if (con != null) {
                  con.close();
            }
        }catch (Exception e){
             e.printStackTrace();
        }
        }
        return centerList;

    }

    public ArrayList getSubjectDetails(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;

        HashMap map = null;
        ArrayList list = new ArrayList();
        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Usp_DiplomaStudent_Get (?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim());
            cstmt.setString(2, "2");
            rs = cstmt.executeQuery();
            while (rs != null && rs.next() == true) {
                map = new HashMap();
                map.put("pin", rs.getString(1));
                map.put("year", rs.getString(7));
                map.put("scheme", rs.getString(8));
                map.put("subCode", rs.getString(9));
                map.put("examFee", rs.getString(10));
                map.put("backLogfee", rs.getString(11));
                map.put("certificatefee", rs.getString(12));
                map.put("condonationfee", rs.getString(13));
                map.put("total", rs.getString(14));
                map.put("paymentStatus", rs.getString(15));
                map.put("feeDetails", rs.getString(16));
                map.put("yr", rs.getString(17));
                map.put("ws", rs.getString(18));
                list.add(map);
                map = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return list;
    }

    public String submitApplicationDetails(RegisterForm rform,String remoteAddress,int status) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        String res = "0";
        String fileName = "", flag = "false";
        try {
            if (rform.getUpload1() != null && rform.getUpload1().toString().length() > 0) {
                flag = uploadDocs(rform.getUpload1(), "Photo.jpg", rform.getAadhar1().trim());
                if (!flag.equals("false")) {
                    fileName = "Photo.jpg";
                }
            }
           // System.out.println("fffffffffffffff"+fileName);
           // if (fileName != "" || (rform.getUpload1New1()!=null && rform.getUpload1New1()!="")) {
            
 if (fileName != "" || (rform.getUpload1New1() != null && rform.getUpload1New1() != "")||status==2) {

                con = DatabaseConnection.getConnectionDiploma();
                cstmt = con.prepareCall("{Call USP_DiplomaRegistration_Insert (?,?,?,?,?,?,?,?,?,?,?,?,?)}");
                cstmt.setString(1, rform.getAadhar1().trim()); ///Reg No
                cstmt.setString(2, rform.getSname());
                cstmt.setString(3, "");
                cstmt.setString(4, "");
                cstmt.setString(5, "");

                cstmt.setString(6, rform.getMobile());
                cstmt.setString(7, fileName);
                cstmt.setString(8, rform.getEcenter());
                cstmt.setString(9, rform.getCenterCode());
              //  System.out.println("rform.getStatusresult()===" + rform.getStatusresult());
                cstmt.setString(10, rform.getStatusresult());
                cstmt.setString(11, remoteAddress);
                cstmt.setString(12, rform.getDistrcit1());
                cstmt.setString(13, rform.getEmail());
                rs = cstmt.executeQuery();
                if (rs != null && rs.next() == true) {
                    res = rs.getString(1);
                //    System.out.println("res--->"+res);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return res;
    }

    public String uploadDocs(FormFile uploadFileName, String fileName, String UserName) {
        String extension = null;
        String flag = "false";
        try {

            String strDirectoytemp = path + UserName + "\\";

            if (strDirectoytemp != null && !"".equals(strDirectoytemp) && strDirectoytemp.length() > 0) { // If directory is not exists it will create
                File directorytemp = new File(strDirectoytemp);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
                File fileToCreatetemp = new File(strDirectoytemp, fileName); // Copy the file into directory
                FileOutputStream fileOutStreamtemp = new FileOutputStream(fileToCreatetemp); // Write the file content into buffer
                if (uploadFileName.getFileSize() > 0) {
                    fileOutStreamtemp.write(uploadFileName.getFileData());
                    fileOutStreamtemp.flush();
                    fileOutStreamtemp.close();
                    flag = strDirectoytemp + fileName;
                } else {
                    flag = "false";
                }
            } else {
                flag = "false";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return flag;
    }

    public String getApplicationStatus(RegisterForm rform, HttpServletRequest req) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        String res = "0";

        try {
            con = DatabaseConnection.getConnectionDiploma();
            cstmt = con.prepareCall("{Call Usp_DiplomaStudent_Get (?,?)}");
            cstmt.setString(1, rform.getAadhar1().trim()); ///Reg No
            cstmt.setString(2, "4");
            rs = cstmt.executeQuery();
            if (rs != null && rs.next() == true) {
                req.setAttribute("appStatus", rs.getString(1));

                res = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return res;
    }

    public ArrayList getDistrictDetails() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnectionDiploma();
            query = "SELECT * FROM STEPS_DISTRICT with(nolock)";
            cstmt = con.prepareCall("{Call Usp_DiplomaStudent_Get (?,?)}");
            cstmt.setString(1, ""); ///Reg No
            cstmt.setString(2, "5");
            rs = cstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("district_ID", rs.getString(1));
                map.put("district_Name", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return list;
    }

    public String getImageInBase64Format(String pin, String filename) throws Exception {

        String filepath = null;
        String b64 = "";
        ByteArrayOutputStream baos = null;
        BufferedImage image = null;
        try {
            filepath = path + pin + "\\" + filename;
            File file = new File(filepath);
            if (!file.exists()) {
                return "";
            }
            image = ImageIO.read(file);
            baos = new ByteArrayOutputStream();
            int lastIndex = file.getName().lastIndexOf('.');

            String extenstion = file.getName().substring(lastIndex);
            if (extenstion != null && extenstion.equalsIgnoreCase(".jpg")) {
                ImageIO.write(image, "jpg", baos);
            } else {
                ImageIO.write(image, "png", baos);
            }

            baos.flush();
            byte[] imageInByteArray = baos.toByteArray();
            b64 = DatatypeConverter.printBase64Binary(imageInByteArray);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (baos != null) {
                baos.close();
            }

            if (image != null) {
            }
        }
        return b64;
    }
    public String getMobileStatus(String mobile) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        String res = "0";

        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_DiplomaStudent_Get (?,?)}");
            cstmt.setString(1, mobile); ///Reg No
            cstmt.setString(2, "6");
            rs = cstmt.executeQuery();
            if (rs != null && rs.next() == true) {
                res = rs.getString(1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }
        }
        return res;
    } 
}
