/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.net.InetAddress;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.db.DatabaseConnection;

/**
 *
 * @author APTOL301655
 */
public class HallticketConfirmationDAO {
    public List<HashMap> getCandidateList(String uname) throws Exception {
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        int totalApplications1 = 0;
        int totalSubmitted = 0;
        int totalfarq = 0;
        int totalfarvq = 0;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call USP_HallticketConfirmation_CandidateList}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("bname", rs.getString(1));
                    map.put("dob", rs.getString(2));
                    map.put("mobile", rs.getString(3));
                    map.put("caste", rs.getString(4));
                    map.put("examination", rs.getString(5));
                    map.put("grade", rs.getString(6));
                    map.put("regno", rs.getString(7));
                    map.put("aadhar", rs.getString(8));
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
public String submitDetails(HttpServletRequest req, String username) {

        Connection con = null;

        CallableStatement cstmt = null;
        ResultSet rs = null;
        HashMap map = null;
        List<HashMap> lstDetails = new ArrayList();
        String res = "0";
        try {
            String aadhar = req.getParameter("aadhardetails");
            String grade = req.getParameter("gradedetails");
            String regno = req.getParameter("regnodetails");
            String hstatus = req.getParameter("hallticketdetails");

         
            
            String sytemIp = req.getRemoteAddr();
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call TWSH_Hallticket_Insert(?,?,?,?,?,?)}");
            cstmt.setString(1, aadhar.substring(0, aadhar.length() - 1));
            cstmt.setString(2, grade.substring(0, grade.length() - 1));
            cstmt.setString(3, hstatus.substring(0, hstatus.length() - 1));
            cstmt.setString(4, regno.substring(0, regno.length() - 1));
            cstmt.setString(5, sytemIp);
            cstmt.setString(6, username);
            rs = cstmt.executeQuery();
            if (rs != null && rs.next()) {
                res = rs.getString(1);
            }

        } catch (Exception ex) {
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        //  System.out.println("resDDD===" + res);
        return res;
    }
}
