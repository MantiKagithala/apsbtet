/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
/**
 *
 * @author APTOL301294
 */
public class DistrictWiseRegReportDao {
    
    
    public List<HashMap> getDistrictWiseReport() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        int totalregAppCnt=0;
        int totaltotalAppCnt=0;
        int totalrepAppCnt=0;
        int totalpvtRepCnt=0;
        int totalInstCount=0;
        int totalpvttotal=0;
        int totalpvtRegCnt=0;
        int totalcompletedPay=0;
        int totaltotalCnt=0;
        int totalpendingPay=0;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Rptsp_TWSH_RegistraionsAbstract}");
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("districtName", rs.getString(1));
                    
                    map.put("instCount", rs.getString(2));
                    totalInstCount = totalInstCount + Integer.parseInt(rs.getString(2).toString());
                    map.put("totalInstCount", totalInstCount);

                    map.put("repAppCnt", rs.getString(3));
                    totalrepAppCnt = totalrepAppCnt + Integer.parseInt(rs.getString(3));
                    map.put("totalrepAppCnt", totalrepAppCnt);
                    
                    map.put("regAppCnt", rs.getString(4));
                    totalregAppCnt = totalregAppCnt + Integer.parseInt(rs.getString(4));
                    map.put("totalregAppCnt", totalregAppCnt);
                    
                    map.put("totalAppCnt", rs.getString(5));
                    totaltotalAppCnt = totaltotalAppCnt + Integer.parseInt(rs.getString(5));
                    map.put("totaltotalAppCnt", totaltotalAppCnt);
                    
                    map.put("pvtRepCnt", rs.getString(6));
                    totalpvtRepCnt = totalpvtRepCnt + Integer.parseInt(rs.getString(6));
                    map.put("totalpvtRepCnt", totalpvtRepCnt);
                    
                    map.put("pvtRegCnt", rs.getString(7));
                    totalpvtRegCnt = totalpvtRegCnt + Integer.parseInt(rs.getString(7));
                    map.put("totalpvtRegCnt", totalpvtRegCnt);
                    
                    map.put("pvttotal", rs.getString(8));
                    totalpvttotal = totalpvttotal + Integer.parseInt(rs.getString(8));
                    map.put("totalpvttotal", totalpvttotal);
                    
                    map.put("totalCnt", rs.getString(9));
                    totaltotalCnt = totaltotalCnt + Integer.parseInt(rs.getString(9));
                    map.put("totaltotalCnt", totaltotalCnt);
                    
                    map.put("pendingPay", rs.getString(10));
                    totalpendingPay = totalpendingPay + Integer.parseInt(rs.getString(10));
                    map.put("totalpendingPay", totalpendingPay);
                    
                    map.put("completedPay", rs.getString(11));
                    totalcompletedPay = totalcompletedPay + Integer.parseInt(rs.getString(11));
                    map.put("totalcompletedPay", totalcompletedPay);
                    
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
}