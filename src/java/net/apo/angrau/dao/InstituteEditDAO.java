/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;

/**
 *
 * @author APTOL301655
 */
public class InstituteEditDAO {

    public HashMap<String, String> getData(RegisterForm rform, HttpServletRequest request) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
//        CallableStatement cstmt = null;
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{call Usp_Institute_Get_TWSH(?)}");
//            cstmt = con.prepareCall("{Call Usp_Student_Edit_TWSH (?)}");
            cstmt.setString(1, rform.getInstcode().trim());
//            System.out.println("instcode=============="+rform.getInstcode());
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("instcode", rs.getString(2));
                request.setAttribute("instreg", rs.getString(3));
                request.setAttribute("instname", rs.getString(4));
                request.setAttribute("instaddress", rs.getString(5));
                request.setAttribute("insttown", rs.getString(6) == null ? "" : rs.getString(6));
                request.setAttribute("instprincipal", rs.getString(8) == null ? "" : rs.getString(8));
                request.setAttribute("instmobile", rs.getString(9) == null ? "" : rs.getString(9));
                request.setAttribute("instdist", rs.getString(10) == null ? "" : rs.getString(10));
                request.setAttribute("instpincode", rs.getString(12) == null ? "" : rs.getString(12));
                map.put("child", "9866747477");
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }

    public List<HashMap> getDistrictWiseReport() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            st = con.prepareStatement("select * from TWSH_INSTITUTE_MASTER (nolock)");
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("instCode", rs.getString(2));
//                    map.put("regCode", rs.getString(3));
                    map.put("instName", rs.getString(4));
                    map.put("address", rs.getString(5));
                    map.put("town", rs.getString(6));
                    if (rs.getString(6) != null) {
                        map.put("town", rs.getString(6));
                    } else {
                        map.put("town", "");
                    }
                    map.put("principleName", rs.getString(8));
                    if (rs.getString(9) != null) {
                        map.put("mobile", rs.getString(9));
                    } else {
                        map.put("mobile", "");
                    }
                    map.put("district", rs.getString(10));
                    if (rs.getString(12) != null) {
                        map.put("pincode", rs.getString(12));
                    } else {
                        map.put("pincode", "");
                    }
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }
    public String submitData(RegisterForm rform,String uname) throws Exception {
        String result = null;

        CallableStatement cstmt = null;
//        PreparedStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        try {
            con = DatabaseConnection.getConnection();
//            cstmt = con.prepareStatement("select rgukt_rollno,name,fathername,category,sc_name,school_type,managment from Studentinfo where rgukt_rollno=? and dob=?");
            cstmt = con.prepareCall("{Call Usp_Institute_update (?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, rform.getInstcode().trim());
            cstmt.setString(2, rform.getInstprincipal().trim());
            cstmt.setString(3, rform.getMobile().trim());
            cstmt.setString(4, rform.getInstname().trim());
            cstmt.setString(5, rform.getInstaddress().trim());
            cstmt.setString(6, rform.getInsttown().trim());
            cstmt.setString(7, rform.getInstpincode().trim());
            cstmt.setString(8, uname);
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                result=rs.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return result;
    }
}
