/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.client.j2se.BufferedImageLuminanceSource;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;
import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import javax.imageio.ImageIO;

/**
 *
 * @author APTOL301294
 */
public class GenerateQrAndBarCode {
    
    public String writeBarCode(String barCodeText, String filePath) {
        String myreturn = "0";
        try {
            String charset = "UTF-8";
            Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            BitMatrix matrix = new MultiFormatWriter().encode(new String(barCodeText.getBytes(charset), charset), BarcodeFormat.CODE_128, 800, 150, hintMap);
            MatrixToImageWriter.writeToFile(matrix, filePath.substring(filePath.lastIndexOf(".") + 1), new File(filePath));
            myreturn = "1";
        } catch (Exception ex) {
            myreturn = "2";
        }
        return myreturn;
    }

    public String readBarCode(String imageName, String filePath) {
        String myreturn = "0";
        try {
            String filepath = "D:\\CODE\\BARCODE\\" + imageName + ".png";
            Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            BinaryBitmap binarybitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(new FileInputStream(filepath)))));
            Result barCodeResult = new MultiFormatReader().decode(binarybitmap);
            myreturn = barCodeResult.getText();
        } catch (Exception ex) {
            myreturn = "2";
        }
        return myreturn;
    }

    public String writeQRCode(String qrCodeText, String filePath) {
        String myreturn = "0";
        try {
            String charset = "UTF-8";
            Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            BitMatrix matrix = new MultiFormatWriter().encode(new String(qrCodeText.getBytes(charset), charset), BarcodeFormat.QR_CODE, 200, 200, hintMap);
            MatrixToImageWriter.writeToFile(matrix, filePath.substring(filePath.lastIndexOf(".") + 1), new File(filePath));
            myreturn = "1";
        } catch (Exception ex) {
            myreturn = "2";
        }
        return myreturn;
    }

    public String readQRCode(String imageName, String filePath) {
        String myreturn = "0";
        try {
            String charset = "UTF-8";
            String filepath = "D:\\CODE\\QRCODE\\" + imageName + ".png";
            Map<EncodeHintType, ErrorCorrectionLevel> hintMap = new HashMap<EncodeHintType, ErrorCorrectionLevel>();
            hintMap.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);
            BinaryBitmap binarybitmap = new BinaryBitmap(new HybridBinarizer(new BufferedImageLuminanceSource(ImageIO.read(new FileInputStream(filepath)))));
            Result qrCodeResult = new MultiFormatReader().decode(binarybitmap);
            myreturn = qrCodeResult.getText();
        } catch (Exception ex) {
            myreturn = "2";
        }
        return myreturn;
    }
    
}
