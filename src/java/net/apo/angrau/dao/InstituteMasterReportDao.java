/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sms.util.MessagePropertiesUtil;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import net.apo.angrau.db.DatabaseConnection;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletOutputStream;
import net.apo.angrau.forms.RegisterForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.action.RegisterFileUpload;

/**
 *
 * @author APTOL301294
 */
public class InstituteMasterReportDao {

    public List<HashMap> getDistrictWiseReport() throws Exception {
        ResultSet rs = null;
        HashMap map = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        List<HashMap> lstDetails = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            st = con.prepareStatement("select *from TWSH_INSTITUTE_MASTER (nolock)");
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    map = new HashMap();
                    map.put("instCode", rs.getString(2));
                    map.put("regCode", rs.getString(3));
                    map.put("instName", rs.getString(4));
                    map.put("address", rs.getString(5));
                    map.put("town", rs.getString(6));
                    if(rs.getString(6)!=null){
                        map.put("town", rs.getString(6));
                    }else map.put("town", "");
                    map.put("principleName", rs.getString(8));
                    if(rs.getString(9)!=null){
                        map.put("mobile", rs.getString(9));
                    }else map.put("mobile", "");
                    map.put("district", rs.getString(10));
                    if(rs.getString(12)!=null){
                        map.put("pincode", rs.getString(12));
                    }else map.put("pincode", "");
                    lstDetails.add(map);
                    map = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return lstDetails;
    }

    public void getPdfDataNew(String filepath, String filename, List<HashMap> list) throws SQLException {
        Font heading = new Font(Font.FontFamily.COURIER, 9, Font.BOLD, BaseColor.BLACK);
        Font f1 = new Font(Font.FontFamily.COURIER, 7, Font.BOLD, BaseColor.BLACK);
        Font header = new Font(Font.FontFamily.COURIER, 6, Font.BOLD, new BaseColor(255, 255, 255));

        try {

            Document document = new Document();
            OutputStream file = new FileOutputStream(new File(filepath + filename));
            PdfWriter writer = PdfWriter.getInstance(document, file);
            document.setPageSize(PageSize.A4);
            document.open();
            Paragraph para = new Paragraph("                                          "
                    + "                                     "
                    + "                                "
                    + "             LIST OF RECOGNISED INSTITUTIONS FOR TWSH - 2021",heading);
            Paragraph para2 = new Paragraph("                                          ");
            PdfPTable table1 = new PdfPTable(10);
            table1.setWidthPercentage(100);
            table1.getDefaultCell().setBorderColor(new BaseColor(0,0,0));
            float[] columnWidthsFOrTable1 = {.8f, 1.6f, 1f, 2f, 3f, 2f, 1f, 1f, 1f, 1f};
            table1.setWidths(columnWidthsFOrTable1);

            PdfPCell hcell1 = null;

            hcell1 = new PdfPCell(new Paragraph("S.NO", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBackgroundColor(new BaseColor(153, 153, 255));
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBorderWidthTop(1f);
            hcell1.setBorderWidthLeft(1f);
            hcell1.setBorderWidthBottom(1f);
            hcell1.setBorderWidthRight(0f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Institute Code", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0, 0, 0));
            hcell1.setBackgroundColor(new BaseColor(153, 153, 255));
            hcell1.setBorderWidthTop(1f);
            hcell1.setBorderWidthLeft(1f);
            hcell1.setBorderWidthBottom(1f);
            hcell1.setBorderWidthRight(0f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Recog Period", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(153, 153, 255));
            hcell1.setBorderWidthTop(1f);
            hcell1.setBorderWidthLeft(1f);
            hcell1.setBorderWidthBottom(1f);
            hcell1.setBorderWidthRight(0f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Institute Name", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(153, 153, 255));
            hcell1.setBorderWidthTop(1f);
            hcell1.setBorderWidthLeft(1f);
            hcell1.setBorderWidthBottom(1f);
            hcell1.setBorderWidthRight(0f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Address", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(153, 153, 255));
            hcell1.setBorderWidthTop(1f);
            hcell1.setBorderWidthLeft(1f);
            hcell1.setBorderWidthBottom(1f);
            hcell1.setBorderWidthRight(0f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Town", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(153, 153, 255));
            hcell1.setBorderWidthTop(1f);
            hcell1.setBorderWidthLeft(1f);
            hcell1.setBorderWidthBottom(1f);
            hcell1.setBorderWidthRight(0f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("District Name", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(153, 153, 255));
            hcell1.setBorderWidthTop(1f);
            hcell1.setBorderWidthLeft(1f);
            hcell1.setBorderWidthBottom(1f);
            hcell1.setBorderWidthRight(0f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Pin Code", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(153, 153, 255));
            hcell1.setBorderWidthTop(1f);
            hcell1.setBorderWidthLeft(1f);
            hcell1.setBorderWidthBottom(1f);
            hcell1.setBorderWidthRight(0f);
            table1.addCell(hcell1);
            
            hcell1 = new PdfPCell(new Paragraph("Principle Name", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(153, 153, 255));
            hcell1.setBorderWidthTop(1f);
            hcell1.setBorderWidthLeft(1f);
            hcell1.setBorderWidthBottom(1f);
            hcell1.setBorderWidthRight(0f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph("Mobile", header));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(153, 153, 255));
            hcell1.setBorderWidthTop(1f);
            hcell1.setBorderWidthLeft(1f);
            hcell1.setBorderWidthBottom(1f);
            hcell1.setBorderWidthRight(1f);
            table1.addCell(hcell1);

                        
            
            int i=0;
            for(HashMap map : list){
            int j = ++i;
            int a=0,b=0,c=0;
            if(j%2==0){
                a=236;b=243;c=253;
            }else{a=255;b=255;c=255;}
            
            hcell1 = new PdfPCell(new Paragraph(String.valueOf(j), f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(a, b, c));
            hcell1.setBorderWidthTop(0.5f);
            hcell1.setBorderWidthLeft(0.5f);
            hcell1.setBorderWidthBottom(0.5f);
            hcell1.setBorderWidthRight(0.5f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(map.get("instCode").toString(), f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(a, b, c));
            hcell1.setBorderWidthTop(0.5f);
            hcell1.setBorderWidthLeft(0.5f);
            hcell1.setBorderWidthBottom(0.5f);
            hcell1.setBorderWidthRight(0.5f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(map.get("regCode").toString(), f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(a, b, c));
            hcell1.setBorderWidthTop(0.5f);
            hcell1.setBorderWidthLeft(0.5f);
            hcell1.setBorderWidthBottom(0.5f);
            hcell1.setBorderWidthRight(0.5f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(map.get("instName").toString(), f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(a, b, c));
            hcell1.setBorderWidthTop(0.5f);
            hcell1.setBorderWidthLeft(0.5f);
            hcell1.setBorderWidthBottom(0.5f);
            hcell1.setBorderWidthRight(0.5f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(map.get("address").toString(), f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(a, b, c));
            hcell1.setBorderWidthTop(0.5f);
            hcell1.setBorderWidthLeft(0.5f);
            hcell1.setBorderWidthBottom(0.5f);
            hcell1.setBorderWidthRight(0.5f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(map.get("town").toString(), f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(a, b, c));
            hcell1.setBorderWidthTop(0.5f);
            hcell1.setBorderWidthLeft(0.5f);
            hcell1.setBorderWidthBottom(0.5f);
            hcell1.setBorderWidthRight(0.5f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(map.get("district").toString(), f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(a, b, c));
            hcell1.setBorderWidthTop(0.5f);
            hcell1.setBorderWidthLeft(0.5f);
            hcell1.setBorderWidthBottom(0.5f);
            hcell1.setBorderWidthRight(0.5f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(map.get("pincode").toString(), f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(a, b, c));
            hcell1.setBorderWidthTop(0.5f);
            hcell1.setBorderWidthLeft(0.5f);
            hcell1.setBorderWidthBottom(0.5f);
            hcell1.setBorderWidthRight(0.5f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(map.get("principleName").toString(), f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBackgroundColor(new BaseColor(a, b, c));
            hcell1.setBorderWidthTop(0.5f);
            hcell1.setBorderWidthLeft(0.5f);
            hcell1.setBorderWidthBottom(0.5f);
            hcell1.setBorderWidthRight(0.5f);
            table1.addCell(hcell1);

            hcell1 = new PdfPCell(new Paragraph(map.get("mobile").toString(), f1));
            hcell1.setHorizontalAlignment(Element.ALIGN_CENTER);
            hcell1.setBackgroundColor(new BaseColor(a, b, c));
            hcell1.setBorderColor(new BaseColor(0,0,0));
            hcell1.setBorderWidthTop(0.5f);
            hcell1.setBorderWidthLeft(0.5f);
            hcell1.setBorderWidthBottom(0.5f);
            hcell1.setBorderWidthRight(0.5f);
            table1.addCell(hcell1);
                
            }
            document.add(para);    
            document.add(para2);    
            document.add(table1);    
            
            //Instructions End

            document.close();

        } catch (Exception ex) {
            ex.printStackTrace();
        } finally {
            // con.close(); 
        }

    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "D://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }

    private void insertCell(PdfPTable table, String text, int align, int colspan, Font font) {

        //create a new cell with the specified Text and Font
        PdfPCell cell = new PdfPCell(new Phrase(text.trim(), font));
        //set the cell alignment
        cell.setHorizontalAlignment(align);
        //set the cell column span in case you want to merge two or more cells
        cell.setColspan(colspan);
        //in case there is no text and you wan to create an empty row
        if (text.trim().equalsIgnoreCase("")) {
            cell.setMinimumHeight(10f);
        }
        //add the call to the table
        table.addCell(cell);

    }

    public static boolean downLoadFiles(HttpServletRequest request, HttpServletResponse response, String filePath, String fileName) throws IOException {
        ServletOutputStream out = null;
        boolean fileDownloadStatus = false;
        try {
            if (filePath != null && fileName != null) {
                filePath = filePath + fileName;
                BufferedInputStream in = null;
                File fileDetailsData = new File(filePath);
                FileInputStream fin = new FileInputStream(fileDetailsData);
                in = new BufferedInputStream(fin);
                out = response.getOutputStream();
                response.setContentType("application/force-download");
                fileName = fileName.trim();
                filePath = filePath.trim();
                if (fileName.contains("-")) {
                    fileName.replace("-", "");
                }
                response.addHeader("Content-Disposition", "attachment; filename=\"" + fileName + "\"");
                byte[] buffer = new byte[4 * 1024];
                int data = 0;
                while ((data = in.read(buffer)) != -1) {
                    out.write(buffer, 0, data);
                }
                out.flush();
                out.close();
                fileDownloadStatus = true;
            } else {
                fileDownloadStatus = false;
            }

        } catch (FileNotFoundException e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } catch (Exception e) {
            fileDownloadStatus = false;
            e.printStackTrace();
        } finally {
            if (out != null) {
                try {
                    out.flush();
                    out.close();
                } catch (IOException ex) {
                    ex.printStackTrace();
                }
            }
        }
        return fileDownloadStatus;
    }

    public static void deleteFile(String filePath) {
        try {
            if (filePath != null) {
                File file = new File(filePath);
                file.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
