/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.dao;
import com.sms.util.SendSMSDTO;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import net.apo.angrau.action.RegisterFileUpload;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;
import org.apache.log4j.Logger;
/**
 *
 * @author APTOL301655
 */
public class ReuploadCCICDAO {
    
    private static final Logger logger = Logger.getLogger(ReuploadDAO.class);

    public HashMap<String,String> getStatus(RegisterForm rform) throws Exception {
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        String result = null;
       HashMap<String,String>  list=new HashMap<String,String>();
        try {
             Date todate1 = new SimpleDateFormat("dd/mm/yyyy").parse(rform.getDob());
            String dob = new SimpleDateFormat("yyyy-mm-dd").format(todate1);
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_CCIC_VerficationStatus(?,?,?)}");
            cstmt.setString(1, rform.getRegno());
            cstmt.setString(2, dob);
            cstmt.setString(3, "9999");
            rs = cstmt.executeQuery();
            if (rs.next() == true) {
               list.put("result",rs.getString(1));
               list.put("aadhar",rs.getString(2));
               list.put("course",rs.getString(3));
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return list;
    }

    public HashMap<String, String> getData(RegisterForm rform, HttpServletRequest request) throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        CallableStatement cstmt = null;
        Connection con = null;
        ResultSet rs = null;
        RegisterForm regform = null;
        try {

            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_CCIC_Reupload_Get (?,?,?)}");
            cstmt.setString(1, rform.getAadhar());
            cstmt.setString(2, rform.getCourse());
            cstmt.setString(3, "9999");

            rs = cstmt.executeQuery();
            if (rs.next() == true) {
                regform = new RegisterForm();
                request.setAttribute("bname", rs.getString(1));
                request.setAttribute("fname", rs.getString(2));
                request.setAttribute("dob", rs.getString(3));
                request.setAttribute("gender", rs.getString(4));
                request.setAttribute("caste", rs.getString(5));
                request.setAttribute("mobile", rs.getString(6));
                request.setAttribute("email", rs.getString(7));
                request.setAttribute("aadhar", rs.getString(8));
                request.setAttribute("course", rs.getString(9));
                request.setAttribute("examination", rs.getString(10));
                request.setAttribute("coursename", rs.getString(11));
                request.setAttribute("gradename", rs.getString(12));
                request.setAttribute("aadhar1", "XXXXXXXX" + rs.getString(8).substring(8, 12));
            
                request.setAttribute("upload1c", rs.getString(15));
                request.setAttribute("upload2c", rs.getString(16));
                request.setAttribute("upload3c", rs.getString(17));
                request.setAttribute("upload4c", rs.getString(18));
                request.setAttribute("upload5c", rs.getString(19));
              
                request.setAttribute("upload1n", rs.getString(27));
                request.setAttribute("upload2n", rs.getString(28));
                request.setAttribute("upload3n", rs.getString(29));
                request.setAttribute("upload4n", rs.getString(30));
                request.setAttribute("upload5n", rs.getString(31));
                
                request.setAttribute("institute", rs.getString(32));
                request.setAttribute("edistrict", rs.getString(33));
                request.setAttribute("ecenter", rs.getString(34));
                request.setAttribute("edate", rs.getString(35));
                request.setAttribute("ebatch", rs.getString(36));
                request.setAttribute("address", rs.getString(37)+","+rs.getString(38)+","+rs.getString(39)+","+rs.getString(40)+","+rs.getString(41)+","+rs.getString(42)+","+rs.getString(43));
                  request.setAttribute("coursename", rs.getString(45));
                map.put("child", "9866747477");
//                list.add(map);
            }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (rs != null) {
                rs.close();
            }
            if (cstmt != null) {
                cstmt.close();
            }
            if (con != null) {
                con.close();
            }


        }
        return map;
    }

    public String getvalidatingHallTicketNum(String aadhar, String grade) throws Exception {
        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String mb = "";
        String mm = "";

        try {
//            query = "select Mobile from StudentInfo_Final  where MED_HALL_TICKET_NO='" + hallticket + "'  and DOB='" + mm + "'";
               query = "select MobileNo from CCIC_ApplicantRegistrationDetails with(nolock) where AadharNumber=? and CourseID=?";
          con = DatabaseConnection.getConnection();
            st = con.prepareStatement(query);
            st.setString(1,aadhar);
            st.setString(2,grade);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    mb = rs.getString(1);
                }
            } else {
                mb = "";
            }
        } catch (Exception e) {
            mb = "";
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mb;
    }

    public String getvalidatingOTP(String hallticket, String mobile) throws Exception {

        String query = null;
        PreparedStatement st = null;
        Connection con = null;
        ResultSet rs = null;
        String otp = "";

        try {
            con = DatabaseConnection.getConnection();
            query = "select top(1) OTP  from OTP_Details with(nolock) where Mobile_Num='" + mobile + "' and Aadhar_Id='" + hallticket + "' order by insertDate desc";
            System.out.println("query " + query);
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    otp = rs.getString(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return otp;
    }

    public int SaveOTP(String hallticket, String mobile, String otp, String systemIp) throws Exception {
        int count = 0;
        ResultSet rs = null;
        Connection con = null;
        PreparedStatement st = null;
        CallableStatement cstmt = null;
        ArrayList list = new ArrayList();
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call proc_OTP_Details_Insert(?,?,?,?)}");
            cstmt.setString(1, hallticket);
            cstmt.setString(2, mobile);
            cstmt.setString(3, otp);
            cstmt.setString(4, systemIp);
            rs = cstmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    count = rs.getInt(1);
                }
            }
        } catch (Exception e) {

            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return count;
    }

    public String insertSmsLogDetails(SendSMSDTO sendSMSDTO) throws Exception {
        Connection con = null;
//        Statement st = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String mobiles = "";
        String query = null;
        PreparedStatement ps = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "insert into SmsLog(MessageDescription,Messageto,LoginId,Systemip,senddate,sendstatus,Recevername,Receverdesignation,Receverdepartment) values('" + sendSMSDTO.getSms() + "',"
                    + "'" + sendSMSDTO.getMobileNumber() + "','" + sendSMSDTO.getLoginId() + "','10.100.102.80',getDate(),"
                    + "'" + sendSMSDTO.getSendStatus() + "','Aponline','Teacher','Teacher') ";
            ps = con.prepareStatement(query);
            ps.executeUpdate();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
                if (rs != null) {
                    rs.close();
                }
//                if (st != null) {
//                    st.close();
//                }
                if (pstmt != null) {
                    pstmt.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return mobiles;
    }

    public String insertFiles(RegisterForm rform, String remoteAddress) {
        Connection con = null;
        //   CallableStatement cstmt = null;
        PreparedStatement cstmt = null;
        ResultSet rs = null;
        HashMap hashMap = null;
        List<HashMap> lstDetails = new ArrayList();
        String result = "";

        String fileName1 = "";
        String fileName2 = "";
        String fileName3 = "";
        String fileName4 = "";
        String fileName5 = "";
        String fileName6 = "";
       


//        RegisterFileUpload rf = new RegisterFileUpload();
   RegisterFileUpload rf = new RegisterFileUpload();

        boolean flag = false;

        String aadhar = rform.getAadhar();
        String grade = rform.getCourse();
        System.out.println("course==================="+rform.getCourse());
        if (rform.getUpload1c().equalsIgnoreCase("2") ) {
            if (rform.getUpload1() != null && rform.getUpload1().toString().length() > 0) {
                flag = rf.uploadDocs(rform.getUpload1(), rform.getUpload1n().substring(0, rform.getUpload1n().length() - 4)+"_REUPLOAD.jpg", aadhar,grade);
                if (flag == true) {
                    fileName1 = rform.getUpload1n().substring(0, rform.getUpload1n().length() - 4)+"_REUPLOAD.jpg";
                }
            }
        } else {
            fileName1 = rform.getUpload1n();
        }
        
         if (rform.getUpload2c().equalsIgnoreCase("2") ) {
            if (rform.getUpload2() != null && rform.getUpload2().toString().length() > 0) {
                flag = rf.uploadDocs(rform.getUpload2(), rform.getUpload2n().substring(0, rform.getUpload2n().length() - 4)+"_REUPLOAD.jpg", aadhar,grade);
                if (flag == true) {
                    fileName2 =rform.getUpload2n().substring(0, rform.getUpload2n().length() - 4)+"_REUPLOAD.jpg";
                }
            }
        } else {
            fileName2 = rform.getUpload2n();
        }
         
           if (rform.getUpload3c().equalsIgnoreCase("2") ) {
            if (rform.getUpload3() != null && rform.getUpload3().toString().length() > 0) {
                flag = rf.uploadDocs(rform.getUpload3(), rform.getUpload3n().substring(0, rform.getUpload3n().length() - 4)+"_REUPLOAD.jpg", aadhar,grade);
                if (flag == true) {
                    fileName3 = rform.getUpload3n().substring(0, rform.getUpload3n().length() - 4)+"_REUPLOAD.jpg";
                }
            }
        } else {
            fileName3 = rform.getUpload3n();
        }
           
         if (rform.getUpload4c().equalsIgnoreCase("2") ) {
            if (rform.getUpload4() != null && rform.getUpload4().toString().length() > 0) {
                flag = rf.uploadDocs(rform.getUpload4(), rform.getUpload4n().substring(0, rform.getUpload4n().length() - 4)+"_REUPLOAD.jpg", aadhar,grade);
                if (flag == true) {
                    fileName4 = rform.getUpload4n().substring(0, rform.getUpload4n().length() - 4)+"_REUPLOAD.jpg";
                }
            }
        } else {
            fileName4 = rform.getUpload4n();
        }
         
         if (rform.getUpload5c().equalsIgnoreCase("2") ) {
            if (rform.getUpload5() != null && rform.getUpload5().toString().length() > 0) {
                flag = rf.uploadDocs(rform.getUpload5(), aadhar + "_Hallticket_REUPLOAD.jpg", aadhar,grade);
                if (flag == true) {
                    fileName5 = aadhar + "_Hallticket_REUPLOAD.jpg";
                }
            }
        } else {
            fileName5 = rform.getUpload5n();
        }
       
           
        try {
            con = DatabaseConnection.getConnection();
            cstmt = con.prepareCall("{Call Usp_CCIC_Reupload_Update (?,?,?,?,?,?,?,?,?)}");
            cstmt.setString(1, rform.getAadhar());
            cstmt.setString(2, rform.getCourse());
            cstmt.setString(3, fileName1);
            cstmt.setString(4, fileName2);
            cstmt.setString(5, fileName3);
            cstmt.setString(6, fileName4);
            cstmt.setString(7, fileName5);
            cstmt.setString(8, "2");
            cstmt.setString(9,remoteAddress);
            int i = cstmt.executeUpdate();
            if (i == 1) {
                result = "1";
            } else {
                result = "0";
            }
//            rs = cstmt.executeQuery();
//            if (rs.next() == true) {
//                result = rs.getString(1);
//            }
        } catch (Exception ex) {
            logger.info("Error================" + ex.getMessage());
            ex.printStackTrace();

        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (cstmt != null) {
                    cstmt.close();
                }
                if (con != null) {
                    con.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return result;
    }
     public ArrayList getGradesList() throws Exception {
        String query = null;

        ArrayList list = new ArrayList();
        HashMap<String, String> map = new HashMap<String, String>();
        PreparedStatement pstmt = null;
        Connection con = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select Grade,GRADE_DESC from TWSH_GRADE_MASTER with(nolock) where rowid not in('22','23','10','11') order by RowID asc";
            pstmt = con.prepareStatement(query);
            rs = pstmt.executeQuery();
            while (rs != null && rs.next()) {
                map = new HashMap();
                map.put("gcode", rs.getString(1));
                map.put("gname", rs.getString(2));
                list.add(map);
            }

        } catch (Exception e) {
        } finally {
        }
        return list;
    }

}
