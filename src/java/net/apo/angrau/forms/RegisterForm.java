/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.forms;

import java.util.ArrayList;
import org.apache.struts.action.ActionForm;
import org.apache.struts.upload.FormFile;

/**
 *
 * @author APTOL301655
 */
public class RegisterForm extends ActionForm {
    private String bundlecode;   
    private String tobescannedbooklets;
    private String scannedbooklets;
    private String totalbooklets;
    private String epaper;
  private String bundle;
  private String barcode;
  private String coursename;
    private String statuscase;
    private String hallticketflag;
    private String regno;
    private String gradename1;
    private String instcode;
    private String instname;
    private String instaddress;
    private String instprincipal;
    private String instmobile;
    private String insttown;
    private String instdist;
    private String instpincode;
    private String instreg;
    private String sscyear;
    private String lowerreghallticket;
    private String lowerregflag;
    private String gender1;
    private String caste1;
    private String dob1;
    private String maxMarks;    
    private String interhallticket;
    private String sschallticket;
    private String intermonth;
    private String interyear;
    private String interflag;
    private String sscflag;
    private String upload1New;
private String upload2New;
private String upload3New;
private String upload4New;
private String upload5New;
private String photoNew;
private String signatureNew;
private String blindfileNew;
private String upload1New1;
private String upload2New1;
private String upload3New1;
private String upload4New1;
private String upload5New1;
private String photoNew1;
private String signatureNew1;
private String blindfileNew1;
private String hstatus;
private String blindno;
private String blindflag;
private String statusresult;
private String statusflag;
private String district1;
private String aadhar1;
private String aadhar2;
private String apdistrict1;
private String ecenter1;
private String mandal1;
private String grade1;
private String grade2;
private String languagename;
private String examinationname;
private String gradename;
    private String blind;
    private String bname;
    private String fname;
    private String mname;
    private String photoflag;
    private String dob;
    private String hallticket;
    private String sarea;
    private String caste;
    private String occupation;
    private String mobile;
    private String houseno;
    private String locality;
    private String state;
    private String distrcit;
    private String mandal;
    private String pincode;
    private String defense;
    private String cwsn;
    private String ncc;
    private String sports;
    private String rank;
    private String paymentmode;
    private double amount;
    private String otp;
    private int age;
    private String income;
    private String gender;
    private String distrcit1;
    private String marks;
    private String totalmarks;
    private String dobc;
    private String dobflag;
    private String ews;
    private String stype;
    private String smgt;
    private String sname;
    private String email;
    private String insertcount;
    private String otpv;
    private String localstatus;
    private String apdistrict;
    private String tsdistrict;
    private String sportsintermnational;
    private String sportsinterstate;
    private String sportsnational;
    private ArrayList distLists = new ArrayList();
    private ArrayList distListap = new ArrayList();
    private FormFile localfile;
    private FormFile sportsfile;
    private FormFile spotsstatefile;
    private FormFile sportsnationalfile;
    private String dobinvalid;
    private String iyear;
    private String istate;
    private String idistrict;
    private String iiyear;
    private String iistate;
    private String iidistrict;
    private String iiiyear;
    private String iiistate;
    private String iiidistrict;
    private FormFile ifile;
    private FormFile iifile;
    private FormFile iiifile;
    
    private String ivyear;
    private String ivinstitute;
    private String ivmandal;
    private String ivdistrict;
    private String ivinstitute1;
    private String ivmandal1;
    private String ivdistrict1;
    private String ivstate;
    private String vyear;
    private String vinstitute;
    private String vmandal;
    private String vdistrict;
    private String vinstitute1;
    private String vmandal1;
    private String vdistrict1;
    private String vstate;
    private String viyear;
    private String viinstitute;
    private String vimandal;
    private String vidistrict;
    private String viinstitute1;
    private String vimandal1;
    private String vidistrict1;
    private String vistate;
    private String viiyear;
    private String viiinstitute;
    private String viimandal;
    private String viidistrict;
    private String viiinstitute1;
    private String viimandal1;
    private String viidistrict1;
    private String viistate;
    private String viiiyear;
    private String viiiinstitute;
    private String viiimandal;
    private String viiidistrict;
    private String viiiinstitute1;
    private String viiimandal1;
    private String viiidistrict1;
    private String viiistate;
    private String ixyear;
    private String ixinstitute;
    private String ixmandal;
    private String ixdistrict;
    private String ixinstitute1;
    private String ixmandal1;
    private String ixdistrict1;
    private String ixstate;
    private String xyear;
    private String xinstitute;
    private String xmandal;
    private String xdistrict;
    private String xinstitute1;
    private String xmandal1;
    private String xdistrict1;
    private String xstate;
    private String imandal;
    private String iimandal;
    private String iiimandal;
     private String sameschool;
    private String schoolcode;

    private String course;
    private String userName;
    private ArrayList courseList;
    
     private String vstatus;
     private String photoc;
     private String photon;
     private String signaturec;
     private String signaturen;
     private String upload1c;
     private String upload1n;
        private String upload2c;
     private String upload2n;
        private String upload3c;
     private String upload3n;
        private String upload4c;
     private String upload4n;
        private String upload5c;
     private String upload5n;
      private String blindc;
     private String blindn;
     private String maskmobile;
     private String coursedesc;
     private String qualification;

     private String attendance;
     private String fileFlag;
     private String batchCode;
private String batchName;
private ArrayList batchlist = new ArrayList();
private ArrayList  courseWiseList = new ArrayList();

    private ArrayList centerList = new ArrayList();
    private String centerCode;

     
    public String getSameschool() {
        return sameschool;
    }

    public void setSameschool(String sameschool) {
        this.sameschool = sameschool;
    }

    public String getSchoolcode() {
        return schoolcode;
    }

    public void setSchoolcode(String schoolcode) {
        this.schoolcode = schoolcode;
    }
    
    public String getPhotoflag() {
        return photoflag;
    }

    public void setPhotoflag(String photoflag) {
        this.photoflag = photoflag;
    }

    public String getDobinvalid() {
        return dobinvalid;
    }

    public void setDobinvalid(String dobinvalid) {
        this.dobinvalid = dobinvalid;
    }

    public String getDobc() {
        return dobc;
    }

    public void setDobc(String dobc) {
        this.dobc = dobc;
    }

    public String getDobflag() {
        return dobflag;
    }

    public void setDobflag(String dobflag) {
        this.dobflag = dobflag;
    }

    public String getImandal() {
        return imandal;
    }

    public void setImandal(String imandal) {
        this.imandal = imandal;
    }

    public String getIimandal() {
        return iimandal;
    }

    public void setIimandal(String iimandal) {
        this.iimandal = iimandal;
    }

    public String getIiimandal() {
        return iiimandal;
    }

    public void setIiimandal(String iiimandal) {
        this.iiimandal = iiimandal;
    }

    public String getIdistrict() {
        return idistrict;
    }

    public void setIdistrict(String idistrict) {
        this.idistrict = idistrict;
    }

    public String getIidistrict() {
        return iidistrict;
    }

    public void setIidistrict(String iidistrict) {
        this.iidistrict = iidistrict;
    }

    public String getIiidistrict() {
        return iiidistrict;
    }

    public void setIiidistrict(String iiidistrict) {
        this.iiidistrict = iiidistrict;
    }

    public String getIyear() {
        return iyear;
    }

    public void setIyear(String iyear) {
        this.iyear = iyear;
    }

    public String getIstate() {
        return istate;
    }

    public void setIstate(String istate) {
        this.istate = istate;
    }

  
    public String getIiyear() {
        return iiyear;
    }

    public void setIiyear(String iiyear) {
        this.iiyear = iiyear;
    }

    public String getIistate() {
        return iistate;
    }

    public void setIistate(String iistate) {
        this.iistate = iistate;
    }

  

    public String getIiiyear() {
        return iiiyear;
    }

    public void setIiiyear(String iiiyear) {
        this.iiiyear = iiiyear;
    }

    public String getIiistate() {
        return iiistate;
    }

    public void setIiistate(String iiistate) {
        this.iiistate = iiistate;
    }

  

    public FormFile getIfile() {
        return ifile;
    }

    public void setIfile(FormFile ifile) {
        this.ifile = ifile;
    }

    public FormFile getIifile() {
        return iifile;
    }

    public void setIifile(FormFile iifile) {
        this.iifile = iifile;
    }

    public FormFile getIiifile() {
        return iiifile;
    }

    public void setIiifile(FormFile iiifile) {
        this.iiifile = iiifile;
    }

    public String getLocalstatus() {
        return localstatus;
    }

    public void setLocalstatus(String localstatus) {
        this.localstatus = localstatus;
    }

    public String getApdistrict() {
        return apdistrict;
    }

    public void setApdistrict(String apdistrict) {
        this.apdistrict = apdistrict;
    }

    public String getTsdistrict() {
        return tsdistrict;
    }

    public void setTsdistrict(String tsdistrict) {
        this.tsdistrict = tsdistrict;
    }

    public ArrayList getDistLists() {
        return distLists;
    }

    public void setDistLists(ArrayList distLists) {
        this.distLists = distLists;
    }

    public ArrayList getDistListap() {
        return distListap;
    }

    public void setDistListap(ArrayList distListap) {
        this.distListap = distListap;
    }

    public FormFile getLocalfile() {
        return localfile;
    }

    public void setLocalfile(FormFile localfile) {
        this.localfile = localfile;
    }

    public void setOtpv(String otpv) {
        this.otpv = otpv;
    }

    public String getStype() {
        return stype;
    }

    public void setStype(String stype) {
        this.stype = stype;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInsertcount() {
        return insertcount;
    }

    public void setInsertcount(String insertcount) {
        this.insertcount = insertcount;
    }

    public String getSmgt() {
        return smgt;
    }

    public void setSmgt(String smgt) {
        this.smgt = smgt;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public String getVinstitute1() {
        return vinstitute1;
    }

    public void setVinstitute1(String vinstitute1) {
        this.vinstitute1 = vinstitute1;
    }

    public String getMarks() {
        return marks;
    }

    public void setMarks(String marks) {
        this.marks = marks;
    }

    public String getTotalmarks() {
        return totalmarks;
    }

    public void setTotalmarks(String totalmarks) {
        this.totalmarks = totalmarks;
    }

    public String getEws() {
        return ews;
    }

    public void setEws(String ews) {
        this.ews = ews;
    }

    public FormFile getRuralfile() {
        return ruralfile;
    }

    public void setRuralfile(FormFile ruralfile) {
        this.ruralfile = ruralfile;
    }

    public FormFile getStatusfile() {
        return statusfile;
    }

    public void setStatusfile(FormFile statusfile) {
        this.statusfile = statusfile;
    }

    public String getVmandal1() {
        return vmandal1;
    }

    public void setVmandal1(String vmandal1) {
        this.vmandal1 = vmandal1;
    }

    public String getVdistrict1() {
        return vdistrict1;
    }

    public void setVdistrict1(String vdistrict1) {
        this.vdistrict1 = vdistrict1;
    }

    public String getVstate() {
        return vstate;
    }

    public void setVstate(String vstate) {
        this.vstate = vstate;
    }

    public String getViinstitute1() {
        return viinstitute1;
    }

    public void setViinstitute1(String viinstitute1) {
        this.viinstitute1 = viinstitute1;
    }

    public String getVimandal1() {
        return vimandal1;
    }

    public void setVimandal1(String vimandal1) {
        this.vimandal1 = vimandal1;
    }

    public String getVidistrict1() {
        return vidistrict1;
    }

    public void setVidistrict1(String vidistrict1) {
        this.vidistrict1 = vidistrict1;
    }

    public String getVistate() {
        return vistate;
    }

    public void setVistate(String vistate) {
        this.vistate = vistate;
    }

    public String getViiinstitute1() {
        return viiinstitute1;
    }

    public void setViiinstitute1(String viiinstitute1) {
        this.viiinstitute1 = viiinstitute1;
    }

    public String getViimandal1() {
        return viimandal1;
    }

    public void setViimandal1(String viimandal1) {
        this.viimandal1 = viimandal1;
    }

    public String getViidistrict1() {
        return viidistrict1;
    }

    public void setViidistrict1(String viidistrict1) {
        this.viidistrict1 = viidistrict1;
    }

    public String getViistate() {
        return viistate;
    }

    public void setViistate(String viistate) {
        this.viistate = viistate;
    }

    public String getViiiinstitute1() {
        return viiiinstitute1;
    }

    public void setViiiinstitute1(String viiiinstitute1) {
        this.viiiinstitute1 = viiiinstitute1;
    }

    public String getViiimandal1() {
        return viiimandal1;
    }

    public void setViiimandal1(String viiimandal1) {
        this.viiimandal1 = viiimandal1;
    }

    public String getViiidistrict1() {
        return viiidistrict1;
    }

    public void setViiidistrict1(String viiidistrict1) {
        this.viiidistrict1 = viiidistrict1;
    }

    public String getViiistate() {
        return viiistate;
    }

    public void setViiistate(String viiistate) {
        this.viiistate = viiistate;
    }

    public String getIxinstitute1() {
        return ixinstitute1;
    }

    public void setIxinstitute1(String ixinstitute1) {
        this.ixinstitute1 = ixinstitute1;
    }

    public String getIxmandal1() {
        return ixmandal1;
    }

    public void setIxmandal1(String ixmandal1) {
        this.ixmandal1 = ixmandal1;
    }

    public String getIxdistrict1() {
        return ixdistrict1;
    }

    public void setIxdistrict1(String ixdistrict1) {
        this.ixdistrict1 = ixdistrict1;
    }

    public String getIxstate() {
        return ixstate;
    }

    public void setIxstate(String ixstate) {
        this.ixstate = ixstate;
    }

    public String getXinstitute1() {
        return xinstitute1;
    }

    public void setXinstitute1(String xinstitute1) {
        this.xinstitute1 = xinstitute1;
    }

    public String getXmandal1() {
        return xmandal1;
    }

    public void setXmandal1(String xmandal1) {
        this.xmandal1 = xmandal1;
    }

    public String getXdistrict1() {
        return xdistrict1;
    }

    public void setXdistrict1(String xdistrict1) {
        this.xdistrict1 = xdistrict1;
    }

    public String getXstate() {
        return xstate;
    }

    public void setXstate(String xstate) {
        this.xstate = xstate;
    }

    public String getIvstate() {
        return ivstate;
    }

    public void setIvstate(String ivstate) {
        this.ivstate = ivstate;
    }

    public String getIvinstitute1() {
        return ivinstitute1;
    }

    public void setIvinstitute1(String ivinstitute1) {
        this.ivinstitute1 = ivinstitute1;
    }

    public String getIvmandal1() {
        return ivmandal1;
    }

    public void setIvmandal1(String ivmandal1) {
        this.ivmandal1 = ivmandal1;
    }

    public String getIvdistrict1() {
        return ivdistrict1;
    }

    public void setIvdistrict1(String ivdistrict1) {
        this.ivdistrict1 = ivdistrict1;
    }

    public String getDistrcit1() {
        return distrcit1;
    }

    public void setDistrcit1(String distrcit1) {
        this.distrcit1 = distrcit1;
    }

    public String getMandal1() {
        return mandal1;
    }

    public void setMandal1(String mandal1) {
        this.mandal1 = mandal1;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
    private String mode;
    private ArrayList stateList = new ArrayList();
    private ArrayList gradeslist = new ArrayList();

    public String getIvyear() {
        return ivyear;
    }

    public void setIvyear(String ivyear) {
        this.ivyear = ivyear;
    }

    public String getIvinstitute() {
        return ivinstitute;
    }

    public void setIvinstitute(String ivinstitute) {
        this.ivinstitute = ivinstitute;
    }

    public String getIvmandal() {
        return ivmandal;
    }

    public void setIvmandal(String ivmandal) {
        this.ivmandal = ivmandal;
    }

    public String getIvdistrict() {
        return ivdistrict;
    }

    public void setIvdistrict(String ivdistrict) {
        this.ivdistrict = ivdistrict;
    }

    public String getVyear() {
        return vyear;
    }

    public void setVyear(String vyear) {
        this.vyear = vyear;
    }

    public String getVinstitute() {
        return vinstitute;
    }

    public void setVinstitute(String vinstitute) {
        this.vinstitute = vinstitute;
    }

    public String getVmandal() {
        return vmandal;
    }

    public void setVmandal(String vmandal) {
        this.vmandal = vmandal;
    }

    public String getVdistrict() {
        return vdistrict;
    }

    public void setVdistrict(String vdistrict) {
        this.vdistrict = vdistrict;
    }

    public String getViyear() {
        return viyear;
    }

    public void setViyear(String viyear) {
        this.viyear = viyear;
    }

    public String getViinstitute() {
        return viinstitute;
    }

    public void setViinstitute(String viinstitute) {
        this.viinstitute = viinstitute;
    }

    public String getVimandal() {
        return vimandal;
    }

    public void setVimandal(String vimandal) {
        this.vimandal = vimandal;
    }

    public String getVidistrict() {
        return vidistrict;
    }

    public void setVidistrict(String vidistrict) {
        this.vidistrict = vidistrict;
    }

    public String getViiyear() {
        return viiyear;
    }

    public void setViiyear(String viiyear) {
        this.viiyear = viiyear;
    }

    public String getViiinstitute() {
        return viiinstitute;
    }

    public void setViiinstitute(String viiinstitute) {
        this.viiinstitute = viiinstitute;
    }

    public String getViimandal() {
        return viimandal;
    }

    public void setViimandal(String viimandal) {
        this.viimandal = viimandal;
    }

    public String getViidistrict() {
        return viidistrict;
    }

    public void setViidistrict(String viidistrict) {
        this.viidistrict = viidistrict;
    }

    public String getViiiyear() {
        return viiiyear;
    }

    public void setViiiyear(String viiiyear) {
        this.viiiyear = viiiyear;
    }

    public String getViiiinstitute() {
        return viiiinstitute;
    }

    public void setViiiinstitute(String viiiinstitute) {
        this.viiiinstitute = viiiinstitute;
    }

    public String getViiimandal() {
        return viiimandal;
    }

    public void setViiimandal(String viiimandal) {
        this.viiimandal = viiimandal;
    }

    public String getViiidistrict() {
        return viiidistrict;
    }

    public void setViiidistrict(String viiidistrict) {
        this.viiidistrict = viiidistrict;
    }

    public String getIxyear() {
        return ixyear;
    }

    public void setIxyear(String ixyear) {
        this.ixyear = ixyear;
    }

    public String getIxinstitute() {
        return ixinstitute;
    }

    public void setIxinstitute(String ixinstitute) {
        this.ixinstitute = ixinstitute;
    }

    public String getIxmandal() {
        return ixmandal;
    }

    public void setIxmandal(String ixmandal) {
        this.ixmandal = ixmandal;
    }

    public String getIxdistrict() {
        return ixdistrict;
    }

    public void setIxdistrict(String ixdistrict) {
        this.ixdistrict = ixdistrict;
    }

    public String getXyear() {
        return xyear;
    }

    public void setXyear(String xyear) {
        this.xyear = xyear;
    }

    public String getXinstitute() {
        return xinstitute;
    }

    public void setXinstitute(String xinstitute) {
        this.xinstitute = xinstitute;
    }

    public String getXmandal() {
        return xmandal;
    }

    public void setXmandal(String xmandal) {
        this.xmandal = xmandal;
    }

    public String getXdistrict() {
        return xdistrict;
    }

    public void setXdistrict(String xdistrict) {
        this.xdistrict = xdistrict;
    }

    public String getIncome() {
        return income;
    }

    public void setIncome(String income) {
        this.income = income;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public ArrayList getStateList() {
        return stateList;
    }

    public void setStateList(ArrayList stateList) {
        this.stateList = stateList;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }
    
    private String village;
    private FormFile photo;
    private FormFile incomefile;
    private FormFile castefile;
    private FormFile defensefile;
    private FormFile phfile;
    private FormFile nccfile;
    private FormFile sscfile;
    private FormFile transferfile;
    private FormFile rankfile;
    private FormFile ruralfile;
    private FormFile statusfile;
    private FormFile ivfile;
    private FormFile vfile;
    private FormFile vifile;
    private FormFile viifile;
    private FormFile viiifile;
    private FormFile ixfile;
    private FormFile xfile;
    private FormFile dobfile;
    private FormFile signature;

    public String getVillage() {
        return village;
    }

    public void setVillage(String village) {
        this.village = village;
    }

    public FormFile getDobfile() {
        return dobfile;
    }

    public void setDobfile(FormFile dobfile) {
        this.dobfile = dobfile;
    }

    public String getSportsintermnational() {
        return sportsintermnational;
    }

    public void setSportsintermnational(String sportsintermnational) {
        this.sportsintermnational = sportsintermnational;
    }

    public String getSportsinterstate() {
        return sportsinterstate;
    }

    public void setSportsinterstate(String sportsinterstate) {
        this.sportsinterstate = sportsinterstate;
    }

    public String getSportsnational() {
        return sportsnational;
    }

    public void setSportsnational(String sportsnational) {
        this.sportsnational = sportsnational;
    }

    public FormFile getSpotsstatefile() {
        return spotsstatefile;
    }

    public void setSpotsstatefile(FormFile spotsstatefile) {
        this.spotsstatefile = spotsstatefile;
    }

    public FormFile getSportsnationalfile() {
        return sportsnationalfile;
    }

    public void setSportsnationalfile(FormFile sportsnationalfile) {
        this.sportsnationalfile = sportsnationalfile;
    }

    public FormFile getPhoto() {
        return photo;
    }

    public void setPhoto(FormFile photo) {
        this.photo = photo;
    }

    public FormFile getIvfile() {
        return ivfile;
    }

    public void setIvfile(FormFile ivfile) {
        this.ivfile = ivfile;
    }

    public FormFile getVfile() {
        return vfile;
    }

    public void setVfile(FormFile vfile) {
        this.vfile = vfile;
    }

    public FormFile getVifile() {
        return vifile;
    }

    public void setVifile(FormFile vifile) {
        this.vifile = vifile;
    }

    public FormFile getViifile() {
        return viifile;
    }

    public void setViifile(FormFile viifile) {
        this.viifile = viifile;
    }

    public FormFile getViiifile() {
        return viiifile;
    }

    public void setViiifile(FormFile viiifile) {
        this.viiifile = viiifile;
    }

    public FormFile getIxfile() {
        return ixfile;
    }

    public void setIxfile(FormFile ixfile) {
        this.ixfile = ixfile;
    }

    public FormFile getXfile() {
        return xfile;
    }

    public void setXfile(FormFile xfile) {
        this.xfile = xfile;
    }

    public String getBname() {
        return bname;
    }

    public void setBname(String bname) {
        this.bname = bname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMname() {
        return mname;
    }

    public void setMname(String mname) {
        this.mname = mname;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getHallticket() {
        return hallticket;
    }

    public void setHallticket(String hallticket) {
        this.hallticket = hallticket;
    }

    public String getSarea() {
        return sarea;
    }

    public void setSarea(String sarea) {
        this.sarea = sarea;
    }

    public String getCaste() {
        return caste;
    }

    public void setCaste(String caste) {
        this.caste = caste;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getHouseno() {
        return houseno;
    }

    public void setHouseno(String houseno) {
        this.houseno = houseno;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDistrcit() {
        return distrcit;
    }

    public void setDistrcit(String distrcit) {
        this.distrcit = distrcit;
    }

    public String getMandal() {
        return mandal;
    }

    public void setMandal(String mandal) {
        this.mandal = mandal;
    }

    public String getPincode() {
        return pincode;
    }

    public void setPincode(String pincode) {
        this.pincode = pincode;
    }

    public String getDefense() {
        return defense;
    }

    public void setDefense(String defense) {
        this.defense = defense;
    }

    public String getCwsn() {
        return cwsn;
    }

    public void setCwsn(String cwsn) {
        this.cwsn = cwsn;
    }

    public String getNcc() {
        return ncc;
    }

    public void setNcc(String ncc) {
        this.ncc = ncc;
    }

    public String getSports() {
        return sports;
    }

    public void setSports(String sports) {
        this.sports = sports;
    }

    public String getRank() {
        return rank;
    }

    public void setRank(String rank) {
        this.rank = rank;
    }

    public String getPaymentmode() {
        return paymentmode;
    }

    public void setPaymentmode(String paymentmode) {
        this.paymentmode = paymentmode;
    }

    public FormFile getIncomefile() {
        return incomefile;
    }

    public void setIncomefile(FormFile incomefile) {
        this.incomefile = incomefile;
    }

    public FormFile getCastefile() {
        return castefile;
    }

    public void setCastefile(FormFile castefile) {
        this.castefile = castefile;
    }

    public FormFile getDefensefile() {
        return defensefile;
    }

    public void setDefensefile(FormFile defensefile) {
        this.defensefile = defensefile;
    }

    public FormFile getPhfile() {
        return phfile;
    }

    public void setPhfile(FormFile phfile) {
        this.phfile = phfile;
    }

    public FormFile getNccfile() {
        return nccfile;
    }

    public void setNccfile(FormFile nccfile) {
        this.nccfile = nccfile;
    }

    public FormFile getSportsfile() {
        return sportsfile;
    }

    public void setSportsfile(FormFile sportsfile) {
        this.sportsfile = sportsfile;
    }

    public FormFile getSscfile() {
        return sscfile;
    }

    public void setSscfile(FormFile sscfile) {
        this.sscfile = sscfile;
    }

    public FormFile getTransferfile() {
        return transferfile;
    }

    public void setTransferfile(FormFile transferfile) {
        this.transferfile = transferfile;
    }

    public FormFile getRankfile() {
        return rankfile;
    }

    public void setRankfile(FormFile rankfile) {
        this.rankfile = rankfile;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public ArrayList getCourseList() {
        return courseList;
    }

    public void setCourseList(ArrayList courseList) {
        this.courseList = courseList;
    }

    public String getStatusflag() {
        return statusflag;
    }

    public void setStatusflag(String statusflag) {
        this.statusflag = statusflag;
    }

    public String getBlind() {
        return blind;
    }

    public void setBlind(String blind) {
        this.blind = blind;
    }
    
    private FormFile blindfile;

    public FormFile getBlindfile() {
        return blindfile;
    }

    public void setBlindfile(FormFile blindfile) {
        this.blindfile = blindfile;
    }
    
    private String institute;
    private String language;
    private String examination;
    private String grade;
    private String ecenter;
    private String ebatch;
    private String edate;
    private String district;
    private String aadhar;
  
    public String getInstitute() {
        return institute;
    }

    public void setInstitute(String institute) {
        this.institute = institute;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public String getExamination() {
        return examination;
    }

    public void setExamination(String examination) {
        this.examination = examination;
    }

    public String getGrade() {
        return grade;
    }

    public void setGrade(String grade) {
        this.grade = grade;
    }

    public String getEcenter() {
        return ecenter;
    }

    public void setEcenter(String ecenter) {
        this.ecenter = ecenter;
    }

    public String getEbatch() {
        return ebatch;
    }

    public void setEbatch(String ebatch) {
        this.ebatch = ebatch;
    }

    public String getEdate() {
        return edate;
    }

    public void setEdate(String edate) {
        this.edate = edate;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getAadhar() {
        return aadhar;
    }

    public void setAadhar(String aadhar) {
        this.aadhar = aadhar;
    }

    public FormFile getSignature() {
        return signature;
    }

    public void setSignature(FormFile signature) {
        this.signature = signature;
    }

    public String getDistrict1() {
        return district1;
    }

    public void setDistrict1(String district1) {
        this.district1 = district1;
    }
    private FormFile upload1;
    private FormFile upload2;
    private FormFile upload3;
    private FormFile upload4;
    private FormFile upload5;

    public FormFile getUpload1() {
        return upload1;
    }

    public void setUpload1(FormFile upload1) {
        this.upload1 = upload1;
    }

    public FormFile getUpload2() {
        return upload2;
    }

    public void setUpload2(FormFile upload2) {
        this.upload2 = upload2;
    }

    public FormFile getUpload3() {
        return upload3;
    }

    public void setUpload3(FormFile upload3) {
        this.upload3 = upload3;
    }

    public FormFile getUpload4() {
        return upload4;
    }

    public void setUpload4(FormFile upload4) {
        this.upload4 = upload4;
    }

    public String getAadhar1() {
        return aadhar1;
    }

    public void setAadhar1(String aadhar1) {
        this.aadhar1 = aadhar1;
    }

    public String getAadhar2() {
        return aadhar2;
    }

    public void setAadhar2(String aadhar2) {
        this.aadhar2 = aadhar2;
    }

    public String getApdistrict1() {
        return apdistrict1;
    }

    public void setApdistrict1(String apdistrict1) {
        this.apdistrict1 = apdistrict1;
    }

    public String getEcenter1() {
        return ecenter1;
    }

    public void setEcenter1(String ecenter1) {
        this.ecenter1 = ecenter1;
    }

    public String getGrade1() {
        return grade1;
    }

    public void setGrade1(String grade1) {
        this.grade1 = grade1;
    }

    public ArrayList getGradeslist() {
        return gradeslist;
    }

    public void setGradeslist(ArrayList gradeslist) {
        this.gradeslist = gradeslist;
    }

    public String getGrade2() {
        return grade2;
    }

    public void setGrade2(String grade2) {
        this.grade2 = grade2;
    }

    public String getLanguagename() {
        return languagename;
    }

    public void setLanguagename(String languagename) {
        this.languagename = languagename;
    }

    public String getExaminationname() {
        return examinationname;
    }

    public void setExaminationname(String examinationname) {
        this.examinationname = examinationname;
    }

    public String getGradename() {
        return gradename;
    }

    public void setGradename(String gradename) {
        this.gradename = gradename;
    }

    public FormFile getUpload5() {
        return upload5;
    }

    public void setUpload5(FormFile upload5) {
        this.upload5 = upload5;
    }

    public String getStatusresult() {
        return statusresult;
    }

    public void setStatusresult(String statusresult) {
        this.statusresult = statusresult;
    }

    public String getBlindno() {
        return blindno;
    }

    public void setBlindno(String blindno) {
        this.blindno = blindno;
    }

    public String getBlindflag() {
        return blindflag;
    }

    public void setBlindflag(String blindflag) {
        this.blindflag = blindflag;
    }

    public String getVstatus() {
        return vstatus;
    }

    public void setVstatus(String vstatus) {
        this.vstatus = vstatus;
    }

    public String getPhotoc() {
        return photoc;
    }

    public void setPhotoc(String photoc) {
        this.photoc = photoc;
    }

    public String getPhoton() {
        return photon;
    }

    public void setPhoton(String photon) {
        this.photon = photon;
    }

    public String getSignaturec() {
        return signaturec;
    }

    public void setSignaturec(String signaturec) {
        this.signaturec = signaturec;
    }

    public String getSignaturen() {
        return signaturen;
    }

    public void setSignaturen(String signaturen) {
        this.signaturen = signaturen;
    }

    public String getUpload1c() {
        return upload1c;
    }

    public void setUpload1c(String upload1c) {
        this.upload1c = upload1c;
    }

    public String getUpload1n() {
        return upload1n;
    }

    public void setUpload1n(String upload1n) {
        this.upload1n = upload1n;
    }

    public String getUpload2c() {
        return upload2c;
    }

    public void setUpload2c(String upload2c) {
        this.upload2c = upload2c;
    }

    public String getUpload2n() {
        return upload2n;
    }

    public void setUpload2n(String upload2n) {
        this.upload2n = upload2n;
    }

    public String getUpload3c() {
        return upload3c;
    }

    public void setUpload3c(String upload3c) {
        this.upload3c = upload3c;
    }

    public String getUpload3n() {
        return upload3n;
    }

    public void setUpload3n(String upload3n) {
        this.upload3n = upload3n;
    }

    public String getUpload4c() {
        return upload4c;
    }

    public void setUpload4c(String upload4c) {
        this.upload4c = upload4c;
    }

    public String getUpload4n() {
        return upload4n;
    }

    public void setUpload4n(String upload4n) {
        this.upload4n = upload4n;
    }

    public String getUpload5c() {
        return upload5c;
    }

    public void setUpload5c(String upload5c) {
        this.upload5c = upload5c;
    }

    public String getUpload5n() {
        return upload5n;
    }

    public void setUpload5n(String upload5n) {
        this.upload5n = upload5n;
    }

    public String getBlindc() {
        return blindc;
    }

    public void setBlindc(String blindc) {
        this.blindc = blindc;
    }

    public String getBlindn() {
        return blindn;
    }

    public void setBlindn(String blindn) {
        this.blindn = blindn;
    }

    public String getMaskmobile() {
        return maskmobile;
    }

    public void setMaskmobile(String maskmobile) {
        this.maskmobile = maskmobile;
    }

    public String getHstatus() {
        return hstatus;
    }

    public void setHstatus(String hstatus) {
        this.hstatus = hstatus;
    }
    
    private String vcCheckBox;

    public String getVcCheckBox() {
        return vcCheckBox;
    }

    public void setVcCheckBox(String vcCheckBox) {
        this.vcCheckBox = vcCheckBox;
    }

    public String getUpload1New() {
        return upload1New;
    }

    public void setUpload1New(String upload1New) {
        this.upload1New = upload1New;
    }

    public String getUpload2New() {
        return upload2New;
    }

    public void setUpload2New(String upload2New) {
        this.upload2New = upload2New;
    }

    public String getUpload3New() {
        return upload3New;
    }

    public void setUpload3New(String upload3New) {
        this.upload3New = upload3New;
    }

    public String getUpload4New() {
        return upload4New;
    }

    public void setUpload4New(String upload4New) {
        this.upload4New = upload4New;
    }

    public String getUpload5New() {
        return upload5New;
    }

    public void setUpload5New(String upload5New) {
        this.upload5New = upload5New;
    }

    public String getPhotoNew() {
        return photoNew;
    }

    public void setPhotoNew(String photoNew) {
        this.photoNew = photoNew;
    }

    public String getSignatureNew() {
        return signatureNew;
    }

    public void setSignatureNew(String signatureNew) {
        this.signatureNew = signatureNew;
    }

    public String getBlindfileNew() {
        return blindfileNew;
    }

    public void setBlindfileNew(String blindfileNew) {
        this.blindfileNew = blindfileNew;
    }

    public String getUpload1New1() {
        return upload1New1;
    }

    public void setUpload1New1(String upload1New1) {
        this.upload1New1 = upload1New1;
    }

    public String getUpload2New1() {
        return upload2New1;
    }

    public void setUpload2New1(String upload2New1) {
        this.upload2New1 = upload2New1;
    }

    public String getUpload3New1() {
        return upload3New1;
    }

    public void setUpload3New1(String upload3New1) {
        this.upload3New1 = upload3New1;
    }

    public String getUpload4New1() {
        return upload4New1;
    }

    public void setUpload4New1(String upload4New1) {
        this.upload4New1 = upload4New1;
    }

    public String getUpload5New1() {
        return upload5New1;
    }

    public void setUpload5New1(String upload5New1) {
        this.upload5New1 = upload5New1;
    }

    public String getPhotoNew1() {
        return photoNew1;
    }

    public void setPhotoNew1(String photoNew1) {
        this.photoNew1 = photoNew1;
    }

    public String getSignatureNew1() {
        return signatureNew1;
    }

    public void setSignatureNew1(String signatureNew1) {
        this.signatureNew1 = signatureNew1;
    }

    public String getBlindfileNew1() {
        return blindfileNew1;
    }

    public void setBlindfileNew1(String blindfileNew1) {
        this.blindfileNew1 = blindfileNew1;
    }
     private ArrayList institutionList = new ArrayList();
    private String institutionCode;
    private ArrayList subjectList=new ArrayList();
    private String subjectCode;
    private String course1;
    private String category;
    private String state1;
    private ArrayList courseList1=new ArrayList();
    private ArrayList subjectList1=new ArrayList();
    private String subjectCode1;

    public ArrayList getInstitutionList() {
        return institutionList;
    }

    public void setInstitutionList(ArrayList institutionList) {
        this.institutionList = institutionList;
    }

    public String getInstitutionCode() {
        return institutionCode;
    }

    public void setInstitutionCode(String institutionCode) {
        this.institutionCode = institutionCode;
    }

    public ArrayList getSubjectList() {
        return subjectList;
    }

    public void setSubjectList(ArrayList subjectList) {
        this.subjectList = subjectList;
    }

    public String getSubjectCode() {
        return subjectCode;
    }

    public void setSubjectCode(String subjectCode) {
        this.subjectCode = subjectCode;
    }

    public String getCourse1() {
        return course1;
    }

    public void setCourse1(String course1) {
        this.course1 = course1;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getState1() {
        return state1;
    }

    public void setState1(String state1) {
        this.state1 = state1;
    }

    public ArrayList getCourseList1() {
        return courseList1;
    }

    public void setCourseList1(ArrayList courseList1) {
        this.courseList1 = courseList1;
    }

    public ArrayList getSubjectList1() {
        return subjectList1;
    }

    public void setSubjectList1(ArrayList subjectList1) {
        this.subjectList1 = subjectList1;
    }

    public String getSubjectCode1() {
        return subjectCode1;
    }

    public void setSubjectCode1(String subjectCode1) {
        this.subjectCode1 = subjectCode1;
    }

    public String getCoursedesc() {
        return coursedesc;
    }

    public void setCoursedesc(String coursedesc) {
        this.coursedesc = coursedesc;
    }

    public String getInterhallticket() {
        return interhallticket;
    }

    public void setInterhallticket(String interhallticket) {
        this.interhallticket = interhallticket;
    }

    public String getIntermonth() {
        return intermonth;
    }

    public void setIntermonth(String intermonth) {
        this.intermonth = intermonth;
    }

    public String getInteryear() {
        return interyear;
    }

    public void setInteryear(String interyear) {
        this.interyear = interyear;
    }

    public String getInterflag() {
        return interflag;
    }

    public void setInterflag(String interflag) {
        this.interflag = interflag;
    }

    public String getSschallticket() {
        return sschallticket;
    }

    public void setSschallticket(String sschallticket) {
        this.sschallticket = sschallticket;
    }

    public String getSscflag() {
        return sscflag;
    }

    public void setSscflag(String sscflag) {
        this.sscflag = sscflag;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public String getGender1() {
        return gender1;
    }

    public void setGender1(String gender1) {
        this.gender1 = gender1;
    }

    public String getCaste1() {
        return caste1;
    }

    public void setCaste1(String caste1) {
        this.caste1 = caste1;
    }

    public String getLowerreghallticket() {
        return lowerreghallticket;
    }

    public void setLowerreghallticket(String lowerreghallticket) {
        this.lowerreghallticket = lowerreghallticket;
    }

    public String getLowerregflag() {
        return lowerregflag;
    }

    public void setLowerregflag(String lowerregflag) {
        this.lowerregflag = lowerregflag;
    }

    public String getDob1() {
        return dob1;
    }

    public void setDob1(String dob1) {
        this.dob1 = dob1;
    }

    public String getSscyear() {
        return sscyear;
    }

    public void setSscyear(String sscyear) {
        this.sscyear = sscyear;
    }

    public String getInstcode() {
        return instcode;
    }

    public void setInstcode(String instcode) {
        this.instcode = instcode;
    }

    public String getInstname() {
        return instname;
    }

    public void setInstname(String instname) {
        this.instname = instname;
    }

    public String getInstaddress() {
        return instaddress;
    }

    public void setInstaddress(String instaddress) {
        this.instaddress = instaddress;
    }

    public String getInstprincipal() {
        return instprincipal;
    }

    public void setInstprincipal(String instprincipal) {
        this.instprincipal = instprincipal;
    }

    public String getInstmobile() {
        return instmobile;
    }

    public void setInstmobile(String instmobile) {
        this.instmobile = instmobile;
    }

    public String getInsttown() {
        return insttown;
    }

    public void setInsttown(String insttown) {
        this.insttown = insttown;
    }

    public String getInstdist() {
        return instdist;
    }

    public void setInstdist(String instdist) {
        this.instdist = instdist;
    }

    public String getInstpincode() {
        return instpincode;
    }

    public void setInstpincode(String instpincode) {
        this.instpincode = instpincode;
    }

    public String getInstreg() {
        return instreg;
    }

    public void setInstreg(String instreg) {
        this.instreg = instreg;
    }

    public String getHallticketflag() {
        return hallticketflag;
    }

    public void setHallticketflag(String hallticketflag) {
        this.hallticketflag = hallticketflag;
    }

    public String getRegno() {
        return regno;
    }

    public void setRegno(String regno) {
        this.regno = regno;
    }

    public String getGradename1() {
        return gradename1;
    }

    public void setGradename1(String gradename1) {
        this.gradename1 = gradename1;
    }

    public String getAttendance() {
        return attendance;
    }

    public void setAttendance(String attendance) {
        this.attendance = attendance;
    }

    public String getFileFlag() {
        return fileFlag;
    }

    public void setFileFlag(String fileFlag) {
        this.fileFlag = fileFlag;
    }

    public String getMaxMarks() {
        return maxMarks;
    }

    public void setMaxMarks(String maxMarks) {
        this.maxMarks = maxMarks;
    }

    public String getStatuscase() {
        return statuscase;
    }

    public void setStatuscase(String statuscase) {
        this.statuscase = statuscase;
    }

    public String getCoursename() {
        return coursename;
    }

    public void setCoursename(String coursename) {
        this.coursename = coursename;
    }

    public String getBatchCode() {
        return batchCode;
    }

    public void setBatchCode(String batchCode) {
        this.batchCode = batchCode;
    }

    public String getBatchName() {
        return batchName;
    }

    public void setBatchName(String batchName) {
        this.batchName = batchName;
    }

    public ArrayList getBatchlist() {
        return batchlist;
    }

    public void setBatchlist(ArrayList batchlist) {
        this.batchlist = batchlist;
    }

    public ArrayList getCenterList() {
        return centerList;
    }

    public void setCenterList(ArrayList centerList) {
        this.centerList = centerList;
    }

    public String getCenterCode() {
        return centerCode;
    }

    public void setCenterCode(String centerCode) {
        this.centerCode = centerCode;
    }

    public String getEpaper() {
        return epaper;
    }

    public void setEpaper(String epaper) {
        this.epaper = epaper;
    }

    public String getBundle() {
        return bundle;
    }

    public void setBundle(String bundle) {
        this.bundle = bundle;
    }

    public String getBarcode() {
        return barcode;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public ArrayList getCourseWiseList() {
        return courseWiseList;
    }

    public void setCourseWiseList(ArrayList courseWiseList) {
        this.courseWiseList = courseWiseList;
    }

    public String getTobescannedbooklets() {
        return tobescannedbooklets;
    }

    public void setTobescannedbooklets(String tobescannedbooklets) {
        this.tobescannedbooklets = tobescannedbooklets;
    }

    public String getScannedbooklets() {
        return scannedbooklets;
    }

    public void setScannedbooklets(String scannedbooklets) {
        this.scannedbooklets = scannedbooklets;
    }

    public String getTotalbooklets() {
        return totalbooklets;
    }

    public void setTotalbooklets(String totalbooklets) {
        this.totalbooklets = totalbooklets;
    }

    public String getBundlecode() {
        return bundlecode;
    }

    public void setBundlecode(String bundlecode) {
        this.bundlecode = bundlecode;
    }
    
}
