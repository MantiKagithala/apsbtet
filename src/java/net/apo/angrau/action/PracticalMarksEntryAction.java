/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.File;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import sun.misc.BASE64Decoder;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.dao.PracticalMarksEntryDAO;

/**
 *
 * @author 1582792
 */
public class PracticalMarksEntryAction extends DispatchAction {

    private static final String SUCCESS = "success";
    PracticalMarksEntryDAO dao = new PracticalMarksEntryDAO();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RegisterForm myform = (RegisterForm) form;
        String target = "success";
        try {
            HttpSession ses = request.getSession();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                myform.setUserName(username);
//                myform.setCourse("ALL");
//                List list = dao.getColleageDetails(myform, request);

//                if (list != null && !list.isEmpty()) {
//                    request.setAttribute("doculist", list);
//                } else {
//                    request.setAttribute("doculist", "");
//                    request.setAttribute("result", "No Details Found");
//                }
                ArrayList<HashMap> pList = dao.getcourses(username);
                myform.setCourseList(pList);
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("success");
    }

   

 public ActionForward getData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        RegisterForm myform = (RegisterForm) form;
        String target = "success";
        try {
            HttpSession ses = request.getSession();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                myform.setUserName(username);
               String courseId = myform.getCourse();
               //String courseId="others";
              //  System.out.println("cccc" + courseId);
                
                if (courseId.equalsIgnoreCase("OA") || courseId.equalsIgnoreCase("AC") || courseId.equalsIgnoreCase("PC") || courseId.equalsIgnoreCase("WD") || courseId.equalsIgnoreCase("MM")) {
                    List list = dao.getColleageDetails(myform, request);

                    if (list != null && !list.isEmpty()) {
                        request.setAttribute("doculist", list);
                    } else {
                        request.setAttribute("doculist", "");
                        request.setAttribute("result", "No Details Found");
                    }
                    ArrayList<HashMap> pList = dao.getcourses(username);
                    myform.setCourseList(pList);
                    target = "success";
           
                } else if ( courseId.equalsIgnoreCase("CS") || courseId.equalsIgnoreCase("LS") || courseId.equalsIgnoreCase("AY")) {
                    List list = dao.getStudentDetailsIS(myform, request);

                    if (list != null && !list.isEmpty()) {
                        request.setAttribute("doculist", list);
                    } else {
                        request.setAttribute("doculist", "");
                        request.setAttribute("result", "No Details Found");
                    }
                    ArrayList<HashMap> pList = dao.getcourses(username);
                    myform.setCourseList(pList);
                    target = "successIS";
                }  else  if (courseId.equalsIgnoreCase("AO") || courseId.equalsIgnoreCase("FO") || courseId.equalsIgnoreCase("FP") || courseId.equalsIgnoreCase("FB")) {
                    List list = dao.getDetailsFP(myform, request);

                    if (list != null && !list.isEmpty()) {
                        request.setAttribute("doculist", list);
                    } else {
                        request.setAttribute("doculist", "");
                        request.setAttribute("result", "No Details Found");
                    }
                    ArrayList<HashMap> pList = dao.getcourses(username);
                    myform.setCourseList(pList);
                    target = "successothers";
                }else  if (courseId.equalsIgnoreCase("ID") || courseId.equalsIgnoreCase("FG")) {
                    List list = dao.getDetailsID(myform, request);

                    if (list != null && !list.isEmpty()) {
                        request.setAttribute("doculist", list);
                    } else {
                        request.setAttribute("doculist", "");
                        request.setAttribute("result", "No Details Found");
                    }
                    ArrayList<HashMap> pList = dao.getcourses(username);
                    myform.setCourseList(pList);
                    target = "successID";
                }else  if (courseId.equalsIgnoreCase("IS")) {
                    List list = dao.getDetailsIntegritySafety(myform, request);

                    if (list != null && !list.isEmpty()) {
                        request.setAttribute("doculist", list);
                    } else {
                        request.setAttribute("doculist", "");
                        request.setAttribute("result", "No Details Found");
                    }
                    ArrayList<HashMap> pList = dao.getcourses(username);
                    myform.setCourseList(pList);
                    target = "successIntegritySafety";
                }else  if (courseId.equalsIgnoreCase("YO")) {
                    List list = dao.getDetailsYo(myform, request);

                    if (list != null && !list.isEmpty()) {
                        request.setAttribute("doculist", list);
                    } else {
                        request.setAttribute("doculist", "");
                        request.setAttribute("result", "No Details Found");
                    }
                    ArrayList<HashMap> pList = dao.getcourses(username);
                    myform.setCourseList(pList);
                    target = "successYo";
                }
                
                else {
                    target = "unauthorise";
                }
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        //System.out.println("marksEntryTile  " + target);
        return mapping.findForward(target);
    }


   public ActionForward submitIndustryDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "0";
        String target = "success";
        try {
            RegisterForm rForm = (RegisterForm) form;
            HttpSession ses = request.getSession();
            String remoteAddress = request.getRemoteAddr();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                result = dao.submitIndustryDetails(request, username, remoteAddress);
                if ("1".equals(result)) {
                    request.setAttribute("result1", "Data Successfully Saved");
                } else {
                    request.setAttribute("result", "Data Insertion Failed");
                }
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getData(mapping, form, request, response);
        return mapping.findForward("successIS");
    }     
        
    public ActionForward submitOfficeDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "0";
        String target = "success";
        try {
            RegisterForm rForm = (RegisterForm) form;
            HttpSession ses = request.getSession();
            String remoteAddress = request.getRemoteAddr();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                result = dao.submitOfficeDetails(request, username, remoteAddress);
                if ("1".equals(result)) {
                    request.setAttribute("result1", "Data Successfully Saved");
                } else {
                    request.setAttribute("result", "Data Insertion Failed");
                }
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getData(mapping, form, request, response);
        return mapping.findForward(SUCCESS);
    }

     public ActionForward submitOtherDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "0";
        String target = "success";
        try {
            RegisterForm rForm = (RegisterForm) form;
            HttpSession ses = request.getSession();
            String remoteAddress = request.getRemoteAddr();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                result = dao.submitOtherDetails(request, username, remoteAddress);
                if ("1".equals(result)) {
                    request.setAttribute("result1", "Data Successfully Saved");
                } else {
                    request.setAttribute("result", "Data Insertion Failed");
                }
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getData(mapping, form, request, response);
        return mapping.findForward("successothers");
    }
     
     public ActionForward submitIDDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "0";
        String target = "success";
        try {
            RegisterForm rForm = (RegisterForm) form;
            HttpSession ses = request.getSession();
            String remoteAddress = request.getRemoteAddr();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                result = dao.submitIDDetails(request, username, remoteAddress);
                if ("1".equals(result)) {
                    request.setAttribute("result1", "Data Successfully Saved");
                } else {
                    request.setAttribute("result", "Data Insertion Failed");
                }
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getData(mapping, form, request, response);
        return mapping.findForward("successID");
    }
     

   public ActionForward submitIntegritySafetyDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "0";
        String target = "success";
        try {
            RegisterForm rForm = (RegisterForm) form;
            HttpSession ses = request.getSession();
            String remoteAddress = request.getRemoteAddr();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                result = dao.submitIntegritySafetyDetails(request, username, remoteAddress);
                if ("1".equals(result)) {
                    request.setAttribute("result1", "Data Successfully Saved");
                } else {
                    request.setAttribute("result", "Data Insertion Failed");
                }
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getData(mapping, form, request, response);
        return mapping.findForward("successIntegritySafety");
    }
     
     public ActionForward submitYo(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "0";
        String target = "success";
        try {
            RegisterForm rForm = (RegisterForm) form;
            HttpSession ses = request.getSession();
            String remoteAddress = request.getRemoteAddr();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                result = dao.submitYoDetails(request, username, remoteAddress);
                if ("1".equals(result)) {
                    request.setAttribute("result1", "Data Successfully Saved");
                } else {
                    request.setAttribute("result", "Data Insertion Failed");
                }
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        getData(mapping, form, request, response);
        return mapping.findForward("successYo");
    }
     

}
