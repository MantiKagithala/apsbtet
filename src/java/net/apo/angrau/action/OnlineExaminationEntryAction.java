/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.ap.payment.PaymentGatewayIntegration;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.OnlineExaminationEntryDAO;

import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class OnlineExaminationEntryAction extends DispatchAction {

    OnlineExaminationEntryDAO dao = new OnlineExaminationEntryDAO();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {

        return mapping.findForward("success");
    }

    public ActionForward getDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        ArrayList StudentDetails = new ArrayList();
        ArrayList distList = null;
        String staus = dao.getApplicationDetails(rForm, request);
        System.out.println("ssssssssssss"+staus);
        // String staus="1";
        if ("1".equals(staus)) {

//            distList = dao.getDistrictDetails();
//            if (centerList != null && centerList.size() > 0) {
//                rForm.setCenterList(centerList);
//                request.setAttribute("centerList", centerList);
//            }
//             if (distList != null && distList.size() > 0) {
//                rForm.setDistLists(distList);
//                request.setAttribute("distLists", distList);
//            }
            dao.getApplicationStatus(rForm, request);
            StudentDetails = dao.getSubjectDetails(rForm);
            if (StudentDetails != null && StudentDetails.size() > 0) {
                request.setAttribute("listData", StudentDetails);
            } else {
                request.setAttribute("nodata", "No Subject details found");
            }
            request.setAttribute("applicationDetails", "1");

        } else if("2".equals(staus)){
            request.setAttribute("result", "Your are not Eligible due to less Attendance ");
        }else if("3".equals(staus)){
            request.setAttribute("result", "Your are Not Eligible");
        } else if("4".equals(staus)){
            request.setAttribute("result", "Your PIN is not available in Board Approved list. Contact your college for future details");
        }else if("5".equals(staus)){
            request.setAttribute("result", "Your PIN is not available in Board approved list.Contact your college for future details");
        }
        else {
            request.setAttribute("result", "No Applicant details found");
        }
        rForm.setGrade1("");
        rForm.setCenterCode("0");
        rForm.setEcenter("0");
        rForm.setDistrcit1("0");
        rForm.setMobile("");
        rForm.setEmail("");
        
        return mapping.findForward("success");
    }

    /*   public ActionForward submitApplicantDetails(ActionMapping mapping, ActionForm form,
     HttpServletRequest request, HttpServletResponse response)
     throws Exception {

     HttpSession session = request.getSession();
     RegisterForm rForm = (RegisterForm) form;
     ArrayList StudentDetails = new ArrayList();
     String staus = "";
     String count =dao.getApplicationStatus(rForm, request);
     if(count!="" && Integer.parseInt(count)>0){
     staus = dao.submitApplicationDetails(rForm);
     if ("1".equals(staus)) {
     request.setAttribute("succresult", "Successfully Details are submited");
     } else {
     request.setAttribute("result", "Failed to submit Please try Again");
     }
     }else{
         
     }
     return mapping.findForward("success");
     } */
    public ActionForward submitApplicantDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String url = "";
        String link = "";
        double totalFinalAmount = 0.0;
        //  totalFinalAmount = rfo
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        ArrayList StudentDetails = new ArrayList();
        String staus = "";String target="success";
        String remoteAddress = request.getRemoteAddr();
        String count = dao.getApplicationStatus(rForm, request);
       // System.out.println("ssssssssssssssssssss");
        // double totalFinalAmount = 0.0;
        //  totalFinalAmount = Double.parseDouble(rForm.getTotalmarks());
//        if (count != "" && Integer.parseInt(count) == 0) {
        if (count != "" && (Integer.parseInt(count) == 0 || Integer.parseInt(count) == 2)) {
           staus = dao.submitApplicationDetails(rForm,remoteAddress,Integer.parseInt(count));
        //    System.out.println("ppppppppppppppp"+staus);
            if ("1".equals(staus)) {
                request.setAttribute("succresult", "Successfully Details are submited");
                PaymentGatewayIntegration payAPI = new PaymentGatewayIntegration();
              //  System.out.println("amount=====================" + rForm.getStatusresult());
              //  System.out.println("semister=====================" + rForm.getTotalmarks());
                String reqEncData = payAPI.payRequestDiploma(rForm.getAadhar1(), rForm.getStatusresult(), rForm.getPaymentmode(), Double.parseDouble(rForm.getTotalmarks()));
            //    System.out.println("reqEncData======" + reqEncData);
                if (reqEncData.equals("PG_ERROR")) {
             //       System.out.println("payLoad====");
                } else {
                    link = reqEncData;
                }
                response.sendRedirect(link);
                target=null;
            } else {
                request.setAttribute("result", "Failed to submit Please try Again");
            }
        } else {
         //   System.out.println("else loop ");
            PaymentGatewayIntegration payAPI = new PaymentGatewayIntegration();
         //       System.out.println("amount=====================" + rForm.getStatusresult());
         //       System.out.println("semister=====================" + rForm.getTotalmarks());
                String reqEncData = payAPI.payRequestDiploma(rForm.getAadhar1(), rForm.getStatusresult(), rForm.getPaymentmode(), Double.parseDouble(rForm.getTotalmarks()));
              //   System.out.println("reqEncData======" + reqEncData);
                if (reqEncData.equals("PG_ERROR")) {
              //      System.out.println("payLoad====");
                } else {
                    link = reqEncData;
                }
                response.sendRedirect(link);
                target=null; 
        }
         
          return mapping.findForward(target);

    }

    public ActionForward getAmountDetailsAjx(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        ArrayList StudentDetails = new ArrayList();


        StudentDetails = dao.getSubjectDetails(rForm);
        if (StudentDetails != null && StudentDetails.size() > 0) {
            request.setAttribute("listData", StudentDetails);
        } else {
            request.setAttribute("nodata", "No Subject details found");
        }
        request.setAttribute("applicationDetails", "1");

        return mapping.findForward("success");
    }
    
      public ActionForward getCenters(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
          PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
        //    System.out.println("sasas"+district);
            ArrayList centerlist = dao.getCenterDetails(district);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("id") + "'>" + (String) centerMap.get("name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }
      public ActionForward getMobileData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
          PrintWriter out = response.getWriter();
        String namesList = "0";
        
        try { 
            String mobile = request.getParameter("mobile");
            System.out.println("mobile===="+mobile);
           // String mobilests ="1";
           String mobilests = dao.getMobileStatus(mobile);
            if (mobilests != null && Integer.parseInt(mobilests) > 0) {
                namesList="1";
            }else{
                 namesList="0";
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }


}