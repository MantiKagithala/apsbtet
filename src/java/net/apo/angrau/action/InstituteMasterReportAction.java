/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import static com.ap.Action.DownloadHallticketAction.PDF;
import static com.ap.Action.DownloadHallticketAction.getDrivePath;
import com.ap.DAO.HallticketDownloadDao;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import net.apo.angrau.dao.InstituteMasterReportDao;
import net.apo.angrau.dao.RegisterBeforeLoginPrintDao;

/**
 *
 * @author APTOL301294
 */
public class InstituteMasterReportAction extends DispatchAction {

    InstituteMasterReportDao dao = new InstituteMasterReportDao();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<HashMap> list = dao.getDistrictWiseReport();
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("success");
    }

    public ActionForward getDownloadPdf(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        RegisterForm rForm = (RegisterForm) form;
        File directorytemp = null;
        String temporaryfolderPth = "";
        try {
            List<HashMap> list = dao.getDistrictWiseReport();
            if (list.size() > 0) {
                temporaryfolderPth = getDrivePath() + PDF;
                if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                    directorytemp = new File(temporaryfolderPth);
                    if (!directorytemp.exists()) {
                        directorytemp.mkdirs();
                    }
                }
                String filename = "InstituteMaster.pdf";
                dao.getPdfDataNew(temporaryfolderPth, filename, list);
                boolean downloadstatus = false;
                downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
                ArrayList gradelist = new ArrayList();
                gradelist = new HallticketDownloadDao().getGradesList();
                rForm.setGradeslist(gradelist);
                if (downloadstatus == true) {
                    dao.deleteFile(temporaryfolderPth + filename);
                }
                target = "success";
            } else {
                request.setAttribute("result1", "No data found with the given input");
                target = "success";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mapping.findForward(target);
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "E://";
        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;
            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }
}