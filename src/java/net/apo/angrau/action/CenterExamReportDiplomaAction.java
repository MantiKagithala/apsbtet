/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static net.apo.angrau.action.NRSHReportAction.getDrivePath;
import net.apo.angrau.dao.CenterExamReportDiplomaDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
/**
 *
 * @author APTOL301294
 */
public class CenterExamReportDiplomaAction extends DispatchAction {

    CenterExamReportDiplomaDao dao = new CenterExamReportDiplomaDao();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        ArrayList gradeList = dao.getGradeList(userName);
        rForm.setGradeslist(gradeList);
        return mapping.findForward("success");
    }
    
    public ActionForward getBranchList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String userName = session.getAttribute("userName").toString();
            String yearORSem = request.getParameter("courseId");
            ArrayList subjectsList = dao.getBrachList(yearORSem,userName);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("braC") + "'>" + (String) subMap.get("braN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }
    
    public ActionForward getSubject(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
         HttpSession session = request.getSession();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String yearSem = request.getParameter("courseId");
            String branchC = request.getParameter("branchC");
            String userName = session.getAttribute("userName").toString();
            ArrayList subjectsList = dao.getSubjectList(yearSem,branchC,userName);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("subC") + "'>" + (String) subMap.get("subN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }
    
      public ActionForward getExamDates(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String subCode = request.getParameter("subCode");
            String yearSem = request.getParameter("courseId");
            String branchCode = request.getParameter("branchCode");
            String userName = session.getAttribute("userName").toString();
            ArrayList subjectsList = dao.getExamDates(yearSem,subCode,branchCode,userName);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("examC") + "'>" + (String) subMap.get("examN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }
    
    public ActionForward getCentrBasicData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
        rForm.setUserName(userName);
        String yearSem = rForm.getGrade();
        String subjectID = rForm.getSubjectCode();
        String examDate = rForm.getEdate();
        String branchCode = rForm.getBatchCode();
        List<HashMap> list = dao.getCenteBasicData(userName, yearSem, subjectID,examDate,branchCode);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        ArrayList gradeList = dao.getGradeList(userName);
        rForm.setGradeslist(gradeList);
        ArrayList subList = dao.getSubjectList(yearSem,branchCode,userName);
        rForm.setSubjectList(subList);
        rForm.setSubjectCode(subjectID);
        ArrayList examDateList = dao.getExamDates(yearSem,subjectID,branchCode,userName);
        rForm.setStateList(examDateList);
        rForm.setEdate(examDate);
        ArrayList branchList = dao.getBrachList(yearSem,userName);
        rForm.setBatchlist(branchList);
        rForm.setBatchCode(branchCode);
        request.setAttribute("yearSem", yearSem);
        request.setAttribute("subCode", subjectID);
        request.setAttribute("examDate", examDate);
        request.setAttribute("branchCode", branchCode);
        return mapping.findForward("success");
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        String gradeCode = request.getParameter("gcode");
        String subCode = request.getParameter("subCode");
        return mapping.findForward("successExcel");
    }

    public ActionForward getPdfData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filename = null;
        File directorytemp = null;
        String temporaryfolderPth = "";
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        try {
            temporaryfolderPth = getDrivePath() + PDF;
            if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                directorytemp = new File(temporaryfolderPth);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
            }
            
            String yearSem = request.getParameter("yearSem");
            String branchCode = request.getParameter("branchCode");
            String subjectID = request.getParameter("subCode");
            String examDate = request.getParameter("examDate");
            filename = "SBTET_DIPLOMA_ATTENDANCE_SHEET_"+userName+".pdf";
            dao.getPdfDataTW(temporaryfolderPth, filename, userName, yearSem, branchCode,subjectID,examDate);
            boolean downloadstatus = false;
            downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
            if (downloadstatus == true) {
                dao.deleteFile(temporaryfolderPth + filename);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "E://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }
}