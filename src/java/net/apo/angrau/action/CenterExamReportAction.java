/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static net.apo.angrau.action.NRSHReportAction.getDrivePath;
import net.apo.angrau.dao.CenterExamReportDao;
import net.apo.angrau.dao.NRReportDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
/**
 *
 * @author APTOL301294
 */
public class CenterExamReportAction extends DispatchAction {

    CenterExamReportDao dao = new CenterExamReportDao();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        ArrayList gradeList = dao.getGradeList();
        rForm.setGradeslist(gradeList);
        ArrayList batchList = dao.getBatchList();
        rForm.setBatchlist(batchList);
        return mapping.findForward("success");
    }
    
    public ActionForward getCentrBasicData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
        rForm.setUserName(userName);
        String gradeCode = rForm.getGrade();
        String batchCode = rForm.getBatchCode();
        String paper = rForm.getAadhar();//paper
        String examDate = rForm.getEdate();
        List<HashMap> list = dao.getCenteBasicData(userName, gradeCode, batchCode,paper,examDate);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        ArrayList gradeList = dao.getGradeList();
        rForm.setGradeslist(gradeList);
        ArrayList batchList = dao.getBatchList();
        rForm.setBatchlist(batchList);
        request.setAttribute("gradeCode", gradeCode);
        request.setAttribute("batchCode", batchCode);
        request.setAttribute("paper", paper);
        request.setAttribute("examDate", examDate);
        return mapping.findForward("success");
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        String gradeCode = request.getParameter("gcode");
        String batchCode = request.getParameter("bcode");
      
        return mapping.findForward("successExcel");
    }

    public ActionForward getPdfData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filename = null;
        File directorytemp = null;
        String temporaryfolderPth = "";
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        try {
            temporaryfolderPth = getDrivePath() + PDF;
            if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                directorytemp = new File(temporaryfolderPth);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
            }
            filename = "CenterData_"+userName+".pdf";
            String gradeCode = request.getParameter("gradeCode");
            String batchCode = request.getParameter("batchCode");
            String paper = request.getParameter("paper");
            String examDate = request.getParameter("examDate");
            dao.getPdfDataTW(temporaryfolderPth, filename, userName, gradeCode, batchCode,paper,examDate);

            boolean downloadstatus = false;
            downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
            if (downloadstatus == true) {
                dao.deleteFile(temporaryfolderPth + filename);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "E://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }
}