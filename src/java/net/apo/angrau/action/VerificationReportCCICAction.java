/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.ap.WithLoginAction.*;
import com.ap.Action.*;
import com.ap.DAO.CourseStatusReportDao;
import com.ap.Form.RegisterFormPhD;
import com.ap.payment.PaymentGatewayIntegrationPHD;
import net.apo.angrau.db.DatabaseConnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sms.util.SMSSendService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.dao.VerificationReportCCICDao;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import sun.misc.BASE64Decoder;

/**
 *
 * @author APTOL301655
 */
public class VerificationReportCCICAction extends DispatchAction {

    private static final String SUCCESS = "success";
    VerificationReportCCICDao dao = new VerificationReportCCICDao();
    public static final String PDF = "PGCET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<HashMap> list = dao.getCandidateReport("", "");
        if (list != null && !list.isEmpty()) {
            request.setAttribute("candiList", list);
        } else {
            request.setAttribute("candiList1", list);
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("success");
    }

    public ActionForward getExcelData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            List<HashMap> list = dao.getCandidateReport("", "");
            if (list != null && !list.isEmpty()) {
                request.setAttribute("candiList", list);
            } else {
                request.setAttribute("candiList1", list);
                request.setAttribute("result", "No Details Found");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("excelReport");
    }

    public ActionForward getDataPrint(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "";
        RegisterFormPhD rForm = (RegisterFormPhD) form;
        try {
            String type = request.getParameter("type");
            String flag = request.getParameter("flag");
            HashMap<String, String> list = dao.getData(rForm, request);
            request.setAttribute("masterData", list);
            request.setAttribute("type", rForm.getCourse());
            request.setAttribute("flag", rForm.getPgmarksbsc());
            target = "print";
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return mapping.findForward(target);
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "D://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }

    public ActionForward getPdfData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filename = null;
        File directorytemp = null;
        String temporaryfolderPth = "";
        try {
            temporaryfolderPth = getDrivePath() + PDF;
            if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                directorytemp = new File(temporaryfolderPth);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
            }

            filename = "CCICCertificateverfication.pdf";
            dao.getPdfData("", temporaryfolderPth, filename, "0");

            boolean downloadstatus = false;
            downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
            if (downloadstatus == true) {
                dao.deleteFile(temporaryfolderPth + filename);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}