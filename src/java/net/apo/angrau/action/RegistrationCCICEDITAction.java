/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.ap.payment.PaymentGatewayIntegration;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.RegisterCCICDao;
import net.apo.angrau.dao.RegisterCCICDaoEdit;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import net.apo.angrau.forms.RegisterForm;
import org.json.JSONObject;
import org.json.XML;

/**
 *
 * @author APTOL301294
 */
public class RegistrationCCICEDITAction extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String PAYMENT = "payment";
    RegisterCCICDaoEdit rdao = new RegisterCCICDaoEdit();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session = request.getSession();
         String uname = session.getAttribute("userName").toString();
        try {
           List<HashMap> list = rdao.getCandidateList(uname);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } 
        } catch (Exception e) {
            e.printStackTrace();;
        }
        return mapping.findForward("success");
    }
     public ActionForward getDataPrint(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        RegisterForm rForm = (RegisterForm) form;
        try {
                ArrayList stateist = new ArrayList();
            stateist = rdao.getStateDetails();
            rForm.setStateList(stateist);
            HashMap<String, String> list = rdao.getDataEdit(rForm, request);
            if (list.size() > 0) {
                request.setAttribute("masterData", list);
                target = "regularEdit";
            } else {
                request.setAttribute("result","No Data Found For Applicant");
                target = "success";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mapping.findForward(target);
    }
    //If Yes Case

    public ActionForward editPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session = request.getSession();
        String target = "";
        try {
            String loginDistrict = session.getAttribute("distcd").toString();
            String instituteCode = session.getAttribute("instituteCode").toString();
            String centerCode = session.getAttribute("ccode").toString();
            request.setAttribute("ccd", rForm.getCourse1());
            request.setAttribute("ccode", centerCode);
            request.setAttribute("loginDistrict", loginDistrict);
            request.setAttribute("instituteCode", instituteCode);
            request.setAttribute("aadharInput", rForm.getAadhar2());
            String course = rForm.getHallticket().substring(0, 2);
            String aadhar = rForm.getAadhar2();
            rForm.setCourse(course);
            rForm.setAadhar(rForm.getAadhar2());
            ArrayList list = rdao.getMasterData(course, rForm.getHallticket(), instituteCode);
            int status1 = 0;

            String uname = session.getAttribute("userName").toString();
            if (list.size() > 0) {
                String statuslist = rdao.checkCourseInBacklogList(course, aadhar, instituteCode, rForm.getHallticket());
                if (list != null && list.size() > 0) {
                    if (statuslist != null) {
                        status1 = Integer.parseInt(statuslist);
                    }
                }
                if (status1 == 0) {
                    HashMap map = (HashMap) list.get(0);
                    String courseName = rdao.getCourseName(course);
                    request.setAttribute("course", course);
                    request.setAttribute("courseName", courseName);
                    ArrayList stateist = rdao.getStateDetails();
                    rForm.setStateList(stateist);
                    request.setAttribute("name", map.get("name"));
                    request.setAttribute("fname", map.get("fname"));
                    request.setAttribute("mobile", map.get("mobile").toString());
                    request.setAttribute("oldHallticketNo", rForm.getHallticket());
                    ArrayList subjectslist = rdao.getSubjectsByCourses(loginDistrict, instituteCode, course, rForm.getHallticket());
                    request.setAttribute("namesList", subjectslist);
                    target = "getSuccess";
                } else if (status1 == 1) {
                    HashMap<String, String> list1 = rdao.getDataForPaymentDob(rForm, request, uname);
                    request.setAttribute("paymentList", list1);
                    target = "payment";
                } else if (status1 == 2) {
                    ArrayList list2 = rdao.getDataForPaymentSuccess(rForm, uname);
                    request.setAttribute("paymentres", list2);
                    target = "paymentsuccess";
                } else if (status1 == 3) {
//                    ArrayList list2 = rdao.getDataForPaymentSuccess(rForm, uname);
                    request.setAttribute("result", "No Backlog Provision for this course");
                    target = "paymentsuccess";
                } else if (status1 == 4) {
                    request.setAttribute("result", "Already this hallticket registered with another aadhar");
                    rForm.setStatusflag("");
                    target = "success";
                } else if (status1 == 5) {
                    request.setAttribute("result", "Aadhar already mapped with another institute");
                    rForm.setStatusflag("");
                    target = "success";
                }
            } else {
                ArrayList stateist = new ArrayList();
                stateist = rdao.getStateDetails();
                rForm.setStateList(stateist);

                request.setAttribute("result", "No Details Mapping Course With Institution");
                rForm.setStatusflag("");
                target = "success";
            }

        } catch (Exception e) {
            e.printStackTrace();;
        }
        return mapping.findForward(target);
    }

    public ActionForward getSubjectsByCourses(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "";
        try {

            String distCode = request.getParameter("district");
            String instCode = request.getParameter("instCode");
            String oldHallTicketNo = request.getParameter("oldHallTicketNo");
            String backlogCourseId = request.getParameter("backlogCourseId");

            ArrayList centerlist = rdao.getSubjectsByCourses(distCode, instCode, backlogCourseId, oldHallTicketNo);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += (String) centerMap.get("sub_Id") + "~" + (String) centerMap.get("sub_Name") + ",";
                    }
                }
                if (namesList.endsWith(",")) {
                    namesList = namesList.substring(0, namesList.length() - 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }
    //No Case Change Course

    public ActionForward getCheckCourseStatus(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "";
        HttpSession session = request.getSession();
        try {

            String loginDistrict = session.getAttribute("distcd").toString();
            String loginInstituteCode = session.getAttribute("instituteCode").toString();
            String courseId = request.getParameter("courseId");
            String aadhar = request.getParameter("aadhar");

            HashMap<String, String> centerlist = rdao.getCheckCourseStatus(courseId, aadhar, loginInstituteCode);
            if (centerlist != null && centerlist.size() > 0) {
                if (centerlist != null) {
                    namesList = (String) centerlist.get("key") + "~" + (String) centerlist.get("value") + "~" + (String) centerlist.get("fileFlag") + "~" + (String) centerlist.get("fileFlagDesc");
                }
            }


        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getInstituteByDistrict(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String distCode = request.getParameter("district");
            ArrayList centerlist = rdao.getInstituteByDistrict(distCode);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("inst_Id") + "'>" + (String) centerMap.get("inst_Name") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward checkCourseInBacklogList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String namesList = "";
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        try {
//            String distCode = request.getParameter("courseId");
//            String aadhar = request.getParameter("aadhar");
//            String loginInstituteCode = session.getAttribute("instituteCode").toString();
//            ArrayList centerlist = rdao.checkCourseInBacklogList(distCode, aadhar, loginInstituteCode);
//            if (centerlist != null && centerlist.size() > 0) {
//                Iterator centerlistItr = centerlist.iterator();
//                while (centerlistItr != null && centerlistItr.hasNext()) {
//                    Object obj = (Object) centerlistItr.next();
//                    HashMap centerMap = null;
//                    if (obj instanceof HashMap) {
//                        centerMap = (HashMap) obj;
//                    }
//                    if (centerMap != null) {
//                        namesList = (String) centerMap.get("key") + "~" + (String) centerMap.get("value");
//                    }
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getCoursesByInstCode(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String instCode = request.getParameter("instCode");
            String distCode = request.getParameter("district");
            ArrayList centerlist = rdao.getCoursesByInstCode(distCode, instCode);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("course_Id") + "'>" + (String) centerMap.get("course_Name") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getCoursesByInstCodeForBacklog(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String instCode = request.getParameter("instCode");
            String distCode = request.getParameter("district");
            ArrayList centerlist = rdao.getCoursesByInstCodeForBacklog(distCode, instCode);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("course_Id") + "'>" + (String) centerMap.get("course_Name") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getCoursesByInstCodeAndHallticket(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String instCode = request.getParameter("instCode");
            String distCode = request.getParameter("district");
            String hallticket = request.getParameter("hallticket");
            ArrayList centerlist = rdao.getCoursesByInstCodeAndHallticket(distCode, instCode, hallticket);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("course_Id") + "'>" + (String) centerMap.get("course_Name") + "</option>";
                    }
                }
            } else {
                namesList = "0";
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        String hallticket = rForm.getHallticket();
        String dob = rForm.getDob();
        String target = "";
        ArrayList apdist = new ArrayList();
        ArrayList tsdist = new ArrayList();
        int status = 0;
        try {

            ArrayList stateist = new ArrayList();
            stateist = rdao.getStateDetails();
            rForm.setStateList(stateist);

            HttpSession session = request.getSession();
            String uname = session.getAttribute("userName").toString();
            rForm.setAadhar(rForm.getAadhar1());
            rForm.setCourse(rForm.getCourse1());
            status = Integer.parseInt(rForm.getStatusresult());
            if (status == 0) {
                request.setAttribute("result", "No Details Found");
                target = "success";
            }  else if (status == 2) {
                ArrayList list = rdao.getDataForPaymentSuccess(rForm, uname);
                request.setAttribute("paymentres", list);
                target = "paymentsuccess";
            } else {
                request.setAttribute("result", "No Details Found in this Institute");
                ArrayList distlist = new ArrayList();
                distlist = rdao.getDistrictDetails();
                rForm.setDistLists(distlist);
                target = "success";
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return mapping.findForward(target);
    }

    public ActionForward editData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        String hallticket = rForm.getHallticket();
        String dob = rForm.getDob();
        String target = "";
        ArrayList distlist = new ArrayList();
        String status = "0";
        int status1 = 0;
        try {
            status = rdao.getDetails(rForm);
            HttpSession session = request.getSession();
            String uname = session.getAttribute("userName").toString();
            if (status.equalsIgnoreCase("0")) {
                request.setAttribute("result", "No Details Found");
                rForm.setStatusflag("");
                target = "success";
            } else {
                rForm.setAadhar(rForm.getAadhar2());
                String grade = status;
                rForm.setGrade(status);
                String language = "";
                String examination = "";
                String examinationname = "";
                String gradename = "";
                if (grade.toUpperCase().startsWith("T")) {
                    examination = "TW";
                    examinationname = "TypeWriting";
                } else {
                    examination = "SH";
                    examinationname = "ShortHand";
                }
                String letter = Character.toString(grade.toUpperCase().charAt(1));
                if (letter.startsWith("T")) {
                    language = "Telugu";
                } else if (letter.startsWith("E")) {
                    language = "English";
                } else if (letter.startsWith("H")) {
                    language = "Hindi";
                } else if (letter.startsWith("U")) {
                    language = "Urdu";
                }
                rForm.setExamination(examination);
                rForm.setLanguage(language);
                status1 = rdao.getStatus(rForm);
                if (status1 == 0) {
                    HashMap<String, String> list = rdao.getData(rForm, request);
                    if (list != null && list.size() > 0) {
                        request.setAttribute("masterData", list);
                        ArrayList stateist = new ArrayList();
                        stateist = rdao.getStateDetails();
                        rForm.setStateList(stateist);
                        ArrayList centerlist = rdao.getcenterDistDetails(examination);
                        rForm.setDistLists(centerlist);
                        target = "getSuccess";
                    }
                } else if (status1 == 1) {
                    HashMap<String, String> list = rdao.getDataForPaymentDob(rForm, request, uname);
                    request.setAttribute("paymentList", list);
                    target = "payment";
                } else if (status1 == 2) {
                    ArrayList list = rdao.getDataForPaymentSuccess(rForm, uname);
                    request.setAttribute("paymentres", list);
                    target = "paymentsuccess";
                } else if (status1 == 3) {
                    request.setAttribute("result", "Already one Grade Selected in this Language");
                    rForm.setStatusflag("");
                    target = "success";
                } else {
                    request.setAttribute("result", "No Details Found");
                    rForm.setStatusflag("");
                    target = "success";
                }
            }
            ArrayList stateist = new ArrayList();
            stateist = rdao.getStateDetails();
            rForm.setStateList(stateist);

        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return mapping.findForward(target);
    }
    //No case

    public ActionForward submitData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session = request.getSession();
        try {
            String remoteAddress = request.getRemoteAddr();
            String userName = session.getAttribute("userName").toString();
            rForm.setUserName(userName);
            String course = rForm.getCourse();
            String seatsAllowed = rdao.getInstiAllowedSeats(course, userName);
            if (seatsAllowed.toString().equalsIgnoreCase("True")) {
                String result = rdao.insertFiles(rForm, remoteAddress);
                rForm.setStatusflag("");
                if (result.toString().equalsIgnoreCase("1")) {
                    request.setAttribute("result1", "Data Successfully Saved");
                } else if (result.toString().equalsIgnoreCase("2")) {
                    request.setAttribute("result", "Already institutes seats filled for this course");
                } else {
                    request.setAttribute("result", "Data insertion Failed try again");
                    rForm.setStatusflag("");
                    rForm.setSscyear("");
                    rForm.setSschallticket("");
                    rForm.setGender("");
                    rForm.setBname("");
                    rForm.setFname("");
                    rForm.setDob("");
                    rForm.setAttendance("");
                    rForm.setHouseno("");
                    rForm.setLocality("");
                    rForm.setPincode("");
                    rForm.setMobile("");
                }
            } else {
                request.setAttribute("result", "Already institutes seats filled for this course");
            }
            String loginDistrict = session.getAttribute("distcd").toString();
            String instituteCode = session.getAttribute("instituteCode").toString();
            String centercode = session.getAttribute("ccode").toString();
            ArrayList stateist = rdao.getStateDetails();
            rForm.setStateList(stateist);
            ArrayList courseList = rdao.getCoursesByInstCode(loginDistrict, instituteCode);
            rForm.setCourseList1(courseList);
            request.setAttribute("ccode", centercode);
            request.setAttribute("loginDistrict", loginDistrict);
            request.setAttribute("instituteCode", instituteCode);
            rForm.setStatusflag("");
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        unspecified(mapping,form,request,response);
        return mapping.findForward("success");
    }
    //Yes case

    public ActionForward updateData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session = request.getSession();
        try {
            String remoteAddress = request.getRemoteAddr();
            String userName = session.getAttribute("userName").toString();
            rForm.setUserName(userName);
            // String seatsAllowed = rdao.getInstiAllowedSeats(rForm.getCourse(), userName);
            //if (seatsAllowed.toString().equalsIgnoreCase("True")) {
            String result = rdao.updateData(rForm, remoteAddress);
            System.out.println("result " + result);
            if (result.toString().equalsIgnoreCase("1")) {
                request.setAttribute("result1", "Data Successfully Saved");
                request.setAttribute("status", "1");
                HashMap<String, String> list1 = rdao.getDataForPaymentDob(rForm, request, userName);
                request.setAttribute("paymentList", list1);
                return mapping.findForward(PAYMENT);
            } else {
                request.setAttribute("result", "Data insertion Failed try again");
                rForm.setStatusflag("");
                rForm.setSscyear("");
                rForm.setSschallticket("");
                rForm.setGender("");
                rForm.setBname("");
                rForm.setFname("");
                rForm.setDob("");
                rForm.setAttendance("");
                rForm.setHouseno("");
                rForm.setLocality("");
                rForm.setPincode("");
                rForm.setMobile("");
            }
            //} else {
            //  request.setAttribute("result", "Already institutes seats filled for this course");
            //}
            ArrayList stateist = new ArrayList();
            stateist = rdao.getStateDetails();
            rForm.setStateList(stateist);

            String loginDistrict = session.getAttribute("distcd").toString();
            String instituteCode = session.getAttribute("instituteCode").toString();


            ArrayList courseList = rdao.getCoursesByInstCode(loginDistrict, instituteCode);
            rForm.setCourseList1(courseList);


        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return mapping.findForward("success");
    }

    public String payment(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
////
//    public String payment(ActionForm form, String payMode, String createdBy, HttpServletRequest request,){
        System.out.println("Hii Entered into Payment..");
        RegisterForm collageForm = (RegisterForm) form;
        StringBuilder requestSB = new StringBuilder();
        String checkSumValue = "";
        String userName = "";
        String url = "";
        String link = "";
//        HttpSession session = request.getSession();
        ActionForward actionForward = new ActionForward();
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";


        double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
        PaymentGatewayIntegration payAPI = new PaymentGatewayIntegration();
        System.out.println("grade=====================" + collageForm.getCourse());
        String reqEncData = payAPI.payRequestFlowCCIC(collageForm.getAadhar(), collageForm.getCourse(), collageForm.getPaymentmode(), collageForm.getAmount());
        System.out.println("reqEncData======" + reqEncData);
        if (reqEncData.equals("PG_ERROR")) {
            System.out.println("payLoad====");
        } else {
            link = reqEncData;
        }
        response.sendRedirect(link);
        return null;
    }

    public ActionForward getMandals(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
            ArrayList mandalsList = rdao.getMandalDetails(district);
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("mandal_ID") + "'>" + (String) mandalMap.get("mandal_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getCentersDistrict(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String examination = request.getParameter("examination");
            ArrayList centerlist = rdao.getcenterDistDetails(examination);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("district_ID") + "'>" + (String) centerMap.get("district_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getCenters(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
            ArrayList centerlist = rdao.getcenterDetails(district);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("ccode") + "'>" + (String) centerMap.get("cname") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getDistrictList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            ArrayList mandalsList = rdao.getDistrictDetails();
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("district_ID") + "'>" + (String) mandalMap.get("district_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getVillages(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("mandal");
            ArrayList mandalsList = rdao.getMandalDetails(district);
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("vcode") + "'>" + (String) mandalMap.get("vname") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getMandalsAt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        ArrayList mandalsList = new ArrayList();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
            String state = request.getParameter("state");
            if (state.equals("Andra Pradesh")) {
                mandalsList = rdao.getMandalDetails(district);
            } else if (state.equals("Telagana")) {
                mandalsList = rdao.getMandalDetailsAt(district);

            }
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("mandal_ID") + "'>" + (String) mandalMap.get("mandal_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getDistrictListAt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        ArrayList mandalsList = new ArrayList();
        try {
            String state = request.getParameter("state");
            if (state.equals("Andra Pradesh")) {
                mandalsList = rdao.getDistrictDetails();
            } else if (state.equals("Telagana")) {
                mandalsList = rdao.getDistrictDetailsAt();
            }
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("district_ID") + "'>" + (String) mandalMap.get("district_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward validatingaadharNum(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        try {
            String mobile = request.getParameter("mobile");
            String email = request.getParameter("email");
            String aadharNum = request.getParameter("aadharNum");
            //       System.out.println("aadharNum================="+aadharNum);
            int k = rdao.getvalidatingAadharNum(mobile, aadharNum);
            out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward getStatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadharNum");
            String grade = request.getParameter("grade");
            String language = "";
            String examination = "";
            String examinationname = "";
            String gradename = "";
            if (grade.toUpperCase().startsWith("T")) {
                examination = "TW";
                examinationname = "TypeWriting";
            } else {
                examination = "SH";
                examinationname = "ShortHand";
            }
            String letter = Character.toString(grade.toUpperCase().charAt(1));
            if (letter.startsWith("T")) {
                language = "Telugu";
            } else if (letter.startsWith("E")) {
                language = "English";
            } else if (letter.startsWith("H")) {
                language = "Hindi";
            } else if (letter.startsWith("U")) {
                language = "Urdu";
            }
            rForm.setAadhar(aadharNum);
            rForm.setGrade(grade);
            rForm.setExamination(examination);
            rForm.setLanguage(language);
            int k = rdao.getStatus(rForm);
            out.println(k + "_" + aadharNum + "_" + examination + "_" + language + "_" + grade + "_" + examinationname);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward getTimeStatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadharNum");
            String date = request.getParameter("date");
            String batch = request.getParameter("batch");
            String grade = request.getParameter("grade");
            int k = rdao.getTimeStatus(aadharNum, date, batch, grade);
            out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward statusexam(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {

        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadhar");
            String language = request.getParameter("language");
            String examination = request.getParameter("examination");
            int k = rdao.getStatusExam(aadharNum, examination, language);
            out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward sadaremstatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        int k = 0;
        try {
            String blindno = request.getParameter("blindno");
            JSONObject jobj = new JSONObject();
            jobj.put("userName", "SADAREM");
            jobj.put("password", "SADAREM");
            jobj.put("sadaremID", blindno);

            String input = jobj.toString();
            Client client = Client.create();
            WebResource webResource = client
                    .resource("https://sadarem.ap.gov.in/SadaremTESTAPI/rest/generic/getSADAREMValidation");
            ClientResponse resp = webResource.type("application/json").accept("application/json")
                    .post(ClientResponse.class, input);
            if (resp.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            String output = resp.getEntity(String.class);
            JSONObject jobj1 = new JSONObject(output);
            JSONObject jobj2 = new JSONObject(jobj1.getString("RESPONSE"));
            if (jobj2.getString("STATUS_CODE").equals("100") && jobj2.getString("TYPEOFDISABILITY").contains("Visual")) {
                k = 1;
            } else {
                k = 0;
            }
            out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward getStatusQualification(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadharNum");
            String qualification = request.getParameter("qualification");
            String grade = request.getParameter("course1");
            String sschallticket = null;
            String interhallticket = null;
            String intermonth = null;
            String interyear = null;
            HashMap<String, String> sscdets = new HashMap<String, String>();
            int count = 0;
            int k = 0;
            String lowerreghallticket = "";
            int lowercount = 0;
            int ssccount = 0;
            int intercount = 0;
            sschallticket = request.getParameter("sschallticket");
            if (qualification.equalsIgnoreCase("INTER")) {
                interhallticket = request.getParameter("interhallticket");
                intermonth = request.getParameter("intermonth");
                interyear = request.getParameter("interyear");
            }
            if (qualification.equalsIgnoreCase("0") && sschallticket != null) {
                sscdets = rdao.getSSCDetails(sschallticket, aadharNum);
                if (sscdets.size() > 0) {
                    if (sscdets.get("aadharcount").toString().equals("0")) {
                        k = 1;
                        out.println(k + "_" + sscdets.get("bname") + "_" + sscdets.get("fname") + "_" + sscdets.get("gender") + "_" + sscdets.get("caste") + "_" + sscdets.get("dob") + "_" + sscdets.get("Examinationstatus"));
                    } else {
                        k = 2;
                        out.println(k + "_" + lowercount);
                    }
                } else {
                    out.println(k + "_" + lowercount);
                }
            } else if (qualification.equalsIgnoreCase("INTER")) {
                intercount = rdao.getIntercount(aadharNum, interhallticket);
                if (intercount == 0) {
                    Client client = Client.create();
                    WebResource webResource = client
                            .resource("http://resultsws.bie.ap.gov.in/result/bieresults/student?year=" + interyear + "&month=" + intermonth + "&cat=" + "&rollno=" + interhallticket + "&userid=SBTETb!E@(4");
                    ClientResponse response1 = webResource.get(ClientResponse.class);
                    String jsonStr = response1.getEntity(String.class);
                    JSONObject jobj1 = XML.toJSONObject(jsonStr);
                    Object obj = jobj1.get("Student_Result");
                    if (obj instanceof JSONObject) {
                        JSONObject jobj = new JSONObject(jobj1.getString("Student_Result"));
                        if (jobj.has("result") && (jobj.getString("result").equalsIgnoreCase("1") || jobj.getString("result").equalsIgnoreCase("2") || jobj.getString("result").equalsIgnoreCase("3") || jobj.getString("result").equalsIgnoreCase("C"))) {
                            count = 11;
                            String gender = "";
                            String gender1 = "";
                            gender1 = jobj.getString("sex");
                            if (gender1.equalsIgnoreCase("F")) {
                                gender = "FEMALE";
                            } else {
                                gender = "MALE";
                            }
                            out.println(count + "_" + jobj.getString("cname") + "_" + jobj.getString("fname") + "_" + gender + "_" + jobj.getString("caste_desc"));
                        } else if (jobj.has("result")) {
                            count = 10;
                            String gender = "";
                            String gender1 = "";
                            gender1 = jobj.getString("sex");
                            if (gender1.equalsIgnoreCase("F")) {
                                gender = "FEMALE";
                            } else {
                                gender = "MALE";
                            }
                            out.println(count + "_" + jobj.getString("cname") + "_" + jobj.getString("fname") + "_" + gender + "_" + jobj.getString("caste_desc"));
                        }
                    } else {
                        count = 12;
                        out.println(count);
                    }
                } else {
                    count = 13;
                    out.println(count);
                }
            } else {
                out.println(k);
            }
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

//    static {
//        disableSslVerification();
//    }
    private static void disableSslVerification() {
        try {
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[]{new X509TrustManager() {
                    public X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }

                    public void checkClientTrusted(X509Certificate[] certs, String authType) {
                    }

                    public void checkServerTrusted(X509Certificate[] certs, String authType) {
                    }
                }
            };

            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

   }
