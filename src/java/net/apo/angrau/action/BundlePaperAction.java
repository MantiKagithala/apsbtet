/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.BundleDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301655
 */
public class BundlePaperAction extends DispatchAction {

    private static final String SUCCESS = "success";
    BundleDAO dao = new BundleDAO();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String uname = session.getAttribute("userName").toString();
        ArrayList gradelist = new ArrayList();
        gradelist = dao.getGradesList();
        rForm.setGradeslist(gradelist);
        return mapping.findForward("success");
    }

    public ActionForward getStatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        HttpSession session = request.getSession();
        try {
            String barcode = "";
            String grade = "";
            String paper = "";
            String examinationname = "";
            int k = 0;
            barcode = request.getParameter("barcode");
            grade = request.getParameter("grade");
            paper = request.getParameter("ecenter");
            examinationname = request.getParameter("examinationname");
            HashMap<String, String> paperdets = dao.getDetails(grade, paper, barcode,examinationname);
            if (paperdets.size() > 0) {
                out.println(paperdets.get("status") + "_" + paperdets.get("total")+"_"+paperdets.get("regno").toString()+"_"+paperdets.get("bundlecount").toString()+"_"+paperdets.get("scannedmismatchcount").toString());
            } else {
                k = 0;
                out.println(k);
            }
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }
   public ActionForward getBundleStatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        HttpSession session = request.getSession();
        try {
            String bundlecode = "";
            String grade = "";
            String paper = "";
            int k = 0;
            bundlecode = request.getParameter("bundlecode");
            HashMap<String, String> paperdets = dao.getDetailsStatus(bundlecode);
            if (paperdets.size() > 0) {
                out.println(paperdets.get("status"));
            } else {
                k = 0;
                out.println(k);
            }
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public ActionForward submitDetails(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "0";
        String target = "success";
        HashMap<String, String> statusdets = new HashMap<String, String>();
        try {
            RegisterForm rForm = (RegisterForm) form;
            String grade=rForm.getGrade();
            HttpSession ses = request.getSession();
            if (ses.getAttribute("userName") != null) {
                String username = (String) ses.getAttribute("userName");
                statusdets = dao.submitDetails(request, username,rForm);
                rForm.setBarcode("");
                rForm.setBundlecode("");
                if (statusdets.get("status").toString().equalsIgnoreCase("1")) {
                    request.setAttribute("result2", statusdets.get("statusmsg"));
                } else {
                    request.setAttribute("result", statusdets.get("statusmsg"));
                }
                ArrayList gradelist = new ArrayList();
                gradelist = dao.getGradesList();
                rForm.setGradeslist(gradelist);
                rForm.setGrade(grade);
            } else {
                target = "unauthorise";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(SUCCESS);
    }
}
