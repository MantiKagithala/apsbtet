/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.NRReportDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class NRSHReportAction extends DispatchAction {

    NRReportDao dao = new NRReportDao();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
         rForm.setBlind("2");
        ArrayList gradelist = dao.getGradeList(rForm);
        rForm.setGradeslist(gradelist);
         ArrayList centerlist = dao.getCenterList();
        rForm.setDistLists(centerlist);
        ArrayList batchlist = dao.getBatchList();
        rForm.setBatchlist(batchlist);
             if(session.getAttribute("RoleId").toString().equalsIgnoreCase("13")){
            request.setAttribute("statusflag","0");
            rForm.setBlind("0");
            String ecenter=dao.getCentercode(userName);
            rForm.setEcenter1(ecenter);
        }else{
                request.setAttribute("statusflag","1");
                 rForm.setBlind("1");
        }
        return mapping.findForward("success");
    }

    public ActionForward getData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
        rForm.setUserName(userName);
        String gradeCode = rForm.getGrade();
        String batchCode = rForm.getBatchCode();
         String centerCode="";
          if(session.getAttribute("RoleId").toString().equalsIgnoreCase("13")){
         centerCode = rForm.getEcenter1();
          }else{
                 centerCode = rForm.getEcenter();
          }
        List<HashMap> list = dao.getSHReport(centerCode, gradeCode, batchCode);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        request.setAttribute("gcode", gradeCode);
        request.setAttribute("bcode", batchCode);
         request.setAttribute("ccode", centerCode);
         rForm.setBlind("2");
        ArrayList gradelist = dao.getGradeList(rForm);
        rForm.setGradeslist(gradelist);
          ArrayList centerlist = dao.getCenterList();
        rForm.setDistLists(centerlist);
        ArrayList batchlist = dao.getBatchList();
        rForm.setBatchlist(batchlist);
           if(session.getAttribute("RoleId").toString().equalsIgnoreCase("13")){
            request.setAttribute("statusflag","0");
            rForm.setBlind("0");
               String ecenter=dao.getCentercode(userName);
            rForm.setEcenter1(ecenter);
        }else{
                request.setAttribute("statusflag","1");
                 rForm.setBlind("1");
        }
        return mapping.findForward("success");
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        String gradeCode = request.getParameter("gcode");
        String batchCode = request.getParameter("bcode");
        String centerCode = request.getParameter("ccode");
        List<HashMap> list = dao.getSHReport(centerCode, gradeCode, batchCode);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("successExcel");
    }

    public ActionForward getPdfData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filename = null;
        File directorytemp = null;
        String temporaryfolderPth = "";
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        try {
            temporaryfolderPth = getDrivePath() + PDF;
            if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                directorytemp = new File(temporaryfolderPth);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
            }
            String gradeCode = request.getParameter("gcode");
            String batchCode = request.getParameter("bcode");
              String centerCode = request.getParameter("ccode");
            filename = "SHNRReport.pdf";
            dao.getPdfData(temporaryfolderPth, filename, centerCode, gradeCode, batchCode);

            boolean downloadstatus = false;
            downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
            if (downloadstatus == true) {
                dao.deleteFile(temporaryfolderPth + filename);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "E://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }
}