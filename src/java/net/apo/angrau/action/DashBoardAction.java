/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.sms.util.MessagePropertiesUtil;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.dao.AdminEditDao;
import net.apo.angrau.dao.DashBoardDao;
import net.apo.angrau.dao.RegisterDao;
import net.apo.angrau.dao.RegisterationAfterLoginDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class DashBoardAction extends DispatchAction {

    DashBoardDao dao = new DashBoardDao();
    RegisterationAfterLoginDAO rdao = new RegisterationAfterLoginDAO();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<HashMap> list = dao.getCandidateList();
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("success");
    }

    public ActionForward getDataPrint(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        RegisterForm rForm = (RegisterForm) form;
        try {
            String[] str = request.getParameter("testval").split(",");
            String  aa = str[0];
            String  grade = str[1];
            String  mobile = str[2];
            String  reg = str[3];
            rForm.setMobile(mobile);
            rForm.setHallticket(reg);
            rForm.setAadhar(aa);
            String language = "";
            String examination = "";
            if (grade.toUpperCase().startsWith("T")) {
                examination = "TW";
            } else {
                examination = "SH";
            }
            String letter = Character.toString(grade.toUpperCase().charAt(1));
            if (letter.startsWith("T")) {
                language = "Telugu";
            } else if (letter.startsWith("E")) {
                language = "English";
            } else if (letter.startsWith("H")) {
                language = "Hindi";
            } else if (letter.startsWith("U")) {
                language = "Urdu";
            }
            rForm.setExamination(examination);
            rForm.setLanguage(language);
            HashMap<String, String> list = dao.getData(rForm, request);
            if (list.size() > 0) {
                request.setAttribute("masterData", list);
                target = "print";
            } else {
                request.setAttribute("result","No Data Found For Applicant");
                target = "success";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mapping.findForward(target);
    }

   
}