/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.ap.WithLoginAction.*;
import com.ap.Action.*;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.dao.BarcodeReportDao;
import net.apo.angrau.dao.DiplomaBasicSupportDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301295
 */
public class DiplomaBasicSupportAction extends DispatchAction {

    DiplomaBasicSupportDao dao = new DiplomaBasicSupportDao();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        return mapping.findForward("success");
    }
    
    public ActionForward getData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        List<HashMap> list1 = dao.getStudentInfoGet(rForm.getEcenter());
        List<HashMap> list2 = dao.getStudentPersonalAcadamicDetailsGet(rForm.getEcenter());
        List<HashMap> list3 = dao.getDiplomaInternetDataGet(rForm.getEcenter());
        if (list1 != null && !list1.isEmpty()) {
            request.setAttribute("candiList1", list1);
        } else {
            request.setAttribute("list1", "No Details Found");
        }
        if (list2 != null && !list2.isEmpty()) {
            request.setAttribute("candiList2", list2);
        } else {
            request.setAttribute("list2", "No Details Found");
        }
        if (list3 != null && !list3.isEmpty()) {
            request.setAttribute("candiList3", list3);
        } else {
            request.setAttribute("list3", "No Details Found");
        }
        return mapping.findForward("success");
    }
}
