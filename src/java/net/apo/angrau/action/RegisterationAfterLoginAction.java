/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import com.ap.payment.PaymentGatewayIntegration;
import com.sms.util.SMSSendService;
import com.sms.util.SendEmail;
import com.sms.util.SendSMSDTO;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.awt.image.BufferedImage;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.dao.RegisterationAfterLoginDAO;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.json.XML;

/**
 *
 * @author APTOL301655
 */
public class RegisterationAfterLoginAction extends DispatchAction {

    private static final String SUCCESS = "success";
    private static final String PAYMENT = "payment";
    RegisterationAfterLoginDAO rdao = new RegisterationAfterLoginDAO();
    private static final Logger logger = Logger.getLogger(RegisterationAfterLoginAction.class);

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        ArrayList gradelist = new ArrayList();
        gradelist = rdao.getGradesList();
        rForm.setGradeslist(gradelist);
        ArrayList stateist = new ArrayList();
        stateist = rdao.getStateDetails();
        rForm.setStateList(stateist);
//             request.setAttribute("institute",session.getAttribute("colleageName"));
         rForm.setBname("");
              rForm.setFname("");
              rForm.setDob("");
              rForm.setCaste("");
              rForm.setCaste1("");
              rForm.setGender("");
              rForm.setGender1("");
              rForm.setMobile("");
              rForm.setEmail("");
              rForm.setHouseno("");
             rForm.setLocality("");
             rForm.setVillage("");
             rForm.setState("0");
             rForm.setPincode("");
             rForm.setAadhar1("");
             rForm.setAadhar2("");
             rForm.setGrade1("0");
             rForm.setGrade2("0");
             rForm.setHallticket("");
             rForm.setBlindno("");
             rForm.setBlindflag("N");
             rForm.setSschallticket("");
             rForm.setSscflag("N");
             rForm.setInterhallticket("");
             rForm.setIntermonth("0");
             rForm.setInteryear("0");
             rForm.setInterflag("N");
             rForm.setLowerreghallticket("");
             rForm.setLowerregflag("N");
        return mapping.findForward("success");
    }

    public ActionForward getData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        String hallticket = rForm.getHallticket();
        String dob = rForm.getDob();
        String target = "";
        ArrayList apdist = new ArrayList();
        ArrayList tsdist = new ArrayList();
        int status = 0;
        HttpSession session = request.getSession();

        try {
            String uname = session.getAttribute("userName").toString();
            rForm.setAadhar(rForm.getAadhar1());
            rForm.setGrade(rForm.getGrade1());
            status = Integer.parseInt(rForm.getStatusresult());
            if (status == 0) {
                request.setAttribute("result", "No Details Found");
                ArrayList distlist = new ArrayList();
                distlist = rdao.getDistrictDetails();
                rForm.setDistLists(distlist);
                target = "success";
            } else if (status == 1) {
                HashMap<String, String> list = rdao.getDataForPaymentDob(rForm, request, uname);
                if (list != null && list.size() > 0) {
                    request.setAttribute("paymentList", list);
                    request.setAttribute("status", "0");
                    target = "payment";
                } else {
                    ArrayList gradelist = new ArrayList();
                    gradelist = rdao.getGradesList();
                    rForm.setGradeslist(gradelist);
                    ArrayList stateist = new ArrayList();
                    stateist = rdao.getStateDetails();
                    rForm.setStateList(stateist);
                    request.setAttribute("result", "No Details Found in this Institute");
                    rForm.setStatusflag("");
                    target = "success";
                }
            } else if (status == 2) {
                ArrayList list = rdao.getDataForPaymentSuccess(rForm, uname);
                if (list != null && list.size() > 0) {
                    request.setAttribute("paymentres", list);
                    target = "paymentsuccess";
                } else {
                    ArrayList gradelist = new ArrayList();
                    gradelist = rdao.getGradesList();
                    rForm.setGradeslist(gradelist);
                    ArrayList stateist = new ArrayList();
                    stateist = rdao.getStateDetails();
                    rForm.setStateList(stateist);
                    request.setAttribute("result2", "Payment Already Done");
                    rForm.setStatusflag("");
                    target = "success";
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return mapping.findForward(target);
    }

    public ActionForward editData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String hallticket = rForm.getHallticket();
        String dob = rForm.getDob();
        String target = "";
        ArrayList distlist = new ArrayList();
        String status = "0";
        String estatus = "0";
        int hallticketstatus = 0;
        int status1 = 0;
          HashMap<String,String> pevexamdetails=new HashMap<String,String>();
        try {
            String uname = session.getAttribute("userName").toString();
             rForm.setAadhar(rForm.getAadhar2());
                String grade = "";
                String language = "";
                String examination = "";
            pevexamdetails = rdao.getDetails(rForm,request);
            if (pevexamdetails.size()==0) {
                ArrayList gradelist = new ArrayList();
                gradelist = rdao.getGradesList();
                rForm.setGradeslist(gradelist);
                ArrayList stateist = new ArrayList();
                stateist = rdao.getStateDetails();
                rForm.setStateList(stateist);
                request.setAttribute("result", "No Details Found");
                rForm.setStatusflag("");
                target = "success";
            }else  if (pevexamdetails.size()>0) {
                status=pevexamdetails.get("grade");
                estatus=pevexamdetails.get("ExaminationStatus");
                hallticketstatus=Integer.parseInt(pevexamdetails.get("hallticketstatus"));
                if(estatus.equalsIgnoreCase("Pass")){
                      ArrayList gradelist = new ArrayList();
                gradelist = rdao.getGradesList();
                rForm.setGradeslist(gradelist);
                ArrayList stateist = new ArrayList();
                stateist = rdao.getStateDetails();
                rForm.setStateList(stateist);
                request.setAttribute("result2", "Already Passed in this exam");
                rForm.setStatusflag("");
                target = "success";
                }else if(hallticketstatus>0){
                      ArrayList gradelist = new ArrayList();
                gradelist = rdao.getGradesList();
                rForm.setGradeslist(gradelist);
                ArrayList stateist = new ArrayList();
                stateist = rdao.getStateDetails();
                rForm.setStateList(stateist);
                request.setAttribute("result", "RegistrationNo Tagged to Another Aadhar");
                rForm.setStatusflag("");
                target = "success";
                }
                else{
                       examination=pevexamdetails.get("examination");
                       language=pevexamdetails.get("language");
                       status=pevexamdetails.get("grade");
                        rForm.setGrade(status);
                rForm.setExamination(examination);
                rForm.setLanguage(language);
                status1 = rdao.getStatus(rForm);
                if (status1 == 0) {
//                    HashMap<String, String> list = rdao.getData(rForm, request);
//                    if (list != null && list.size() > 0) {
                        request.setAttribute("masterData", pevexamdetails);
//                    distlist = rdao.getDistrictDetails();
//                    rForm.setDistLists(distlist);
                        ArrayList stateist = new ArrayList();
                        stateist = rdao.getStateDetails();
                        rForm.setStateList(stateist);
                        target = "getSuccess";
//                    }
                } else if (status1 == 1) {
                    HashMap<String, String> list = rdao.getDataForPaymentDob(rForm, request, uname);
                    if (list != null && list.size() > 0) {
                        request.setAttribute("paymentList", list);
                          request.setAttribute("status", "0");
                        target = "payment";
                    } else {
                        ArrayList gradelist = new ArrayList();
                        gradelist = rdao.getGradesList();
                        rForm.setGradeslist(gradelist);
                        ArrayList stateist = new ArrayList();
                        stateist = rdao.getStateDetails();
                        rForm.setStateList(stateist);
                        request.setAttribute("result", "No Details Found in this Institute");
                        rForm.setStatusflag("");
                        target = "success";
                    }
                } else if (status1 == 2) {
                    ArrayList list = rdao.getDataForPaymentSuccess(rForm, uname);
                    if (list != null && list.size() > 0) {
                        request.setAttribute("paymentres", list);
                        target = "paymentsuccess";
                    } else {
                        ArrayList gradelist = new ArrayList();
                        gradelist = rdao.getGradesList();
                        rForm.setGradeslist(gradelist);
                        ArrayList stateist = new ArrayList();
                        stateist = rdao.getStateDetails();
                        rForm.setStateList(stateist);
                        request.setAttribute("result2", "Payment Already Done");
                        rForm.setStatusflag("");
                        target = "success";
                    }
                } else if (status1 == 3) {
                    ArrayList gradelist = new ArrayList();
                    gradelist = rdao.getGradesList();
                    rForm.setGradeslist(gradelist);
                    ArrayList stateist = new ArrayList();
                    stateist = rdao.getStateDetails();
                    rForm.setStateList(stateist);
                    request.setAttribute("result", "Already one Grade Selected in this Language");
                    rForm.setStatusflag("");
                    target = "success";
                } else {
                    request.setAttribute("result", "No Details Found");
                    rForm.setStatusflag("");
                    target = "success";
                }
                }
            }
        }
         catch (Exception ex) {
            ex.printStackTrace();

        }
        return mapping.findForward(target);
    }

    public ActionForward submitData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "";
        ArrayList paymentList = new ArrayList();
        String resultStatus = null;
        SendSMSDTO sendSMSDTO = new SendSMSDTO();
        ArrayList list = new ArrayList();
        ArrayList<InternetAddress> ToMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> CCMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> BCCMailsList = new ArrayList<InternetAddress>();
        InternetAddress temailId = null;
        InternetAddress ccmailId = null;
        InternetAddress bccmailId = null;
        HttpSession session = request.getSession();
        Connection con = null;
        String query = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String value = null;
        String course = null;
        String email = null;
        try {
            RegisterForm rForm = (RegisterForm) form;
            String remoteAddress = request.getRemoteAddr();
            String uname = session.getAttribute("userName").toString();
            result = rdao.insertFiles(rForm, remoteAddress, uname);
//              result = "1";
            if (result.equalsIgnoreCase("1")) {
                // For SMS Integration 
                try {
//                    String sms = "Dear Applicant,Your request for seeking Admission into Degree has been registered successfully  Registrar,ANGRAU   ";
//                    resultStatus = SMSSendService.sendSMS(sms, rForm.getMobile());
//                    sendSMSDTO.setMobileNumber(rForm.getMobile());
//                    sendSMSDTO.setSystemIp(request.getRemoteAddr());
//                    sendSMSDTO.setSms(sms);
//                    sendSMSDTO.setLoginId("");
//                    if (resultStatus.equalsIgnoreCase("OK") || resultStatus.equalsIgnoreCase("Message Sent Successfully") || resultStatus.equalsIgnoreCase("SENT")) {
//                        sendSMSDTO.setSendStatus("Sent");
//                    } else {
//                        sendSMSDTO.setSendStatus("Not Send");
//                    }
//                    rdao.insertSmsLogDetails(sendSMSDTO);
                    // Email Configuration 
//                    if (email != null) {
//                        String emails = email;
//                        temailId = new InternetAddress(emails, "");
//                        String bccemails = "rajamallu.salla@aptonline.in";
//                        String ccemailId = "janakiramaiah.peddi@aptonline.in";
//                        bccmailId = new InternetAddress(bccemails, "");
//                        ccmailId = new InternetAddress(ccemailId, "");
//                        ToMailsList.add(temailId);
//                        BCCMailsList.add(bccmailId);
//                        CCMailsList.add(ccmailId);
//                        boolean emailResult = SendEmail.sendEmailANGRANGA(ToMailsList, CCMailsList, BCCMailsList, "PG Admission Registration", "Dear Applicant,<br>Your request for seeking Admission into Degree has been registered successfully  <br>Registrar,<br>ANGRAU<br><br><br>Note: This is auto generated e-mail, please do not reply.");
//                    }
              
 // Email Configuration 
                    if (rForm.getEmail() != null) {
                        String emails = rForm.getEmail();
                        String Examination = rForm.getGradename();
                       // String Examination = null;

                       // System.out.println("hhhhhhhhhhhhhhhhhh");
                        temailId = new InternetAddress(emails, "");
                        String bccemails = "rajamallu.salla@aptonline.in";
                        String ccemailId = "janakiramaiah.peddi@aptonline.in";
                        bccmailId = new InternetAddress(bccemails, "");
                        ccmailId = new InternetAddress(ccemailId, "");
                        ToMailsList.add(temailId);
                        BCCMailsList.add(bccmailId);
                        CCMailsList.add(ccmailId);
                        boolean emailResult = SendEmail.sendEmailANGRANGA(ToMailsList, CCMailsList, BCCMailsList, "SBTET Registration", "Dear Applicant,<br>Your application for enrolling " + Examination + " examination has been registered successfully. Application will be processed further only after making the payment. Please visit www.sbtetap.gov.in  for further and latest updates. <br>Secretary,<br>SBTET AP<br><br><br>Note: This is auto generated e-mail, please do not reply.");
                        
                        try {
                            int i = 0;
                            con = DatabaseConnection.getConnection();
                            query = "insert into  SBTET_Email_Log (EmailID,status,Subject,created_date)  values ('" + emails + "','" + emailResult + "','Registration',getdate())\n";
                            pstmt = con.prepareStatement(query);
                            i = pstmt.executeUpdate();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            if (pstmt != null) {
                                pstmt.close();
                            }
                            if (con != null) {
                                con.close();
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }

                request.setAttribute("result1", "Data Successfully Submitted");
                  request.setAttribute("status", "1");
                HashMap<String, String> list1 = rdao.getDataForPaymentDob(rForm, request, uname);
                request.setAttribute("paymentList", list1);
                return mapping.findForward(PAYMENT);
            } else {
//                          //    request.setAttribute("result", "Data Successfully Saved");

                request.setAttribute("result", "Data Failed to Saved");
                rForm.setBname("");
              rForm.setFname("");
              rForm.setDob("");
              rForm.setCaste("");
              rForm.setCaste1("");
              rForm.setGender("");
              rForm.setGender1("");
              rForm.setMobile("");
              rForm.setEmail("");
              rForm.setHouseno("");
             rForm.setLocality("");
             rForm.setVillage("");
             rForm.setState("0");
             rForm.setPincode("");
             rForm.setAadhar1("");
             rForm.setAadhar2("");
             rForm.setGrade1("0");
             rForm.setGrade2("0");
             rForm.setHallticket("");
             rForm.setBlindno("");
             rForm.setBlindflag("N");
             rForm.setSschallticket("");
             rForm.setSscflag("N");
             rForm.setInterhallticket("");
             rForm.setIntermonth("0");
             rForm.setInteryear("0");
             rForm.setInterflag("N");
                ArrayList gradelist = new ArrayList();
                gradelist = rdao.getGradesList();
                rForm.setGradeslist(gradelist);
                ArrayList stateist = new ArrayList();
                stateist = rdao.getStateDetails();
                rForm.setStateList(stateist);
                return mapping.findForward("success");

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        unspecified(mapping, form, request, response);
        return mapping.findForward(SUCCESS);
    }

    public String payment(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
////
//    public String payment(ActionForm form, String payMode, String createdBy, HttpServletRequest request,){
        System.out.println("Hii Entered into Payment..");
        RegisterForm collageForm = (RegisterForm) form;
        StringBuilder requestSB = new StringBuilder();
        String checkSumValue = "";
        String userName = "";
        String url = "";
        String link = "";
//        HttpSession session = request.getSession();
        ActionForward actionForward = new ActionForward();
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";


        double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
        PaymentGatewayIntegration payAPI = new PaymentGatewayIntegration();
//        System.out.println("grade==============================" + collageForm.getGrade());
        String reqEncData = payAPI.payRequestFlowMpc(collageForm.getAadhar(), collageForm.getGrade(), collageForm.getPaymentmode(), collageForm.getAmount());
        System.out.println("reqEncData======" + reqEncData);
        if (reqEncData.equals("PG_ERROR")) {
            System.out.println("payLoad====");
        } else {
            link = reqEncData;
        }
        response.sendRedirect(link);
        return null;
    }

    public ActionForward getMandals(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
            ArrayList mandalsList = rdao.getMandalDetails(district);
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("mandal_ID") + "'>" + (String) mandalMap.get("mandal_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

  public ActionForward getCentersDistrict(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        HttpSession session=request.getSession();
        String uname=session.getAttribute("userName").toString();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String examination = request.getParameter("examination");
            String grade = request.getParameter("grade");
            ArrayList centerlist = rdao.getcenterDistDetails(examination,grade,uname);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("district_ID") + "'>" + (String) centerMap.get("district_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

        public ActionForward getCenters(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        HttpSession session=request.getSession();
       
        try {
            String uname=session.getAttribute("userName").toString();
            String district = request.getParameter("district");
            String examination = request.getParameter("examination");
            ArrayList centerlist = rdao.getcenterDetails(district, examination,uname);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("ccode") + "'>" + (String) centerMap.get("cname") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getDistrictList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            ArrayList mandalsList = rdao.getDistrictDetails();
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("district_ID") + "'>" + (String) mandalMap.get("district_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getVillages(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("mandal");
            ArrayList mandalsList = rdao.getMandalDetails(district);
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("vcode") + "'>" + (String) mandalMap.get("vname") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getMandalsAt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        ArrayList mandalsList = new ArrayList();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
            String state = request.getParameter("state");
            if (state.equals("Andra Pradesh")) {
                mandalsList = rdao.getMandalDetails(district);
            } else if (state.equals("Telagana")) {
                mandalsList = rdao.getMandalDetailsAt(district);

            }
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("mandal_ID") + "'>" + (String) mandalMap.get("mandal_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getDistrictListAt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        ArrayList mandalsList = new ArrayList();
        try {
            String state = request.getParameter("state");
            if (state.equals("Andra Pradesh")) {
                mandalsList = rdao.getDistrictDetails();
            } else if (state.equals("Telagana")) {
                mandalsList = rdao.getDistrictDetailsAt();
            }
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("district_ID") + "'>" + (String) mandalMap.get("district_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward validatingaadharNum(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        try {
            String mobile = request.getParameter("mobile");
            String email = request.getParameter("email");
            String aadharNum = request.getParameter("aadharNum");
            //       System.out.println("aadharNum================="+aadharNum);
            int k = rdao.getvalidatingAadharNum(mobile, aadharNum);
            out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward getStatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadharNum");
            String grade = request.getParameter("grade");
            int count = 0;
            String language = "";
            String examination = "";
            String examinationname = "";
            String gradename = "";
            if (grade.toUpperCase().startsWith("T")) {
                examination = "TW";
                examinationname = "TypeWriting";
            } else {
                examination = "SH";
                examinationname = "ShortHand";
            }
            String letter = Character.toString(grade.toUpperCase().charAt(1));
            if (letter.startsWith("T")) {
                language = "Telugu";
            } else if (letter.startsWith("E")) {
                language = "English";
            } else if (letter.startsWith("H")) {
                language = "Hindi";
            } else if (letter.startsWith("U")) {
                language = "Urdu";
            }
            rForm.setAadhar(aadharNum);
            rForm.setGrade(grade);
            rForm.setExamination(examination);
            rForm.setLanguage(language);
            int k = rdao.getStatus(rForm);
                out.println(k + "_" + aadharNum + "_" + examination + "_" + language + "_" + grade + "_" + examinationname );
                out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;
    }

    public ActionForward getTimeStatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadharNum");
            String date = request.getParameter("date");
            String batch = request.getParameter("batch");
            String grade = request.getParameter("grade");
            int k = rdao.getTimeStatus(aadharNum, date, batch, grade);
            out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward statusexam(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadhar");
            String language = request.getParameter("language");
            String examination = request.getParameter("examination");
            int k = rdao.getStatusExam(aadharNum, examination, language);
            out.println(k);
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward sadaremstatus(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        int k = 0;
        int count=0;
        try {
            disableSslVerification();
            String blindno = request.getParameter("blindno");
            String aadhar = request.getParameter("aadhar");
            count=rdao.getSadaremcount(aadhar,blindno);
            if(count==0){
            JSONObject jobj = new JSONObject();
            jobj.put("userName", "APSBTSadarem");
            jobj.put("password", "zg3zw/JwQ8r1u5d59kmKGl5s3jas4+yyZFmPaJbha0Nr2e1CsfqzrCLMxt+6ANo1");
            jobj.put("sadaremID", blindno);

            String input = jobj.toString();
            Client client = Client.create();
            WebResource webResource = client
                    .resource("https://sadarem.ap.gov.in/SDRMRSERV/rest/generic/getSADAREMValidation");
            ClientResponse resp = webResource.type("application/json").accept("application/json")
                    .post(ClientResponse.class, input);
            if (resp.getStatus() != 200) {
                throw new RuntimeException("Failed : HTTP error code : "
                        + response.getStatus());
            }

            String output = resp.getEntity(String.class);
            JSONObject jobj1 = new JSONObject(output);
            JSONObject jobj2 = new JSONObject(jobj1.getString("RESPONSE"));
          if(jobj2.getString("STATUS_CODE").equals("100")&&jobj2.getString("TYPEOFDISABILITY").contains("Visual")){
             k = 1;
            } else {
                k = 0;
            }
            }else{
                k=2;
            }
            out.println(k);
            out.flush();
        } catch (Exception e) {
             logger.error(e.getMessage());
//             logger.error(e.printStackTrace());
            e.printStackTrace();
        }
        return null;

    }

    public ActionForward getStatusQualification(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        PrintWriter out = response.getWriter();
        RegisterForm rForm = new RegisterForm();
        try {
            String aadharNum = request.getParameter("aadharNum");
            String qualification = request.getParameter("qualification");
            String grade = request.getParameter("grade");
            String sschallticket = null;
            String interhallticket = null;
            String intermonth = null;
            String interyear = null;
            HashMap<String, String> sscdets = new HashMap<String, String>();
            int count = 0;
            int k = 0;
            String lowerreghallticket="";
            int lowercount=0;
            int ssccount=0;
            int intercount=0;
            sschallticket = request.getParameter("sschallticket");
            if (qualification.equalsIgnoreCase("INTER")) {
                interhallticket = request.getParameter("interhallticket");
                intermonth = request.getParameter("intermonth");
                interyear = request.getParameter("interyear");
            }
            if (qualification.equalsIgnoreCase("0") && sschallticket != null) {
                  if(grade.equalsIgnoreCase("TEH")||grade.equalsIgnoreCase("TTH")||grade.equalsIgnoreCase("THH")){
             lowerreghallticket = request.getParameter("lowerreghallticket");
               lowercount= rdao.getLowerExamDetails(lowerreghallticket,aadharNum);
             
            }
                sscdets = rdao.getSSCDetails(sschallticket,aadharNum);
                if (sscdets.size() > 0) {
                    if(sscdets.get("aadharcount").toString().equals("0")){
                          k = 1;
                    out.println(k + "_"+ sscdets.get("bname") + "_" + sscdets.get("fname") + "_" + sscdets.get("gender") + "_" + sscdets.get("caste") + "_" + sscdets.get("dob")+"_"+lowercount);
                    }else{
                        k=2;
                         out.println(k+"_"+lowercount);
                   }
                    } else {
                    out.println(k+"_"+lowercount);
                }
            } else if (qualification.equalsIgnoreCase("INTER")) {
                 intercount=rdao.getIntercount(aadharNum,interhallticket);
                 if(intercount==0){
                Client client = Client.create();
                WebResource webResource = client
                        .resource("http://resultsws.bie.ap.gov.in/result/bieresults/student?year=" + interyear + "&month=" + intermonth + "&cat=" + "&rollno=" + interhallticket + "&userid=SBTETb!E@(4");
                ClientResponse response1 = webResource.get(ClientResponse.class);
                String jsonStr = response1.getEntity(String.class);
                JSONObject jobj1 = XML.toJSONObject(jsonStr);
                Object obj = jobj1.get("Student_Result");
                if (obj instanceof JSONObject) {
                    JSONObject jobj = new JSONObject(jobj1.getString("Student_Result"));
                    if (jobj.has("result") && (jobj.getString("result").equalsIgnoreCase("1") || jobj.getString("result").equalsIgnoreCase("2") || jobj.getString("result").equalsIgnoreCase("3") || jobj.getString("result").equalsIgnoreCase("C"))) {
                        count = 11;
                        String gender = "";
                        String gender1 = "";
                        gender1 = jobj.getString("sex");
                        if (gender1.equalsIgnoreCase("F")) {
                            gender = "FEMALE";
                        } else {
                            gender = "MALE";
                        }
                        out.println(count + "_" + jobj.getString("cname") + "_" + jobj.getString("fname") + "_" + gender + "_" + jobj.getString("caste_desc"));
                    } else if (jobj.has("result")) {
                        count = 10;
                        String gender = "";
                        String gender1 = "";
                        gender1 = jobj.getString("sex");
                        if (gender1.equalsIgnoreCase("F")) {
                            gender = "FEMALE";
                        } else {
                            gender = "MALE";
                        }
                        out.println(count + "_" + jobj.getString("cname") + "_" + jobj.getString("fname") + "_" + gender + "_" + jobj.getString("caste_desc"));
                    }
                } else {
                    count = 12;
                    out.println(count);
                }
                 }else{
                    count = 13;
                    out.println(count); 
                 }
            } else {
                out.println(k);
            }
            out.flush();
        } catch (Exception e) {
            // logger.error(e.getMessage());
            e.printStackTrace();
        }
        return null;

    }
   private  void disableSslVerification() {
        try{
            // Create a trust manager that does not validate certificate chains
            TrustManager[] trustAllCerts = new TrustManager[] {new X509TrustManager() {
                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
                public void checkClientTrusted(X509Certificate[] certs, String authType) {
                }
                public void checkServerTrusted(X509Certificate[] certs, String authType) {
                }
            }
            };
 
            // Install the all-trusting trust manager
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
 
            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };
 
            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (KeyManagementException e) {
            e.printStackTrace();
        }
    }

}
