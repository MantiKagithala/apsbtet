/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.dao.RegisterBeforeLoginPrintDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class RegisterPrintBeforeLoginAction extends DispatchAction {

    RegisterBeforeLoginPrintDao dao = new RegisterBeforeLoginPrintDao();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        request.setAttribute("applicationPage", "applicationPage");
        RegisterForm rForm = (RegisterForm) form;
        ArrayList gradelist = new ArrayList();
        gradelist = new RegisterBeforeLoginPrintDao().getGradesList();
        rForm.setGradeslist(gradelist);
        return mapping.findForward("success");
    }

    public ActionForward print(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "";
        RegisterForm rForm = (RegisterForm) form;
        try {
            if (rForm.getOtp() != null) {
                String otp = dao.getvalidatingOTP(rForm.getHallticket(), rForm.getMobile());
                if (otp.equals(rForm.getOtp())) {
                    // if (otp.equalsIgnoreCase(rForm.getOtp())) {
                    HashMap<String, String> list = dao.getData(rForm, request);
                    request.setAttribute("masterData", list);
                    target = "print";
                } else {
                    rForm.setDob("");
                    rForm.setHallticket("");
                    request.setAttribute("result", "Invalid OTP Please Try Again");
                    request.setAttribute("applicationPage", "applicationPage");
                    target = "success";
                }
            } else {
                rForm.setDob("");
                rForm.setHallticket("");
                request.setAttribute("result", "OTP Failed To Validate Please Try Again");
                request.setAttribute("applicationPage", "applicationPage");
                target = "success";
            }
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return mapping.findForward(target);
    }

    public ActionForward otpPage(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";

        RegisterForm rForm = (RegisterForm) form;
        try {
            String grade = rForm.getGrade1();
            rForm.setGrade(rForm.getGrade1());
            String language = "";
            String examination = "";
            if (grade.toUpperCase().startsWith("T")) {
                examination = "TW";
            } else {
                examination = "SH";
            }
            String letter = Character.toString(grade.toUpperCase().charAt(1));
            if (letter.startsWith("T")) {
                language = "Telugu";
            } else if (letter.startsWith("E")) {
                language = "English";
            } else if (letter.startsWith("H")) {
                language = "Hindi";
            } else if (letter.startsWith("U")) {
                language = "Urdu";
            }
            rForm.setExamination(examination);
            rForm.setLanguage(language);
            ArrayList listOfMap = dao.getApplicationStatus(rForm);
            if (listOfMap.size() > 0) {
                HashMap map = (HashMap) listOfMap.get(0);
                String recordstatus = map.get("key").toString();
                if (recordstatus.equals("2") || recordstatus.equals("3")) {
                    HashMap<String, String> list = dao.getData(rForm, request);
                    request.setAttribute("masterData", list);
                    target = "print";
                } else {
                    request.setAttribute("result", map.get("keyValue").toString());
                    request.setAttribute("applicationPage", "applicationPage");
                    target = "success";
                }
            } 
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mapping.findForward(target);
    }
}