/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.File;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import static net.apo.angrau.action.AttendanceCTwentyEntryAction.PDF;
import static net.apo.angrau.action.NRSHReportAction.getDrivePath;
import net.apo.angrau.dao.AttendanceCTwentyEntryDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class AttendanceCTwentyEntryAction extends DispatchAction {

    AttendanceCTwentyEntryDao dao = new AttendanceCTwentyEntryDao();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
//        ArrayList gradeList = dao.getGradeList(userName);
//        rForm.setGradeslist(gradeList);

        ArrayList branchList = dao.getBrachList("1YR", userName);
        rForm.setBatchlist(branchList);

        return mapping.findForward("success");
    }

    public ActionForward getBranchList(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String userName = session.getAttribute("userName").toString();
            String yearORSem = request.getParameter("courseId");
            ArrayList subjectsList = dao.getBrachList(yearORSem, userName);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("braC") + "'>" + (String) subMap.get("braN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getSubject(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String yearSem = request.getParameter("courseId");
            String branchC = request.getParameter("branchC");
            String userName = session.getAttribute("userName").toString();
            ArrayList subjectsList = dao.getSubjectList(yearSem, branchC, userName);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("subC") + "'>" + (String) subMap.get("subN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getExamDates(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String yearSem = request.getParameter("courseId");
            String branchCode = request.getParameter("branchCode");
            String userName = session.getAttribute("userName").toString();
            ArrayList subjectsList = dao.getMonths(yearSem, branchCode, userName);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("examC") + "'>" + (String) subMap.get("examN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getPeriod(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        HttpSession session = request.getSession();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String yearSem = request.getParameter("courseId");
            String branchCode = request.getParameter("branchCode");
            String month = request.getParameter("edate");
            String userName = session.getAttribute("userName").toString();
            ArrayList subjectsList = dao.getPeriod(yearSem, branchCode, userName, month);
            if (subjectsList != null && subjectsList.size() > 0) {
                Iterator subListItr = subjectsList.iterator();
                while (subListItr != null && subListItr.hasNext()) {
                    Object obj = (Object) subListItr.next();
                    HashMap subMap = null;
                    if (obj instanceof HashMap) {
                        subMap = (HashMap) obj;
                    }
                    if (subMap != null) {
                        namesList += "<option value='" + (String) subMap.get("priC") + "'>" + (String) subMap.get("priN") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getCentrBasicData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
        rForm.setUserName(userName);
        String yearSem = rForm.getGrade();
        String totalWorkingDays = rForm.getSubjectCode();
        String month = rForm.getEdate();
        String branch = rForm.getBatchCode();
        String period = rForm.getEpaper();
        List<HashMap> list = dao.getCenteBasicData(userName, yearSem, totalWorkingDays, month, branch, period);

        if (list != null && !list.isEmpty()) {

            HashMap map = (HashMap) list.get(0);
            String submitStatus = map.get("submitStatus").toString();
            if (submitStatus.equalsIgnoreCase("0") || submitStatus.equalsIgnoreCase("1")) {
                request.setAttribute("listData", list);
            } else {
                request.setAttribute("listData1", list);
            }
        } else {
            request.setAttribute("result", "No Details Found");
        }

//        ArrayList gradeList = dao.getGradeList(userName);
//        rForm.setGradeslist(gradeList);

        //ArrayList subList = dao.getSubjectList(yearSem,branch,userName);
        //rForm.setSubjectList(subList);
        //rForm.setSubjectCode(section);

        ArrayList branchList = dao.getBrachList(yearSem, userName);
        rForm.setBatchlist(branchList);
        rForm.setBatchCode(branch);

        ArrayList examDateList = dao.getMonths(yearSem, branch, userName);
        rForm.setStateList(examDateList);
        rForm.setEdate(month);

        ArrayList periodList = dao.getPeriod(yearSem, branch, userName, month);
        rForm.setCenterList(periodList);
        rForm.setEpaper(period);


        request.setAttribute("period", period);
        request.setAttribute("yearSem", yearSem);
        request.setAttribute("totalWorkingDays", totalWorkingDays);
        request.setAttribute("month", month);
        request.setAttribute("branchCode", branch);
        return mapping.findForward("success");
    }

    public ActionForward getSubmitData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String userName = session.getAttribute("userName").toString();
        rForm.setUserName(userName);
        rForm.setIncome(request.getRemoteHost());
        String yearSem = rForm.getGrade();
        String month = rForm.getEdate();
        String barnch = rForm.getBatchCode();
        String period = rForm.getEpaper();

        String collegeWorkingDays = rForm.getSubjectCode();

        ArrayList myreturn = dao.getSubmitData(rForm);
        HashMap map1 = (HashMap) myreturn.get(0);


        List<HashMap> list = dao.getCenteBasicData(userName, yearSem, rForm.getSubjectCode(), month, rForm.getBatchCode(), period);

        HashMap map = (HashMap) list.get(0);
        String submitStatus = map.get("submitStatus").toString();
        if (submitStatus.equalsIgnoreCase("0") || submitStatus.equalsIgnoreCase("1")) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("listData1", list);
        }

        String successId = map1.get("successId").toString();
        if (successId.equalsIgnoreCase("1")) {
            request.setAttribute("result", map1.get("successMsg"));
        } else {
            request.setAttribute("result1", map1.get("successMsg"));
        }

//        ArrayList gradeList = dao.getGradeList(userName);
//        rForm.setGradeslist(gradeList);

        ArrayList branchList = dao.getBrachList(yearSem, userName);
        rForm.setBatchlist(branchList);
        rForm.setBatchCode(barnch);

        ArrayList examDateList = dao.getMonths(yearSem, barnch, userName);
        rForm.setStateList(examDateList);
        rForm.setEdate(month);

        ArrayList periodList = dao.getPeriod(yearSem, barnch, userName, month);
        rForm.setCenterList(periodList);
        rForm.setEpaper(period);

        request.setAttribute("period", period);
        request.setAttribute("yearSem", yearSem);
        request.setAttribute("month", month);
        request.setAttribute("branchCode", barnch);
        request.setAttribute("totalWorkingDays", collegeWorkingDays);

        return mapping.findForward("success");
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();

        String period = request.getParameter("period");
        String yearSem = request.getParameter("yearSem");
        String month = request.getParameter("month");
        String branchCode = request.getParameter("branchCode");
        String totalWorkingDays = request.getParameter("totalWorkingDays");

        List<HashMap> list = dao.getCenteBasicData(userName, yearSem, totalWorkingDays, month, branchCode, period);

        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }

        return mapping.findForward("successExcel");
    }

    public ActionForward getPdfData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String filename = null;
        File directorytemp = null;
        String temporaryfolderPth = "";
        HttpSession session = request.getSession();
        String userName = session.getAttribute("userName").toString();
        try {
            temporaryfolderPth = getDrivePath() + PDF;
            if (temporaryfolderPth != null && !"".equals(temporaryfolderPth) && temporaryfolderPth.length() > 0) {
                directorytemp = new File(temporaryfolderPth);
                if (!directorytemp.exists()) {
                    directorytemp.mkdirs();
                }
            }

            String yearSem = request.getParameter("yearSem");
            String branchCode = request.getParameter("branchCode");
            String subjectID = request.getParameter("subCode");
            String examDate = request.getParameter("examDate");
            filename = "SBTET_DIPLOMA_ATTENDANCE_SHEET_" + userName + ".pdf";
            dao.getPdfDataTW(temporaryfolderPth, filename, userName, yearSem, branchCode, subjectID, examDate);
            boolean downloadstatus = false;
            downloadstatus = dao.downLoadFiles(request, response, temporaryfolderPth, filename);
            if (downloadstatus == true) {
                dao.deleteFile(temporaryfolderPth + filename);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getDrivePath() {
        String drivepath = null;
        String dDrive = "C://";
        String eDrive = "E://";

        try {
            File fileDrive = new File(eDrive);
            if (fileDrive.exists()) {
                drivepath = eDrive;

            } else {
                drivepath = dDrive;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return drivepath;
    }
}