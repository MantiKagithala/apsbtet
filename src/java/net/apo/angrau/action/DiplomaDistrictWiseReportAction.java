/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.dao.DiplomaDistrictWiseReportDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class DiplomaDistrictWiseReportAction extends DispatchAction {

    DiplomaDistrictWiseReportDao dao = new DiplomaDistrictWiseReportDao();
    public static final String PDF = "SBTET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<HashMap> list = dao.getDistrictWiseReport("1","0");
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        request.setAttribute("status", "1");
        request.setAttribute("distCode", "0");
        return mapping.findForward("success");
    }

    public ActionForward getData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        List<HashMap> list = dao.getDistrictWiseReport("2",rForm.getDistrict());
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData1", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        request.setAttribute("status", "2");
        request.setAttribute("distCode",rForm.getDistrict());
        return mapping.findForward("success");
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String status = request.getParameter("status");
        String distCode = request.getParameter("distCode");
        List<HashMap> list = dao.getDistrictWiseReport(status,distCode);
        if (list != null && !list.isEmpty()) {
            if(status.equalsIgnoreCase("1")) request.setAttribute("listData", list);
            else request.setAttribute("listData1", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("successExcel");
    }
}