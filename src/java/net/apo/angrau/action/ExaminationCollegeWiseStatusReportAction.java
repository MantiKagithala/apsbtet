/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.ExaminationCollegeWiseStatusReportDao;

import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301461
 */
public class ExaminationCollegeWiseStatusReportAction extends DispatchAction {

    ExaminationCollegeWiseStatusReportDao dao = new ExaminationCollegeWiseStatusReportDao();
   
    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
         String target="success";
        String userName = session.getAttribute("userName").toString();
         if(userName!=null && userName!=""){
        List<HashMap> list = dao.getExaminationCollegeWiseDetails(userName);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        }else{
       target="unauthorise";
       }
        return mapping.findForward(target);
    }

    public ActionForward downloadExcel(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        String target="successExcel";
        String userName = session.getAttribute("userName").toString();
       if(userName!=null && userName!=""){
        List<HashMap> list = dao.getExaminationCollegeWiseDetails(userName);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
       }else{
       target="unauthorise";
       }
        return mapping.findForward(target);
    }
}