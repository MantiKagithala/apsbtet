/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.ap.payment.PaymentGateWayDto;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.text.DecimalFormat;

/**
 *
 * @author 1582792
 */
public class PaymentGatewayIntegrationDegree {
    
//    public static final Double REG_FEE = 2000.00;
//    public static final String REG_FEE_FORMAT = "2,000.00/-";

    public String payRequestFlow(String applicationServiceNumber, String payMode, double amount) {

        PaymentGateWayDto dto = getOnlinePaymentDetails(applicationServiceNumber, payMode, amount);
            String reqNum = "";
            if (!reqNum.equals("NA")) {
                PaymentGatewayRequestHelperDegree helper = new PaymentGatewayRequestHelperDegree(dto);
                String req[] = helper.pgRequest(applicationServiceNumber,payMode,amount);
                
                return PGURL(req[0], req[1]);
            } else {
                return "PG_ERROR";
            }

    }

    public String PGURL(String payLoad, String sum) {
        System.out.println("u r hereeeeeeeeeeeeeeeeeeeeeeeeee");
       // return "https://pgadmissions2020angrau.aptonline.in/PayInteract/redirect?payLoad=" + payLoad + "&sum=" + sum;
        //  return "https://uat-nbpsdm.aponline.gov.in/PayInteract/redirect?payLoad=" + payLoad + "&sum=" + sum;
          //https://ugadmissionsangrau.aptonline.in/
          return "https://ugadmissionsangrau.aptonline.in/PayInteract/redirect?payLoad=" + payLoad + "&sum=" + sum;
    }

    public static void main(String[] args) {

        PaymentGatewayIntegrationDegree pi = new PaymentGatewayIntegrationDegree();
        //pi.payRequestFlow("CSN2020072704", "PTM", "10.0");

    }

   

    public static double paymentCharges(Double amount, double chargePer, double taxPer) {
        try {
            DecimalFormat df = new DecimalFormat("#.##");
            String chrg = "0.00";

            double charge = ((chargePer * amount) / 100);
            double tax = ((taxPer * charge) / 100);
            chrg = df.format(charge + tax);

            return Double.parseDouble(chrg);
        } catch (NumberFormatException e) {
            return 0.0;
        }
    }

    public PaymentGateWayDto getOnlinePaymentDetails(String appNumber, String payMode, double amount) {
        Connection con = null;
        ResultSet rs = null;
        CallableStatement stmt = null;
        PaymentGateWayDto dto = new PaymentGateWayDto();
        try {
                    System.out.println("result  hhhhh " + rs);
                    dto.setRequestID(appNumber);
                    dto.setCustomerName("");
                    dto.setEmailAddress("");
                    dto.setPhoneNo("");
                    dto.setAddress("address");
                    dto.setCity("AP");
                    dto.setState("AndhraPradesh");
                    dto.setPincode("500072");
                    dto.setClientid("100");
                    dto.setDeptID("1001");
                    dto.setServiceID("6512");
                    dto.setChecksum("741852963");
                    dto.setReturnURL("");
                    dto.setFailureURL("");
                    dto.setAdditionlInfo1("NA");
                    dto.setAdditionlInfo2("NA");
                    dto.setAdditionlInfo3("NA");
                    dto.setAdditionlInfo4("NA");
                    dto.setAdditionlInfo5("NA");
                    dto.setBaseAmount(amount);
                    dto.setPayType(payMode);
                    dto.setPaymentMode(payMode);
                    //Payment charge based payment mode
                    dto.setConvienceCharges(0);
                    dto.setTransactionAmount(0);
//                }
//            }
        } catch (Exception e) {
            System.out.println("esssssssssssssssssssssx: " + e);
//            log.error("Ex 163: ", e);
            dto = null;
        } finally {
        }
        return dto;
    }
}


