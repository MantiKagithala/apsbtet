/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.InstituteEditDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301655
 */
public class InstituteEditAction extends DispatchAction {
    InstituteEditDAO dao = new InstituteEditDAO();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        List<HashMap> list = dao.getDistrictWiseReport();
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
        return mapping.findForward("success");
    }  
    
    
     public ActionForward getDataPrint(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        RegisterForm rForm = (RegisterForm) form;
        try {
            HashMap<String, String> list = dao.getData(rForm, request);
            if (list.size() > 0) {
                request.setAttribute("masterData", list);
                target = "getSuccess";
            } else {
                request.setAttribute("result","No Data Found For Applicant");
                target = "success";
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mapping.findForward(target);
    }
     public ActionForward submitData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session=request.getSession();
        try {
            String uname=session.getAttribute("userName").toString();
           String result = dao.submitData(rForm,uname);
            if (result.equalsIgnoreCase("1")) {
                request.setAttribute("result1", "Institute Details updated");
               
            } else {
                request.setAttribute("result","Institute Details updation Failed");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        unspecified(mapping,form,request,response);
        return mapping.findForward(target);
    }

}
