/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;
import com.ap.WithLoginAction.*;
import com.ap.Action.*;
import com.ap.DAO.CourseStatusReportDao;
import com.ap.Form.RegisterFormPhD;
import com.ap.payment.PaymentGatewayIntegrationPHD;
import net.apo.angrau.db.DatabaseConnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sms.util.SMSSendService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.BarcodeCenterReportDao;
import net.apo.angrau.dao.BarcodeReportDao;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import sun.misc.BASE64Decoder;
/**
 *
 * @author APTOL301655
 */
public class barcodeCenterReportAction extends DispatchAction {
     private static final String SUCCESS = "success";
    BarcodeCenterReportDao dao = new BarcodeCenterReportDao();
    public static final String PDF = "PGCET\\";
 public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = request.getSession();
        RegisterForm rForm = (RegisterForm) form;
        String uname = session.getAttribute("userName").toString();
        ArrayList gradelist = new ArrayList();
        gradelist = dao.getGradesList();
        rForm.setGradeslist(gradelist);
        return mapping.findForward("success");
    }
    public ActionForward getDataPrint(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
     RegisterForm rForm = (RegisterForm) form;
        List<HashMap> list = dao.getCandidateReport(rForm);
        String ecenter=rForm.getEcenter();
        if (list != null && !list.isEmpty()) {
            request.setAttribute("candiList", list);
        } else {
            request.setAttribute("candiList1", list);
            request.setAttribute("result", "No Details Found");
        }
          ArrayList gradelist = new ArrayList();
        gradelist = dao.getGradesList();
        rForm.setGradeslist(gradelist);
        rForm.setEcenter(ecenter);
        request.setAttribute("ecenter",ecenter);
        return mapping.findForward("success");
    }

    public ActionForward getExcelData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
          RegisterForm rForm = (RegisterForm) form;
           String ecenter= request.getParameter("ecenter");
        List<HashMap> list = dao.getCandidateReport(rForm);
            if (list != null && !list.isEmpty()) {
                request.setAttribute("candiList", list);
            } else {
                request.setAttribute("candiList1", list);
                request.setAttribute("result", "No Details Found");
            }
            ArrayList gradelist = new ArrayList();
          gradelist = dao.getGradesList();
        rForm.setGradeslist(gradelist);
        rForm.setEcenter(ecenter);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("excelReport");
    }

}
