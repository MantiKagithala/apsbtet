/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;


import com.sms.util.SMSSendService;
import com.sms.util.SendEmail;
import com.sms.util.SendSMSDTO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.dao.ResendAfterLoginDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301655
 */
public class ResendAfterLoginAction extends DispatchAction {
    
     ResendAfterLoginDAO dao = new ResendAfterLoginDAO();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        ArrayList listData = new ArrayList();
        HttpSession session=request.getSession();
        String uname=session.getAttribute("userName").toString();
        List<HashMap> list = dao.getDetails(uname);
        if (list != null && !list.isEmpty()) {
            request.setAttribute("listData", list);
            System.out.println("list=============="+list);
        } else {
            request.setAttribute("result", "No Details Found");
        }
       // request.setAttribute("listData", "listData");
        return mapping.findForward("success");
    }

    public ActionForward getData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        String target = "";
        RegisterForm rForm = (RegisterForm) form;
          HttpSession session=request.getSession();
        try {
            
           String uname=session.getAttribute("userName").toString();
                    HashMap map = dao.getData(rForm, request,uname);
                    if (map != null && map.size() > 0) {
                        ArrayList distList = new ArrayList();
                        ArrayList distListTs = new ArrayList();
                        request.setAttribute("gridata", "gridata");
                        target = "getSuccess";
                    } else {
                        request.setAttribute("result", "No Details Found");
                        target = "success";
                    }
        } catch (Exception ex) {
        }
        return mapping.findForward(target);
    }

    public ActionForward submitData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "";
        ArrayList paymentList = new ArrayList();
        String resultStatus = null;
        SendSMSDTO sendSMSDTO = new SendSMSDTO();
        ArrayList list = new ArrayList();
        ArrayList<InternetAddress> ToMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> CCMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> BCCMailsList = new ArrayList<InternetAddress>();
        InternetAddress temailId = null;
        InternetAddress ccmailId = null;
        InternetAddress bccmailId = null;

        Connection con = null;
        String query = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String value = null;
        String course = null;
        String email = null;
        try {
            RegisterForm rForm = (RegisterForm) form;
            String remoteAddress = request.getRemoteAddr();
            result = dao.insertFiles(rForm, remoteAddress);
            rForm.setAadhar1(rForm.getAadhar());
                rForm.setGrade1(rForm.getGrade());
            if (result.equalsIgnoreCase("1")) {
                request.setAttribute("result2", "Uploaded Successfully");
                // Added on 22-Nov-2020 for SMS and Email Integration
//                    try {
//                        ReuploadDAO smsdaoInsert = new ReuploadDAO();
//                        
//                            String sms = "Dear Applicant,Your application grievance request for admission into BiPC Stream UG Courses has been submitted successfully. Please log on to the UG Admissions portal (www.angrau.ac.in) for further and latest updates. Registrar,ANGRAU   ";
//                            resultStatus = SMSSendService.sendSMS(sms, rForm.getMobile());
//                            sendSMSDTO.setMobileNumber(rForm.getMobile());
//                            sendSMSDTO.setSystemIp(request.getRemoteAddr());
//                            sendSMSDTO.setSms(sms);
//                            sendSMSDTO.setLoginId("");
//                            if (resultStatus.equalsIgnoreCase("OK") || resultStatus.equalsIgnoreCase("Message Sent Successfully") || resultStatus.equalsIgnoreCase("SENT")) {
//                                sendSMSDTO.setSendStatus("Sent");
//                            } else {
//                                sendSMSDTO.setSendStatus("Not Send");
//                            }
//                            smsdaoInsert.insertSmsLogDetails(sendSMSDTO);
//                            //  Email Configuration 
//                             email = rForm.getEmail();
//                            if (email != null) {
//                                String emails = email;
//                                temailId = new InternetAddress(emails, "");
//                                String bccemails = "janakiramaiah.peddi@aptonline.in";
//                        String ccemailId = "ugadmissionsangrau@gmail.com";
//                                bccmailId = new InternetAddress(bccemails, "");
//                        ccmailId = new InternetAddress(ccemailId, "");
//                                ToMailsList.add(temailId);
//                                BCCMailsList.add(bccmailId);
//                        CCMailsList.add(ccmailId);
//                                boolean emailResult = SendEmail.sendEmailANGRANGA(ToMailsList, CCMailsList, BCCMailsList, "UG Admission Grievance Registration", "Dear Applicant,<br>Your application grievance request for admission into BiPC Stream UG Courses has been submitted successfully.\n"
//                                        + "<br>Please log on to the UG Admissions portal (www.angrau.ac.in) for further and latest updates.  <br>Registrar,<br>ANGRAU<br><br><br>Note: This is auto generated e-mail, please do not reply.");
//                            }
//                        
//                    } catch (Exception ex) {
//                        ex.printStackTrace();
//                    }
            } else {
                request.setAttribute("result", "Data Failed to Saved");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward("success");
    }
}
