/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.ap.payment.PaymentWSAccess;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.PaymentGatewayResponseDegreeDAO;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1582792
 */
public class PaymentGatewayResponseDegree extends DispatchAction {

    final static String SUCCESS = "success";

    @Override
    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String res = "";
        String decryptData = "";
        ArrayList list = new ArrayList();
        Map map = new HashMap();
        String reqId = "";
        String appId = "";
        String PgRefNo = "";
        String baseAmt = "";
        String Conv_Charges = "";
        String gst = "";
        String serviceId = "";
        String checksum = "";
        String customerName = "";
        String serviceCharges = "";
        String status = "";
        HttpSession session = request.getSession();
        PaymentGatewayResponseDegreeDAO dao = new PaymentGatewayResponseDegreeDAO();
        ArrayList paymentDoneList = new ArrayList();
        try {
            res = request.getParameter("resp").toString().replace(" ", "+");
//            System.out.println("Res :" + res);
//             PaymentResponseDto dto = PaymentGatewayResponseHelper.pgResponse(res);
//             System.out.println("haiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiiii"+dto);
//            String link ="";
//              if (dto != null) {
//                
//                link = "/register.do";
//                response.sendRedirect(request.getContextPath() + link);
//            }
//        } catch (Exception e) {
//            request.setAttribute("failedResponse", true);
//            return mapping.findForward("redirectToPay");
//        }
//        return null;
//    }
            if (res != null) {
                // dao.encryptedRes(res, userName);
                //decryptData = decrypt(res);

                decryptData = PaymentWSAccess.decrypt(res);

            //     decryptData = "367120201025114638333|EBS25104F9A61CDB|1200|20|1001|6512|1431178474|AAA";//decrypt("");
                String[] arrData = decryptData.split("\\|");
                System.out.println("Decrypted Array Dataa-->" + arrData);
                for (int i = 0; i < arrData.length; i++) {
                    if (i == 0) {
                        map.put("reqId", arrData[i]);
                        reqId = arrData[i];
                        System.out.println("reqId reqId -->" + reqId);
                    } else if (i == 1) {
                        map.put("PgRefNo", arrData[i]);
                        PgRefNo = arrData[i];
                        System.out.println("PgRefNo" + PgRefNo);
                    } else if (i == 2) {
                        map.put("baseAmt", arrData[i]);
                        baseAmt = arrData[i];
                        System.out.println("baseAmt" + baseAmt);
                    } else if (i == 3) {
                        map.put("Conv_Charges", arrData[i]);
                        Conv_Charges = arrData[i];
                    } else if (i == 4) {
                        map.put("gst", arrData[i]); // gst as DeptID
                        gst = arrData[i];
                    } else if (i == 5) {
                        map.put("serviceId", arrData[i]);
                        serviceId = arrData[i];
                    } else if (i == 6) {
                        map.put("checksum", arrData[i]);
                        checksum = arrData[i];
                    } else if (i == 7) {
                        map.put("customerName", arrData[i]);
                        customerName = arrData[i];
                    }
                }
                status = dao.paymentSuccess(reqId, appId, PgRefNo, baseAmt, Conv_Charges, gst, serviceId, checksum, customerName, serviceCharges, appId);
//                list.add(map);
//                request.setAttribute("paymentres", list);

                if (status != null || status.equalsIgnoreCase("Inserted Successfully")) {

                    paymentDoneList = dao.getDataAfterPayment(reqId);
                    request.setAttribute("paymentres", paymentDoneList);
                    //   target="success";

                }

            }

        } catch (Exception e) {
            request.setAttribute("failed", "Your transaction was declained due to Server Failure. Money will be credited your bank account within 7 working days");
            e.printStackTrace();
        }
        return mapping.findForward(SUCCESS);

    }

    
}
