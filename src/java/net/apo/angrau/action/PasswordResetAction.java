/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.ap.WithLoginAction.*;
import com.ap.Action.*;
import com.ap.DAO.CourseStatusReportDao;
import com.ap.WithLoginForm.AvailableSeatsForm;
import com.ap.Form.RegisterFormPhD;
import com.ap.WithLoginForm.LoginForm;
import com.ap.payment.PaymentGatewayIntegrationPHD;
import net.apo.angrau.db.DatabaseConnection;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sms.util.SMSSendService;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.dao.ResetPwdDao;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import sun.misc.BASE64Decoder;

/**
 *
 * @author APTOL301655
 */
public class PasswordResetAction extends DispatchAction {

    ResetPwdDao dao = new ResetPwdDao();
    public static final String PDF = "PGCET\\";

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LoginForm myform = (LoginForm) form;
        try {
            request.setAttribute("form", "form");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("success");
    }

    public ActionForward getOTP(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String maskmobile = "";
        LoginForm myform = (LoginForm) form;
        try {
            String mobile = dao.getMobile(myform.getUserName());
            if (mobile.equalsIgnoreCase("123")) {
                request.setAttribute("result", "You are not valid user for password reset");
                request.setAttribute("form", "form");
            } else if (mobile.equalsIgnoreCase("")) {
                request.setAttribute("result", "Please Enter Valid User");
                request.setAttribute("form", "form");
            } else {
                if (mobile.length() > 4) {
                    maskmobile = mobile.substring(mobile.length() - 4);
                    String otp = "";
                    String code = "";
                    String randomInt = null;
                    Random randomGenerator = new Random();
                    for (int idx = 1; idx <= 5; ++idx) {
                        randomInt = Integer.toHexString(randomGenerator.nextInt(6));
                        code = code + "" + randomInt;
                        otp = otp + "" + randomInt;
                    }

                    if (!otp.equalsIgnoreCase("")) {
                        String status = SMSSendService.sendSMS("Your OTP for Reset Password : " + otp, mobile);
                        //String status = "SENT";
                        if (status.equalsIgnoreCase("SENT")) {
                            request.setAttribute("otp", otp);
                            int succes = dao.SaveOTP(myform.getUserName(), mobile, otp, request.getRemoteHost());
                            if (succes == 1) {
                                request.setAttribute("mobile", mobile);
                                request.setAttribute("userName", myform.getUserName());
                                request.setAttribute("maskmobile", "XXXXXX" + maskmobile);
                                request.setAttribute("smsForm", "smsForm");
                            } else {
                                request.setAttribute("result", "OTP Failed To Save Please Try Again");
                                request.setAttribute("form", "form");
                            }
                        } else {
                            request.setAttribute("result", "OTP Failed To Send Please Try Again");
                            request.setAttribute("form", "form");
                        }
                    } else {
                        request.setAttribute("result", "OTP Generation Failed Tray Again");
                        request.setAttribute("form", "form");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return mapping.findForward("success");
    }

    public ActionForward updatePassword(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        LoginForm rForm = (LoginForm) form;
        try {

            if (rForm.getPasswordNew() != null) { //otp
                String otp = dao.getvalidatingOTP(rForm.getUserName(), rForm.getStatus());
                if (otp.equalsIgnoreCase(rForm.getPasswordNew())) {
                    int flag = dao.updatePassword(rForm);
                    if (flag == 1) {
                        rForm.setUserName("");
                        request.setAttribute("result1", "Your Password Reseted Successfully as demo");
                        request.setAttribute("form", "form");
                    }
                } else {
                    rForm.setUserName("");
                    request.setAttribute("result", "Invalid OTP Please Try Again");
                    request.setAttribute("form", "form");
                }
            } else {
                rForm.setUserName("");
                request.setAttribute("result", "OTP Failed To Validate Please Try Again");
                request.setAttribute("form", "form");
            }
        } catch (Exception e) {
            e.printStackTrace();;
        }
        return mapping.findForward("success");
    }

}
