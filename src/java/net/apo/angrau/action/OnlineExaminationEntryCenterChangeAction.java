/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.apo.angrau.dao.OnlineExaminationCenterChangeDAO;

import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301294
 */
public class OnlineExaminationEntryCenterChangeAction extends DispatchAction {

    OnlineExaminationCenterChangeDAO dao = new OnlineExaminationCenterChangeDAO();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        return mapping.findForward("success");
    }

    public ActionForward getData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        ArrayList list = dao.getMasterData(rForm.getAadhar1(), request);
        if(list.size()==0){
             request.setAttribute("result1", "No Details Found With Given PIN Number");
        }else{
            request.setAttribute("showDiv", "showDiv");
        }
        ArrayList distList = dao.getDistrictDetails();
        if (distList != null && distList.size() > 0) {
            rForm.setDistLists(distList);
            request.setAttribute("distLists", distList);
        }
        rForm.setEcenter("0");
        return mapping.findForward("success");
    }

    public ActionForward updateData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        dao.getMasterData(rForm.getAadhar1(), request);
        String remoteAddress = request.getRemoteAddr();
        rForm.setBlind(remoteAddress);
        String updateStatus = dao.getUpdateData(rForm);
        if (updateStatus.equalsIgnoreCase("1")) {
            rForm.setAadhar1("");
            request.setAttribute("result", "Updated Successfully");
        }else{
            request.setAttribute("result1", "Failed to update try again");
        }
        ArrayList distList = dao.getDistrictDetails();
        if (distList != null && distList.size() > 0) {
            rForm.setDistLists(distList);
             rForm.setDistrcit1("0");
            request.setAttribute("distLists", distList);
        }
        return mapping.findForward("success");
    }
    
    public ActionForward getCenters(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
            ArrayList centerlist = dao.getCenterDetails(district);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("id") + "'>" + (String) centerMap.get("name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }
}