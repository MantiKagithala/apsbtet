/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.UUID;
import net.apo.angrau.db.DatabaseConnection;

/**
 *
 * @author janakiram
 */
public class UUIDGeneration {

    public ArrayList getRegistrationNumbers() {
        ArrayList schmgtcatList = null;
        PreparedStatement st = null;
        Connection con = null;
        String query = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            query = "select top(20) RegNo from TBL_TWSH_Registration";
            schmgtcatList = new ArrayList<String>();
            st = con.prepareStatement(query);
            rs = st.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    schmgtcatList.add(rs.getString(1));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }
                if (rs != null) {
                    rs.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        // APTO.setSchoolmgtList(schmgtcatList);

        return schmgtcatList;
    }

    public ArrayList insertRegistrationNumbers() {
        ArrayList schmgtcatList = null;
        PreparedStatement st = null;
        PreparedStatement stmt = null;
        Connection con = null;
        String query = null;
        String query1 = null;
        ResultSet rs = null;
        try {
            con = DatabaseConnection.getConnection();
            //FetchdataFormDB db= new FetchdataFormDB();
            query1 = "select top(20) RegNo from TBL_TWSH_Registration (nolock) where  regno in (\n"
                    + "'TEL21050103002',\n"
                    + "'TEL21050103001',\n"
                    + "'TEJ21050101001',\n"
                    + "'TTL21050101001',\n"
                    + "'TEH21050101001',\n"
                    + "'TEL21050117001') ";
            schmgtcatList = new ArrayList<String>();
            stmt = con.prepareStatement(query1);
            rs = stmt.executeQuery();
            if (rs != null) {
                while (rs.next()) {
                    schmgtcatList.add(rs.getString(1));
                }
            }

            System.out.println("list===" + schmgtcatList);

            query = "insert into TWSH_PrinterData  values (?,?,?,?,?,?,?)";
            st = con.prepareStatement(query);
            for (int i = 0; i < schmgtcatList.size(); i++) {
                st.setString(1, schmgtcatList.get(i).toString());
                st.setString(2, UUID.randomUUID().toString().replace("-", ""));
                st.setString(3, "" + UUID.randomUUID().toString().replace("-", ""));
                st.setString(4, "" + UUID.randomUUID().toString().replace("-", ""));
                st.setString(5, "" + UUID.randomUUID().toString().replace("-", ""));
                st.setString(6, "2021-06-19");
                st.setString(7, "2");

                st.executeUpdate();
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (st != null) {
                    st.close();
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        return schmgtcatList;
    }

    public static void main(String[] args) {
        UUIDGeneration gen = new UUIDGeneration();
        gen.insertRegistrationNumbers();
    }
}
