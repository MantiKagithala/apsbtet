/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;
import net.apo.angrau.dao.ReuploadDAO;
import com.sms.util.SMSSendService;
import com.sms.util.SendEmail;
import com.sms.util.SendSMSDTO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import javax.imageio.ImageIO;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.DatatypeConverter;
import net.apo.angrau.dao.ReuploadCCICDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author APTOL301655
 */
public class ReuploadActionCCIC extends DispatchAction{
     private ReuploadCCICDAO dao = new ReuploadCCICDAO();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        request.setAttribute("form", "form");
        return mapping.findForward("success");
    }

    public ActionForward getOtp(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String maskmobile = "";
        RegisterForm rForm = (RegisterForm) form;
         HashMap<String,String> list=new HashMap<String,String> ();
         list= dao.getStatus(rForm);
         String recordstatus=list.get("result");
        rForm.setAadhar(list.get("aadhar"));
        rForm.setCourse(list.get("course"));
        if (recordstatus.equals("2")) {
            String mobile = dao.getvalidatingHallTicketNum(rForm.getAadhar(), rForm.getCourse());
            if (mobile.equalsIgnoreCase("")) {
                request.setAttribute("result", "Please Enter Valid Data");
                request.setAttribute("form", "form");
            } else {
                if (mobile.length() > 4) {
                    maskmobile = mobile.substring(mobile.length() - 4);
                    String otp = "";
                    String code = "";
                    String randomInt = null;
                    Random randomGenerator = new Random();
                    for (int idx = 1; idx <= 5; ++idx) {
                        randomInt = Integer.toHexString(randomGenerator.nextInt(6));
                        code = code + "" + randomInt;
                        otp = otp + "" + randomInt;
                    }
                    if (!otp.equalsIgnoreCase("")) {
                        String status = SMSSendService.sendSMS("Dear Applicant, Your OTP: "+otp+" - SBTETAP", mobile);
//                        String status = "SENT";
                        if (status.equalsIgnoreCase("SENT")) {
                            request.setAttribute("otp", otp);
//                            int succes = dao.SaveOTP(rForm.getAadhar(), mobile, "00000", request.getRemoteHost());
                               int succes = dao.SaveOTP(rForm.getAadhar(), mobile, otp, request.getRemoteHost());
                            if (succes == 1) {
                                request.setAttribute("aadhar", rForm.getAadhar());
                                request.setAttribute("course", rForm.getCourse());
                                request.setAttribute("mobile", mobile);
                                request.setAttribute("maskmobile", "XXXXXX" + maskmobile);
                                request.setAttribute("smsForm", "smsForm");
                            } else {
                                request.setAttribute("result", "OTP Failed To Save Please Try Again");
                                rForm.setRegno("");
                                rForm.setDob("");
                              
                                request.setAttribute("form", "form");
                            }
                        } else {
                            request.setAttribute("result", "OTP Failed To Send Please Try Again");
                            rForm.setRegno("");
                            rForm.setDob("");
                           
                            request.setAttribute("form", "form");
                        }
                    } else {
                        request.setAttribute("result", "OTP Generation Failed Try Again");
                        rForm.setRegno("");
                        rForm.setDob("");
                        
                        request.setAttribute("form", "form");
                    }
                }
            }
        } else if (recordstatus.equals("1")) {
            request.setAttribute("result", "Pending at Verification Officer");
            rForm.setRegno("");
            rForm.setDob("");
            request.setAttribute("form", "form");
        } else if (recordstatus.equals("3")) {
            request.setAttribute("result2", "Application Approved");
            rForm.setRegno("");
            rForm.setDob("");
            request.setAttribute("form", "form");
        } else if (recordstatus.equals("4")) {
              rForm.setRegno("");
            rForm.setDob("");
            request.setAttribute("result", "Application Rejected ");
            request.setAttribute("form", "form");
        } else if (recordstatus.equals("5")) {
              rForm.setRegno("");
            rForm.setDob("");
            request.setAttribute("result", "Pending at Verification Officer(Second Verification) ");
            request.setAttribute("form", "form");
        }else if (recordstatus.equals("6")) {
              rForm.setRegno("");
            rForm.setDob("");
            request.setAttribute("result", "Reupload enabled in Institute Login.");
            request.setAttribute("form", "form");
        }
        else if (recordstatus.equals("0")) {
              rForm.setRegno("");
            rForm.setDob("");
            request.setAttribute("result", "No Data Found ");
            request.setAttribute("form", "form");
        }
//        unspecified(mapping,form,request,response);
        return mapping.findForward("success");
    }

    public ActionForward getData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        String target = "";
        RegisterForm rForm = (RegisterForm) form;
          
        try {
            
            if (rForm.getOtp() != null) {
                String otp = dao.getvalidatingOTP(rForm.getAadhar(), rForm.getMobile());
                if (otp.equalsIgnoreCase(rForm.getOtp())) {
//                if(1==1){
                    HashMap map = dao.getData(rForm, request);
                    if (map != null && map.size() > 0) {
                        ArrayList distList = new ArrayList();
                        ArrayList distListTs = new ArrayList();
                        request.setAttribute("gridata", "gridata");
                        target = "getSuccess";
                    } else {
                        request.setAttribute("result", "No Details Found");
                        target = "success";
                    }



                } else {
                    request.setAttribute("result", "OTP Failed To Validate Please Try Again");
                    request.setAttribute("form", "form");
                    target = "success";
                }
            } else {
                request.setAttribute("result", "OTP Failed To Validate Please Try Again");
                request.setAttribute("form", "form");
                target = "success";
            }
        } catch (Exception ex) {
        }
        return mapping.findForward(target);
    }

    public ActionForward submitData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        String result = "";
        ArrayList paymentList = new ArrayList();
        String resultStatus = null;
        SendSMSDTO sendSMSDTO = new SendSMSDTO();
        ArrayList list = new ArrayList();
        ArrayList<InternetAddress> ToMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> CCMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> BCCMailsList = new ArrayList<InternetAddress>();
        InternetAddress temailId = null;
        InternetAddress ccmailId = null;
        InternetAddress bccmailId = null;

        Connection con = null;
        String query = null;
        PreparedStatement st = null;
        ResultSet rs = null;
        String value = null;
        String course = null;
        String email = null;
        try {
            RegisterForm rForm = (RegisterForm) form;
            String remoteAddress = request.getRemoteAddr();
            result = dao.insertFiles(rForm, remoteAddress);
            rForm.setRegno(rForm.getAadhar());
                rForm.setDob(rForm.getDob());
            if (result.equalsIgnoreCase("1")) {
                request.setAttribute("result2", "Uploaded Successfully");
                request.setAttribute("form", "form");
            } else {
                request.setAttribute("result", "Data Failed to Saved");
                request.setAttribute("form", "form");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward("success");
    }
}
