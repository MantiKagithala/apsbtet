/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import com.ap.payment.PaymentGatewayIntegration;
import com.sms.util.SendEmail;
import com.sms.util.SendSMSDTO;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Random;
import javax.mail.internet.InternetAddress;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.DtoofficersDAO;
import net.apo.angrau.db.DatabaseConnection;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;
import com.sms.util.SMSSendService;
/**
 *
 * @author APTOL301655
 */
public class DtoofficerAction extends DispatchAction {

    private static final String PAYMENT = "payment";
    private DtoofficersDAO dao = new DtoofficersDAO();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        ArrayList stateist = new ArrayList();
        stateist = dao.getStateDetails();
        rForm.setStateList(stateist);
        request.setAttribute("form", "form");
        return mapping.findForward("success");
    }

    public ActionForward getData(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response) {
        String target = "";
        RegisterForm rForm = (RegisterForm) form;
        try {
            rForm.setAadhar(rForm.getAadhar1());
            rForm.setCourse("OA");
            int status = 0;
            status = dao.getStatus(rForm);
            String sschallticket = rForm.getSschallticket();
            String aadhar = rForm.getAadhar();
            String sscyear = rForm.getSscyear();
            if (status == 0) {
//                request.setAttribute("result", "No Details Found");
                request.setAttribute("sscyear", rForm.getSscyear());
                HashMap<String, String> ssclist = dao.getSSCDetails(sschallticket, aadhar, request);
                if ((ssclist.size() > 0) && (request.getAttribute("Examinationstatus").toString().equalsIgnoreCase("FAIL"))) {
                    request.setAttribute("result", "Not Eligible");
                    request.setAttribute("form", "form");
                    rForm.setStatusflag("");
                    target = "success";
                } else {
                    request.setAttribute("sschallticket", sschallticket);
                    request.setAttribute("aadhar", aadhar);
                    request.setAttribute("sscyear", sscyear);
                    ArrayList distlist = new ArrayList();
                    distlist = dao.getDistrictDetails();
                    rForm.setDistLists(distlist);
                    rForm.setAadhar(rForm.getAadhar1());
                    target = "getSuccess";
                }
            } else if (status == 1) {
                HashMap<String, String> list = dao.getDataForPaymentDob(rForm, request);
                if (list != null && list.size() > 0) {
                    request.setAttribute("paymentList", list);
                    request.setAttribute("status", "0");
                    target = "payment";
                } else {
                    request.setAttribute("result", "No Details Found in this Institute");
                    rForm.setStatusflag("");
                    target = "success";
                }
            } else if (status == 2) {
                ArrayList list = dao.getDataForPaymentSuccess(rForm);
                if (list != null && list.size() > 0) {
                    request.setAttribute("paymentres", list);
                    target = "paymentsuccess";
                } else {
                    request.setAttribute("result2", "Payment Already Done");
                    rForm.setStatusflag("");
                    target = "success";
                }
            }
            ArrayList stateist = new ArrayList();
            stateist = dao.getStateDetails();
            rForm.setStateList(stateist);
        } catch (Exception ex) {
        }
        return mapping.findForward(target);
    }

    public ActionForward getMandalsAt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        ArrayList mandalsList = new ArrayList();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
            String state = request.getParameter("state");
            if (state.equals("Andra Pradesh")) {
                mandalsList = dao.getMandalDetails(district);
            } else if (state.equals("Telagana")) {
                mandalsList = dao.getMandalDetailsAt(district);

            }
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("mandal_ID") + "'>" + (String) mandalMap.get("mandal_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getDistrictListAt(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        ArrayList mandalsList = new ArrayList();
        try {
            String state = request.getParameter("state");
            if (state.equals("Andra Pradesh")) {
                mandalsList = dao.getDistrictDetails();
            } else if (state.equals("Telagana")) {
                mandalsList = dao.getDistrictDetailsAt();
            }
            if (mandalsList != null && mandalsList.size() > 0) {
                Iterator mandalListItr = mandalsList.iterator();
                while (mandalListItr != null && mandalListItr.hasNext()) {
                    Object obj = (Object) mandalListItr.next();
                    HashMap mandalMap = null;
                    if (obj instanceof HashMap) {
                        mandalMap = (HashMap) obj;
                    }
                    if (mandalMap != null) {
                        namesList += "<option value='" + (String) mandalMap.get("district_ID") + "'>" + (String) mandalMap.get("district_Name") + "</option>";
                    }

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward getCenters(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        PrintWriter out = response.getWriter();
        String namesList = "<option value='0'>--Select--</option>";
        try {
            String district = request.getParameter("district");
            ArrayList centerlist = dao.getcenterDetails(district);
            if (centerlist != null && centerlist.size() > 0) {
                Iterator centerlistItr = centerlist.iterator();
                while (centerlistItr != null && centerlistItr.hasNext()) {
                    Object obj = (Object) centerlistItr.next();
                    HashMap centerMap = null;
                    if (obj instanceof HashMap) {
                        centerMap = (HashMap) obj;
                    }
                    if (centerMap != null) {
                        namesList += "<option value='" + (String) centerMap.get("ccode") + "'>" + (String) centerMap.get("cname") + "</option>";
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
        }
        out.println(namesList);
        out.flush();
        return null;
    }

    public ActionForward submitData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session = request.getSession();
          ArrayList<InternetAddress> ToMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> CCMailsList = new ArrayList<InternetAddress>();
        ArrayList<InternetAddress> BCCMailsList = new ArrayList<InternetAddress>();
        InternetAddress temailId = null;
        InternetAddress ccmailId = null;
        InternetAddress bccmailId = null;
         Connection con = null;
        String query = null;
        PreparedStatement pstmt = null;
        ResultSet rs = null;
        String value = null;
        String course = null;
        String email = null;
        String resultStatus = null;
        SendSMSDTO sendSMSDTO = new SendSMSDTO();
        try {
            String remoteAddress = request.getRemoteAddr();
            String result = dao.insertFiles(rForm, remoteAddress);
            if (result.equalsIgnoreCase("1")) {
                                // SMS Configuration
                
                /*    String sms = "Dear Applicant,Your request for seeking Admission into Degree has been registered successfully  Registrar,ANGRAU   ";
                    resultStatus = SMSSendService.sendSMS(sms, rForm.getMobile());
                    sendSMSDTO.setMobileNumber(rForm.getMobile());
                    sendSMSDTO.setSystemIp(request.getRemoteAddr());
                    sendSMSDTO.setSms(sms);
                    sendSMSDTO.setLoginId("");
                    if (resultStatus.equalsIgnoreCase("OK") || resultStatus.equalsIgnoreCase("Message Sent Successfully") || resultStatus.equalsIgnoreCase("SENT")) {
                        sendSMSDTO.setSendStatus("Sent");
                    } else {
                        sendSMSDTO.setSendStatus("Not Send");
                    }
                    dao.insertSmsLogDetails(sendSMSDTO);
                    */
                
                // SMS Configuration
// Email Configuration
                    try {
                    if (rForm.getEmail() != null) {
                        String emails = rForm.getEmail();
                          System.out.println("emails---"+emails);
                        String Examination = rForm.getCoursedesc();
//                          String Examination1 = rForm.getCoursedesc();
                       // String Examination = null;
//                          System.out.println("Examination1"+Examination1);
//                        System.out.println("Examination---"+Examination);
//                        System.out.println("hhhhhhhhhhhhhhhhhh");
                        temailId = new InternetAddress(emails, "");
                        String bccemails = "rajamallu.salla@aptonline.in";
                        String ccemailId = "janakiramaiah.peddi@aptonline.in";
                        bccmailId = new InternetAddress(bccemails, "");
                        ccmailId = new InternetAddress(ccemailId, "");
                        ToMailsList.add(temailId);
                        BCCMailsList.add(bccmailId);
                        CCMailsList.add(ccmailId);
                        boolean emailResult = SendEmail.sendEmailANGRANGA(ToMailsList, CCMailsList, BCCMailsList, "CCIC Registration", "Dear Applicant,<br>Your application for enrolling " + Examination + " examination has been registered successfully. Application will be processed further only after making the payment. Please visit www.sbtetap.gov.in  for further and latest updates. <br>Secretary,<br>SBTET AP<br><br><br>Note: This is auto generated e-mail, please do not reply.");
                        
                        try {
                            int i = 0;
                            con = DatabaseConnection.getConnection();
                            query = "insert into  CCIC_Email_Log (EmailID,status,Subject,created_date)  values ('" + emails + "','" + emailResult + "','Registration',getdate())\n";
                            pstmt = con.prepareStatement(query);
                            i = pstmt.executeUpdate();
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        } finally {
                            if (pstmt != null) {
                                pstmt.close();
                            }
                            if (con != null) {
                                con.close();
                            }
                        }
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                // Email Configuration
                
                request.setAttribute("result1", "Data Successfully Submitted");
                HashMap<String, String> list1 = dao.getDataForPaymentDob(rForm, request);
                request.setAttribute("paymentList", list1);
                return mapping.findForward(PAYMENT);
            } else {
                request.setAttribute("result", "Data Insertion Failed");
                request.setAttribute("form", "form");
                rForm.setStatusflag("");
                rForm.setSscyear("");
                rForm.setSschallticket("");
                rForm.setGender("");
                rForm.setBname("");
                rForm.setFname("");
                rForm.setDob("");
                rForm.setAttendance("");
                rForm.setHouseno("");
                rForm.setLocality("");
                rForm.setPincode("");
                rForm.setMobile("");
            }
            ArrayList stateist = new ArrayList();
            stateist = dao.getStateDetails();
            rForm.setStateList(stateist);
        } catch (Exception ex) {
            ex.printStackTrace();

        }
        return mapping.findForward("success");
    }

    public String payment(ActionMapping mapping, ActionForm form,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
////
//    public String payment(ActionForm form, String payMode, String createdBy, HttpServletRequest request,){
        System.out.println("Hii Entered into Payment..");
        RegisterForm collageForm = (RegisterForm) form;
        StringBuilder requestSB = new StringBuilder();
        String checkSumValue = "";
        String userName = "";
        String url = "";
        String link = "";
//        HttpSession session = request.getSession();
        ActionForward actionForward = new ActionForward();
        String path = request.getContextPath();
        String basePath = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + path + "/";


        double gst = 0.0;
        double tax = 0.0;
        double ConvenienceCharges = 0.0;
        double GST_Charges = 0.0;
        double totConvGst = 0.0;
        double totalFinalAmount = 0.0;
        PaymentGatewayIntegration payAPI = new PaymentGatewayIntegration();
        System.out.println("grade=====================" + collageForm.getGrade());
        String reqEncData = payAPI.payRequestFlowBefoeCCIC(collageForm.getAadhar(), collageForm.getGrade(), collageForm.getPaymentmode(), collageForm.getAmount());
        System.out.println("reqEncData======" + reqEncData);
        if (reqEncData.equals("PG_ERROR")) {
            System.out.println("payLoad====");
        } else {
            link = reqEncData;
        }
        response.sendRedirect(link);
        return null;
    }
}
