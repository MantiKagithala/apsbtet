/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package net.apo.angrau.action;

import java.util.HashMap;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import net.apo.angrau.dao.ResetPasswordDAO;
import net.apo.angrau.forms.RegisterForm;
import org.apache.struts.action.ActionForm;
import org.apache.struts.action.ActionForward;
import org.apache.struts.action.ActionMapping;
import org.apache.struts.actions.DispatchAction;

/**
 *
 * @author 1582792
 */
public class ResetPasswordAction extends DispatchAction {

    ResetPasswordDAO dao = new ResetPasswordDAO();

    public ActionForward unspecified(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        HttpSession session = null;
        session = request.getSession();
        try {
            String roleId = (String) session.getAttribute("RoleId");
            if ("11".equalsIgnoreCase(roleId) || "10".equalsIgnoreCase(roleId)) {
                List<HashMap> list = dao.getLoginDetailsReport();
                if (list != null && !list.isEmpty()) {
                    request.setAttribute("listData", list);
                } else {
                    request.setAttribute("result", "No Details Found");
                }
            } else {
                request.setAttribute("result", "Service Not Available For You");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        //  roleid = (String) session.getAttribute("RoleId");

        return mapping.findForward("success");
    }

    public ActionForward submitData(ActionMapping mapping, ActionForm form, HttpServletRequest request, HttpServletResponse response) throws Exception {
        String target = "success";
        RegisterForm rForm = (RegisterForm) form;
        HttpSession session = request.getSession();
        try {
            String uname = session.getAttribute("userName").toString();
            String result = dao.submitData(rForm, uname);
            if (result.equalsIgnoreCase("1")) {
                request.setAttribute("result1", "Password Reseted Successfully");

            } else {
                request.setAttribute("result", "Password Reseted Failed");
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        unspecified(mapping, form, request, response);
        return mapping.findForward(target);
    }
}
